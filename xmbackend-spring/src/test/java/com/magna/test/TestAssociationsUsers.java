/**
 * 
 */
package com.magna.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.magna.xmbackend.XmBackendApplication;

/**
 * @author Bhabadyuti Bal
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={XmBackendApplication.class})
public class TestAssociationsUsers {

	@Autowired
	ApplicationContext context;
	
	@Test
	public void testReadUsers(){
		
	}
}
