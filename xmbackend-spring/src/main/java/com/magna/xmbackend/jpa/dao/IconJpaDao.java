/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.IconsTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface IconJpaDao extends CrudRepository<IconsTbl, String> {

    IconsTbl findByIconName(String iconName);
    
    Iterable<IconsTbl> findByIconType(String iconType);
}
