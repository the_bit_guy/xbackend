/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.magna.xmbackend.entities.DirectoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface DirectoryJpaDao extends CrudRepository<DirectoryTbl, String>{

	DirectoryTbl findByNameIgnoreCase(String dirName);

}
