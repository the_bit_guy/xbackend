/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.magna.xmbackend.entities.DirectoryRefTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface DirectoryRefJpaDao extends CrudRepository<DirectoryRefTbl, String>{

}
