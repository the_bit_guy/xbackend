/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author dhana
 */
@Transactional
public interface UserApplicationJpaDao extends CrudRepository<UserApplicationsTbl, String> {

    @Modifying
    @Query("update UserApplicationsTbl uat set uat.status = :status where uat.userApplicationId = :id")
    int setStatusForUserApplicationsTbl(@Param("status") String status, @Param("id") String id);

    Iterable<UserApplicationsTbl> findByPosition(String position);
    
    Iterable<UserApplicationsTbl> findByStatus(String status);
    
    @Query("select uat from UserApplicationsTbl uat where uat.position=:iconPat or uat.position=:buttonPat or uat.position=:menuPat")
    Iterable<UserApplicationsTbl> findByPositionPattern(@Param("iconPat")String iconPat, @Param("buttonPat")String buttonPat, @Param("menuPat")String menuPat);

    UserApplicationsTbl findByUserApplicationId(String userApplicationId);

	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the user applications tbl
	 */
	UserApplicationsTbl findByNameIgnoreCase(String name);
}
