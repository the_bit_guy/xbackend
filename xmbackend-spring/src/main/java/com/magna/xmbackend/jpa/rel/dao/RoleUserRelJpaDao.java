package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface RoleUserRelJpaDao extends CrudRepository<RoleUserRelTbl, String> {

    /**
     *
     * @param roleId
     * @return RoleUserRelTbl
     */
    @Query("from RoleUserRelTbl RURT where RURT.roleId=:roleId and RURT.roleAdminAreaRelId IS NULL")
    Iterable<RoleUserRelTbl> findRoleUserRelTblByRoleId(@Param("roleId") RolesTbl roleId);

    /**
     *
     * @param roleId
     * @param roleAdminAreaRelId
     * @return RoleUserRelTbl
     */
    @Query("from RoleUserRelTbl RURT where RURT.roleId=:roleId and RURT.roleAdminAreaRelId=:roleAdminAreaRelId")
    Iterable<RoleUserRelTbl> findRoleUserRelTblByRoleIdRoleAARelId(@Param("roleId") RolesTbl roleId,
            @Param("roleAdminAreaRelId") RoleAdminAreaRelTbl roleAdminAreaRelId);
    
    List<RoleUserRelTbl> findByUserId(final UsersTbl userTbl);
    
    @Query("from RoleUserRelTbl RURT where RURT.roleId=:roleId and RURT.userId=:userId")
    List<RoleUserRelTbl> findByRoleIdAndUserId(@Param("roleId") RolesTbl rolesTbl, @Param("userId") UsersTbl usersTbl);
    
    RoleUserRelTbl findByRoleAdminAreaRelIdAndUserId(RoleAdminAreaRelTbl roleAdminAreaRelId, UsersTbl userId);
}
