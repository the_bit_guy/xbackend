package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.RolesTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface RoleJpaDao extends CrudRepository<RolesTbl, String> {

}
