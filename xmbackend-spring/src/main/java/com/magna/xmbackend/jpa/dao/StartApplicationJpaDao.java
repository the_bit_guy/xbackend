/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface StartApplicationJpaDao extends CrudRepository<StartApplicationsTbl, String> {
	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the start applications tbl
	 */
	StartApplicationsTbl findByNameIgnoreCase(String name);

    @Modifying
    @Query("update StartApplicationsTbl sat set sat.status = :status where sat.startApplicationId = :id")
    int setStatusForStartApplicationsTbl(@Param("status") String status, @Param("id") String id);

    @Query("from ProjectStartAppRelTbl psart where psart.adminAreaProjectRelId = (select AAPRT.adminAreaProjectRelId from AdminAreaProjectRelTbl AAPRT where AAPRT.projectId=:projectId and AAPRT.siteAdminAreaRelId=(select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART where SAART.siteId=:siteId and SAART.adminAreaId=:adminAreaId))")
    Iterable<StartApplicationsTbl> findStartAppByProjectSiteAAId(@Param("siteId") SitesTbl siteId, @Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("projectId") ProjectsTbl projectId);

    //Iterable<StartApplicationsTbl> findStartAppByProjectIdAAId(AdminAreasTbl adminAreasTbl, ProjectsTbl projectsTbl);
    
    @Query("from StartApplicationsTbl SAT WHERE SAT.status IN (:status) AND SAT.startApplicationId IN ( SELECT AASART.startApplicationId FROM SitesTbl ST, "
    		+ "AdminAreasTbl AAT, SiteAdminAreaRelTbl SAART, AdminAreaStartAppRelTbl AASART WHERE ST.siteId = SAART.siteId AND AAT.adminAreaId = SAART.adminAreaId AND "
    		+ "AASART.siteAdminAreaRelId = SAART.siteAdminAreaRelId AND SAART.adminAreaId = :adminAreaId AND ST.status IN (:status) "
    		+ "AND AAT.status IN (:status) AND SAART.status IN (:status) AND AASART.status IN (:status))")
    Iterable<StartApplicationsTbl> findStartAppByAdminAreaId(@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("status") List<String> status);
    

    @Query("from StartApplicationsTbl SAT WHERE SAT.status IN (:status) AND SAT.startApplicationId IN ( SELECT PSART.startApplicationId FROM SitesTbl ST, "
    		+ "AdminAreasTbl AAT, ProjectsTbl PT, SiteAdminAreaRelTbl SAART, AdminAreaProjectRelTbl AAPRT, ProjectStartAppRelTbl PSART WHERE ST.siteId = SAART.siteId "
    		+ " AND AAT.adminAreaId = SAART.adminAreaId AND SAART.siteAdminAreaRelId = AAPRT.siteAdminAreaRelId AND PT.projectId = AAPRT.projectId AND "
    		+ "AAPRT.adminAreaProjectRelId = PSART.adminAreaProjectRelId AND SAART.adminAreaId = :adminAreaId AND AAPRT.projectId =:projectId AND "
    		+ "ST.status IN (:status) AND AAT.status IN (:status) AND PT.status IN (:status) AND "
    		+ "SAART.status IN (:status) AND AAPRT.status IN (:status) AND PSART.status IN (:status) )")
    Iterable<StartApplicationsTbl> findStartAppByProjectIdAdminAreaId(@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("projectId") ProjectsTbl projectsTbl, 
    		@Param("status") List<String> status);
    
    @Query("FROM StartApplicationsTbl SAT WHERE SAT.status IN (:status) AND SAT.startApplicationId IN ( SELECT USART.startApplicationId FROM UsersTbl UT, "
    		+ "UserStartAppRelTbl USART WHERE UT.userId = USART.userId AND UT.username = :username AND UT.status IN (:status) AND "
    		+ "USART.status IN (:status) )")
    Iterable<StartApplicationsTbl> findStartAppByUserName(@Param("username") String username, @Param("status") List<String> status);
}
