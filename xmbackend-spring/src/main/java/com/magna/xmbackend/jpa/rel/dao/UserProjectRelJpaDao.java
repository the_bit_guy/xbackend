package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface UserProjectRelJpaDao extends CrudRepository<UserProjectRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE UserProjectRelTbl uprt set uprt.status = :status where uprt.userProjectRelId = :id")
    int setStatusForUserProjectRelTbl(@Param("status") String status, @Param("id") String id);
    
    List<UserProjectRelTbl> findByUserId(UsersTbl usersTbl);
    
    List<UserProjectRelTbl> findByProjectId(ProjectsTbl projectsTbl);
    
    List<UserProjectRelTbl> findByUserIdAndProjectId(UsersTbl usersTbl, ProjectsTbl projectsTbl);
    
    @Query("from UserProjectRelTbl upt where upt.userId=:userId and upt.projectId=:projectId")
    UserProjectRelTbl findByUserIdAndProjId(@Param("userId") UsersTbl usersTbl, @Param("projectId") ProjectsTbl projectsTbl);
    
    @Query("select uprt.projectExpiryDays from UserProjectRelTbl uprt where uprt.userId = :usersTbl and uprt.projectId= :projectsTbl")
    String findProjectExpiryDaysByUserIdAndProjectId(@Param("usersTbl") UsersTbl usersTbl, @Param("projectsTbl") ProjectsTbl projectsTbl);

    @Modifying
    @Query("update UserProjectRelTbl uprt set uprt.projectExpiryDays=:expDays where uprt.userProjectRelId=:id")
    int updateProjectExpiryDays(@Param("id") String id, @Param("expDays") String expDays);
    
}
