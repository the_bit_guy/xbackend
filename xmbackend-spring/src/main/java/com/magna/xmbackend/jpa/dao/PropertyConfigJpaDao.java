/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.PropertyConfigTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface PropertyConfigJpaDao extends CrudRepository<PropertyConfigTbl, Integer> {

	/*@Modifying
	@Query("update PropertyConfigTbl prop set prop.confValue = :configValue where prop.id.confKey = :configKey and prop.id.category = :configCategory")
	int updateConfigValueByKeyAndCat(@Param("configValue") String configValue, @Param("configKey") String configKey,
			@Param("configCategory") String configCategory);*/
	
	public Iterable<PropertyConfigTbl> findByCategory(String categotyName);
	
	public PropertyConfigTbl findByCategoryAndProperty(String category, String property);
	
	@Modifying
	@Query("update PropertyConfigTbl pct set pct.value = :value, pct.updateDate = :updateDate where pct.category = :category and pct.property = :property")
	int updatePropertyDetails(@Param("category") String category, @Param("property") String property, @Param("value") String value, @Param("updateDate") Date updateDate);
}
