/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface UserStartAppRelJpaDao  extends CrudRepository<UserStartAppRelTbl, String> {
    
	List<UserStartAppRelTbl> findByUserId(UsersTbl userId);
	
	@Modifying
	@Query("update UserStartAppRelTbl usart set usart.status=:status where usart.userStartAppRelId=:userStartAppRelId")
	int setStatusByUserStartAppRelId(@Param("status") String status, @Param("userStartAppRelId") String userStartAppRelId);

	List<UserStartAppRelTbl> findByStartApplicationId(StartApplicationsTbl startApplicationId);
}
