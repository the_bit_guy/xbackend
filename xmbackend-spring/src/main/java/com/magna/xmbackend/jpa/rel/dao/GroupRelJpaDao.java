/**
 * 
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.entities.GroupsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface GroupRelJpaDao extends CrudRepository<GroupRefTbl, String>{

	GroupRefTbl findByGroupIdAndObjectId(GroupsTbl groupId, String objectId);
	
	List<GroupRefTbl> findByObjectId(String objectId);
	
	List<GroupRefTbl> findByGroupId(GroupsTbl groupId);
	
}
