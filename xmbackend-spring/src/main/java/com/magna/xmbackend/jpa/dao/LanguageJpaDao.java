package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.LanguagesTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface LanguageJpaDao extends CrudRepository<LanguagesTbl, String> {

	LanguagesTbl findByLanguageName(String name);
}
