package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface AdminAreaProjectRelJpaDao extends CrudRepository<AdminAreaProjectRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE AdminAreaProjectRelTbl aaprt set aaprt.status = :status where aaprt.adminAreaProjectRelId = :id")
    int setStatusForAdminAreaProjectRelTbl(@Param("status") String status, @Param("id") String id);

    /**
     *
     * @param siteAdminAreaRelId
     * @return AdminAreaProjectRelTbls
     */
    @Query("from AdminAreaProjectRelTbl aaprt where aaprt.siteAdminAreaRelId = :siteAdminAreaRelId ")
    Iterable<AdminAreaProjectRelTbl> getAdminAreaProjectRelTblsBySiteAdminAreaRelId(
            @Param("siteAdminAreaRelId") SiteAdminAreaRelTbl siteAdminAreaRelId);

    /**
     *
     * @param projectsTbl
     * @return AdminAreaProjectRelTbls
     */
    List<AdminAreaProjectRelTbl> findByProjectId(ProjectsTbl projectsTbl);

    @Query(value = "SELECT * FROM ADMIN_AREA_PROJECT_REL_TBL AP, ADMIN_AREA_PROJ_APP_REL_TBL APA where "
            + "AP.ADMIN_AREA_PROJECT_REL_ID = APA.ADMIN_AREA_PROJECT_REL_ID AND AP.SITE_ADMIN_AREA_REL_ID = ?1 "
            + "AND APA.PROJECT_APPLICATION_ID = ?2", nativeQuery = true)
    List<AdminAreaProjectRelTbl> findBySiteAdminAreaRelIdAndProjectAppId(String siteAdminAreaRelId,
            String projectApplicationId);

    /**
     *
     * @param adminAreaRelTbls
     * @return AdminAreaProjectRelTbl
     */
    List<AdminAreaProjectRelTbl> findBySiteAdminAreaRelIdIn(List<SiteAdminAreaRelTbl> adminAreaRelTbls);

    /**
     *
     * @param adminAreaRelTbls
     * @param status
     * @return AdminAreaProjectRelTbl
     */
    List<AdminAreaProjectRelTbl> findBySiteAdminAreaRelIdInAndStatus(List<SiteAdminAreaRelTbl> adminAreaRelTbls, String status);

    /**
     *
     * @param siteAdminAreaRelTbls
     * @param projectsTbl
     * @return AdminAreaProjectRelTbl
     */
    List<AdminAreaProjectRelTbl> findBySiteAdminAreaRelIdInAndProjectId(List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls, ProjectsTbl projectsTbl);

    /**
     * Find by site admin area rel id and project id.
     *
     * @param siteAdminAreaRelTbl the site admin area rel tbl
     * @param projectsTbl the projects tbl
     * @return the admin area project rel tbl
     */
    AdminAreaProjectRelTbl findBySiteAdminAreaRelIdAndProjectId(SiteAdminAreaRelTbl siteAdminAreaRelTbl, ProjectsTbl projectsTbl);

}
