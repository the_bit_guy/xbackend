/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.BaseApplicationsTbl;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author dhana
 */
@Transactional
public interface BaseApplicationJpaDao extends CrudRepository<BaseApplicationsTbl, String> {

    @Modifying
    @Query("update BaseApplicationsTbl bat set bat.status = :status where bat.baseApplicationId = :id")
    int setStatusForBaseApplicationsTbl(@Param("status") String status, @Param("id") String id);

	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the base applications tbl
	 */
	BaseApplicationsTbl findByNameIgnoreCase(String name);
}
