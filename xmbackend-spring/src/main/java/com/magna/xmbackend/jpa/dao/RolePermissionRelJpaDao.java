package com.magna.xmbackend.jpa.dao;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RolesTbl;

@Transactional
public interface RolePermissionRelJpaDao extends CrudRepository<RolePermissionRelTbl, String> {
    /**
     * 
     * @param roleId
     * @param permId
     * @return int
     */
    @Modifying
    @Query("DELETE FROM RolePermissionRelTbl RPRT WHERE RPRT.roleId = :roleId AND RPRT.permissionId=:permId")
    int deleteRolePermissionRelation(@Param("roleId") RolesTbl roleId, @Param("permId") PermissionTbl permId);
    
    /**
     * Find role permission rel tbl by role id.
     *
     * @param roleId the role id
     * @return the iterable
     */
    @Query("from RolePermissionRelTbl RPRT WHERE RPRT.roleId = :roleId")
    Iterable<RolePermissionRelTbl> findRolePermissionRelTblByRoleId(@Param("roleId") RolesTbl roleId);
    
    Iterable<RolePermissionRelTbl> findByRoleIdIn(Set<RolesTbl> rolesTbl);
}
