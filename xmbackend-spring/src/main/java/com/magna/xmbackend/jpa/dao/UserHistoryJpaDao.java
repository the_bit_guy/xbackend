/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.UserHistoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface UserHistoryJpaDao extends CrudRepository<UserHistoryTbl, Long>{
	
}
