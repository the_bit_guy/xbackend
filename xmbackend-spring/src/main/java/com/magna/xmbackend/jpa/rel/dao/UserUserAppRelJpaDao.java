package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface UserUserAppRelJpaDao  extends CrudRepository<UserUserAppRelTbl, String> {
    
	/**
	 * 
	 * @param userId
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserId(UsersTbl userId);
	
	/**
	 * 
	 * @param userId
	 * @param siteAdminAreaRelId
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserIdAndSiteAdminAreaRelId(UsersTbl userId, SiteAdminAreaRelTbl siteAdminAreaRelId);
	
	/**
	 * 
	 * @param userId
	 * @param userRelType
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserIdAndUserRelType(UsersTbl userId, String userRelType);
	
	/**
	 * 
	 * @param userId
	 * @param siteAdminAreaRelId
	 * @param userRelType
	 * @return
	 */
	List<UserUserAppRelTbl> findByUserIdAndSiteAdminAreaRelIdAndUserRelType(UsersTbl userId, SiteAdminAreaRelTbl siteAdminAreaRelId, String userRelType);
	
	
	UserUserAppRelTbl findByUserIdAndSiteAdminAreaRelIdAndUserApplicationIdAndUserRelType(UsersTbl userId, SiteAdminAreaRelTbl siteAdminAreaRelId, UserApplicationsTbl userApplicationsTbl, String userRelType);
	
	
	List<UserUserAppRelTbl> findByUserIdAndSiteAdminAreaRelIdAndUserApplicationId(UsersTbl userId, SiteAdminAreaRelTbl siteAdminAreaRelId, UserApplicationsTbl userApplicationsTbl);
	
	
	/**
	 * 
	 * @param userApplicationId
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserApplicationId(UserApplicationsTbl userApplicationId);
	
	/**
	 * 
	 * @param userApplicationsTbls
	 * @param userId
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserApplicationIdInAndUserId(List<UserApplicationsTbl> userApplicationsTbls, UsersTbl userId);
	
	
	UserUserAppRelTbl findByUserUserAppRelId(String id);
	
	
	/**
	 * 
	 * @param userApplicationsTbls
	 * @param userId
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findByUserApplicationIdIn(List<UserApplicationsTbl> userApplicationsTbls);
	
	/**
	 * 
	 * @param siteAdminAreaRelTbls
	 * @param usersTbl
	 * @return List<UserUserAppRelTbl>
	 */
	List<UserUserAppRelTbl> findBySiteAdminAreaRelIdInAndUserId(List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls, UsersTbl usersTbl);

	@Modifying
	@Query("update UserUserAppRelTbl uuart set uuart.userRelType=:userRelType where uuart.userUserAppRelId=:userUserAppRelId")
	int updateUserRelsTypeByIds(@Param("userUserAppRelId") String userUserAppRelId, @Param("userRelType") String userRelType);

	
	@Query("from UserUserAppRelTbl UUART where UUART.userId = (select ut.userId from UsersTbl ut where ut.username=:userName) AND UUART.siteAdminAreaRelId in "
			+ "(select SAART.siteAdminAreaRelId FROM SiteAdminAreaRelTbl SAART where SAART.adminAreaId = :adminAreaId) and UUART.userApplicationId in "
			+ "(select UAT.userApplicationId from UserApplicationsTbl UAT where UAT.status IN ( :status ) and UAT.position "
			+ "in ('*ButtonTask*', '*IconTask*', '*MenuTask*'))")
	List<UserUserAppRelTbl> findUserUserAppRelTbls(@Param("userName") String userName,
			@Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("status") List<String> status);
	
	@Modifying
	@Query("update UserUserAppRelTbl uuart set uuart.userRelType = :userRelType where uuart.userUserAppRelId=:userUserAppRelId")
	int updateUserRelType(@Param("userRelType") String userRelType, @Param("userUserAppRelId") String userUserAppRelId);
}
