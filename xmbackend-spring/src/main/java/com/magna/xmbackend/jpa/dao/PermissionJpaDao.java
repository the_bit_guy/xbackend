package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Transactional
public interface PermissionJpaDao extends CrudRepository<PermissionTbl, String> {

    /**
     *
     * @param roleId
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl where permtbl.permissionId IN (select RPRT.permissionId from RolePermissionRelTbl RPRT where RPRT.roleId=:roleId) and permtbl.permissionType='OBJECT_PERMISSION'")
    Iterable<PermissionTbl> findObjectPermissionByRoleId(@Param("roleId") RolesTbl roleId);

    /**
     *
     * @param roleId
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl where permtbl.permissionId IN (select RPRT.permissionId from RolePermissionRelTbl RPRT where RPRT.roleId=:roleId) and permtbl.permissionType='RELATION_PERMISSION'")
    Iterable<PermissionTbl> findObjectRelPermissionByRoleId(@Param("roleId") RolesTbl roleId);

    /**
     *
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl where permtbl.permissionType='OBJECT_PERMISSION'")
    Iterable<PermissionTbl> findAllObjectPermission();

    /**
     *
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl where permtbl.permissionType='RELATION_PERMISSION'")
    Iterable<PermissionTbl> findAllObjectRelationPermission();

    /**
     *
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl where permtbl.permissionType='AA_BASED_RELATION_PERMISSION'")
    Iterable<PermissionTbl> findAllAABasedRelationPermission();

    /**
     * @param apiPath
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl, PermissionApiMappingTbl PAMT WHERE permtbl.permissionId=PAMT.permissionId AND PAMT.api=:apiPath AND permtbl.permissionType='UNAUTHORIZED'")
    PermissionTbl isExclusionApi(@Param("apiPath") String apiPath);

    /**
     *
     * @param apiPath
     * @param userName
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl, PermissionApiMappingTbl PAMT WHERE permtbl.permissionId=PAMT.permissionId AND PAMT.api=:apiPath AND permtbl.permissionId IN (SELECT DISTINCT PT.permissionId FROM PermissionTbl PT, RolesTbl RT, RolePermissionRelTbl RPT WHERE RT.roleId = RPT.roleId AND RPT.permissionId = PT.permissionId AND RT.roleId IN (SELECT DISTINCT RT.roleId FROM UsersTbl UT, RolesTbl RT, RoleUserRelTbl RUT WHERE UT.userId = RUT.userId AND RT.roleId = RUT.roleId AND RUT.roleAdminAreaRelId IS NULL AND UT.username =:userName)))")
    PermissionTbl validateUserApiHavingPermission(@Param("apiPath") String apiPath, @Param("userName") String userName);

    /**
     * 
     * @param apiPath
     * @param userName
     * @param roleAdminAreaRelId
     * @return PermissionTbl
     */
    @Query("from PermissionTbl permtbl, PermissionApiMappingTbl PAMT WHERE permtbl.permissionId=PAMT.permissionId AND PAMT.api=:apiPath AND permtbl.permissionId IN (SELECT DISTINCT PT.permissionId FROM PermissionTbl PT, RolesTbl RT, RolePermissionRelTbl RPT WHERE RT.roleId = RPT.roleId AND RPT.permissionId = PT.permissionId AND RT.roleId IN (SELECT DISTINCT RT.roleId FROM UsersTbl UT, RolesTbl RT, RoleUserRelTbl RUT WHERE UT.userId = RUT.userId AND RT.roleId = RUT.roleId AND RUT.roleAdminAreaRelId=:roleAdminAreaRelId AND UT.username =:userName)))")
    PermissionTbl validateUserApiHavingPermissionWithAA(@Param("apiPath") String apiPath, @Param("userName") String userName, @Param("roleAdminAreaRelId") RoleAdminAreaRelTbl roleAdminAreaRelId);

    /**
     *
     * @param userName
     * @param permissionName
     * @return PermissionTbl
     */
    @Query("from PermissionTbl PT WHERE PT.permissionId IN (SELECT DISTINCT PT.permissionId FROM PermissionTbl PT, RolesTbl RT, RolePermissionRelTbl RPT WHERE RT.roleId = RPT.roleId AND RPT.permissionId = PT.permissionId AND RT.roleId IN (SELECT DISTINCT RT.roleId FROM UsersTbl UT, RolesTbl RT, RoleUserRelTbl RUT WHERE RT.roleId = RUT.roleId AND UT.userId = RUT.userId AND UT.username = :userName)) AND PT.name=:permissionName)")
    PermissionTbl isObjectAccessAllowed(@Param("userName") String userName, @Param("permissionName") String permissionName);
}
