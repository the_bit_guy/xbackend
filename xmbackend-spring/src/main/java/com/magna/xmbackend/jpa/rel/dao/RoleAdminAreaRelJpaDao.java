package com.magna.xmbackend.jpa.rel.dao;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author vijay
 */
@Transactional
public interface RoleAdminAreaRelJpaDao extends CrudRepository<RoleAdminAreaRelTbl, String> {

    /**
     *
     * @param roleId
     * @return RoleUserRelTbl
     */
    @Query("from RoleAdminAreaRelTbl RAART where RAART.roleId=:roleId")
    Iterable<RoleAdminAreaRelTbl> findRoleAdminAreaRelTblByRoleId(@Param("roleId") RolesTbl roleId);

    /**
     *
     * @param adminAreaId
     * @return RoleUserRelTbl
     */
    @Query("from RoleAdminAreaRelTbl RAART where RAART.adminAreaId=:adminAreaId")
    RoleAdminAreaRelTbl findRoleAdminAreaRelTblByAAId(@Param("adminAreaId") AdminAreasTbl adminAreaId);
}
