/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface ProjectJpaDao extends CrudRepository<ProjectsTbl, String> {

    @Modifying
    @Query("update ProjectsTbl pt set pt.status = :status, pt.updateDate = :updateDate where pt.projectId = :id")
    int setStatusAndUpdateDateForProjectsTbl(
            @Param("status") String status,
            @Param("updateDate") Date updateDate,
            @Param("id") String id);
    
    public ProjectsTbl findByProjectId(String projectId);
    
    /**
     * Find by name.
     *
     * @param name the name
     * @return the projects tbl
     */
    ProjectsTbl findByNameIgnoreCase(String name);
    
    /**
     * Find project tbl by site AA proj app id.
     *
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectAppId the project app id
     * @return the iterable
     */
    @Query("From AdminAreaProjectRelTbl aaprt where aaprt.adminAreaProjectRelId = "
    		+ "(select aapart.adminAreaProjectRelId FROM AdminAreaProjAppRelTbl aapart "
    		+ "where aapart.projectApplicationId=:projectAppId) "
    		+ "and aaprt.siteAdminAreaRelId = (select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART "
    		+ "where SAART.siteId=:siteId and SAART.adminAreaId=:adminAreaId)")
	Iterable<ProjectsTbl> findProjectTblBySiteAAProjAppId(@Param("siteId") SitesTbl siteId,
			@Param("adminAreaId") AdminAreasTbl adminAreaId,
			@Param("projectAppId") ProjectApplicationsTbl projectAppId);
    
    
    @Query("from ProjectsTbl pt where pt.status IN (:status) and pt.projectId in (select aaprt.projectId from SitesTbl st, "
    		
    		+ "AdminAreasTbl aat, SiteAdminAreaRelTbl saart, AdminAreaProjectRelTbl aaprt where st.siteId=saart.siteId "
    		
    		+ " and aat.adminAreaId=saart.adminAreaId and saart.siteAdminAreaRelId=aaprt.siteAdminAreaRelId and "
    		
    		+ "st.status IN (:status) and aat.status IN (:status) and saart.status IN (:status) and aaprt.status IN (:status) "
    		+ "and st.siteId=:siteId and aaprt.projectId in (select uprt.projectId from UsersTbl ut, UserProjectRelTbl uprt "
    		+ "where ut.status IN (:status) and uprt.status IN (:status) and ut.userId=uprt.userId and ut.username=:username))")
    List<ProjectsTbl> findProjectsTbls(@Param("siteId") String siteId, @Param("username") String username, @Param("status") List<String> status);
}
