package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.PermissionApiMappingTbl;

@Transactional
public interface PermissionApIMappingJpaDao extends CrudRepository<PermissionApiMappingTbl, String> {

    /**
     *
     * @return PermissionApiMappingTbl
     */
    @Query("from PermissionApiMappingTbl PAMT WHERE PAMT.permissionId in (SELECT PT.permissionId FROM PermissionTbl PT)")
    Iterable<PermissionApiMappingTbl> findRelationPermissionApi();

	PermissionApiMappingTbl findByApi(String api);

}
