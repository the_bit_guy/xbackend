/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface EmailNotificationConfigJpaDao extends CrudRepository<EmailNotificationConfigTbl, String> {
    
    EmailNotificationConfigTbl findByEvent(String event);
}
