package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;

/**
 *
 * @author vijay
 */
@Transactional
public interface ProjectStartAppRelJpaDao extends CrudRepository<ProjectStartAppRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE ProjectStartAppRelTbl psart set psart.status = :status where psart.projectStartAppRelId = :id")
    int setStatusForProjectStartAppRelTbl(@Param("status") String status, @Param("id") String id);
    
    @Query("from ProjectStartAppRelTbl psart where psart.adminAreaProjectRelId = (select AAPRT.adminAreaProjectRelId from AdminAreaProjectRelTbl AAPRT where AAPRT.projectId=:projectId and AAPRT.siteAdminAreaRelId=(select SAART.siteAdminAreaRelId from SiteAdminAreaRelTbl SAART where SAART.siteId=:siteId and SAART.adminAreaId=:adminAreaId))")
    Iterable<ProjectStartAppRelTbl> findStartAppByProjectSiteAAId(@Param("siteId") SitesTbl siteId, @Param("adminAreaId") AdminAreasTbl adminAreaId, @Param("projectId") ProjectsTbl projectId);
    
    /**
     * Find start app by admin area project rel id.
     *
     * @param adminAreaProjectRelId the admin area project rel id
     * @return the iterable
     */
    Iterable<ProjectStartAppRelTbl> findStartAppByAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId);
    
    /**
     * 
     * @param adminAreaProjectRelTbls
     * @return List<ProjectStartAppRelTbl>
     */
    List<ProjectStartAppRelTbl> findByAdminAreaProjectRelIdIn(List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls);
}
