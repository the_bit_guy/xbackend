package com.magna.xmbackend.jpa.dao;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author Vijay
 */
@Transactional
public interface UserJpaDao extends CrudRepository<UsersTbl, String> {

	/**
	 *
	 * @param startWith
	 * @return UsersTbl
	 */
	@Query("SELECT usersTbl From UsersTbl usersTbl where LOWER(usersTbl.username) LIKE LOWER(:startWith)")
	Iterable<UsersTbl> findUserNameStartWith(@Param("startWith") String startWith);


	/**
	 * Find by username.
	 *
	 * @param name the name
	 * @return the users tbl
	 */
	UsersTbl findByUsername(String username);
	
	/**
	 * 
	 * @param username
	 * @return UsersTbl
	 */
	UsersTbl findByUsernameIgnoreCase(String username);
	
	/**
	 * 
	 * @param username
	 * @param status
	 * @return UsersTbl
	 */
	UsersTbl findByUsernameIgnoreCaseAndStatus(String username, String status);
	
	@Query("select ut.userId from UsersTbl ut where ut.username = :username")
	String findUserIdByUserName(@Param("username") String username);
	
	/**
	 * Find by user id.
	 *
	 * @param userId the user id
	 * @return the users tbl
	 */
	UsersTbl findByUserId(String userId);

	/**
	 * Sets the status and update date for users tbl.
	 *
	 * @param status the status
	 * @param updateDate the update date
	 * @param id the id
	 * @return the int
	 */
	@Modifying
	@Query("update UsersTbl ut set ut.status = :status, ut.updateDate = :updateDate where ut.userId = :userId")
	int setStatusAndUpdateDateForUsersTbl(@Param("status") String status, @Param("updateDate") Date updateDate,
			@Param("userId") String userId);
}
