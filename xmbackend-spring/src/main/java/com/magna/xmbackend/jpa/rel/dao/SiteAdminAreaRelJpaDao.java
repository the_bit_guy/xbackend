package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;

/**
 *
 * @author dhana
 */
@Transactional
public interface SiteAdminAreaRelJpaDao extends CrudRepository<SiteAdminAreaRelTbl, String> {

    /**
     *
     * @param status
     * @param id
     * @return integer
     */
    @Modifying
    @Query("UPDATE SiteAdminAreaRelTbl saart set saart.status = :status where saart.siteAdminAreaRelId = :id")
    int setStatusForSiteAdminAreaRelTbl(@Param("status") String status, @Param("id") String id);
    
    
    /**
     * Find admin area by site id.
     *
     * @param siteId the site id
     * @return the iterable
     */
    Iterable<SiteAdminAreaRelTbl> findAdminAreaBySiteId(SitesTbl siteId);
    
    /**
     * 
     * @param adminAreaId
     * @return List<SiteAdminAreaRelTbl>
     */
    List<SiteAdminAreaRelTbl> findByAdminAreaId(AdminAreasTbl adminAreaId);
    
    /**
     * 
     * @param siteAdminAreaRelIds
     * @param adminAreaId
     * @return List<SiteAdminAreaRelTbl>
     */
    List<SiteAdminAreaRelTbl> findBySiteAdminAreaRelIdInAndAdminAreaId(List<String> siteAdminAreaRelIds, AdminAreasTbl adminAreaId);
}
