/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.AdminAreasTbl;

@Transactional
public interface AdminAreaJpaDao extends CrudRepository<AdminAreasTbl, String> {

    @Modifying
    @Query("update AdminAreasTbl aat set aat.status = :status where aat.adminAreaId = :id")
    int setStatusForAdminAreasTbl(@Param("status") String status, @Param("id") String id);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the admin areas tbl
     */
    AdminAreasTbl findByNameIgnoreCase(String name);
}
