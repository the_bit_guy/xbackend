package com.magna.xmbackend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class XmBackendApplication extends SpringBootServletInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(XmBackendApplication.class);

    public static void main(String[] args) {
        LOG.info("Inside main");
        SpringApplication.run(XmBackendApplication.class, args);
        LOG.info("Exit main");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(XmBackendApplication.class);
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:lang/error_code");
        messageSource.setCacheSeconds(3600);//1 hrs (3600 sec)
        return messageSource;
    }
}
