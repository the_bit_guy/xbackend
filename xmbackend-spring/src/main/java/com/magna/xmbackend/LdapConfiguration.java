/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.enums.LdapKey;

/**
 *
 * @author dhana
 */
@Configuration
public class LdapConfiguration {

	@Autowired
	PropertyConfigJpaDao propertyConfigJpaDao;

	@Bean
	public LdapContextSource contextSource() {
		Map<String, String> ldapMap = getLdapPropertiesMap();
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(ldapMap.get(LdapKey.LDAP_URL.name()) + ":" + ldapMap.get(LdapKey.LDAP_PORT_NUMBER.name()));
		contextSource.setBase(ldapMap.get(LdapKey.LDAP_BASE.name()));
		contextSource.setUserDn(ldapMap.get(LdapKey.LDAP_USERNAME.name()));
		contextSource.setPassword(ldapMap.get(LdapKey.LDAP_PASSWORD.name()));
		contextSource.setReferral("follow");
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	@Bean
	public Map<String, String> getLdapPropertiesMap() {
		Iterable<PropertyConfigTbl> configTblItrble = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.LDAP.name());
		Iterator<PropertyConfigTbl> iterator = configTblItrble.iterator();
		Map<String, String> ldapPropsMap = new HashMap<>();
		while (iterator.hasNext()) {
			PropertyConfigTbl tbl = iterator.next();
			ldapPropsMap.put(tbl.getProperty(), tbl.getValue());
		}
		return ldapPropsMap;
	}
}
