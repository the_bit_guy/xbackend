/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.UserStartAppRelAuditMgr;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.StartApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserStartAppRelAuditMgrImpl implements UserStartAppRelAuditMgr {
	
    private static final Logger LOG = LoggerFactory.getLogger(UserStartAppRelAuditMgrImpl.class);
	
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private StartApplicationJpaDao startApplicationJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	

	@Override
	public void userStartAppMultiSaveAuditor(UserStartAppRelBatchRequest usarbReq,
			UserStartAppRelBatchResponse usarbRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> userStartAppMultiSaveAuditor");

		List<UserStartAppRelRequest> userStartAppRelRequests = usarbReq.getUserStartAppRelRequests();

		List<UserStartAppRelTbl> userStartAppRelTbls = usarbRes.getUserStartectAppRelTbls();
		List<Map<String, String>> statusMap = usarbRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(userStartAppRelTbls, httpServletRequest);

		} else {
			//Partial success case
			// iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, userStartAppRelRequests, httpServletRequest);

			// iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(userStartAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< userStartAppMultiSaveAuditor");
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<UserStartAppRelRequest> userStartAppRelRequests, HttpServletRequest httpServletRequest) {

		for (Map<String, String> map : statusMap) {
			for (UserStartAppRelRequest userStartAppRelReq : userStartAppRelRequests) {
				String startAppId = userStartAppRelReq.getStartAppId();
				String userId = userStartAppRelReq.getUserId();
				String status = userStartAppRelReq.getStatus();
				
				StartApplicationsTbl startAppTbl = this.startApplicationJpaDao.findOne(startAppId);
				UsersTbl usersTbl = userJpaDao.findOne(userId);
				
				
				if (startAppTbl != null && usersTbl != null) {
					String message = map.get("en");
					String startAppName = startAppTbl.getName();
					String username = usersTbl.getUsername();
					if (message.contains(username) && message.contains(startAppName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, "UserStartApp",
										username, startAppName,null, 
										null, status,
										null, message, "UserStartAppRelation Create", "Failure");
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
				}
			}
		}
	
		
	}

	private void createSuccessAudit(List<UserStartAppRelTbl> userStartectAppRelTbls,
			HttpServletRequest httpServletRequest) {
		userStartectAppRelTbls.forEach(userStartAppTbl -> {
			String startAppName = userStartAppTbl.getStartApplicationId().getName();
			String username = userStartAppTbl.getUserId().getUsername();
			String status = userStartAppTbl.getStatus();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "UserStartApp", username,
							startAppName, status, null,
							null, null, null, "UserStartAppRelation Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
	}

	@Override
	public void userStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> userStartAppMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "UserStartApp", null, null, null, null, null, null, ex.getMessage(), "UserStartAppRelation Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< userStartAppMultiSaveFailureAuditor");
		
	}

	@Override
	public void userStartAppMultiDeleteSuccessAudit(UserStartAppRelTbl userStartAppRelTbls,
			HttpServletRequest httpServletRequest) {

		String startAppName = userStartAppRelTbls.getStartApplicationId().getName();
		String username = userStartAppRelTbls.getUserId().getUsername();
		String status = userStartAppRelTbls.getStatus();
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "UserStartApp", username,
						startAppName, status, null,
						null, null, null, "UserStartAppRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

}
