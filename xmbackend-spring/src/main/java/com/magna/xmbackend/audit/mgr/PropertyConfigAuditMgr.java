/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface PropertyConfigAuditMgr<T> {

	void propertyConfigUpdateSuccessAudit(HttpServletRequest hsr, PropertyConfigResponse pcr);

	void propertyConfigUpdateFailureAudit(HttpServletRequest hsr, Object t, String errMsg);

}
