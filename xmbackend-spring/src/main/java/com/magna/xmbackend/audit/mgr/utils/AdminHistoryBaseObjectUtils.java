/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr.utils;

import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class AdminHistoryBaseObjectUtils {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminHistoryBaseObjectUtils.class);

    public AdminHistoryBaseObjectsTbl makeAdminHistoryBaseObjectsTbl(
            HttpServletRequest servletRequest,
            String changes,
            String errorMessage,
            String objectName,
            String operation,
            String result) {
        LOG.info("---> makeAdminHistoryBaseObjectsTbl");
        Date date = new Date();

        AdminHistoryBaseObjectsTbl adminHistoryBaseObjectsTbl
                = new AdminHistoryBaseObjectsTbl();

        adminHistoryBaseObjectsTbl.setAdminName(servletRequest.getHeader("USER_NAME"));
        adminHistoryBaseObjectsTbl.setApiRequestPath(servletRequest.getServletPath());
        adminHistoryBaseObjectsTbl.setChanges(changes);
        adminHistoryBaseObjectsTbl.setErrorMessage(errorMessage);
        adminHistoryBaseObjectsTbl.setLogTime(date);
        adminHistoryBaseObjectsTbl.setObjectName(objectName);
        adminHistoryBaseObjectsTbl.setOperation(operation);
        adminHistoryBaseObjectsTbl.setResult(result);

        LOG.info("<--- makeAdminHistoryBaseObjectsTbl");
        return adminHistoryBaseObjectsTbl;
    }
    
    
    
    public String extractErrMsgFromMap(Map<String, String> statusMap) {
        String errMsg = "";
        for (Map.Entry<String, String> entry : statusMap.entrySet()) {
            errMsg = errMsg + entry.getKey() + " : " + entry.getValue() + " ";
        }
        return errMsg;
    }
}
