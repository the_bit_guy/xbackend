/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserAuditMgr {


	void userMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void userMultiSaveSuccessAuditor(HttpServletRequest httpServletRequest, List<UserRequest> userRequests,
			UserResponse response);

	void siteDeleteSuccessAudit(HttpServletRequest httpServletRequest, String userId, String name);

	void userUpdateSuccessAudit(HttpServletRequest httpServletRequest, UserRequest userRequest);

	void userUpdateFailureAudit(HttpServletRequest httpServletRequest, UserRequest userRequest, String message);

}
