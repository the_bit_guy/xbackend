package com.magna.xmbackend.audit.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.AdminAreaAudiMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;

/**
 *
 * @author Administrator
 */
@Component
public class AdminAreaAudiMgrImpl implements AdminAreaAudiMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaAudiMgrImpl.class);

    final String OBJECT_NAME = "AdminAreasTbl";
    final String OPERATION_MULTI_DELETE = "Admin Area Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param adminAreasTbl
     * @param servletRequest
     */
    @Override
    public void adminAreaCreateSuccessAuditor(AdminAreasTbl adminAreasTbl,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> adminAreaCreateSuccessAuditor");
        String name = adminAreasTbl.getName();
        String changes = "Admin Area created with name " + name;
        String errorMessage = "";
        String operation = "admin area create";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changes, errorMessage,
                        OBJECT_NAME, operation, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaCreateSuccessAuditor");
    }

    /**
     *
     * @param adminAreaRequest
     * @param servletRequest
     */
    @Override
    public void adminAreaCreateFailureAuditor(AdminAreaRequest adminAreaRequest,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> adminAreaCreateFailureAuditor");
        String name = adminAreaRequest.getName();
        String errorMessage = "Admin Area creation with name "
                + name + " failed";
        String changes = "";
        String operation = "admin area create";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changes, errorMessage,
                        OBJECT_NAME, operation, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaCreateFailureAuditor");
    }

    /**
     *
     * @param adminAreasTbl
     * @param servletRequest
     */
    @Override
    public void adminAreaUpdateSuccessAuditor(AdminAreasTbl adminAreasTbl,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> adminAreaUpdateSuccessAuditor");
        String name = adminAreasTbl.getName();
        String changes = "Admin Area with name " + name +" got updated.";
        String errorMessage = "";
        String operation = "AdminArea update";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changes, errorMessage,
                        OBJECT_NAME, operation, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaUpdateSuccessAuditor");
    }

    /**
     *
     * @param adminAreaRequest
     * @param servletRequest
     */
    @Override
    public void adminAreaUpdateFailureAuditor(AdminAreaRequest adminAreaRequest,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> adminAreaUpdateFailureAuditor");
        String name = adminAreaRequest.getName();
        String errorMessage = "Admin Area updation with id name "
                + name + " failed";
        String changes = "";
        String operation = "admin area update";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changes, errorMessage,
                        OBJECT_NAME, operation, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaUpdateFailureAuditor");
    }

    /**
     *
     * @param status
     * @param id
     * @param servletRequest
     * @param isSuccess
     * @param aat
     */
    @Override
    public void admainAreaUpdateStatusAuditor(String status, String id,
            HttpServletRequest servletRequest,
            boolean isSuccess, final AdminAreasTbl aat) {
        LOG.info(">>>> admainAreaUpdateStatusAuditor::isSuccess {}", isSuccess);
        String errorMessage = "";
        String result = "failed";
        String adminAreaName = "";
        final StringBuilder changeBuff = new StringBuilder();
        if (null != aat) {
            adminAreaName = aat.getName();
        }
        if (isSuccess) {
            changeBuff.append("Admin Area updated ")
                    .append(" with name:")
                    .append(adminAreaName).append(" success");
            result = "success";
        } else {
            errorMessage = "AdminAreas updation with id "
                    + id + " failed";
        }

        String operation = "admin area update";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, operation, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< admainAreaUpdateStatusAuditor::isSuccess");
    }

    /**
     *
     * @param id
     * @param servletRequest
     * @param isSuccess
     * @param aat
     */
    @Override
    public void adminAreaDeleteStatusAuditor(String id,
            HttpServletRequest servletRequest,
            boolean isSuccess, final AdminAreasTbl aat) {
        LOG.info(">>>> adminAreaDeleteStatusAuditor::isSuccess {}", isSuccess);
        String errorMessage = "";
        String result = "failed";
        String adminAreaName = "";
        final StringBuilder changeBuff = new StringBuilder();
        if (null != aat) {
            adminAreaName = aat.getName();
        }
        if (isSuccess) {
            changeBuff.append("AdminArea deletion with")
                    .append(" name:").append(adminAreaName).append(" success");
            result = "success";
        } else {
            errorMessage = "AdminAreas deletion with name "
                    + adminAreaName + " failed";
        }

        String operation = "admin area delete";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        servletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, operation, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaDeleteStatusAuditor");
    }

    /**
     *
     * @param adminAreaResponse
     * @param adminAreaIds
     * @param httpServletRequest
     */
    @Override
    public void adminAreaMultiDeleteAuditor(final AdminAreaResponse adminAreaResponse,
            final Set<String> adminAreaIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> adminAreaMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = adminAreaResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given Admin Area Ids:").append(String.join(" ", adminAreaIds));
        if (!statusMaps.isEmpty() && (adminAreaIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (adminAreaIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< adminAreaMultiDeleteAuditor");
    }
}
