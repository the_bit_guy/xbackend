/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.vo.project.ProjectRequest;

/**
 *
 * @author dhana
 */
public interface ProjectAuditMgr {

    void projectCreateAuditor(ProjectsTbl projectsTbl,
            HttpServletRequest servletRequest);

    void projectCreateFailAuditor(ProjectRequest projectRequest,
            String errMsg,
            HttpServletRequest servletRequest);

    void projectDeleteFailureAuditor(String projectId,
            String errMsg,
            HttpServletRequest servletRequest);

    void projectDeleteSuccessAuditor(String projectId, String projectName,
            HttpServletRequest servletRequest);

    
    void projectUpdateSuccessAuditor(ProjectsTbl projectsTbl, HttpServletRequest servletRequest);
    
    void projectUpdateStautsSuccessAuditor(String projectId, String projectName, String status, HttpServletRequest servletRequest);
    
    void projectUpdateFailAuditor(String projectId, String errMsg, HttpServletRequest servletRequest);
}
