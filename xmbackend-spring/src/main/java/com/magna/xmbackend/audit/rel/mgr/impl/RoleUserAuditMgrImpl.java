/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.RoleUserAuditMgr;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.RoleJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class RoleUserAuditMgrImpl implements RoleUserAuditMgr{

	private static final Logger LOG = LoggerFactory.getLogger(RoleUserAuditMgrImpl.class);
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private RoleJpaDao roleJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	@Autowired
	private RoleAdminAreaRelJpaDao roleAdminAreaRelJpaDao;
	
	
	@Override
	public void roleUserMultiSaveAuditor(List<RoleUserRelRequest> roleUserRelRequestList,
			RoleUserRelResponse ruResp, HttpServletRequest httpServletRequest) {
		LOG.info(">>> roleUserMultiSaveAuditor");
		
		Iterable<RoleUserRelTbl> roleUserRelTbls = ruResp.getRoleUserRelTbls();
		List<Map<String, String>> statusMap = ruResp.getStatusMap();
		
		if (statusMap.isEmpty()) {
			roleUserRelTbls.forEach(rut-> {
				RolesTbl roleTbl = rut.getRoleId();
				UsersTbl userTbl = rut.getUserId();
				RoleAdminAreaRelTbl roleAARelTbl = rut.getRoleAdminAreaRelId();
				String adminAreaName = (roleAARelTbl != null) ? roleAARelTbl.getAdminAreaId().getName():null;
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "RoleUser", roleTbl.getName(), userTbl.getUsername(),
								adminAreaName, null, null, null, null, "RoleUserRel Create",
								"Success");
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});
		} else {
			// iterate the status map to log the failed ones
			for (Map<String, String> map : statusMap) {
				for (RoleUserRelRequest roleUserRelReq : roleUserRelRequestList) {
					RolesTbl roleTbl = this.roleJpaDao.findOne(roleUserRelReq.getRoleId());
					UsersTbl userTbl = this.userJpaDao.findByUserId(roleUserRelReq.getUserId());
					String roleAAId = roleUserRelReq.getRoleAdminAreaRelId();
					String adminAreaName =( roleAAId != null)?roleAdminAreaRelJpaDao.findOne(roleAAId).getAdminAreaId().getName():null;
					if (userTbl != null && roleTbl != null) {
						String message = map.get("en");
						if (message.contains(userTbl.getUsername()) && message.contains(roleTbl.getName())) {
							AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
									.makeAdminHistoryRelationTbl(httpServletRequest, "RoleUser",
											roleTbl.getName(),userTbl.getUsername(), adminAreaName, null, null,
											null, message, "RoleUserRel Create",
											"Failure");
							this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
							break;
						}
					}
				}
			}
			
			// iterate the userProjectRelTbls for success ones
			roleUserRelTbls.forEach(rut-> {
				RolesTbl roleTbl = rut.getRoleId();
				UsersTbl userTbl = rut.getUserId();
				RoleAdminAreaRelTbl roleAARelTbl = rut.getRoleAdminAreaRelId();
				String adminAreaName = (roleAARelTbl != null) ? roleAARelTbl.getAdminAreaId().getName():null;
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "RoleUser", roleTbl.getName(),
								userTbl.getUsername(), adminAreaName, null, null, null, null, "RoleUserRel Create",
								"Success");
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			});
		}
		
		LOG.info(">>> roleUserMultiSaveAuditor");
	}

	@SuppressWarnings("unused")
	private String getRolesForUser(UsersTbl userTbl) {
		String roles = userTbl.getRoleUserRelTblCollection().stream()
				.map(roleUserTbl -> roleUserTbl.getRoleId().getName()).collect(Collectors.joining(","));
		return roles;
	}

	@Override
	public void roleUserMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> roleUserMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "RoleUser", null, null, null, null, null, null, ex.getMessage(), "RoleUserRel Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< roleUserMultiSaveFailureAuditor");
		
	}

	@Override
	public void roleUserMultiDeleteSuccessAudit(RoleUserRelTbl roleUserRelTbl, HttpServletRequest httpServletRequest) {

		RolesTbl roleTbl = roleUserRelTbl.getRoleId();
		UsersTbl userTbl = roleUserRelTbl.getUserId();
		RoleAdminAreaRelTbl roleAARelTbl = roleUserRelTbl.getRoleAdminAreaRelId();
		String adminAreaName = (roleAARelTbl != null) ? roleAARelTbl.getAdminAreaId().getName():null;
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "RoleUser", roleTbl.getName(), userTbl.getUsername(),
						adminAreaName, null, null, null, null, "RoleUserRel Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

}
