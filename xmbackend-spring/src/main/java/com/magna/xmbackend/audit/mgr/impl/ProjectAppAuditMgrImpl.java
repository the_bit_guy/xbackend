package com.magna.xmbackend.audit.mgr.impl;

import com.magna.xmbackend.audit.mgr.ProjectAppAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class ProjectAppAuditMgrImpl implements ProjectAppAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectAppAuditMgrImpl.class);

    final String OBJECT_NAME = "ProjectApplicationsTbl";
    final String OPERATION_CREATE = "Project App Create";
    final String OPERATION_UPDATE = "Project App Update";
    final String OPERATION_DELETE = "Project App Delete";
    final String OPERATION_MULTI_DELETE = "Project App Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param projectApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void projectAppCreateSuccessAuditor(final ProjectApplicationsTbl projectApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> projectAppCreateSuccessAuditor");
        final String name = projectApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Project Application created with ")
                .append(" name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_CREATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppCreateSuccessAuditor");
    }

    /**
     *
     * @param projectApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void projectAppCreateFailureAuditor(final ProjectApplicationRequest projectApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> projectAppCreateFailureAuditor");
        final String name = projectApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Project Application creation failed for name")
                .append(name);
        final String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppCreateFailureAuditor");
    }

    /**
     *
     * @param projectApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void projectAppUpdateSuccessAuditor(final ProjectApplicationsTbl projectApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> projectAppUpdateSuccessAuditor");
        final String name = projectApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Project Application updated with ")
               .append(" and name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppUpdateSuccessAuditor");
    }

    /**
     *
     * @param projectApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void projectAppUpdateFailureAuditor(final ProjectApplicationRequest projectApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> projectAppUpdateFailureAuditor");
        final String name = projectApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Project Application update failed for name")
                .append(name);
        String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppUpdateFailureAuditor");
    }

    /**
     *
     * @param status
     * @param projectAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param pat
     */
    @Override
    public void projectAppUpdateStatusAuditor(final String status,
            final String projectAppId, final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final ProjectApplicationsTbl pat) {
        LOG.info(">>>> projectAppUpdateStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String projAppName = "";
        if (null != pat) {
            projAppName = pat.getName();
        }
        if (isSuccess) {
            changeBuff.append("Project Application updated")
                    .append(" with name:")
                    .append(projAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("Project Application updation with name ")
                    .append(projAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(),
                        errorBuff.toString(), OBJECT_NAME, OPERATION_UPDATE,
                        result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppUpdateStatusAuditor::isSuccess");
    }

    /**
     *
     * @param projectAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param pat
     */
    @Override
    public void projectAppDeleteStatusAuditor(final String projectAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final ProjectApplicationsTbl pat) {
        LOG.info(">>>> projectAppDeleteStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String projectAppName = "";
        if (null != pat) {
            projectAppName = pat.getName();
        }
        if (isSuccess) {
            changeBuff.append("Project Application deletion")
                    .append(" and name:").append(projectAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("Project Application deletion")
                    .append(" and name:").append(projectAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorBuff.toString(),
                        OBJECT_NAME, OPERATION_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppDeleteStatusAuditor");
    }

    /**
     *
     * @param projectApplicationResponse
     * @param projectAppIds
     * @param httpServletRequest
     */
    @Override
    public void projectAppMultiDeleteAuditor(final ProjectApplicationResponse projectApplicationResponse,
            final Set<String> projectAppIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> projectAppMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = projectApplicationResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given ProjectApp Ids:").append(String.join(" ", projectAppIds));
        if (!statusMaps.isEmpty() && (projectAppIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (projectAppIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectAppMultiDeleteAuditor");
    }
}
