/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.DirectoryAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.directory.DirectoryRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class DirectoryAuditMgrImpl implements DirectoryAuditMgr{

	private static final Logger LOG
    = LoggerFactory.getLogger(DirectoryAuditMgrImpl.class);
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;
    
	
	@Override
	public void directoryCreateSuccessAudit(HttpServletRequest hsr, DirectoryTbl dt) {
		LOG.info(">>>> directorySuccessAudit");
		String name = dt.getName();
        String changes = "Created directory with name " + name;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "DirectoryTbl", "directory create", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info("<<<< directorySuccessAudit");
	}

	@Override
	public void directoryCreateFailureAudit(HttpServletRequest hsr, DirectoryRequest dr, String errMsg) {

        LOG.info(">>>> directoryCreateFailureAudit");
        String name = dr.getName();
        String errorMessage = "Directory creation failed for name " + name
                + "with exception " + errMsg;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errorMessage,
                                "DirectoryTbl", "directory create", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< directoryCreateFailureAudit");
    
		
	}

	@Override
	public void dirDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg) {
		LOG.info(">>>> dirDeleteFailureAudit with id {}", id);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                                "DirectoryTbl", "directory delete", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideDeleteFailureAudit ");
		
	}

	@Override
	public void dirDeleteSuccessAudit(HttpServletRequest hsr, String id, String name) {
		LOG.info(">>>> dirDeleteSuccessAudit with id {}", id);
        String changes = "Directory with name " + name
                + " deleted successfully";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr,
                                changes, "", "DirectoryTbl",
                                "directory delete", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info(">>>> dirDeleteSuccessAudit");
	}

	@Override
	public void dirUpdateFailureAudit(HttpServletRequest hsr, DirectoryRequest dr, String errMsg) {
		LOG.info(">>>> dirUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "DirectoryTbl", "directory update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info(">>>> dirUpdateFailureAudit");
	}

	@Override
	public void dirUpdateSuccessAudit(HttpServletRequest hsr, String id, String name) {
		LOG.info(">>>> dirUpdateSuccessAudit");
        String changes = "Directory with name " + name
                + " got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "DirectoryTbl", "directory update", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< dirUpdateSuccessAudit");
		
	}


}
