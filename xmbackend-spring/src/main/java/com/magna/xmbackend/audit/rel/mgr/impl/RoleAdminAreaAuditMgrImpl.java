/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.RoleAdminAreaAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.RoleJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class RoleAdminAreaAuditMgrImpl implements RoleAdminAreaAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(RoleAdminAreaAuditMgrImpl.class);
	@Autowired
	private AdminAreaJpaDao adminAreaJpaDao;
	@Autowired
	private RoleJpaDao roleJpaDao;
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	
	

	@Override
	public void roleAdminAreaCreateSuccessAudit(RoleAdminAreaRelResponse batchRes,
			List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList, HttpServletRequest httpServletRequest) {
		
		LOG.info(">>> roleAdminAreaCreateSuccessAudit");

		Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = batchRes.getRoleAdminAreaRelTbls();
		List<Map<String, String>> statusMap = batchRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(roleAdminAreaRelTbls, httpServletRequest);

		} else {
			//Partial success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, roleAdminAreaRelRequestList, httpServletRequest);

			//Iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(roleAdminAreaRelTbls, httpServletRequest);
		}
		LOG.info("<<< roleAdminAreaCreateSuccessAudit");
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList, HttpServletRequest httpServletRequest) {

		for (Map<String, String> map : statusMap) {
			for (RoleAdminAreaRelRequest adminAreaProjAppRelReq : roleAdminAreaRelRequestList) {
				String adminAreaId = adminAreaProjAppRelReq.getAdminAreaId();
				String roleId = adminAreaProjAppRelReq.getRoleId();
				
				AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findOne(adminAreaId);
				String adminAreaName = "";
				if(adminAreasTbl != null) {
					adminAreaName = adminAreasTbl.getName();
				}
				
				RolesTbl rolesTbl = this.roleJpaDao.findOne(roleId);
				String roleName = "";
				if(rolesTbl != null) {
					roleName = rolesTbl.getName();
				}
				String message = map.get("en");
				if (message.contains(adminAreaName) && message.contains(roleName)) {
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "RoleAdminArea",
									roleName, adminAreaName,null, 
									null, null,
									null, message, "RoleAdminArea Create", "Failure");
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
					break;
				}
			}
		}
		
	
		
	}

	private void createSuccessAudit(Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls,
			HttpServletRequest httpServletRequest) {
		roleAdminAreaRelTbls.forEach(roleAdminAreaRelTbl->{
			String adminAreaName = roleAdminAreaRelTbl.getAdminAreaId().getName();
			String roleName = roleAdminAreaRelTbl.getRoleId().getName();
			
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "RoleAdminArea", roleName,
							adminAreaName, null, null,
							null, null, null, "RoleAdminArea Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void roleAdminAreaMultiDeleteSuccess(RoleAdminAreaRelTbl roleAdminAreaRelTbl,
			HttpServletRequest httpServletRequest) {

		String adminAreaName = roleAdminAreaRelTbl.getAdminAreaId().getName();
		String roleName = roleAdminAreaRelTbl.getRoleId().getName();
		
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "RoleAdminArea", roleName,
						adminAreaName, null, null,
						null, null, null, "RoleAdminArea Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

}
