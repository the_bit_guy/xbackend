package com.magna.xmbackend.audit.mgr.impl;

import com.magna.xmbackend.audit.mgr.UserAppAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class UserAppAuditMgrImpl implements UserAppAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserAppAuditMgrImpl.class);

    final String OBJECT_NAME = "UserApplicationsTbl";
    final String OPERATION_CREATE = "User App Create";
    final String OPERATION_UPDATE = "User App Update";
    final String OPERATION_DELETE = "User App Delete";
    final String OPERATION_MULTI_DELETE = "User App Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param userApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void userAppCreateSuccessAuditor(final UserApplicationsTbl userApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> userAppCreateSuccessAuditor");
        final String name = userApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("User Application created with ")
               .append(" and name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_CREATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppCreateSuccessAuditor");
    }

    /**
     *
     * @param userApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void userAppCreateFailureAuditor(final UserApplicationRequest userApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> userAppCreateFailureAuditor");
        final String name = userApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("User Application creation failed for name")
                .append(name);
        final String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppCreateFailureAuditor");
    }

    /**
     *
     * @param userApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void userAppUpdateSuccessAuditor(final UserApplicationsTbl userApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> userAppUpdateSuccessAuditor");
        final String name = userApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("User Application updated with ")
                .append(" and name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppUpdateSuccessAuditor");
    }

    /**
     *
     * @param userApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void userAppUpdateFailureAuditor(final UserApplicationRequest userApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> userAppUpdateFailureAuditor");
        final String name = userApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("User Application update failed for name")
                .append(name);
        String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppUpdateFailureAuditor");
    }

    /**
     *
     * @param status
     * @param userAppId
     * @param httpServletRequest
     * @param isSuccess
     */
    @Override
    public void userAppUpdateStatusAuditor(final String status, final String userAppId,
            final HttpServletRequest httpServletRequest, 
            final boolean isSuccess, final UserApplicationsTbl uat) {
        LOG.info(">>>> userAppUpdateStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String userAppName = "";
         if (null != uat) {
            userAppName = uat.getName();
        }
        if (isSuccess) {
            changeBuff.append("User Application updated with ")
                    .append(userAppId).append(" and with name:")
                    .append(userAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("User Application updation with name ")
                    .append(userAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(),
                        errorBuff.toString(), OBJECT_NAME, OPERATION_UPDATE,
                        result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppUpdateStatusAuditor::isSuccess");
    }

    /**
     *
     * @param userAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param uat
     */
    @Override
    public void userAppDeleteStatusAuditor(final String userAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final UserApplicationsTbl uat) {
        LOG.info(">>>> userAppDeleteStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String userAppName = "";
        if (null != uat) {
            userAppName = uat.getName();
        }
        if (isSuccess) {
            changeBuff.append("User Application deletion with ")
                    .append(" name:").append(userAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("User Application deletion with ")
                    .append("  name:").append(userAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorBuff.toString(),
                        OBJECT_NAME, OPERATION_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
    }

    /**
     *
     * @param userApplicationResponse
     * @param userAppIds
     * @param httpServletRequest
     */
    @Override
    public void userAppMultiDeleteAuditor(final UserApplicationResponse userApplicationResponse,
            final Set<String> userAppIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> userAppMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = userApplicationResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given UserApp Ids:").append(String.join(" ", userAppIds));
        if (!statusMaps.isEmpty() && (userAppIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (userAppIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userAppMultiDeleteAuditor");
    }
}
