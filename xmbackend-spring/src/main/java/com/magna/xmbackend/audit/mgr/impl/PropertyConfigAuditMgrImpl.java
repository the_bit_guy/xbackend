/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.PropertyConfigAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class PropertyConfigAuditMgrImpl<T> implements PropertyConfigAuditMgr<T> {
	
	
	private static final Logger LOG
    = LoggerFactory.getLogger(PropertyConfigAuditMgrImpl.class);
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;
    
    
	@Override
	public void propertyConfigUpdateSuccessAudit(HttpServletRequest hsr, PropertyConfigResponse pcr) {
		LOG.info(">>>> propertyConfigUpdateSuccessAudit");
		
		Iterable<PropertyConfigTbl> propertyConfigTbls = pcr.getPropertyConfigTbls();
        for (PropertyConfigTbl propertyConfigTbl : propertyConfigTbls) {
        	String category = propertyConfigTbl.getCategory();
        	
        	if(category.equals(ConfigCategory.LDAP.name())){
        		
        		String changes = "LDAP configuration got updated";
		        AdminHistoryBaseObjectsTbl ahbot
		                = this.adminHistoryBaseObjectUtils
		                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
		                                "PropertyConfigTbl", "ldap configuration update", "success");
		        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        		
        	} else if(category.equals(ConfigCategory.SMTP.name())){
        		String changes = "SMTP configuration got updated";
		        AdminHistoryBaseObjectsTbl ahbot
		                = this.adminHistoryBaseObjectUtils
		                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
		                                "PropertyConfigTbl", "smtp configuration update", "success");
		        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        	} else if(category.equals(ConfigCategory.SINGLETON.name())){
        		String changes = "SINGLETON configuration got updated";
		        AdminHistoryBaseObjectsTbl ahbot
		                = this.adminHistoryBaseObjectUtils
		                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
		                                "PropertyConfigTbl", "singleton configuration update", "success");
		        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        	} else if(category.equals(ConfigCategory.PROJECTPOPUP.name())){
        		String changes = "PROJECT_POPUP configuration got updated";
		        AdminHistoryBaseObjectsTbl ahbot
		                = this.adminHistoryBaseObjectUtils
		                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
		                                "PropertyConfigTbl", "project popup configuration update", "success");
		        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        	} else if(category.equals(ConfigCategory.PROJECTEXPIRYDAYS.name())){
        		String changes = "PROJECTEXPIRYDAYS configuration got updated";
		        AdminHistoryBaseObjectsTbl ahbot
		                = this.adminHistoryBaseObjectUtils
		                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
		                                "PropertyConfigTbl", "project expiry configuration update", "success");
		        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        	}
        	break;
		}
        LOG.info("<<<< propertyConfigUpdateSuccessAudit");
		
	}
	@Override
	public void propertyConfigUpdateFailureAudit(HttpServletRequest hsr, Object t, String errMsg) {
		LOG.info(">>>> propertyConfigUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "PropertyConfig", "PropertyConfig update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info(">>>> propertyConfigUpdateFailureAudit");
	}

}
