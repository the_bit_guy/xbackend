/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.GroupAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.enums.Groups;
import com.magna.xmbackend.vo.group.GroupCreateRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class GroupAuditMgrImpl implements GroupAuditMgr {
	
	private static final Logger LOG
    = LoggerFactory.getLogger(GroupAuditMgrImpl.class);
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

	@Override
	public void groupCreateSuccessAudit(HttpServletRequest hsr, GroupsTbl gt) {
		LOG.info(">>>> groupCreateSuccessAudit");
		String name = gt.getName();
		String groupType = gt.getGroupType();
		String changes = "";
		if (groupType.equals(Groups.USER.name())) {
			changes = "User Group created with name " + name;
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "User group create", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECT.name())) {
			changes = "Project Group created with name " + name;
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "Project group create", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.USERAPPLICATION.name())) {
			changes = "UserApplication Group created with name " + name;
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "UserApp group create", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
			changes = "ProjectApplication Group created with name " + name;
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "ProjectApp group create", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		}
        
		
		LOG.info("<<<< groupCreateSuccessAudit");
		
	}

	@Override
	public void groupCreateFailureAudit(HttpServletRequest hsr, GroupCreateRequest gcr, String errMsg) {
		LOG.info(">>>> groupCreateFailureAudit");
        String name = gcr.getGroupName();
        String errorMessage = "Group creation failed for name " + name
                + "with exception " + errMsg;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errorMessage,
                                "GroupTbl", "group create", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< groupCreateFailureAudit");
		
	}

	@Override
	public void groupDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg) {
		LOG.info(">>>> groupDeleteFailureAudit with id {}", id);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                                "GroupTbl", "group delete", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< groupDeleteFailureAudit ");
		
	}

	@Override
	public void groupDeleteSuccessAudit(HttpServletRequest hsr, String id, String name, String groupType) {
		LOG.info(">>>> groupDeleteSuccessAudit with id {}", id);
		String changes = "";
		if (groupType.equals(Groups.USER.name())) {
			changes = "User Group with name " + name + " deleted";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "User group delete", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECT.name())) {
			changes = "Project Group with name " + name + " deleted";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "Project group delete", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.USERAPPLICATION.name())) {
			changes = "UserApplication Group with name " + name + " deleted";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "UserApp group delete", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
			changes = "ProjectApplication Group with name " + name + " deleted";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "ProjectApp group delete", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		}

		LOG.info(">>>> groupDeleteSuccessAudit");

	}

	@Override
	public void groupUpdateSuccessAudit(HttpServletRequest hsr, GroupsTbl gt) {
		LOG.info(">>>> groupUpdateSuccessAudit");
		String name = gt.getName();
		String groupType = gt.getGroupType();
		String changes = "";
		if (groupType.equals(Groups.USER.name())) {
			changes = "User Group with name " + name + " updated";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "User group update", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECT.name())) {
			changes = "Project Group with name " + name + " updated";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "Project group update", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.USERAPPLICATION.name())) {
			changes = "UserApplication Group with name " + name + " updated";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "UserApp group update", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		} else if (groupType.equals(Groups.PROJECTAPPLICATION.name())) {
			changes = "ProjectApplication Group with name " + name + " updated";
			AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(hsr,
					changes, "", "GroupTbl", "ProjectApp group update", "success");
			this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		}
        LOG.info("<<<< groupUpdateSuccessAudit");
		
	}

	@Override
	public void groupUpdateFailureAudit(HttpServletRequest hsr, GroupCreateRequest gcr, String errMsg) {
		LOG.info(">>>> groupUpdateFailureAudit");
		String groupType = gcr.getGroupType();
		String operation = "";
		if(groupType.equals(Groups.USER.name())){
			operation = "User group update";
		} else if(groupType.equals(Groups.PROJECT.name())){
			operation = "Project group update ";
		} else if(groupType.equals(Groups.USERAPPLICATION.name())){
			operation = "UserApplication group update ";
		} else if(groupType.equals(Groups.PROJECTAPPLICATION.name())){
			operation = "ProjectApplication group update ";
		}
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "GroupTbl", operation, "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info(">>>> groupUpdateFailureAudit");
		
	}


}
