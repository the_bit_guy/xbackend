package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.AdminAreaStartAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.jpa.dao.StartApplicationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;


@Component
public class AdminAreaStartAppAuditMgrImpl implements AdminAreaStartAppAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AdminAreaStartAppAuditMgrImpl.class);
	
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
	@Autowired
	private StartApplicationJpaDao startApplicationJpaDao;
	
	

	@Override
	public void adminAreaStartAppMultiSaveAuditor(AdminAreaStartAppRelBatchRequest batchRequest,
			AdminAreaStartAppRelBatchResponse batchResponse, HttpServletRequest httpServletRequest) {
		LOG.info(">>> adminAreaStartAppMultiSaveAuditor");

		List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests = batchRequest.getAdminAreaStartAppRelRequests();

		List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls = batchResponse.getAdminAreaStartAppRelTbls();
		List<Map<String, String>> statusMap = batchResponse.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(adminAreaStartAppRelTbls, httpServletRequest);

		} else {
			//Partial success case
			// iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, adminAreaStartAppRelRequests, httpServletRequest);

			// iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(adminAreaStartAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< adminAreaStartAppMultiSaveAuditor");
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests, HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (AdminAreaStartAppRelRequest adminAreaStartAppReq : adminAreaStartAppRelRequests) {
				String siteAdminAreaRelId = adminAreaStartAppReq.getSiteAdminAreaRelId();
				String startAppId = adminAreaStartAppReq.getStartAppId();
				String status = adminAreaStartAppReq.getStatus();
				
				SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaRelId);
				StartApplicationsTbl startAppTbl = this.startApplicationJpaDao.findOne(startAppId);
				
				
				if (siteAdminAreaRelTbl != null && startAppTbl != null) {
					String message = map.get("en");
					String startAppName = startAppTbl.getName();
					String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
					if (message.contains(startAppName) && message.contains(adminAreaName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaStartApp",
										adminAreaName, startAppName,null, 
										null, status,
										null, message, "AdminAreaStartApp Create", "Failure");
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
				}
			}
		}
		
		
	}

	private void createSuccessAudit(List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls,
			HttpServletRequest httpServletRequest) {
		adminAreaStartAppRelTbls.forEach(adminAreaStartAppRelTbl -> {
			String adminAreaName = adminAreaStartAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
			String startAppName = adminAreaStartAppRelTbl.getStartApplicationId().getName();
			String status = adminAreaStartAppRelTbl.getStatus();
			
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaStartApp", adminAreaName,
							startAppName, null, null,
							status, null, null, "AdminAreaStartApp Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void adminAreaStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "AdminAreaStartApp", null, null, null, null, null, null, ex.getMessage(), "AdminAreaStartApp Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}

	@Override
	public void adminAreaStartAppMultiDeleteSuccessAudit(AdminAreaStartAppRelTbl adminAreaStartAppRelTbl,
			HttpServletRequest httpServletRequest) {

		String adminAreaName = adminAreaStartAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
		String startAppName = adminAreaStartAppRelTbl.getStartApplicationId().getName();
		String status = adminAreaStartAppRelTbl.getStatus();
		
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "AdminAreaStartApp", adminAreaName,
						startAppName, null, null,
						status, null, null, "AdminAreaStartApp Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}



}
