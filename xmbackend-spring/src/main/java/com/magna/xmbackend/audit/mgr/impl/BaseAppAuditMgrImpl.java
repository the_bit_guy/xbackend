package com.magna.xmbackend.audit.mgr.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.BaseAppAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;

/**
 *
 * @author Admin
 */
@Component
public class BaseAppAuditMgrImpl implements BaseAppAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(BaseAppAuditMgrImpl.class);

    final String OBJECT_NAME = "BaseApplicationsTbl";
    final String OPERATION_CREATE = "Base App Create";
    final String OPERATION_UPDATE = "Base App Update";
    final String OPERATION_DELETE = "Base App Delete";
    final String OPERATION_MULTI_DELETE = "Base App Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param baseApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void baseAppCreateSuccessAuditor(final BaseApplicationsTbl baseApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> baseAppCreateSuccessAuditor");
        final String name = baseApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Base Application created with ")
             .append(" name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_CREATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppCreateSuccessAuditor");
    }

    /**
     *
     * @param baseApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void baseAppCreateFailureAuditor(final BaseApplicationRequest baseApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> baseAppCreateFailureAuditor");
        final String name = baseApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Base Application creation failed for name")
                .append(name);
        final String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppCreateFailureAuditor");
    }

    /**
     *
     * @param baseApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void baseAppUpdateSuccessAuditor(final BaseApplicationsTbl baseApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> baseAppUpdateSuccessAuditor");
        final String name = baseApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Base Application updated with ")
               .append(" name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppUpdateSuccessAuditor");
    }

    /**
     *
     * @param baseApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void baseAppUpdateFailureAuditor(final BaseApplicationRequest baseApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> baseAppUpdateFailureAuditor");
        final String name = baseApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Base Application update failed for name")
                .append(name);
        String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppUpdateFailureAuditor");
    }

    /**
     *
     * @param status
     * @param baseAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param baseApplicationsTbl
     */
    @Override
    public void baseAppUpdateStatusAuditor(final String status, final String baseAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final BaseApplicationsTbl baseApplicationsTbl) {
        LOG.info(">>>> baseAppUpdateStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String baseAppName = "";
        if (null != baseApplicationsTbl) {
            baseAppName = baseApplicationsTbl.getName();
        }
        if (isSuccess) {
            changeBuff.append("Base Application updated with ")
                   .append(" with name ")
                    .append(baseAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("Base Application updation with name ")
                    .append(baseAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(),
                        errorBuff.toString(), OBJECT_NAME, OPERATION_UPDATE,
                        result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppUpdateStatusAuditor::isSuccess");
    }

    /**
     *
     * @param baseAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param baseApplicationsTbl
     */
    @Override
    public void baseAppDeleteStatusAuditor(final String baseAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final BaseApplicationsTbl baseApplicationsTbl) {
        LOG.info(">>>> baseAppDeleteStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String baseAppName = "";
        if (null != baseApplicationsTbl) {
            baseAppName = baseApplicationsTbl.getName();
        }
        if (isSuccess) {
            changeBuff.append("Base Application deletion ")
                    .append(" with name:").append(baseAppName).append(" success");
            result = "success";
        } else {
            errorBuff.append("Base Application deletion with name ").append(baseAppName)
                    .append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorBuff.toString(),
                        OBJECT_NAME, OPERATION_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppDeleteStatusAuditor");
    }

    /**
     *
     * @param baseApplicationResponse
     * @param baseAppIds
     * @param httpServletRequest
     */
    @Override
    public void baseAppMultiDeleteAuditor(final BaseApplicationResponse baseApplicationResponse,
            final Set<String> baseAppIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> baseAppMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = baseApplicationResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given BaseApp Ids:").append(String.join(" ", baseAppIds));
        if (!statusMaps.isEmpty() && (baseAppIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (baseAppIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< baseAppMultiDeleteAuditor");
    }
}
