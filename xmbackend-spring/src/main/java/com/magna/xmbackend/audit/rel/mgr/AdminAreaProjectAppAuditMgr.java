/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminAreaProjectAppAuditMgr {

	void adminAreaProjectAppMultiSaveAuditor(AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest,
			AdminAreaProjectAppRelBatchResponse aaparbr, HttpServletRequest httpServletRequest);

	void adminAreaProjectAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void adminAreaProjectAppMultiDeleteAuditor(AdminAreaProjAppRelTbl adminAreaProjAppRelTbl,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectAppUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectAppRelUpdateAuditor(AdminAreaProjectAppRelBatchResponse batchResp,
			HttpServletRequest httpServletRequest);

}
