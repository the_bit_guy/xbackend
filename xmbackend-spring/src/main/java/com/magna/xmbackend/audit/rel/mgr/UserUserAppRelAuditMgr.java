/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserUserAppRelAuditMgr {

	void userUserAppMultiSaveAuditor(UserUserAppRelBatchRequest userUserAppRelBatchRequest,
			UserUserAppRelBatchResponse usarbr, HttpServletRequest httpServletRequest);

	void userUserMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void userUserAppMultiDeleteSuccessAudit(UserUserAppRelTbl userUserAppRelTbl, HttpServletRequest httpServletRequest);

}
