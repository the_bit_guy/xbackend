/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserProjectAppRelAuditMgr {

	void userProjectAppMultiSaveAuditor(UserProjectAppRelBatchRequest userProjectAppRelBatchRequest,
			UserProjectAppRelBatchResponse aaparbr, HttpServletRequest httpServletRequest);

	void userProjectMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void userProjectAppMultiDeleteSuccessAudit(UserProjAppRelTbl userProjAppRelTbl,
			HttpServletRequest httpServletRequest);

}
