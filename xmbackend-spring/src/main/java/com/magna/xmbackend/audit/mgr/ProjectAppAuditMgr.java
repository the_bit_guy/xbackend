package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public interface ProjectAppAuditMgr {

    /**
     *
     * @param projectApplicationsTbl
     * @param httpServletRequest
     */
    void projectAppCreateSuccessAuditor(final ProjectApplicationsTbl projectApplicationsTbl,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param projectApplicationRequest
     * @param httpServletRequest
     */
    void projectAppCreateFailureAuditor(final ProjectApplicationRequest projectApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param projectApplicationsTbl
     * @param servletRequest
     */
    void projectAppUpdateSuccessAuditor(final ProjectApplicationsTbl projectApplicationsTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param projectApplicationRequest
     * @param httpServletRequest
     */
    void projectAppUpdateFailureAuditor(final ProjectApplicationRequest projectApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param projectAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param pat
     */
    void projectAppUpdateStatusAuditor(final String status, final String projectAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final ProjectApplicationsTbl pat);

    /**
     *
     * @param projectAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param pat
     */
    void projectAppDeleteStatusAuditor(final String projectAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final ProjectApplicationsTbl pat);

    /**
     *
     * @param projectApplicationResponse
     * @param projectAppIds
     * @param httpServletRequest
     */
    void projectAppMultiDeleteAuditor(final ProjectApplicationResponse projectApplicationResponse,
            final Set<String> projectAppIds, final HttpServletRequest httpServletRequest);
}
