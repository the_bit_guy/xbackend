package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;
import java.util.Set;

/**
 *
 * @author dhana
 */
public interface SiteAdminAreaAuditMgr {

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param servletRequest
     * @param relationName
     * @param operation
     * @param result
     */
    void siteAdminAreaAuditor(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final HttpServletRequest servletRequest,
            final String relationName, final String operation,
            final String result);

    /**
     *
     * @param siteAdminAreaRelRequest
     * @param ccrex
     * @param servletRequest
     */
    void siteAdminAreaCreateFailAuditor(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final CannotCreateRelationshipException ccrex,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param siteAdminAreaRelBatchRequest
     * @param saarbr
     * @param httpServletRequest
     */
    void siteAdminAreaMultiSaveAuditor(final SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest,
            SiteAdminAreaRelBatchResponse saarbr, final HttpServletRequest httpServletRequest);

    /**
     *
     * @param siteAdminAreaId
     * @param errMsg
     * @param httpServletRequest
     * @param siteAdminAreaRelTbl 
     */
    void siteAdminAreaDeleteFailureAuditor(final String siteAdminAreaId,
            final String errMsg, final HttpServletRequest httpServletRequest,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl);

    /**
     *
     * @param siteAdminAreaRelResponse
     * @param siteAdminAreaIds
     * @param httpServletRequest
     */
    void siteAdminAreaMultiDeleteAuditor(
            final SiteAdminAreaRelResponse siteAdminAreaRelResponse,
            final Set<String> siteAdminAreaIds,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param siteAdminAreaId
     * @param httpServletRequest
     * @param siteAdminAreaRelTbl 
     */
    void siteAdminAreaDeleteSuccessAuditor(final String siteAdminAreaId,
            final HttpServletRequest httpServletRequest,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl);

    /**
     *
     * @param siteAdminAreaId
     * @param errMsg
     * @param httpServletRequest
     */
    void siteAdminAreaUpdateFailAuditor(final String siteAdminAreaId,
            final String errMsg, final HttpServletRequest httpServletRequest);

    /**
     *
     * @param siteAdminAreaId
     * @param httpServletRequest
     */
    void siteAdminAreaUpdateSuccessAuditor(final String siteAdminAreaId,
            final HttpServletRequest httpServletRequest);

	void siteAdminAreaMultiDeleteSuccessAudit(SiteAdminAreaRelTbl siteAdminAreaRelTbl,
			HttpServletRequest httpServletRequest);
}
