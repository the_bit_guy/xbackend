/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface ProjectStartAppAuditMgr {

	void projectStartAppMultiSaveAuditor(ProjectStartAppRelBatchRequest projectStartAppRelBatchRequest,
			ProjectStartAppRelBatchResponse psarbr, HttpServletRequest httpServletRequest);

	void projectStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void projectStartAppMultiDeleteSuccessAudit(ProjectStartAppRelTbl projectStartAppRelTbl,
			HttpServletRequest httpServletRequest);

}
