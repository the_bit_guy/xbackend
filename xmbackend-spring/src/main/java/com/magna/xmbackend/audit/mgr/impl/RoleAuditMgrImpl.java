package com.magna.xmbackend.audit.mgr.impl;

import com.magna.xmbackend.audit.mgr.RoleAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class RoleAuditMgrImpl implements RoleAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(RoleAuditMgrImpl.class);

    final String OBJECT_NAME = "RolesTbl";
    final String OPERATION_CREATE = "Role Create";
    final String OPERATION_UPDATE = "Role Update";
    final String OPERATION_DELETE = "Role Delete";
    final String OPERATION_MULTI_DELETE = "Role Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param roleResponse
     * @param httpServletRequest
     */
    @Override
    public void roleCreateSuccessAuditor(final RoleResponse roleResponse,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> roleCreateSuccessAuditor");
        final Iterable<RolesTbl> rolesTbls = roleResponse.getRolesTbls();
        if (null != rolesTbls) {
            rolesTbls.forEach(rolesTbl -> {
                final String roleId = rolesTbl.getRoleId();
                final String name = rolesTbl.getName();
                final StringBuilder changeBuff = new StringBuilder();
                changeBuff.append("Role created with id:")
                        .append(roleId).append(" and name").append(name);
                final String errorMessage = "";
                final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                httpServletRequest, changeBuff.toString(), errorMessage,
                                OBJECT_NAME, OPERATION_CREATE, RESULT_SUCCESS);
                this.adminHistoryBaseObjectsJpaDao.save(ahbot);
            });
        }
        LOG.info("<<<< roleCreateSuccessAuditor");
    }

    /**
     *
     * @param roleRequestList
     * @param httpServletRequest
     */
    @Override
    public void roleCreateFailureAuditor(final List<RoleRequest> roleRequestList,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> roleCreateFailureAuditor");
        if (null != roleRequestList) {
            roleRequestList.forEach(roleRequest -> {
                final String name = roleRequest.getName();
                final StringBuilder errorBuff = new StringBuilder();
                errorBuff.append("Role creation failed for name")
                        .append(name);
                final String changes = "";
                final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                httpServletRequest, changes, errorBuff.toString(),
                                OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
                this.adminHistoryBaseObjectsJpaDao.save(ahbot);
            });
        }
        LOG.info("<<<< roleCreateFailureAuditor");
    }

    /**
     *
     * @param roleResponse
     * @param httpServletRequest
     */
    @Override
    public void roleUpdateSuccessAuditor(final RoleResponse roleResponse,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> roleUpdateSuccessAuditor");
        final Iterable<RolesTbl> rolesTbls = roleResponse.getRolesTbls();
        if (null != rolesTbls) {
            rolesTbls.forEach(rolesTbl -> {
                final String roleId = rolesTbl.getRoleId();
                final String name = rolesTbl.getName();
                final StringBuilder changeBuff = new StringBuilder();
                changeBuff.append("Role updated with id:")
                        .append(roleId).append(" and name").append(name);
                final String errorMessage = "";
                final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                httpServletRequest, changeBuff.toString(), errorMessage,
                                OBJECT_NAME, OPERATION_UPDATE, RESULT_SUCCESS);
                this.adminHistoryBaseObjectsJpaDao.save(ahbot);
            });
        }
        LOG.info("<<<< roleUpdateSuccessAuditor");
    }

    /**
     *
     * @param roleRequestList
     * @param httpServletRequest
     */
    @Override
    public void roleUpdateFailureAuditor(final List<RoleRequest> roleRequestList,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> roleUpdateFailureAuditor");
        if (null != roleRequestList) {
            roleRequestList.forEach(roleRequest -> {
                final String name = roleRequest.getName();
                final StringBuilder errorBuff = new StringBuilder();
                errorBuff.append("Role update failed for name")
                        .append(name);
                String changes = "";
                final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                httpServletRequest, changes, errorBuff.toString(),
                                OBJECT_NAME, OPERATION_UPDATE, RESULT_FAILURE);
                this.adminHistoryBaseObjectsJpaDao.save(ahbot);
            });
        }
        LOG.info("<<<< roleUpdateFailureAuditor");
    }

    /**
     *
     * @param roleId
     * @param httpServletRequest
     * @param isSuccess
     * @param rolesTbl
     */
    @Override
    public void roleDeleteStatusAuditor(final String roleId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final RolesTbl rolesTbl) {
        LOG.info(">>>> roleDeleteStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String roleName = "";
        if (null != rolesTbl) {
            roleName = rolesTbl.getName();
        }
        if (isSuccess) {
            changeBuff.append("Role deletion with id:").append(roleId)
                    .append(" and name:").append(roleName).append(" success");
            result = "success";
        } else {
            errorBuff.append("Role deletion with id:").append(roleId)
                    .append(" and name:").append(roleName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorBuff.toString(),
                        OBJECT_NAME, OPERATION_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< roleDeleteStatusAuditor");
    }

    /**
     *
     * @param roleResponse
     * @param roleIds
     * @param httpServletRequest
     */
    @Override
    public void roleMultiDeleteAuditor(final RoleResponse roleResponse,
            final Set<String> roleIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> roleMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = roleResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given Role Ids:").append(String.join(" ", roleIds));
        if (!statusMaps.isEmpty() && (roleIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (roleIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< roleMultiDeleteAuditor");
    }
}
