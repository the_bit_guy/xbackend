/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserProjectAuditMgr {

	void userProjectMultiSaveAuditor(final UserProjectRelBatchRequest userProjectRelBatchRequest,
			UserProjectRelBatchResponse uprbr, final HttpServletRequest httpServletRequest);
	
	void userProjectMultiSaveFailureAuditor(final HttpServletRequest httpServletRequest, final Exception ex);

	void userProjectMultiDeleteSuccessAudit(UserProjectRelTbl userProjectRelTbl, HttpServletRequest httpServletRequest);

	/*void userProjectMultiDeleteAuditor(final SiteAdminAreaRelResponse siteAdminAreaRelResponse,
			final Set<String> siteAdminAreaIds, final HttpServletRequest httpServletRequest);

	void userProjectUpdateFailAuditor(final String siteAdminAreaId, final String errMsg,
			final HttpServletRequest httpServletRequest);

	*//**
	 *
	 * @param siteAdminAreaId
	 * @param httpServletRequest
	 *//*
	void userProjectUpdateSuccessAuditor(final String siteAdminAreaId, final HttpServletRequest httpServletRequest);*/

}
