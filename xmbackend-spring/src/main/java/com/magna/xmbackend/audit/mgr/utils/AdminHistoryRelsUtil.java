/**
 * 
 */
package com.magna.xmbackend.audit.mgr.utils;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.enums.Groups;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminHistoryRelsUtil {

	private static final Logger LOG = LoggerFactory.getLogger(AdminHistoryRelsUtil.class);
	
	
	/**
	 * 
	 * @param servletRequest
	 * @param relationName
	 * @param relationObjectName1
	 * @param relationObjectName2
	 * @param status
	 * @param errorMessage
	 * @param operation
	 * @param result
	 * @return AdminHistoryRelationsTbl
	 */
	public AdminHistoryRelationsTbl makeAdminHistoryRelationTbl(
            final HttpServletRequest servletRequest, final String relationName,
            final String relationObjectName1,
            final String relationObjectName2,
            final String relationObjectName3,
            final String relationObjectName4,
            final String statusOrRelType,
            final String role,
            final String errorMessage,
            final String operation,
            final String result) {
		LOG.info("---> makeAdminHistoryRelationTbl");
        final Date date = new Date();
        final AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = new AdminHistoryRelationsTbl();
        switch (relationName) {
            case "SiteAdminArea":
                adminHistoryRelationsTbl.setSite(relationObjectName1);
                adminHistoryRelationsTbl.setAdminArea(relationObjectName2);
                adminHistoryRelationsTbl.setStatus(statusOrRelType);
                break;
            case "UserProject":
                adminHistoryRelationsTbl.setUserName(relationObjectName1);
                adminHistoryRelationsTbl.setProject(relationObjectName2);
                adminHistoryRelationsTbl.setStatus(statusOrRelType);
                break;  
            case "UserProjectApp":
                adminHistoryRelationsTbl.setUserName(relationObjectName1);
                adminHistoryRelationsTbl.setProjectApplication(relationObjectName2);
                adminHistoryRelationsTbl.setProject(relationObjectName3);
                adminHistoryRelationsTbl.setAdminArea(relationObjectName4);
                adminHistoryRelationsTbl.setRelationType(statusOrRelType);
                break;
            case "UserUserApp":
				adminHistoryRelationsTbl.setUserName(relationObjectName1);
				adminHistoryRelationsTbl.setUserApplication(relationObjectName2);
				adminHistoryRelationsTbl.setAdminArea(relationObjectName4);
				adminHistoryRelationsTbl.setRelationType(statusOrRelType);
			break;
            case "RoleUser":
                adminHistoryRelationsTbl.setRole(relationObjectName1);
                adminHistoryRelationsTbl.setUserName(relationObjectName2);
                adminHistoryRelationsTbl.setAdminArea(relationObjectName3);
                break;
            case "UserStartApp":
            	adminHistoryRelationsTbl.setUserName(relationObjectName1);
            	adminHistoryRelationsTbl.setStartApplication(relationObjectName2);
            	adminHistoryRelationsTbl.setStatus(statusOrRelType);
            	break;
            case "DirectoryRelation":
            	adminHistoryRelationsTbl.setDirectory(relationObjectName1);
            	if(relationObjectName2.equals(DirectoryObjectType.USER.name())) {
            		adminHistoryRelationsTbl.setUserName(relationObjectName3);
            	} else if(relationObjectName2.equals(DirectoryObjectType.PROJECT.name())){
            		adminHistoryRelationsTbl.setProject(relationObjectName3);
            	} else if(relationObjectName2.equals(DirectoryObjectType.USERAPPLICATION.name())){
            		adminHistoryRelationsTbl.setUserApplication(relationObjectName3);
            	} else if(relationObjectName2.equals(DirectoryObjectType.PROJECTAPPLICATION.name())){
            		adminHistoryRelationsTbl.setProjectApplication(relationObjectName3);
            	}
            	break;
            case "GroupRelation":
            	adminHistoryRelationsTbl.setGroupName(relationObjectName1);
            	if(relationObjectName2.equals(Groups.USER.name())) {
            		adminHistoryRelationsTbl.setUserName(relationObjectName3);
            	} else if(relationObjectName2.equals(Groups.PROJECT.name())){
            		adminHistoryRelationsTbl.setProject(relationObjectName3);
            	} else if(relationObjectName2.equals(Groups.USERAPPLICATION.name())){
            		adminHistoryRelationsTbl.setUserApplication(relationObjectName3);
            	} else if(relationObjectName2.equals(Groups.PROJECTAPPLICATION.name())){
            		adminHistoryRelationsTbl.setProjectApplication(relationObjectName3);
            	}
            	break;
            case "AdminAreaProject":
            	adminHistoryRelationsTbl.setAdminArea(relationObjectName1);
            	adminHistoryRelationsTbl.setProject(relationObjectName2);
            	adminHistoryRelationsTbl.setStatus(statusOrRelType);
            	break;
            case "AdminAreaProjectApp":
            	adminHistoryRelationsTbl.setAdminArea(relationObjectName1);
            	adminHistoryRelationsTbl.setProject(relationObjectName2);
            	adminHistoryRelationsTbl.setProjectApplication(relationObjectName3);
            	adminHistoryRelationsTbl.setRelationType(statusOrRelType);
            	break;
            case "AdminAreaStartApp":
            	adminHistoryRelationsTbl.setAdminArea(relationObjectName1);
            	adminHistoryRelationsTbl.setStartApplication(relationObjectName2);
            	adminHistoryRelationsTbl.setStatus(statusOrRelType);
            	break;
            case "AdminAreaUserApp":
            	adminHistoryRelationsTbl.setAdminArea(relationObjectName1);
            	adminHistoryRelationsTbl.setUserApplication(relationObjectName2);
            	adminHistoryRelationsTbl.setStatus(statusOrRelType);
            	break;
            case "ProjectStartApp":
            	adminHistoryRelationsTbl.setProject(relationObjectName1);
            	adminHistoryRelationsTbl.setStartApplication(relationObjectName2);
            	adminHistoryRelationsTbl.setStatus(statusOrRelType);
            	break;
            case "AdminMenuConfig":
            	//adminHistoryRelationsTbl.setErrorMessage(errorMessage);
            	if(relationObjectName2.equals(DirectoryObjectType.USER.name())) {
            		adminHistoryRelationsTbl.setUserName(relationObjectName1);
            	} else if(relationObjectName2.equals(DirectoryObjectType.USERAPPLICATION.name())){
            		adminHistoryRelationsTbl.setUserApplication(relationObjectName1);
            	} else if(relationObjectName2.equals(DirectoryObjectType.PROJECTAPPLICATION.name())){
            		adminHistoryRelationsTbl.setProjectApplication(relationObjectName1);
            	}
            	break;
            case "RoleAdminArea":
            	adminHistoryRelationsTbl.setRole(relationObjectName1);
            	adminHistoryRelationsTbl.setAdminArea(relationObjectName2);
            	break;
            default:
                break;
        }
        adminHistoryRelationsTbl.setAdminName(servletRequest.getHeader("USER_NAME"));
        adminHistoryRelationsTbl.setApiRequestPath(servletRequest.getServletPath());
        adminHistoryRelationsTbl.setErrorMessage(errorMessage);
        adminHistoryRelationsTbl.setLogTime(date);
        adminHistoryRelationsTbl.setOperation(operation);
        adminHistoryRelationsTbl.setResult(result);
        
        LOG.info("---> makeAdminHistoryRelationTbl");
        return adminHistoryRelationsTbl;
    }
}
