package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Administrator
 */
public interface AdminAreaAudiMgr {

    /**
     *
     * @param adminAreasTbl
     * @param servletRequest
     */
    void adminAreaCreateSuccessAuditor(final AdminAreasTbl adminAreasTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param adminAreaRequest
     * @param servletRequest
     */
    void adminAreaCreateFailureAuditor(final AdminAreaRequest adminAreaRequest,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param adminAreasTbl
     * @param servletRequest
     */
    void adminAreaUpdateSuccessAuditor(final AdminAreasTbl adminAreasTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param adminAreaRequest
     * @param servletRequest
     */
    void adminAreaUpdateFailureAuditor(final AdminAreaRequest adminAreaRequest,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param status
     * @param id
     * @param servletRequest
     * @param isSuccess
     * @param aat
     */
    void admainAreaUpdateStatusAuditor(final String status, final String id,
            final HttpServletRequest servletRequest, 
            final boolean isSuccess, final AdminAreasTbl aat);

    /**
     *
     * @param id
     * @param servletRequest
     * @param isSuccess
     * @param aat
     */
    void adminAreaDeleteStatusAuditor(final String id,
            final HttpServletRequest servletRequest,
            final boolean isSuccess, final AdminAreasTbl aat);

    /**
     *
     * @param adminAreaResponse
     * @param adminAreaIds
     * @param httpServletRequest
     */
    void adminAreaMultiDeleteAuditor(final AdminAreaResponse adminAreaResponse,
            final Set<String> adminAreaIds, final HttpServletRequest httpServletRequest);
}
