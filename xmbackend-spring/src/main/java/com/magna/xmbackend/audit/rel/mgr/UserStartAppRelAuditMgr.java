/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserStartAppRelAuditMgr {

	void userStartAppMultiSaveAuditor(UserStartAppRelBatchRequest userStartAppRelBatchRequest,
			UserStartAppRelBatchResponse usarbr, HttpServletRequest httpServletRequest);

	void userStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void userStartAppMultiDeleteSuccessAudit(UserStartAppRelTbl userStartAppRelTbls,
			HttpServletRequest httpServletRequest);

}
