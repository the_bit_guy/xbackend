package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.UserUserAppRelAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;


@Component
public class UserUserAppRelAuditMgrImpl implements UserUserAppRelAuditMgr {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(UserUserAppRelAuditMgrImpl.class);
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
	@Autowired
	private UserApplicationJpaDao userApplicationJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	
	

	@Override
	public void userUserAppMultiSaveAuditor(UserUserAppRelBatchRequest uuarbReq,
			UserUserAppRelBatchResponse uuarbRes, HttpServletRequest httpServletRequest) {

		LOG.info(">>> userUserAppMultiSaveAuditor");

		List<UserUserAppRelRequest> userUserAppRelRequests = uuarbReq.getUserUserAppRelRequests();

		List<UserUserAppRelTbl> userUserAppRelTbls = uuarbRes.getUserUserAppRelTbls();
		List<Map<String, String>> statusMap = uuarbRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(userUserAppRelTbls, httpServletRequest);

		} else {
			//Partial success case
			// iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, userUserAppRelRequests, httpServletRequest);

			// iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(userUserAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< userProjectAppMultiSaveAuditor");
	
		
	}
	
	private void createSuccessAudit(List<UserUserAppRelTbl> userUserAppRelTbls, HttpServletRequest httpServletRequest) {
		userUserAppRelTbls.forEach(userUserAppRelTbl -> {
			UsersTbl usersTbl = userUserAppRelTbl.getUserId();
			UserApplicationsTbl userApplicationId = userUserAppRelTbl.getUserApplicationId();
			AdminAreasTbl adminAreaTbl= userUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId();

			// ProjectsTbl projectTbl =
			// this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "UserUserApp", usersTbl.getUsername(),
							userApplicationId.getName(), adminAreaTbl.getName(), null,
							userUserAppRelTbl.getUserRelType(), null, null, "UserUserAppRelation Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
	}
	
	private void createFailureAudit(List<Map<String, String>> statusMap, List<UserUserAppRelRequest> userUserAppRelRequests, 
			HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (UserUserAppRelRequest userUserAppRelReq : userUserAppRelRequests) {
				String userAppId = userUserAppRelReq.getUserAppId();
				String siteAdminAreaRelId = userUserAppRelReq.getSiteAdminAreaRelId();
				String userId = userUserAppRelReq.getUserId();
				
				SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaRelId);
				UsersTbl usersTbl = userJpaDao.findOne(userId);
				UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(userAppId);
				
				
				if (siteAdminAreaRelTbl != null && usersTbl != null && userAppTbl != null) {
					String message = map.get("en");
					String userAppName = userAppTbl.getName();
					String username = usersTbl.getUsername();
					String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
					if (message.contains(username) && message.contains(userAppName) && message.contains(adminAreaName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, "UserUserApp",
										username, userAppName,adminAreaName, 
										null, userUserAppRelReq.getUserRelationType(),
										null, message, "UserUserAppRelation Create", "Failure");
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
				}
			}
		}
	}

	@Override
	public void userUserMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> userUserMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "UserUserApp", null, null, null, null, null, null, ex.getMessage(), "UserProjectAppRelation Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< userUserMultiSaveFailureAuditor");
		
	}

	@Override
	public void userUserAppMultiDeleteSuccessAudit(UserUserAppRelTbl userUserAppRelTbl,
			HttpServletRequest httpServletRequest) {

		UsersTbl usersTbl = userUserAppRelTbl.getUserId();
		UserApplicationsTbl userApplicationId = userUserAppRelTbl.getUserApplicationId();
		AdminAreasTbl adminAreaTbl= userUserAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId();

		// ProjectsTbl projectTbl =
		// this.projectJpaDao.findByProjectId(userProjRelReq.getProjectId());
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "UserUserApp", usersTbl.getUsername(),
						userApplicationId.getName(), adminAreaTbl.getName(), null,
						userUserAppRelTbl.getUserRelType(), null, null, "UserUserAppRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

	
}
