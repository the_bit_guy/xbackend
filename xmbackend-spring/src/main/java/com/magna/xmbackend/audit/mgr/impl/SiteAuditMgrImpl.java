/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.SiteAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;

/**
 *
 * @author dhana
 */
@Component
public class SiteAuditMgrImpl implements SiteAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteAuditMgrImpl.class);

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    @Override
    public void siteCreateSuccessAudit(HttpServletRequest hsr, SitesTbl st) {
        LOG.info(">>>> siteCreateSuccessAudit");
        String name = st.getName();
        String changes = "Created site with name " + name;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "SitesTbl", "site create", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteCreateSuccessAudit");
    }

    @Override
    public void siteCreateFailureAudit(HttpServletRequest hsr,
            SiteRequest sr, String errMsg) {
        LOG.info(">>>> siteCreateFailureAudit");
        String name = sr.getName();
        String errorMessage = "Site creation failed for name " + name
                + "with exception " + errMsg;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errorMessage,
                                "SitesTbl", "site create", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteCreateFailureAudit");
    }

    @Override
    public void siteDeleteSuccessAudit(HttpServletRequest hsr, String id,
            String name) {
        LOG.info(">>>> sideDeleteSuccessAudit with id {}", id);
        String changes = "Site with name " + name
                + " deleted successfully";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr,
                                changes, "", "SitesTbl",
                                "site delete", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideDeleteSuccessAudit");
    }

    @Override
    public void siteDeleteFailureAudit(HttpServletRequest hsr,
            String id, String errMsg) {
        LOG.info(">>>> sideDeleteFailureAudit with id {}", id);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                                "SitesTbl", "site delete", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideDeleteFailureAudit ");
    }

    @Override
    public void siteUpdateSuccessAudit(HttpServletRequest hsr, SiteRequest sr) {
        LOG.info(">>>> sideUpdateSuccessAudit");
        String name = sr.getName();
        String changes = "Site with name "+name+" got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "SitesTbl", "site update", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideUpdateSuccessAudit");
    }

    @Override
    public void siteUpdateFailureAudit(HttpServletRequest hsr, SiteRequest sr,
            String errMsg) {
        LOG.info(">>>> sideUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "SitesTbl", "site update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideUpdateFailureAudit");
    }

    @Override
    public void siteUpdateSuccessAudit(HttpServletRequest hsr, String id,
            String name) {
        LOG.info(">>>> siteUpdateSuccessAudit");
        String changes = "Site with name " + name
                + " got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "SitesTbl", "site update", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteUpdateSuccessAudit");
    }

    @Override
    public void siteUpdateFailureAudit(HttpServletRequest hsr, String id,
            String errMsg) {
        LOG.info(">>>> siteUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "SitesTbl", "site update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteUpdateFailureAudit");
    }
}
