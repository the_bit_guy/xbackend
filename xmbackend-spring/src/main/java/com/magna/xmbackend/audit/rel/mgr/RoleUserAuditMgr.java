/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface RoleUserAuditMgr {

	void roleUserMultiSaveAuditor(List<RoleUserRelRequest> roleUserRelRequestList,
			RoleUserRelResponse roleUserRelResponse, HttpServletRequest httpServletRequest);

	void roleUserMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void roleUserMultiDeleteSuccessAudit(RoleUserRelTbl roleUserRelTbl, HttpServletRequest httpServletRequest);
	
	

}
