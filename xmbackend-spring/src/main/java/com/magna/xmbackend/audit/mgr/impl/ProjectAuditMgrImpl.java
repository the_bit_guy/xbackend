/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.ProjectAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.project.ProjectRequest;

/**
 *
 * @author dhana
 */
@Component
public class ProjectAuditMgrImpl implements ProjectAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectAuditMgrImpl.class);

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    @Override
    public void projectCreateAuditor(ProjectsTbl projectsTbl,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectCreateAuditor");
        String projectId = projectsTbl.getProjectId();
        String name = projectsTbl.getName();

        LOG.debug("project id {} with name {} created", projectId, name);

        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "Project Created with name " + name, "", "ProjectTbl",
                        "project create", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectCreateAuditor");
    }

    @Override
    public void projectCreateFailAuditor(ProjectRequest projectRequest,
            String errMsg,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectCreateFailAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, "", errMsg,
                        "ProjectTbl:" + projectRequest.getName(),
                        "project create", "Failure");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectCreateFailAuditor");
    }

    public void projectDeleteFailureAuditor(String projectId,
            String errMsg,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectDeleteAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, "",
                        errMsg,
                        "ProjectTbl",
                        "project delete", "Failure");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectDeleteAuditor");
    }

    @Override
    public void projectDeleteSuccessAuditor(String projectId, String name,
            HttpServletRequest servletRequest) {
        LOG.info(">>>> projectDeleteAuditor");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "Project Deleted with "
                        + " name " + name, "",
                        "ProjectTbl",
                        "project delete", "Success");
        AdminHistoryBaseObjectsTbl ahbotOut
                = this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.debug("ahbotOut {}", ahbotOut);
        LOG.info("<<<< projectDeleteAuditor");
    }

    
    @SuppressWarnings("unused")
	private String extractErrMsg(Map<String, String> statusMap) {
        String errMsg = "";
        for (Map.Entry<String, String> entry : statusMap.entrySet()) {
            errMsg = errMsg + entry.getKey() + " : " + entry.getValue() + " ";
        }
        return errMsg;
    }

    @Override
    public void projectUpdateFailAuditor(String projectId, String errMsg, HttpServletRequest servletRequest) {
        LOG.info(">>>> projectUpdateFailAuditor for projectId {}", projectId);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest,
                        "", errMsg,
                        "ProjectsTbl", "Project Update",
                        "Failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateFailAuditor");
    }

    @Override
    public void projectUpdateSuccessAuditor(ProjectsTbl projectsTbl, HttpServletRequest servletRequest) {
        String projectId = projectsTbl.getProjectId();
        String name = projectsTbl.getName();
        LOG.info(">>>> projectUpdateSuccessAuditor with projectId {} and name {}", projectId, name);
        String changes = "ProjectTbl with name " + name + " updated Successfully";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(servletRequest, changes, "", "ProjectTbl", "project update", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateSuccessAuditor");
    }

    @Override
    public void projectUpdateStautsSuccessAuditor(String projectId, String projectName, String status, HttpServletRequest servletRequest) {
        LOG.info(">>>> projectUpdateStautsSuccessAuditor for id {} with status {}", projectId, status);
        String changes = "ProjectTbl with name "
                + projectName + "updated with status " + status;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(
                                servletRequest, changes, "", "ProjectTbl",
                                "project update", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< projectUpdateStautsSuccessAuditor");
    }

}
