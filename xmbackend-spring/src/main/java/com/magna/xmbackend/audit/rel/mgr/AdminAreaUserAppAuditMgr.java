/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminAreaUserAppAuditMgr {

	void adminAreaUserAppMultiSaveAuditor(AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest,
			AdminAreaUserAppRelBatchResponse aauarbr, HttpServletRequest httpServletRequest);

	void adminAreaUserAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void adminAreaUserAppMultiDeleteSuccessAudit(AdminAreaUserAppRelTbl adminAreaUserAppRelTbl,
			HttpServletRequest httpServletRequest);

}
