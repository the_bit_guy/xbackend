/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.LiveMessageAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class LiveMessageAuditMgrImpl implements LiveMessageAuditMgr{
	
	private static final Logger LOG
    = LoggerFactory.getLogger(LiveMessageAuditMgrImpl.class);
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;
	

	@Override
	public void lmCreateSuccessAudit(HttpServletRequest hsr, LiveMessageTbl lmt) {
		LOG.info(">>>> directorySuccessAudit");
		String name = lmt.getLiveMessageName();
        String changes = "Created liveMsg with name " + name;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "LiveMessageTbl", "liveMsg create", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info("<<<< directorySuccessAudit");
		
	}

	@Override
	public void lmCreateFailureAudit(HttpServletRequest hsr, LiveMessageCreateRequest lmcr, String errMsg) {
		 LOG.info(">>>> lmCreateFailureAudit");
	        String name = lmcr.getLiveMessageName();
	        String errorMessage = "LiveMsg creation failed for name " + name
	                + "with exception " + errMsg;
	        AdminHistoryBaseObjectsTbl ahbot
	                = this.adminHistoryBaseObjectUtils
	                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errorMessage,
	                                "LiveMessageTbl", "liveMsg create", "failure");
	        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
	        LOG.info("<<<< lmCreateFailureAudit");
		
	}

	@Override
	public void lmDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg) {
		LOG.info(">>>> lmDeleteFailureAudit with id {}", id);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                                "LiveMessageTbl", "liveMsg delete", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< lmDeleteFailureAudit ");
		
	}

	@Override
	public void lmDeleteSuccessAudit(HttpServletRequest hsr, String id, String name) {
		LOG.info(">>>> lmDeleteSuccessAudit with id {}", id);
        String changes = "Live msg with name " + name
                + " deleted successfully";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr,
                                changes, "", "LiveMessageTbl",
                                "live Msg delete", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info(">>>> lmDeleteSuccessAudit");
		
	}

	@Override
	public void lmUpdateSuccessAudit(HttpServletRequest hsr, String id, String name) {
		LOG.info(">>>> lmUpdateSuccessAudit");
        String changes = "Live Msg with name " + name
                + " got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "LiveMessageTbl", "live Msg update", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< lmUpdateSuccessAudit");
		
	}

	@Override
	public void lmUpdateFailureAudit(HttpServletRequest hsr, LiveMessageCreateRequest lmcr, String errMsg) {
		LOG.info(">>>> lmUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        "LiveMsg", "live Msg update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info(">>>> lmUpdateFailureAudit");
		
	}

	@Override
	public void lmStatusUpdateSuccessAudit(HttpServletRequest hsr, String id, String liveMsgName) {
		LOG.info(">>>> lmStatusUpdateSuccessAudit");
        String changes = "Live Msg status with live msg name " + liveMsgName
                + " got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                                "LiveMessageStatusTbl", "live Msg status update", "success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< lmStatusUpdateSuccessAudit");
		
	}

	@Override
	public void lmStatusUpdateFailureAudit(HttpServletRequest hsr,
			LiveMessageStatusRequest liveMessageStatusRequest, String message) {
		LOG.info(">>>> lmUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", message,
                        "LiveMsg Status", "live Msg status update", "failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info(">>>> lmUpdateFailureAudit");
		
	}


}
