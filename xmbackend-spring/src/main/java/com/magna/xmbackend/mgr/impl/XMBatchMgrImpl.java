/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.sql.SQLSyntaxErrorException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.mgr.XMBatchMgr;
import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class XMBatchMgrImpl implements XMBatchMgr{

	private static final Logger LOG
    = LoggerFactory.getLogger(XMBatchMgrImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public XmbatchResponse findResultSet(String query) throws SQLSyntaxErrorException {
		LOG.info(">> findResultSet");
		List<Map<String, Object>> resultSet = null;
		try {
			resultSet = this.jdbcTemplate.queryForList(query);
		} catch (Exception e) {
			if(e instanceof SQLSyntaxErrorException || e instanceof BadSqlGrammarException){
				throw new SQLSyntaxErrorException("SQLSyntaxErrorException");
			}
			e.printStackTrace();
		}
		XmbatchResponse xmbatchResponse = new XmbatchResponse(resultSet);
		LOG.info("<< findResultSet");
		return xmbatchResponse;
	}
	
	
	
}
