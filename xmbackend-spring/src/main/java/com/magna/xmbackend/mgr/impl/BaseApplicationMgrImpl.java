package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.audit.mgr.BaseAppAuditMgr;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.CannotDeleteRelationException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.BaseApplicationJpaDao;
import com.magna.xmbackend.mgr.BaseApplicationMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
@Component
public class BaseApplicationMgrImpl implements BaseApplicationMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(BaseApplicationMgrImpl.class);
    @Autowired
    private BaseApplicationJpaDao baseApplicationJpaDao;

    @Autowired
    private Validator validator;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    BaseAppAuditMgr baseAppAuditMgr;

    /**
     *
     * @param validationRequest
     * @return BaseApplicationResponse
     */
    @Override
    public BaseApplicationResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<BaseApplicationsTbl> baseApplicationsTbls
                = baseApplicationJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        baseApplicationsTbls = validator.filterBaseApplicationResponse(isViewInactive, baseApplicationsTbls);
        final BaseApplicationResponse bar
                = new BaseApplicationResponse(baseApplicationsTbls);
        LOG.info("<< findAll");
        return bar;
    }

    /**
     *
     * @param baseApplicationRequest
     * @return
     */
    @Override
    public BaseApplicationsTbl create(
            final BaseApplicationRequest baseApplicationRequest) {
        LOG.info(">> create");
        final BaseApplicationsTbl baseApplicationsTbl = this.convert2Entity(baseApplicationRequest, false);
        final BaseApplicationsTbl bat
                = baseApplicationJpaDao.save(baseApplicationsTbl);
        LOG.info("<< create");
        return bat;
    }

    /**
     *
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    private BaseApplicationsTbl convert2Entity(final BaseApplicationRequest baseApplicationRequest, boolean isUpdate) {
        String baseApplicationId = UUID.randomUUID().toString();
        if (isUpdate) {
            baseApplicationId = baseApplicationRequest.getId();
        }
        final String name = baseApplicationRequest.getName();
        final String iconId = baseApplicationRequest.getIconId();
        final String platform = baseApplicationRequest.getPlatforms();
        final String program = baseApplicationRequest.getProgram();
        final String status = baseApplicationRequest.getStatus();
        final Date date = new Date();
        final List<BaseApplicationTransulation> translations = baseApplicationRequest.getBaseApplicationTransulations();

        if (!isUpdate && null != this.findBaseAppIdForName(name)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getBaseAppNameWithi18nCode(translations, name);
            throw new CannotCreateObjectException(
                    "Base application name already found", "ERR0002", paramMap);
        }

        final BaseApplicationsTbl baseApplicationsTbl = new BaseApplicationsTbl(baseApplicationId);
        baseApplicationsTbl.setName(name);
        baseApplicationsTbl.setIconId(new IconsTbl(iconId));
        baseApplicationsTbl.setPlatforms(platform);
        baseApplicationsTbl.setProgram(program);
        baseApplicationsTbl.setStatus(status);
        baseApplicationsTbl.setCreateDate(date);
        baseApplicationsTbl.setUpdateDate(date);

        final List<BaseAppTranslationTbl> batts = new ArrayList<>();

        for (final BaseApplicationTransulation transulation : translations) {
            final String description = transulation.getDescription();
            final String languageCode = transulation.getLanguageCode();
            final String remarks = transulation.getRemarks();
            final String id = UUID.randomUUID().toString();
            final BaseAppTranslationTbl baseAppTranslationTbl = new BaseAppTranslationTbl(id);
            baseAppTranslationTbl.setDescription(description);
            baseAppTranslationTbl.setLanguageCode(new LanguagesTbl(languageCode));
            baseAppTranslationTbl.setRemarks(remarks);
            baseAppTranslationTbl.setUpdateDate(date);
            baseAppTranslationTbl.setCreateDate(date);
            baseAppTranslationTbl.setBaseApplicationId(new BaseApplicationsTbl(baseApplicationId));
            batts.add(baseAppTranslationTbl);
        }
        baseApplicationsTbl.setBaseAppTranslationTblCollection(batts);
        return baseApplicationsTbl;
    }

    /**
     * Find base app id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findBaseAppIdForName(final String name) {
        final BaseApplicationsTbl baseApplicationsTbl = this.baseApplicationJpaDao.findByNameIgnoreCase(name);
        String baseAppId = null;
        if (baseApplicationsTbl != null) {
            baseAppId = baseApplicationsTbl.getBaseApplicationId();
        }
        LOG.info(">> findBaseAppIdForName - Base App id for name is {} - {}", name, baseAppId);
        return baseAppId;
    }

    @Override
    public final BaseApplicationsTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final BaseApplicationsTbl baseApplicationsTbl = this.baseApplicationJpaDao.findByNameIgnoreCase(name);
        if (null == baseApplicationsTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Base application with name not found", "BA_ERR0002", param);
        }
        LOG.info("<< findByName");
        return baseApplicationsTbl;
    }

    /**
     *
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    @Override
    public BaseApplicationsTbl update(
            final BaseApplicationRequest baseApplicationRequest) {
        LOG.info(">> update");
        final BaseApplicationsTbl batIn = this.convert2Entity(baseApplicationRequest, true);
        final BaseApplicationsTbl batOut = baseApplicationJpaDao.save(batIn);
        LOG.info("<< update");
        return batOut;
    }

    /**
     *
     * @param id
     * @return BaseApplicationsTbl
     */
    @Override
    public BaseApplicationsTbl findById(final String id) {
        LOG.info(">> findById");
        //TODO: need to check if its returning data or coming null in case no records match
        final BaseApplicationsTbl baseApplicationsTbl
                = baseApplicationJpaDao.findOne(id);
        LOG.info("<< findById");
        return baseApplicationsTbl;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String id) {
        LOG.info(">> deleteById {}", id);
        final BaseApplicationsTbl baseApplicationsTbl = this.baseApplicationJpaDao.findOne(id);
        if (this.checkIfRelationExist(baseApplicationsTbl)) {
            final String[] param = {id};
            throw new CannotDeleteRelationException("Base App cannot be deleted, Reason already in use", "BA_ERR0001", param);
        }
        boolean isDeleted = false;
        try {
            baseApplicationJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("BaseApp not found", "P_ERR0016", param);
            }
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param baseAppIds
     * @param httpServletRequest
     * @return BaseApplicationResponse
     */
    @Override
    public BaseApplicationResponse multiDelete(final Set<String> baseAppIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        baseAppIds.forEach(baseAppId -> {
            BaseApplicationsTbl baseApplicationsTbl = null;
            try {
                baseApplicationsTbl = this.findById(baseAppId);
                this.deleteById(baseAppId);
                this.baseAppAuditMgr.baseAppDeleteStatusAuditor(baseAppId,
                        httpServletRequest, true, baseApplicationsTbl);
            } catch (XMObjectNotFoundException objectNotFound) {
                final Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.baseAppAuditMgr.baseAppDeleteStatusAuditor(baseAppId,
                        httpServletRequest, false, baseApplicationsTbl);
            }
        });
        BaseApplicationResponse baseAppResponse = new BaseApplicationResponse(statusMaps);
        LOG.info(">> multiDelete");
        return baseAppResponse;
    }

    /**
     *
     * @param baseApplicationsTbl
     * @return boolean
     */
    private boolean checkIfRelationExist(final BaseApplicationsTbl baseApplicationsTbl) {
        boolean isUsed = true;
        if (baseApplicationsTbl.getUserApplicationsTblCollection().isEmpty()
                && baseApplicationsTbl.getProjectApplicationsTblCollection().isEmpty()
                && baseApplicationsTbl.getStartApplicationsTblCollection().isEmpty()) {
            isUsed = false;
        }
        return isUsed;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public boolean updateStatusById(final String status, final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.baseApplicationJpaDao
                .setStatusForBaseApplicationsTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

}
