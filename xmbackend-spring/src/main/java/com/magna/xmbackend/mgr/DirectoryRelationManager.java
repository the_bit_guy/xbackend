package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface DirectoryRelationManager {

    /**
     *
     * @param batchRequest
     * @param userName
     * @return DirectoryRelBatchResponse
     */
    public DirectoryRelBatchResponse createBatch(final DirectoryRelBatchRequest batchRequest,
            final String userName);

    /**
     *
     * @param directoryRelRequest
     * @return DirectoryRefTbl
     */
    public DirectoryRefTbl create(final DirectoryRelRequest directoryRelRequest);

    /**
     *
     * @param dirRefIds
     * @return Boolean
     */
    public Boolean multiDelete(final Set<String> dirRefIds, HttpServletRequest httpServletRequest);

    /**
     *
     * @param dirId
     * @param objectType
     * @param userName
     * @return DirectoryRelResponse
     */
    public List<DirectoryRelResponse> findDirectoryRelByDirIdAndObjectType(final String dirId,
            final String objectType, final String userName);

}
