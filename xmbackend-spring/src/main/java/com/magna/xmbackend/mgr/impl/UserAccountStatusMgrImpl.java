/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.entities.UserAccountStatusTbl;
import com.magna.xmbackend.jpa.dao.UserAccountStatusJpaDao;
import com.magna.xmbackend.mgr.UserAccountStatusMgr;
import com.magna.xmbackend.vo.userAccountStatus.UserAccountStatusResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class UserAccountStatusMgrImpl implements UserAccountStatusMgr {

	private static final Logger LOG = LoggerFactory.getLogger(UserAccountStatusMgrImpl.class);

	@Autowired
	private UserAccountStatusJpaDao userAccountStatusJpaDao;

	@Override
	public UserAccountStatusResponse findAll() {
		LOG.info(">> findAll");
		Iterable<UserAccountStatusTbl> uasts = this.userAccountStatusJpaDao.findAll();
		UserAccountStatusResponse uasr = new UserAccountStatusResponse(uasts);
		LOG.info("<< findAll");
		return uasr;
	}
}
