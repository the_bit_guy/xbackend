/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr.impl;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ldap.CommunicationException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.LikeFilter;
import org.springframework.ldap.filter.OrFilter;
import org.springframework.ldap.query.ContainerCriteria;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.mgr.LdapMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.enums.LdapAttribute;
import com.magna.xmbackend.vo.enums.LdapKey;
import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmbackend.vo.ldap.LdapUsersResponse;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 *
 * @author dhana
 */
@SuppressWarnings("deprecation")
@Component
public class LdapMgrImpl implements LdapMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(LdapMgrImpl.class);

    @Autowired(required = true)
    @Qualifier(value = "ldapTemplate")
    private LdapTemplate ldapTemplate;
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;
    @Autowired
    @Qualifier(value = "contextSource")
    LdapContextSource contextSource;
    @Autowired
    private MessageMaker messageMaker;

    @Override
    public List<String> getAllUserNames() {
        LOG.info(">> getAllUserNames");
        List<String> userList = ldapTemplate.search(
                query().where("objectclass").is("person"),
                new AttributesMapper<String>() {
            @Override
            public String mapFromAttributes(javax.naming.directory.Attributes atrbts)
                    throws javax.naming.NamingException {
                return atrbts.get("cn").get().toString();
            }

        });
        LOG.info("<< getAllUserNames");
        return userList;
    }

    @Override
    public List<String> getAllOUNames() {
        LOG.info(">> << getAllOUNames >> <<");
        return ldapTemplate.search(
                query().where("objectclass").is("organizationalUnit"),
                new AttributesMapper<String>() {
            @Override
            public String mapFromAttributes(javax.naming.directory.Attributes atrbts)
                    throws javax.naming.NamingException {
                return atrbts.get("ou").get().toString();
            }
        });
    }

    @Override
    public List<LdapUser> getAllLdapUsers() {
        LOG.info(">> << getAllLdapUsers >> <<");
        List<LdapUser> userList = null;
        try {
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            userList = new ArrayList<>();
            userList = ldapTemplate.search(DistinguishedName.EMPTY_PATH,
                    "(objectclass=person)", controls,
                    new LdapUserAttributesMapper());
        } catch (Exception e) {
            if (e instanceof CommunicationException) {
                throw new XMObjectNotFoundException("Invalid Credentials", "LDP_ERR001");
            }
        }

        return userList;
    }

    @Override
    public LdapUser getLdapUserDetails(String userName) {
        LOG.info(">> << getLdapUserDetails >> <<");

        LdapUsersResponse ldapUsersResponse = getAllUsersByAnyName(userName);
        List<LdapUser> users;
        if (null != ldapUsersResponse && (users = ldapUsersResponse.getLdapUsers()) != null && users.size() > 0) {
            for (final LdapUser ldapUser : users) {
                if (userName.equals(ldapUser.getsAMAccountName())) {
                    return ldapUser;
                }
            }
        }
        /*List<LdapUser> list = ldapTemplate.search("",
                new LikeFilter(LdapAttribute.LDAP_SAMACCOUNTNAME.toString(), "*" + userName + "*").encode(),
                new LdapUserAttributesMapper());

        List<LdapUser> list
                = ldapTemplate.search(query()
                        .where("sAMAccountName").is(userName),
                        new LdapUserAttributesMapper());
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }*/
        return null;
    }

    @Override
    public String[] isUserValid(UserCredential userCredential) {
        //TODO: LDAP code to validate user credentials should be here
    	boolean isAuthenticated = false;
    	String username = userCredential.getUsername();
    	String password = userCredential.getPassword();
    	String[] ldapResp = new String[2];
    	Long timeTaken = null;
    	Long currentTime = null;
		if((!username.isEmpty() && username != null)   && (!password.isEmpty() && password != null) ){
			final PropertyConfigTbl propertyConfigTbl = propertyConfigJpaDao.findByCategoryAndProperty(ConfigCategory.LDAP.name(), LdapKey.LDAP_USERNAME.name());
	    	if(propertyConfigTbl != null){
	    		//String domain = propertyConfigTbl.getValue().split("@")[1];
	    		try {
	    			//ContainerCriteria query = query().where("userPrincipalName").is(username+"@"+domain);
	    			ContainerCriteria query = query().where("sAMAccountName").is(username);
	    			currentTime = System.currentTimeMillis();
					//ldapTemplate.authenticate(query,password);
					Long afterLoginTime = System.currentTimeMillis();
					timeTaken = afterLoginTime - currentTime;
					LOG.info("timeTaken for login {}", timeTaken);
	    			isAuthenticated = true;
	    			ldapResp[0] = String.valueOf(isAuthenticated);
	    			ldapResp[1] = String.valueOf(timeTaken)+"ms";
	    		} catch (Exception e) {
	    			Long afterLoginTime = System.currentTimeMillis();
	    			timeTaken = afterLoginTime - currentTime;
	    			LOG.error(">>>>>>>>>>>> LOGIN EXCEPTION <<<<<<<<<<<");
	    			if (e instanceof EmptyResultDataAccessException) {
	    				LOG.error(">>>>>>>>>>>> LOGIN FAILED <<<<<<<<<<<");
	    				isAuthenticated = false;
	    				
	    			}
	    		}
	    		ldapResp[0] = String.valueOf(isAuthenticated);
				ldapResp[1] = String.valueOf(timeTaken)+"ms";
	    	}
		}
    	return ldapResp;
    }

    private class LdapUserAttributesMapper implements AttributesMapper<LdapUser> {

        @Override
        public LdapUser mapFromAttributes(Attributes attributes) throws NamingException {
            LdapUser ldapUser;
            if (attributes == null) {
                return null;
            }
            ldapUser = new LdapUser();
            ldapUser.setCn(attributes.get(LdapAttribute.LDAP_CN.toString()).get().toString());

            /*if (attributes.get("userPassword") != null) {
                String userPassword = null;
                try {
                    userPassword = new String((byte[]) attributes
                            .get("userPassword").get(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    LOG.error("unable to process", e);
                }
                ldapUser.setUserPassword(userPassword);
            }*/
            if (attributes.get(LdapAttribute.LDAP_SAMACCOUNTNAME.toString()) != null) {
                ldapUser.setsAMAccountName(attributes.get(LdapAttribute.LDAP_SAMACCOUNTNAME.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_SN.toString()) != null) {
                ldapUser.setSn(attributes.get(LdapAttribute.LDAP_SN.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_DEPARTMENT.toString()) != null) {
                ldapUser.setDepartment(attributes.get(LdapAttribute.LDAP_DEPARTMENT.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_MAIL.toString()) != null) {
                ldapUser.setEmail(attributes.get(LdapAttribute.LDAP_MAIL.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_USERPRINCIPALNAME.toString()) != null) {
                ldapUser.setUserPrincipalName(attributes.get(LdapAttribute.LDAP_USERPRINCIPALNAME.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_TELEPHONENUMBER.toString()) != null) {
                ldapUser.setTelephoneNumber(attributes.get(LdapAttribute.LDAP_TELEPHONENUMBER.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_GIVENNAME.toString()) != null) {
                ldapUser.setGivenName(attributes.get(LdapAttribute.LDAP_GIVENNAME.toString()).get().toString());
            }
            if (attributes.get(LdapAttribute.LDAP_USERACCOUNTCONTROL.toString()) != null) {
                ldapUser.setUserAccountControl(attributes.get(LdapAttribute.LDAP_USERACCOUNTCONTROL.toString()).get().toString());
            }
            return ldapUser;
        }
    }

    @Override
    public List<String> getAllUserNamesByAnyName(String name) {
        LOG.info(">> << getAllUserNamesByAnyName >> <<");
        OrFilter filter = new OrFilter();
        filter.or(new LikeFilter(LdapAttribute.LDAP_CN.toString(), name + "*"));
        filter.or(new LikeFilter(LdapAttribute.LDAP_SAMACCOUNTNAME.toString(), name + "*"));
        filter.or(new LikeFilter(LdapAttribute.LDAP_USERPRINCIPLENAME.toString(), name + "*"));
        filter.or(new LikeFilter(LdapAttribute.LDAP_GIVENNAME.toString(), name + "*"));
        filter.or(new LikeFilter(LdapAttribute.LDAP_SN.toString(), name + "*"));

        List<String> userList = ldapTemplate.search("", filter.encode(), new AttributesMapper<String>() {
            @Override
            public String mapFromAttributes(javax.naming.directory.Attributes atrbts)
                    throws javax.naming.NamingException {
                return atrbts.get(LdapAttribute.LDAP_CN.toString()).get().toString();
            }
        });
        return userList;
    }

    @Override
    public LdapUsersResponse getAllUsersByAnyName(String name) {
        LOG.info(">> << getAllUsersByAnyName >> <<");
        List<LdapUser> userList = null;
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        try {
            OrFilter filter = new OrFilter();
            filter.or(new LikeFilter(LdapAttribute.LDAP_CN.toString(), name + "*"));
            filter.or(new LikeFilter(LdapAttribute.LDAP_SAMACCOUNTNAME.toString(), name + "*"));
            filter.or(new LikeFilter(LdapAttribute.LDAP_USERPRINCIPLENAME.toString(), name + "*"));
            filter.or(new LikeFilter(LdapAttribute.LDAP_GIVENNAME.toString(), name + "*"));
            filter.or(new LikeFilter(LdapAttribute.LDAP_SN.toString(), name + "*"));
            userList = new ArrayList<>();
            userList = ldapTemplate.search("", filter.encode(), new LdapUserAttributesMapper());
        } catch (Exception e) {
            if (e instanceof CommunicationException) {
                try {
                    throw new XMObjectNotFoundException("Invalid Credentials", "LDP_ERR001");
                } catch (XMObjectNotFoundException objectNotFoundException) {
                    Map<String, String> statusMap = messageMaker.extractFromException(objectNotFoundException);
                    statusMaps.add(statusMap);
                }

            }
        }
        LdapUsersResponse response = new LdapUsersResponse(userList, statusMaps);
        return response;
    }
}
