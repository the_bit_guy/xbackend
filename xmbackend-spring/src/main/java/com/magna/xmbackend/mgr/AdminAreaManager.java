package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
public interface AdminAreaManager {

    /**
     *
     * @param validationRequest
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return AdminAreasTbl
     */
    AdminAreasTbl findById(String id);

    /**
     *
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    AdminAreasTbl save(final AdminAreaRequest adminAreaRequest);

    /**
     *
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    AdminAreasTbl update(final AdminAreaRequest adminAreaRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @return boolean
     */
    public boolean deleteById(final String id);

    /**
     *
     * @param aaIds
     * @param httpServletRequest
     * @return AdminAreaResponse
     */
    AdminAreaResponse multiDelete(final Set<String> aaIds,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByProjectId(final String id);

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByUserAppId(final String id);

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByUserProjectAppId(final String id);

    /**
     *
     * @param id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByStartAppId(final String id);

    /**
     *
     * @param name
     * @return AdminAreasTbl
     */
    AdminAreasTbl findByName(final String name);

    /**
     * Find AA by project id and site id.
     *
     * @param projectId the project id
     * @param siteId the site id
     * @param validationRequest
     * @return the admin area response
     */
    AdminAreaResponse findAAByProjectIdAndSiteId(final String projectId, final String tkt,
            final String siteId, final ValidationRequest validationRequest);

    /**
     * Find AA by start app id and site id.
     *
     * @param startAppId the start app id
     * @param siteId the site id
     * @return the admin area response
     */
    AdminAreaResponse findAAByStartAppIdAndSiteId(final String startAppId,
            final String siteId);

    /**
     * Find AA by user app id and site id.
     *
     * @param userAppId the user app id
     * @param siteId the site id
     * @return the admin area response
     */
    AdminAreaResponse findAAByUserAppIdAndSiteId(final String userAppId,
            final String siteId);

    /**
     * Find AA by project app id and site id.
     *
     * @param projectAppId
     * @param siteId
     * @return
     */
    AdminAreaResponse findAAByProjectAppIdAndSiteId(final String projectAppId,
            final String siteId);
}
