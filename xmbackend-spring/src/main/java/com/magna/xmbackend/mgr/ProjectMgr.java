package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
public interface ProjectMgr {

    /**
     *
     * @param validationRequest
     * @return ProjectResponse
     */
    ProjectResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param projectRequest
     * @return ProjectsTbl
     */
    ProjectsTbl save(final ProjectRequest projectRequest);

    /**
     *
     * @param projectRequest
     * @return ProjectsTbl
     */
    ProjectsTbl update(final ProjectRequest projectRequest);

    /**
     *
     * @param id
     * @return ProjectsTbl
     */
    ProjectsTbl findById(final String id);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);
    
    /**
     * 
     * @param hsr
     * @param projectIds
     * @return ProjectResponse
     */
    ProjectResponse multiDelete(HttpServletRequest hsr, Set<String> projectIds);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean upateStatusById(final String status, final String id);

    /**
     *
     * @param userId
     * @param validationRequest
     * @return ProjectResponse
     */
    ProjectResponse findProjectsByUserId(final String userId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param startAppId
     * @param validationRequest
     * @return ProjectResponse
     */
    ProjectResponse findProjectsByStartAppId(final String startAppId,
            final ValidationRequest validationRequest);

    /**
     * Find all projects by project app site AA id.
     *
     * @param projectAppId the project app id
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @return the project response
     */
    ProjectResponse findAllProjectsByProjectAppSiteAAId(final String projectAppId,
            final String siteId, final String adminAreaId);

    /**
     *
     * @param aaId
     * @param validationRequest 
     * @return ProjectResponse
     */
    ProjectResponse findAllProjectsByAAId(final String aaId, final ValidationRequest validationRequest);

    /**
     *
     * @param aaId
     * @param validationRequest
     * @return ProjectResponse
     */
    ProjectResponse findAllProjectsByAAIdAndActive(final String aaId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param siteId
     * @param userName
     * @param validationRequest
     * @return ProjectResponse
     */
    ProjectResponse findUserProjectsBySiteIdAndUsername(final String siteId, final String tkt,
            final String userName, final ValidationRequest validationRequest);

	/**
	 * Find by name.
	 *
	 * @param name the name
	 * @return the projects tbl
	 */
	ProjectsTbl findByName(String name);
}
