package com.magna.xmbackend.mgr;

import com.magna.xmbackend.validation.vo.ValidationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 *
 * @author vijay
 */
public interface ValidationMgr {

    /**
     *
     * @param validationRequest
     * @return boolean
     */
    boolean isObjectAccessAllowed(ValidationRequest validationRequest);

    /**
     * clear the cache in permission filter
     *
     * @return boolean
     */
    boolean clearCache();
    
    
    //ValidationResponse checkHotlinePermissions(final String username);
    
    
    ValidationResponse checkHotlinePermissions(final String username, final String adminAreaId);

}
