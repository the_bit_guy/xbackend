package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.ProjectAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.DirectoryRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserUserAppRelJpaDao;
import com.magna.xmbackend.mgr.AdminAreaManager;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.SiteMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.ApplicationPosition;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationMenuResponse;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation;

@Component
public class ProjectApplicationMgrImpl implements ProjectApplicationMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectApplicationMgrImpl.class);
    @Autowired
    private ProjectApplicationJpaDao projectApplicationJpaDao;

    @Autowired
    private ProjectMgr projectMgr;

    @Autowired
    private SiteMgr siteMgr;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private AdminAreaManager adminAreaManager;

    @Autowired
    private DirectoryRelJpaDao directoryRelJpaDao;

    @Autowired
    private UserProjectRelJpaDao userProjectRelJpaDao;

    @Autowired
    private UserProjectAppRelJpaDao userProjectAppRelJpaDao;

    @Autowired
    private AdminAreaProjectAppRelJpaDao adminAreaProjectAppRelJpaDao;

    @Autowired
    private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private UserUserAppRelJpaDao userUserAppRelJpaDao;

    @Autowired
    private Validator validator;
    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    UserTktJpaDao userTktJpaDao;

    @Autowired
    ProjectAppAuditMgr projectAppAuditMgr;

    /**
     *
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public final ProjectApplicationResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<ProjectApplicationsTbl> projectApplicationsTbls
                = projectApplicationJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        projectApplicationsTbls = validator.filterProjectApplicationResponse(isViewInactive, projectApplicationsTbls);
        final ProjectApplicationResponse projectApplicationResponse
                = new ProjectApplicationResponse(projectApplicationsTbls);
        LOG.info("<< findAll");
        return projectApplicationResponse;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectApplicationsTbl
     */
    @Override
    public final ProjectApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest) {
        LOG.info(">> findById");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findById isViewInactive={}", isViewInactive);
        ProjectApplicationsTbl pat = projectApplicationJpaDao.findOne(id);
        if (null != pat) {
            pat = validator.filterProjectApplicationResponse(isViewInactive, pat);
        }
        LOG.info("<< findById");
        return pat;
    }

    /**
     *
     * @param id
     * @return ProjectApplicationsTbl
     */
    @Override
    public final ProjectApplicationsTbl findById(final String id) {
        LOG.info(">> findById");
        final ProjectApplicationsTbl pat = projectApplicationJpaDao.findOne(id);
        LOG.info("<< findById");
        return pat;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.removeDirectoryRelations(id);
            projectApplicationJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0014", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param projAppIds
     * @param httpServletRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse multiDelete(final Set<String> projAppIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        projAppIds.forEach(projAppId -> {
            ProjectApplicationsTbl pat = null;
            try {
                pat = findById(projAppId);
                this.deleteById(projAppId);
                this.projectAppAuditMgr.projectAppDeleteStatusAuditor(projAppId, httpServletRequest, true, pat);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.projectAppAuditMgr.projectAppDeleteStatusAuditor(projAppId, httpServletRequest, false, pat);
            }
        });
        final ProjectApplicationResponse userResponse = new ProjectApplicationResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userResponse;
    }

    /**
     *
     * @param id
     */
    private void removeDirectoryRelations(final String id) {
        List<DirectoryRefTbl> directoryRefTbls = this.directoryRelJpaDao.findByObjectId(id);
        if (!directoryRefTbls.isEmpty()) {
            this.directoryRelJpaDao.delete(directoryRefTbls);
        }
    }

    /**
     *
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    @Override
    public final ProjectApplicationsTbl create(
            ProjectApplicationRequest projectApplicationRequest) {
        LOG.info(">> create");
        //this.checkIfObjectAlreadyExist(projectApplicationRequest);
        final ProjectApplicationsTbl projectApplicationsTbl = this.convert2Entity(projectApplicationRequest, false);
        final ProjectApplicationsTbl pat = projectApplicationJpaDao.save(projectApplicationsTbl);
        LOG.info("<< create");
        return pat;
    }

    /*public void checkIfObjectAlreadyExist(ProjectApplicationRequest projectApplicationRequest) {
        List<ProjectApplicationTransulation> projectApplicationTransulations = projectApplicationRequest.getProjectApplicationTransulations();
        if (!projectApplicationTransulations.isEmpty()) {
            for (ProjectApplicationTransulation projectApplicationTransulation : projectApplicationTransulations) {
                String projectName = projectApplicationTransulation.getName();
                if (projectName != null) {
                    String languageCode = projectApplicationTransulation.getLanguageCode();
                    ProjectAppTranslationTbl projectAppTranslationTbl = this.projectAppTranslationJpaDao.findByNameIgnoreCaseAndLanguageCode(projectApplicationTransulation.getName(), new LanguagesTbl(languageCode));
                    if (projectAppTranslationTbl != null) {
                        Map<String, String[]> paramMap = getUserAppWithi18nCode(projectAppTranslationTbl);
                        throw new CannotCreateObjectException("Record Already Exist", "UP_ERR00012", paramMap);
                    }
                }
            }
        }
    }*/
    /**
     *
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    @Override
    public final ProjectApplicationsTbl update(
            ProjectApplicationRequest projectApplicationRequest) {
        LOG.info(">> update");
        final ProjectApplicationsTbl projectApplicationsTbl
                = this.convert2Entity(projectApplicationRequest, true);
        final ProjectApplicationsTbl pat
                = projectApplicationJpaDao.save(projectApplicationsTbl);
        LOG.info("<< update");
        return pat;
    }

    /**
     *
     * @param projectApplicationRequest
     * @param isUpdate
     * @return ProjectApplicationsTbl
     */
    private ProjectApplicationsTbl convert2Entity(
            final ProjectApplicationRequest projectApplicationRequest,
            final boolean isUpdate) {
        final String baseAppId = projectApplicationRequest.getBaseAppId();
        final String iconId = projectApplicationRequest.getIconId();
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = projectApplicationRequest.getId();
        }
        final Date date = new Date();
        final String name = projectApplicationRequest.getName();
        final String description = projectApplicationRequest.getDescription();
        final String isParent = projectApplicationRequest.getIsParent();
        final String position = projectApplicationRequest.getPosition();
        final String status = projectApplicationRequest.getStatus();
        final String isSingleton = projectApplicationRequest.getIsSingleton();
        final List<ProjectApplicationTransulation> translations = projectApplicationRequest.getProjectApplicationTransulations();

        if (!isUpdate && null != this.findProjectAppIdForName(name)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getProjectAppNameWithi18nCode(translations, name);
            throw new CannotCreateObjectException(
                    "Project application name already found", "ERR0002", paramMap);
        }

        final ProjectApplicationsTbl projectApplicationsTbl = new ProjectApplicationsTbl(id);
        projectApplicationsTbl.setName(name);
        projectApplicationsTbl.setDescription(description);
        projectApplicationsTbl.setIconId(new IconsTbl(iconId));
        projectApplicationsTbl.setIsParent(isParent);
        projectApplicationsTbl.setPosition(position);
        projectApplicationsTbl.setStatus(status);
        projectApplicationsTbl.setIsSingleton(isSingleton);
        projectApplicationsTbl.setCreateDate(date);
        projectApplicationsTbl.setUpdateDate(date);

        BaseApplicationsTbl bat2Set = null;
        if (baseAppId != null) {
            bat2Set = new BaseApplicationsTbl(baseAppId);
        }
        projectApplicationsTbl.setBaseApplicationId(bat2Set);

        List<ProjectAppTranslationTbl> patts = new ArrayList<>();

        for (ProjectApplicationTransulation transulation : translations) {
            String paTransId = UUID.randomUUID().toString();
            final String displayDescription = transulation.getDescription();
            final String displayName = transulation.getName();
            final String remarks = transulation.getRemarks();
            final String languageCode = transulation.getLanguageCode();
            if (isUpdate) {
                paTransId = transulation.getId();
            }

            ProjectAppTranslationTbl patt = new ProjectAppTranslationTbl(paTransId);
            patt.setDescription(displayDescription);
            patt.setLanguageCode(new LanguagesTbl(languageCode));
            patt.setName(displayName);
            patt.setProjectApplicationId(new ProjectApplicationsTbl(id));
            patt.setRemarks(remarks);
            patt.setCreateDate(date);
            patt.setUpdateDate(date);

            patts.add(patt);
        }
        projectApplicationsTbl.setProjectAppTranslationTblCollection(patts);

        return projectApplicationsTbl;
    }

    /**
     * Find project app id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findProjectAppIdForName(final String name) {
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findByNameIgnoreCase(name);
        String projectApplicationsId = null;
        if (projectApplicationsTbl != null) {
            projectApplicationsId = projectApplicationsTbl.getProjectApplicationId();
        }
        LOG.info(">> findProjectAppIdForName - User applcaition id for name is {} - {}", name, projectApplicationsId);
        return projectApplicationsId;
    }

    /**
     * Find by name.
     *
     * @param name the name
     * @return the project applications tbl
     */
    @Override
    public final ProjectApplicationsTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final ProjectApplicationsTbl projectAppTbl = this.projectApplicationJpaDao.findByNameIgnoreCase(name);
        if (null == projectAppTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Project application with name not found", "U_ERR0005", param);
        }
        LOG.info("<< findByName");
        return projectAppTbl;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public boolean updateStatusById(final String status, final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.projectApplicationJpaDao
                .setStatusForProjectApplicationsTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @param type
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public final ProjectApplicationResponse findProjectApplicationsByProjectId(final String id,
            final String type, final ValidationRequest validationRequest) {
        ProjectApplicationResponse projectApplicationResponse = null;
        LOG.info(">> findProjectApplicationsByProjectId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectApplicationsByProjectId isViewInactive={}", isViewInactive);
        final ProjectsTbl projectsTbl = validator.validateProject(id, isViewInactive);
        Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
        adminAreaProjectRelTblCollection = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTblCollection);
        List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRelTbls = new ArrayList<>();
        for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
            SiteAdminAreaRelTbl siteAdminAreaRelId = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
            SiteAdminAreaRelTbl filterdSiteAdminAreaRel = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelId);
            if (null != filterdSiteAdminAreaRel) {
                AdminAreasTbl filterdAdminAreaResponse = validator.filterAdminAreaResponse(isViewInactive, filterdSiteAdminAreaRel.getAdminAreaId());
                if (null != filterdAdminAreaResponse) {
                    SitesTbl filterdSiteResponse = validator.filterSiteResponse(isViewInactive, filterdSiteAdminAreaRel.getSiteId());
                    if (null != filterdSiteResponse) {
                        filteredAdminAreaProjectRelTbls.add(adminAreaProjectRelTbl);
                    }
                }
            }
        }
        if (!filteredAdminAreaProjectRelTbls.isEmpty()) {
            Set<ProjectApplicationsTbl> projectApplicationsTbls = new HashSet<>();
            for (final AdminAreaProjectRelTbl adminAreaProjectRelTbl : filteredAdminAreaProjectRelTbls) {
                Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection = adminAreaProjectRelTbl.getAdminAreaProjAppRelTblCollection();
                adminAreaProjAppRelTblCollection = validator.filterAdminAreaProjAppRel(isViewInactive, adminAreaProjAppRelTblCollection);
                productAppTableGetter(adminAreaProjAppRelTblCollection, type, projectApplicationsTbls, isViewInactive);
            }
            if (projectApplicationsTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Admin Area Project relationship found with the Admin Area Project App", "S_ERR0014");
            }
            projectApplicationResponse = new ProjectApplicationResponse(projectApplicationsTbls);
        } else {
            throw new XMObjectNotFoundException("No Admin Area Project Relation found", "S_ERR0006");
        }
        LOG.info("<< findProjectApplicationsByProjectId");
        return projectApplicationResponse;
    }

    /**
     *
     * @param adminAreaProjAppRelTblCollection
     * @param type
     * @param projectApplicationsTbls
     * @param isViewInactive
     */
    private void productAppTableGetter(final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection,
            final String type,
            final Collection<ProjectApplicationsTbl> projectApplicationsTbls,
            final boolean isViewInactive) {
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTblCollection) {
            ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
            final String relType = adminAreaProjAppRelTbl.getRelType();
            if (type == null) {
                projectApplicationsTbl = validator.filterProjectApplicationResponse(isViewInactive, projectApplicationsTbl);
                if (null != projectApplicationsTbl
                        && null != projectApplicationsTbl.getProjectApplicationId()) {
                    projectApplicationsTbls.add(projectApplicationsTbl);
                }
            } else {
                if (type.equalsIgnoreCase(relType)) {
                    projectApplicationsTbls.add(projectApplicationsTbl);
                }
            }
        }
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public final ProjectApplicationResponse findProjectApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest) {
        ProjectApplicationResponse projectApplicationResponse = null;
        LOG.info(">> findProjectApplicationsByBaseAppId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectApplicationsByBaseAppId isViewInactive={}", isViewInactive);
        final BaseApplicationsTbl baseApplicationsTbl = validator.validateBaseApp(id, isViewInactive);
        Collection<ProjectApplicationsTbl> projectApplicationsTbls = baseApplicationsTbl.getProjectApplicationsTblCollection();
        projectApplicationsTbls = validator.filterProjectApplicationResponse(isViewInactive, projectApplicationsTbls);
        if (!projectApplicationsTbls.isEmpty()) {
            projectApplicationResponse = new ProjectApplicationResponse(projectApplicationsTbls);
        } else {
            throw new XMObjectNotFoundException("No Base App Project App Relation found", "BA_ERR0003");
        }
        LOG.info("<< findProjectApplicationsByBaseAppId");
        return projectApplicationResponse;
    }

    /**
     *
     * @param position
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjectAppByPosition(final String position,
            final ValidationRequest validationRequest) {
        LOG.info(">> findProjectAppByPosition {}", position);
        Iterable<ProjectApplicationsTbl> pats = this.projectApplicationJpaDao.findByPosition(position);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectAppByPosition isViewInactive={}", isViewInactive);
        pats = validator.filterProjectApplicationResponse(isViewInactive, pats);
        final ProjectApplicationResponse par = new ProjectApplicationResponse(pats);
        LOG.info("<< findProjectAppByPosition ");
        return par;
    }

    /**
     *
     * @param status
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findAllProjectApplicationsByStatus(final String status) {
        LOG.info(">> findAllProjectApplicationsByStatus {}", status);
        Iterable<ProjectApplicationsTbl> pats = this.projectApplicationJpaDao.findByStatus(status);
        ProjectApplicationResponse par = new ProjectApplicationResponse(pats);
        LOG.info("<< findAllProjectApplicationsByStatus ");
        return par;
    }

    /**
     *
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findAllProjectApplicationsByPositions(final ValidationRequest validationRequest) {
        Iterable<ProjectApplicationsTbl> projectAppIterables = this.projectApplicationJpaDao.findByPositionPattern(ApplicationPosition.ICONTASK.toString(),
                ApplicationPosition.BUTTONTASK.toString(), ApplicationPosition.MENUTASK.toString());
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectApplicationsByPositions isViewInactive={}", isViewInactive);
        projectAppIterables = validator.filterProjectApplicationResponse(isViewInactive, projectAppIterables);
        final ProjectApplicationResponse projAppResp = new ProjectApplicationResponse(projectAppIterables);
        return projAppResp;
    }

    /**
     *
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @Override
    public final ProjectApplicationResponse findProjAppByProjectSiteAAId(final String siteId,
            final String adminAreaId, final String projectId) {
        LOG.info(">> findProjAppByProjectSiteAAId");
        final SitesTbl sitesTbl = siteMgr.findById(siteId);
        if (null == sitesTbl) {
            throw new RuntimeException("Site not found");
        }
        final AdminAreasTbl adminAreasTbl = adminAreaManager.findById(adminAreaId);
        if (null == adminAreasTbl) {
            throw new RuntimeException("Admin Area not found");
        }
        final ProjectsTbl projectsTbl = projectMgr.findById(projectId);
        if (null == projectsTbl) {
            throw new RuntimeException("Project not found");
        }
        final Iterable<ProjectApplicationsTbl> projectApplicationsTbls
                = projectApplicationJpaDao.findPATblBySiteAAProjId(sitesTbl, adminAreasTbl, projectsTbl);
        final ProjectApplicationResponse projectApplicationResponse
                = new ProjectApplicationResponse(projectApplicationsTbls);
        LOG.info("<< findProjAppByProjectSiteAAId");
        return projectApplicationResponse;
    }

    /**
     *
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @param relType
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjAppByProjectSiteAAIdAndRelType(String siteId, String adminAreaId,
            String projectId, String relType) {
        LOG.info(">> findProjAppByProjectSiteAAIdAndRelType");
        if (null == siteId) {
            throw new RuntimeException("Site Id not found");
        }
        if (null == adminAreaId) {
            throw new RuntimeException("Admin Area Id not found");
        }
        if (null == projectId) {
            throw new RuntimeException("Project Id not found");
        }
        if (null == relType) {
            throw new RuntimeException("Relation Type not found");
        }
        final Iterable<ProjectApplicationsTbl> projectApplicationsTbls
                = projectApplicationJpaDao.findPATblBySiteAAProjIdAndRelType(siteId, adminAreaId, projectId, relType);
        final ProjectApplicationResponse projectApplicationResponse
                = new ProjectApplicationResponse(projectApplicationsTbls);
        LOG.info("<< findProjAppByProjectSiteAAIdAndRelType");
        return projectApplicationResponse;
    }

    /**
     *
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjAppByUserProjectId(String userId, String projectId) {
        LOG.info("<< findProjAppByUserProjectId");
        final Iterable<ProjectApplicationsTbl> projectApplicationsTbls = projectApplicationJpaDao
                .findPATblByUserProjectId(userId, projectId);
        final ProjectApplicationResponse projectApplicationResponse = new ProjectApplicationResponse(
                projectApplicationsTbls);
        LOG.info("<< findProjAppByUserProjectId");
        return projectApplicationResponse;
    }

    /**
     *
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findFixedProjAppByUserProjectId(String userId, String projectId) {
        List<UserProjectRelTbl> userProjectRelTbls = this.userProjectRelJpaDao.findByUserIdAndProjectId(new UsersTbl(userId), new ProjectsTbl(projectId));
        List<ProjectApplicationsTbl> projectApplicationsTbls = new ArrayList<>();
        if (!userProjectRelTbls.isEmpty()) {
            List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findByUserProjectRelIdIn(userProjectRelTbls);
            if (!userProjAppRelTbls.isEmpty()) {
                for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
                    projectApplicationsTbls.add(userProjAppRelTbl.getProjectApplicationId());
                }
            }
        }
        List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findByProjectApplicationIdInAndRelType(projectApplicationsTbls, "FIXED");
        List<ProjectApplicationsTbl> projAppTbls = new ArrayList<>();
        if (!adminAreaProjAppRelTbls.isEmpty()) {
            for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
                projAppTbls.add(adminAreaProjAppRelTbl.getProjectApplicationId());
            }
        }
        ProjectApplicationResponse projectApplicationResponse = new ProjectApplicationResponse(projAppTbls);
        return projectApplicationResponse;
    }

    /**
     *
     * @param aaId
     * @param projectId
     * @param userName
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjectAppByAAIdProjectIdUserName(String aaId, String projectId,
            String userName) {
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(aaId));
        List<ProjectApplicationsTbl> applicationsTbls = null;
        if (!siteAdminAreaRelTbls.isEmpty()) {
            List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls
                    = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdInAndProjectId(
                            siteAdminAreaRelTbls, new ProjectsTbl(projectId));
            if (!adminAreaProjectRelTbls.isEmpty()) {
                List<AdminAreaProjAppRelTbl> aaparTbls = this.adminAreaProjectAppRelJpaDao.
                        findByAdminAreaProjectRelIdIn(adminAreaProjectRelTbls);

                applicationsTbls = new ArrayList<>();
                for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : aaparTbls) {
                    applicationsTbls.add(adminAreaProjAppRelTbl.getProjectApplicationId());
                }
            }
        }
        if (applicationsTbls == null) {
            throw new XMObjectNotFoundException("No Project App found", "P_ERR0003");
        }
        ProjectApplicationResponse response = new ProjectApplicationResponse(applicationsTbls);
        return response;
    }

    /**
     *
     * @param aaId
     * @param projectId
     * @param userId
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(String aaId, String projectId,
            String userId) {
        LOG.info(">> findProjectAppByAAIdProjectIdUserId");

        List<UserUserAppRelTbl> userUserAppRelTbls = this.userUserAppRelJpaDao.findByUserId(new UsersTbl(userId));
        List<SiteAdminAreaRelTbl> siteAARelTblsFromUUAppRelTbl = null;
        List<ProjectApplicationsTbl> projectApplicationsTbls = null;
        if (!userUserAppRelTbls.isEmpty()) {
            siteAARelTblsFromUUAppRelTbl = new ArrayList<>();
            for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                siteAARelTblsFromUUAppRelTbl.add(userUserAppRelTbl.getSiteAdminAreaRelId());
            }
            List<String> siteAdminAreaRelIds = null;
            if (!siteAARelTblsFromUUAppRelTbl.isEmpty()) {
                siteAdminAreaRelIds = new ArrayList<>();
                for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAARelTblsFromUUAppRelTbl) {
                    siteAdminAreaRelIds.add(siteAdminAreaRelTbl.getSiteAdminAreaRelId());
                }
                List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findBySiteAdminAreaRelIdInAndAdminAreaId(
                        siteAdminAreaRelIds, new AdminAreasTbl(aaId));
                List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdInAndProjectId(
                        siteAdminAreaRelTbls, new ProjectsTbl(projectId));

                List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjectRelIdIn(adminAreaProjectRelTbls);

                if (!adminAreaProjAppRelTbls.isEmpty()) {
                    projectApplicationsTbls = new ArrayList<>();
                    for (AdminAreaProjAppRelTbl aapart : adminAreaProjAppRelTbls) {
                        projectApplicationsTbls.add(aapart.getProjectApplicationId());
                    }
                }
            }
        }
        if (projectApplicationsTbls == null) {
            throw new XMObjectNotFoundException("No Project App found", "P_ERR0003");
        }
        ProjectApplicationResponse response = new ProjectApplicationResponse(projectApplicationsTbls);
        LOG.info("<< findProjectAppByAAIdProjectIdUserId");
        return response;
    }

    /**
     *
     * @param aaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @Override
    public ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(String aaId, String projectId) {
        LOG.info(">> findProjectAppByAAIdProjectIdUserId");
        List<ProjectApplicationsTbl> projectApplicationsTbls = null;
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(aaId));
        if (!siteAdminAreaRelTbls.isEmpty()) {
            List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdInAndProjectId(
                    siteAdminAreaRelTbls, new ProjectsTbl(projectId));
            if (!adminAreaProjectRelTbls.isEmpty()) {
                List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjectRelIdIn(adminAreaProjectRelTbls);
                if (!adminAreaProjAppRelTbls.isEmpty()) {
                    projectApplicationsTbls = new ArrayList<>();
                    for (AdminAreaProjAppRelTbl aapart : adminAreaProjAppRelTbls) {
                        projectApplicationsTbls.add(aapart.getProjectApplicationId());
                    }
                }
            }
        }
        if (projectApplicationsTbls == null) {
            throw new XMObjectNotFoundException("No Project App found", "P_ERR0003");
        }
        ProjectApplicationResponse response = new ProjectApplicationResponse(projectApplicationsTbls);
        LOG.info("<< findProjectAppByAAIdProjectIdUserId");
        return response;
    }

    /*@Override
    public ProjectApplciationMenuWrapper findProjectApplicationsByAAIdAndProjIdAndUserName(String aaId, String tkt, 
            String projectId, String userName, final ValidationRequest validationRequest) {
        final List<ProjectApplicationMenuResponse> projectApplicationMenuResponses = new ArrayList<>();
        List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findAdminAreaProjAppRelTbls(new AdminAreasTbl(aaId),
                new ProjectsTbl(projectId), Status.ACTIVE.name(), Status.ACTIVE.name());

        List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findUserProjAppRelTbls(new AdminAreasTbl(aaId), new ProjectsTbl(projectId),
                userName, new ProjectsTbl(projectId), Status.ACTIVE.name());

        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectApplicationsByAAIdAndProjIdAndUserName isViewInactive={}", isViewInactive);
        for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
            ProjectApplicationsTbl reqProjectApplicationsTbl = null;
            String siteAdminAreaRelId = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId().getSiteAdminAreaRelId();
            ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
            projectApplicationsTbl = validator.filterProjectApplicationResponse(isViewInactive, projectApplicationsTbl);
            if (null != projectApplicationsTbl && null != projectApplicationsTbl.getProjectApplicationId()) {
                String projectApplicationId = projectApplicationsTbl.getProjectApplicationId();
                if (ApplicationRelationType.FIXED.name().equals(adminAreaProjAppRelTbl.getRelType())) {
                    boolean isAllowed = true;
                    for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
                        if (siteAdminAreaRelId
                                .equals(userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
                                        .getSiteAdminAreaRelId())
                                && projectApplicationId
                                        .equals(userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())
                                && UserRelationType.NOTALLOWED.name().equals(userProjAppRelTbl.getUserRelType())) {
                            isAllowed = false;
                            break;
                        }
                    }
                    if (isAllowed) {
                        reqProjectApplicationsTbl = projectApplicationsTbl;
                    }
                } else if (ApplicationRelationType.NOTFIXED.name().equals(adminAreaProjAppRelTbl.getRelType())
                        || ApplicationRelationType.PROTECTED.name().equals(adminAreaProjAppRelTbl.getRelType())) {
                    for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
                        if (siteAdminAreaRelId
                                .equals(userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
                                        .getSiteAdminAreaRelId())
                                && projectApplicationId
                                        .equals(userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())
                                && UserRelationType.ALLOWED.name().equals(userProjAppRelTbl.getUserRelType())) {
                            reqProjectApplicationsTbl = projectApplicationsTbl;
                            break;
                        }
                    }
                }
            }
            if (reqProjectApplicationsTbl != null) {
                ProjectApplicationMenuResponse projectApplicationMenuResponse = new ProjectApplicationMenuResponse();
                projectApplicationMenuResponse.setProjectApplicationsTbl(reqProjectApplicationsTbl);
                boolean isParent = Boolean.valueOf(reqProjectApplicationsTbl.getIsParent());
                if (isParent) {
                    Iterable<ProjectApplicationsTbl> projectAppChildren = this.projectApplicationJpaDao.findByPosition(reqProjectApplicationsTbl.getProjectApplicationId());
                    projectApplicationMenuResponse.setProjectAppChildren(projectAppChildren);
                }
                projectApplicationMenuResponses.add(projectApplicationMenuResponse);
            }
        }
        return new ProjectApplciationMenuWrapper(projectApplicationMenuResponses);
    }
     */
    
    /**
     * Find project applications by AA id and proj id and user name. XMenu
     *
     * @param aaId the aa id
     * @param tkt the tkt
     * @param projectId the project id
     * @param userName the user name
     * @param validationRequest the validation request
     * @return ProjectApplciationMenuWrapper
     */
    @Override
	public ProjectApplciationMenuWrapper findProjectApplicationsByAAIdAndProjIdAndUserName(String aaId, String tkt,
			String projectId, String userName, final ValidationRequest validationRequest) {
		LOG.info(">> findProjectAppByAAIdProjectIdAndUserName");

		List<String> status = new ArrayList<>();
		status.add(Status.ACTIVE.name());
		boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);

		if (!isViewInactive) {
			final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(tkt,
					Application.CAX_ADMIN_MENU.name());
			if (userTktTbl != null) {
				validationRequest.setUserName(userTktTbl.getUsername());
				isViewInactive = validator.isViewInactiveAllowed(validationRequest);
			}
		}

		final List<ProjectApplicationsTbl> projectApplications = new ArrayList<>();

		final List<ProjectApplicationsTbl> activeProjectApplications = getProjectApplicationsBasedonStatus(aaId,
				userName, projectId, status);

		if (activeProjectApplications != null) {
			projectApplications.addAll(activeProjectApplications);
		}
		if (isViewInactive) {
			status.add(Status.INACTIVE.name());
			final List<ProjectApplicationsTbl> activeInactiveProjectApplications = getProjectApplicationsBasedonStatus(
					aaId, userName, projectId, status);
			if (activeInactiveProjectApplications != null
					&& projectApplications.size() != activeInactiveProjectApplications.size()) {
				for (ProjectApplicationsTbl activeInactiveProjectApplication : activeInactiveProjectApplications) {
					if (!projectApplications.contains(activeInactiveProjectApplication)) {
						activeInactiveProjectApplication.setStatus(Status.INACTIVE.name());
						projectApplications.add(activeInactiveProjectApplication);
					}
				}
			}
		}

		final List<ProjectApplicationMenuResponse> projectApplicationMenuResponses = new ArrayList<>();

		for (ProjectApplicationsTbl projectApplication : projectApplications) {
			ProjectApplicationMenuResponse projectApplicationMenuResponse = new ProjectApplicationMenuResponse();
			projectApplicationMenuResponse.setProjectApplicationsTbl(projectApplication);
			boolean isParent = Boolean.valueOf(projectApplication.getIsParent());
			if (isParent) {
				Iterable<ProjectApplicationsTbl> projectAppChildren = this.projectApplicationJpaDao
						.findByPosition(projectApplication.getProjectApplicationId());
				if (projectAppChildren != null) {
					projectAppChildren = validator.filterProjectApplicationResponse(isViewInactive, projectAppChildren);
				}
				projectApplicationMenuResponse.setProjectAppChildren(projectAppChildren);
			}
			projectApplicationMenuResponses.add(projectApplicationMenuResponse);
		}

		LOG.info("<< findProjectAppByAAIdProjectIdAndUserName");
		return new ProjectApplciationMenuWrapper(projectApplicationMenuResponses);
	}

	/**
	 * Gets the project applications basedon status.
	 *
	 * @param aaId the aa id
	 * @param userName the user name
	 * @param projectId the project id
	 * @param status the status
	 * @return the project applications basedon status
	 */
	private List<ProjectApplicationsTbl> getProjectApplicationsBasedonStatus(final String aaId, final String userName,
			final String projectId, final List<String> status) {
		final List<ProjectApplicationsTbl> projectApplications = new ArrayList<>();

		List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao
				.findAdminAreaProjAppRelTbls(new AdminAreasTbl(aaId), new ProjectsTbl(projectId), status);

		List<UserProjAppRelTbl> userProjAppRelTbls = this.userProjectAppRelJpaDao.findUserProjAppRelTbls(
				new AdminAreasTbl(aaId), new ProjectsTbl(projectId), userName, new ProjectsTbl(projectId), status);

		for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
			ProjectApplicationsTbl reqProjectApplicationsTbl = null;
			String siteAdminAreaRelId = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
					.getSiteAdminAreaRelId();
			ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
			if (null != projectApplicationsTbl && null != projectApplicationsTbl.getProjectApplicationId()) {
				String projectApplicationId = projectApplicationsTbl.getProjectApplicationId();
				if (ApplicationRelationType.FIXED.name().equals(adminAreaProjAppRelTbl.getRelType())) {
					boolean isAllowed = true;
					for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
						if (siteAdminAreaRelId
								.equals(userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
										.getSiteAdminAreaRelId())
								&& projectApplicationId
										.equals(userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())
								&& UserRelationType.NOTALLOWED.name().equals(userProjAppRelTbl.getUserRelType())) {
							isAllowed = false;
							break;
						}
					}
					if (isAllowed) {
						reqProjectApplicationsTbl = projectApplicationsTbl;
					}
				} else if (ApplicationRelationType.NOTFIXED.name().equals(adminAreaProjAppRelTbl.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(adminAreaProjAppRelTbl.getRelType())) {
					for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
						if (siteAdminAreaRelId
								.equals(userProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
										.getSiteAdminAreaRelId())
								&& projectApplicationId
										.equals(userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId())
								&& UserRelationType.ALLOWED.name().equals(userProjAppRelTbl.getUserRelType())) {
							reqProjectApplicationsTbl = projectApplicationsTbl;
							break;
						}
					}
				}
			}
			if (reqProjectApplicationsTbl != null) {
				projectApplications.add(reqProjectApplicationsTbl);
			}
		}

		return projectApplications;
	}
}
