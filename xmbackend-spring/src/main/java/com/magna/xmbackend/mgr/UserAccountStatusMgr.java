/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.userAccountStatus.UserAccountStatusResponse;

/**
 *
 * @author dhana
 */
public interface UserAccountStatusMgr {

    UserAccountStatusResponse findAll();
}
