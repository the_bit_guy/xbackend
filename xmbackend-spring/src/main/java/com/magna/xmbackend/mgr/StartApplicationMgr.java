package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
public interface StartApplicationMgr {

    /**
     *
     * @param validationRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return StartApplicationsTbl
     */
    StartApplicationsTbl findById(final String id);

    /**
     *
     * @param startApplicationRequest
     * @param isUpdate
     * @return StartApplicationsTbl
     */
    StartApplicationsTbl createOrUpdate(final StartApplicationRequest startApplicationRequest,
            final boolean isUpdate);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);

    /**
     *
     * @param startAppIds
     * @param httpServletRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse multiDelete(final Set<String> startAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @param validationRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartApplicationsByUserId(final String id);

    /**
     *
     * @param userName
     * @param validationRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse findUserStartApplicationsByUserName(final String userName, final String tkt,
            final ValidationRequest validationRequest);

    /**
     *
     * @param adminAreaId
     * @param projectId
     * @param validationRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppByAAIdProjectId(final String adminAreaId, final String tkt,
            final String projectId, final ValidationRequest validationRequest);

    /**
     *
     * @param adminAreaId
     * @param validationRequest
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppsByAAId(final String adminAreaId, final String tkt,
            final ValidationRequest validationRequest);

    /**
     *
     * @param projectId
     * @return StartApplicationResponse
     */
    StartApplicationResponse findStartAppsByProjectId(final String projectId);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the start applications tbl
     */
    StartApplicationsTbl findByName(String name);
}
