package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author vijay
 */
public interface RoleMgr {

    /**
     *
     * @param roleRequestList
     * @return RolesTbl
     */
    List<RolesTbl> create(final List<RoleRequest> roleRequestList);

    /**
     *
     * @param roleRequestList
     * @return RolesTbl
     */
    List<RolesTbl> update(final List<RoleRequest> roleRequestList);

    /**
     *
     * @param roleId
     * @return RolesTbl
     */
    public RolesTbl findById(final String roleId);

    /**
     *
     * @return RoleResponse
     */
    public RoleResponse findAll();

    /**
     *
     * @param roleId
     * @return boolean
     */
    public boolean deleteById(final String roleId);

    /**
     *
     * @param roleIds
     * @param httpServletRequest
     * @return RoleResponse
     */
    RoleResponse multiDelete(final Set<String> roleIds,
            final HttpServletRequest httpServletRequest);

}
