/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.GroupAuditMgr;
import com.magna.xmbackend.entities.GroupTranslationTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.GroupsJpaDao;
import com.magna.xmbackend.mgr.GroupsMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.enums.Groups;
import com.magna.xmbackend.vo.group.GroupCreateRequest;
import com.magna.xmbackend.vo.group.GroupResponse;
import com.magna.xmbackend.vo.group.GroupTranslationRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class GroupsMgrImpl implements GroupsMgr{
	
	private static final Logger LOG =  LoggerFactory.getLogger(GroupsMgrImpl.class);

	@Autowired
	private GroupsJpaDao groupsJpaDao;
	@Autowired
	private MessageMaker messageMaker;
	@Autowired
	private GroupAuditMgr groupAuditMgr; 
	
	
	
	@Override
	public GroupsTbl create(GroupCreateRequest groupCreateRequest) {
		LOG.info(">> create");
		final GroupsTbl groupsTblIn = this.convert2Entity(groupCreateRequest, false);
		final GroupsTbl groupsTblOut = this.groupsJpaDao.save(groupsTblIn);
		LOG.info("<< create");
		return groupsTblOut;
	}
	
	private GroupsTbl findByNameAndGroupType(final String groupNameFromReq, final String groupTypeFromReq){
		return this.groupsJpaDao.findByNameIgnoreCaseAndGroupType(groupNameFromReq, groupTypeFromReq);
	}
	
	private GroupsTbl convert2Entity(final GroupCreateRequest groupRequest, final boolean isUpdate){
		final String groupName = groupRequest.getGroupName();
		final String groupType = groupRequest.getGroupType();
		if(!isUpdate){
			GroupsTbl groupsTbl = findByNameAndGroupType(groupName, groupType);
			if(null != groupsTbl ){
				String[] param = {groupRequest.getGroupName()};
				throw new CannotCreateObjectException("Group name already found", "ERR0020", param);
			}
		}
		
		String id = UUID.randomUUID().toString();
		LOG.info(">>> ID: " + id);
		if (isUpdate) {
			id = groupRequest.getId();
		}
		
		final Date date = new Date();
		final String iconId = groupRequest.getIconId();
		GroupsTbl grpsTbl = new GroupsTbl(id);

		List<GroupTranslationTbl> groupTranslationTbls = new ArrayList<>();;
		List<GroupTranslationRequest> groupTranslationRequests  = groupRequest.getGroupTranslationReqs();
		for (GroupTranslationRequest groupTranslationRequest : groupTranslationRequests) {
			final String languageCode = groupTranslationRequest.getLanguageCode();
			final String remarks = groupTranslationRequest.getRemarks();
			final String description = groupTranslationRequest.getDescription();
			
			String groupTransId = UUID.randomUUID().toString();
			LOG.info(">>>Trans ID: " + groupTransId);
			if (isUpdate) {
				groupTransId = groupTranslationRequest.getId();
			}
			GroupTranslationTbl groupTranslationTbl = new GroupTranslationTbl(groupTransId);
			groupTranslationTbl.setLanguageCode(new LanguagesTbl(languageCode));
			groupTranslationTbl.setRemarks(remarks);
			groupTranslationTbl.setDescription(description);
			groupTranslationTbl.setGroupId(new GroupsTbl(id));
			groupTranslationTbl.setCreateDate(date);
			groupTranslationTbl.setUpdateDate(date);
			groupTranslationTbls.add(groupTranslationTbl);
		}
		grpsTbl.setGroupTranslationTblCollection(groupTranslationTbls);
		grpsTbl.setGroupType(groupType);
		grpsTbl.setName(groupName);
		grpsTbl.setIconId(new IconsTbl(iconId));
		grpsTbl.setCreateDate(date);
		grpsTbl.setUpdateDate(date);
		return grpsTbl;
	}

	@Override
	public List<GroupsTbl> findAll() {
		LOG.info(">> group findAll");
		final Iterable<GroupsTbl> groupTblsItr = this.groupsJpaDao.findAll();
		List<GroupsTbl> groupTblsList = (List<GroupsTbl>) groupTblsItr;
		LOG.info("<< group findAll");
		return groupTblsList;
	}

	@Override
	public GroupsTbl update(GroupCreateRequest groupCreateRequest) {
		LOG.info(">> update");
		//try{
			GroupsTbl groupsTblIn = this.convert2Entity(groupCreateRequest, true);
			GroupsTbl groupsTblOut = this.groupsJpaDao.save(groupsTblIn);
		//}catch(Exception e){
		//	LOG.info("updation failed");
		//}
		LOG.info("<< update");
		return groupsTblOut;
	}

	@Override
	public boolean deleteById(String groupId) {
		LOG.info(">> deleteById");
		boolean isDeleted = false;
		try{
			this.groupsJpaDao.delete(groupId);
			isDeleted = true;
		}catch (Exception e) {
			if (e instanceof EmptyResultDataAccessException) {
				final String[] param = {groupId};
				throw new XMObjectNotFoundException("Group not found", "P_ERR0017", param);
			}
		}
		LOG.info("<< deleteById");
		return isDeleted;
	}

	
	@Override
	public GroupResponse multiDelete(Set<String> groupIds, HttpServletRequest hsr) {
		LOG.info(">> multiDelete");
		final Set<Map<String, String>> statusMaps = new HashSet<>();
		groupIds.forEach(grpId -> {
			String name = "";
			try {
				GroupsTbl grTbl2Del = this.findById(grpId);
				if(null != grTbl2Del) {
					name = grTbl2Del.getName();
					String groupType = grTbl2Del.getGroupType();
					this.deleteById(grpId);
					this.groupAuditMgr.groupDeleteSuccessAudit(hsr, grpId, name, groupType);
				}
			} catch (XMObjectNotFoundException objectNotFound) {
                String errorMsg;
                if (!"".equalsIgnoreCase(grpId)) {
                    errorMsg = "Group with id " + grpId + " cannot be deleted";
                } else {
                    errorMsg = "Group with id " + grpId + " and name "
                            + name + "cannot be deleted";
                }
                this.groupAuditMgr.groupDeleteFailureAudit(hsr, name, errorMsg);
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
		});
		GroupResponse groupResponse = new GroupResponse(statusMaps);
		LOG.info(">> multiDelete");
		return groupResponse;
	}
	
	
	@Override
	public List<GroupsTbl> findByGroupType(String groupType) {
		LOG.info(">> findByGroupType");
		List<GroupsTbl> groupsTbls = null;
		if(groupType.equals(Groups.USER.name()) || groupType.equals(Groups.PROJECT.name())
				 || groupType.equals(Groups.USERAPPLICATION.name()) || groupType.equals(Groups.PROJECTAPPLICATION.name())){
			groupsTbls = new ArrayList<>(); 
			groupsTbls = this.groupsJpaDao.findByGroupType(groupType);
		}else{
			String[] param = {groupType};
			throw new XMObjectNotFoundException("Group type not found", "GRP_ERR001", param);
		}
		return groupsTbls;
	}

	
	@Override
	public GroupsTbl findById(String id) {
		GroupsTbl groupsTbl = null;
		if(id != null) {
			groupsTbl = this.groupsJpaDao.findOne(id);
		}
		return groupsTbl;
	}

}
