package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
public interface UserApplicationMgr {

    /**
     *
     * @param validationRequest
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl findById(final String id);

    /**
     *
     * @param userApplicationRequest
     * @param isUpdate
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl createOrUpdate(
            final UserApplicationRequest userApplicationRequest,
            final boolean isUpdate);

    /**
     *
     * @param id
     * @return
     */
    boolean delete(final String id);

    /**
     *
     * @param ids
     * @param httpServletRequest
     * @return UserApplicationResponse
     */
    UserApplicationResponse multiDelete(final Set<String> ids,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param position
     * @param validationRequest
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserAppByPosition(final String position,
            final ValidationRequest validationRequest);

    /**
     *
     * @param status
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppByStatus(final String status);

    /**
     *
     * @param validationRequest
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppByPositions(final ValidationRequest validationRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the user applications tbl
     */
    UserApplicationsTbl findByName(final String name);

    /**
     * Find by user name and language code.
     *
     * @param name the name
     * @param languageCode the language code
     * @return the user applications tbl
     */
    UserApplicationsTbl findByUserNameAndLanguageCode(final String name,
            final String languageCode);

    /**
     *
     * @param id
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserApplicationsByUserId(final String id);

    /**
     *
     * @param adminAreaId
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppsByAAIdAndActive(final String adminAreaId);

    /**
     *
     * @param userId
     * @param adminAreaId
     * @return UserApplicationResponse
     */
    UserApplicationResponse getAllUserAppsByUserAAId(final String userId,
            final String adminAreaId);

    /**
     *
     * @param aaId
     * @param userName
     * @param validationRequest
     * @return UserApplicationMenuWrapper
     */
    UserApplicationMenuWrapper findUserAppByAAIdAndUserName(final String aaId, final String tkt,
            final String userName, final ValidationRequest validationRequest);

}
