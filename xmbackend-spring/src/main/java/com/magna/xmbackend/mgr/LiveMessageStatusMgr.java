/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.LiveMessageStatusTbl;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface LiveMessageStatusMgr {
	
	LiveMessageStatusTbl create(LiveMessageStatusRequest liveMessageStatusRequest);

}
