/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.mgr.HotlineMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.rel.mgr.UserProjectAppRelMgr;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.rel.mgr.UserUserAppRelMgr;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.hotline.HotlineSaveRequest;
import com.magna.xmbackend.vo.hotline.HotlineSaveResponse;
import com.magna.xmbackend.vo.hotline.HotlineUserProjectAppRelRequest;
import com.magna.xmbackend.vo.hotline.HotlineUserProjectRelRequest;
import com.magna.xmbackend.vo.hotline.HotlineUserUserAppRelRequest;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class HotlineMgrImpl implements HotlineMgr{
	
	private static final Logger LOG = LoggerFactory.getLogger(HotlineMgrImpl.class);

	@Autowired
	UserProjectRelMgr userProjectRelMgr;
	@Autowired
	UserProjectRelJpaDao userProjectRelJpaDao;
	@Autowired
	UserUserAppRelMgr userUserAppRelMgr;
	@Autowired
	SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
	@Autowired
	AdminAreaProjectRelMgr adminAreaProjectRelMgr;
	@Autowired
	UserProjectAppRelMgr userProjectAppRelMgr;
	
	
	
	@Override
	public HotlineSaveResponse save(final HotlineSaveRequest hotlineSaveRequest, final String username, final ValidationRequest validationRequest) {
		HotlineSaveResponse hotlineSaveResponse = new HotlineSaveResponse();
		
		List<HotlineUserProjectRelRequest> hotlineUserProjectRelAddRequests = hotlineSaveRequest.getUserProjectAdditionList();
		List<HotlineUserProjectRelRequest> hotlineUserProjectRelDelRequests = hotlineSaveRequest.getUserProjectDeletionList();
		
		List<HotlineUserUserAppRelRequest> hotlineUserUserAppRelAddRequests = hotlineSaveRequest.getUserUserAppRelAdditionList();
		List<HotlineUserUserAppRelRequest> hotlineUserUserAppRelDelRequests = hotlineSaveRequest.getUserUserAppRelDeletionList();
		
		List<HotlineUserProjectAppRelRequest> hotlineUserProjectAppRelAddRequests = hotlineSaveRequest.getUserProjectAppRelAdditionList();
		List<HotlineUserProjectAppRelRequest> hotlineUserProjectAppRelDelRequests = hotlineSaveRequest.getUserProjectAppRelDeletionList();
		
		
		if (hotlineUserProjectRelAddRequests != null && hotlineUserProjectRelAddRequests.size() > 0) {
			LOG.info(">> ");
			List<UserProjectRelRequest> projectRelRequestList = new ArrayList<>();
			for (HotlineUserProjectRelRequest hotlineUserProjectRelRequest : hotlineUserProjectRelAddRequests) {
				UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
				userProjectRelRequest.setProjectId(hotlineUserProjectRelRequest.getProjectId());
				userProjectRelRequest.setUserId(hotlineUserProjectRelRequest.getUserId());
				userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
				projectRelRequestList.add(userProjectRelRequest);
			}
			UserProjectRelBatchRequest batchRequest = new UserProjectRelBatchRequest(projectRelRequestList);
			UserProjectRelBatchResponse userProjectRelBatchResponse = userProjectRelMgr.createBatch(batchRequest, username);
			hotlineSaveResponse.setUserProjectRelBatchResponse(userProjectRelBatchResponse);
			LOG.info("<< ");
		}
		
		if (hotlineUserProjectRelDelRequests != null && hotlineUserProjectRelDelRequests.size() > 0) {
			String userProjectRelId = null;
			for (HotlineUserProjectRelRequest hotlineUserProjectRelRequest : hotlineUserProjectRelDelRequests) {
				String projectId = hotlineUserProjectRelRequest.getProjectId();
				String userId = hotlineUserProjectRelRequest.getUserId();
				List<UserProjectRelTbl> userProjRelTblList = null;
				if (null != projectId) {
					userProjRelTblList = new ArrayList<>();
					userProjRelTblList = this.userProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
					for (UserProjectRelTbl userProjectRelTbl : userProjRelTblList) {
						if (userProjectRelTbl.getProjectId().getProjectId().equals(projectId)
								&& userProjectRelTbl.getUserId().getUserId().equals(userId)) {
							userProjectRelId = userProjectRelTbl.getUserProjectRelId();
							this.userProjectRelMgr.delete(userProjectRelId);
							break;
						}
					}
				}

			}
		}
		
		if (hotlineUserUserAppRelAddRequests != null && hotlineUserUserAppRelAddRequests.size() > 0) {
			final List<UserUserAppRelRequest> userUserAppRelRequestList = new ArrayList<>();
			for (final HotlineUserUserAppRelRequest hotlineUserUserAppRelRequest : hotlineUserUserAppRelAddRequests) {
				String userId = hotlineUserUserAppRelRequest.getUserId();
				String adminAreaId = hotlineUserUserAppRelRequest.getAdminAreaId();
				String userAppId = hotlineUserUserAppRelRequest.getUserAppId();
				String userRelationType = hotlineUserUserAppRelRequest.getUserRelationType();
				if (ApplicationRelationType.FIXED.name().equals(userRelationType)
						|| ApplicationRelationType.PROTECTED.name().equals(userRelationType)) {
					List<UserUserAppRelation> userUserAppRelations = userUserAppRelMgr.findUserUserAppRelsByUserIdAndAAId(userId, adminAreaId, validationRequest);
					String userUserAppRelId = "";
					if (userUserAppRelations != null && userUserAppRelations.size() > 0) {
						for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
							if (userUserAppRelation.getUserApplicationId().getUserApplicationId().equals(userAppId)) {
								userUserAppRelId = userUserAppRelation.getUserUserAppRelId();
								this.userUserAppRelMgr.deleteById(userUserAppRelId);
								break;
							}
						}
					}
				} else if (ApplicationRelationType.NOTFIXED.name().equals(userRelationType)) {
					UserUserAppRelRequest userUserAppRelRequest = new UserUserAppRelRequest();
					userUserAppRelRequest.setUserId(userId);
					userUserAppRelRequest.setUserAppId(userAppId);
					List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
					SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelTbls.get(0);
					String siteAdminAreaRelId = null;
					if (null != siteAdminAreaRelTbl) {
						siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
					}
					userUserAppRelRequest.setSiteAdminAreaRelId(siteAdminAreaRelId);
					userUserAppRelRequest.setUserRelationType(UserRelationType.ALLOWED.name());
					userUserAppRelRequestList.add(userUserAppRelRequest);
				}
			}
			UserUserAppRelBatchRequest batchRequest = new UserUserAppRelBatchRequest(userUserAppRelRequestList);
			UserUserAppRelBatchResponse userUserAppRelBatchResponse = this.userUserAppRelMgr.createBatch(batchRequest, username);
			hotlineSaveResponse.setUserUserAppRelBatchResponse(userUserAppRelBatchResponse);
		}
		
		
		if (hotlineUserUserAppRelDelRequests != null && hotlineUserUserAppRelDelRequests.size() > 0) {
			if (hotlineUserUserAppRelDelRequests  != null && hotlineUserUserAppRelDelRequests.size() > 0) {
				final List<UserUserAppRelRequest> userUserAppRelRequestList = new ArrayList<>();
				for(HotlineUserUserAppRelRequest hotlineUserUserAppRelRequest : hotlineUserUserAppRelDelRequests){
					String userId = hotlineUserUserAppRelRequest.getUserId();
					String adminAreaId = hotlineUserUserAppRelRequest.getAdminAreaId();
					String userAppId = hotlineUserUserAppRelRequest.getUserAppId();
					String userRelationType = hotlineUserUserAppRelRequest.getUserRelationType();
					if (ApplicationRelationType.FIXED.name().equals(userRelationType) 
							|| ApplicationRelationType.PROTECTED.name().equals(userRelationType)) {
						UserUserAppRelRequest userUserAppRelRequest = new UserUserAppRelRequest();
						userUserAppRelRequest.setUserId(userId);
						userUserAppRelRequest.setUserAppId(userAppId);
						userUserAppRelRequest.setSiteAdminAreaRelId(adminAreaId);
						userUserAppRelRequest.setUserRelationType(UserRelationType.NOTALLOWED.name());
						userUserAppRelRequestList.add(userUserAppRelRequest);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(userRelationType)) {
						List<UserUserAppRelation> userUserAppRelations = userUserAppRelMgr.findUserUserAppRelsByUserIdAndAAId(userId, adminAreaId, validationRequest);
						String userUserAppRelId = "";
						if (userUserAppRelations != null && userUserAppRelations.size() > 0) {
							for (UserUserAppRelation userUserAppRelation : userUserAppRelations) {
								if (userUserAppRelation.getUserApplicationId().getUserApplicationId().equals(userAppId)) {
									userUserAppRelId = userUserAppRelation.getUserUserAppRelId();
									this.userUserAppRelMgr.deleteById(userUserAppRelId);
									break;
								}
							}
						}
					}
				}
			}
		}
		
		
		if (hotlineUserProjectAppRelAddRequests != null && hotlineUserProjectAppRelAddRequests.size() > 0) {
			if (hotlineUserProjectAppRelAddRequests != null && hotlineUserProjectAppRelAddRequests.size() > 0) {
				final List<UserProjectAppRelRequest> userProjectAppRelRequestList = new ArrayList<>();
				for (final HotlineUserProjectAppRelRequest hotlineUserProjectAppRelRequest : hotlineUserProjectAppRelAddRequests) {
					
					String userId = hotlineUserProjectAppRelRequest.getUserId();
					String projectId = hotlineUserProjectAppRelRequest.getProjectId();
					String projectAppId = hotlineUserProjectAppRelRequest.getProjectAppId();
					String adminAreaId = hotlineUserProjectAppRelRequest.getAdminAreaId();
					String userRelationType = hotlineUserProjectAppRelRequest.getUserRelationType();
					
					UserProjectRelTbl userProjectRelTbl = this.userProjectRelMgr.findUserProjectRelByUserIdProjectId(userId, projectId);
					String userProjectRelId = userProjectRelTbl.getUserProjectRelId();
					
					if (ApplicationRelationType.FIXED.name().equals(userRelationType) 
							|| ApplicationRelationType.PROTECTED.name().equals(userRelationType)) {
						String userProjAppRelId = "";
						List<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelMgr.findByUserIdProjectIdAAId(userId, projectId, adminAreaId, validationRequest);
						if(userProjAppRelTbls.size() > 0){
							for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
								if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId().equals(projectAppId)) {
									userProjAppRelId = userProjAppRelTbl.getUserProjAppRelId();
									break;
								}
							}
						}
						this.userProjectAppRelMgr.delete(userProjAppRelId);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(userRelationType)) {
						List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
						SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelTbls.get(0);
						String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
						AdminAreaProjectRelTbl adminAreaProjRelTbl = adminAreaProjectRelMgr.findBySiteAdminAreaRelIdAndProjectId(siteAdminAreaRelId, projectId);
						String adminAreaProjectRelId = adminAreaProjRelTbl.getAdminAreaProjectRelId();
						
						UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
						userProjectAppRelRequest.setUserProjectRelId(userProjectRelId);
						userProjectAppRelRequest.setAdminAreaProjRelId(adminAreaProjectRelId);
						userProjectAppRelRequest.setProjectAppId(projectAppId);
						userProjectAppRelRequest.setUserRelationType(UserRelationType.ALLOWED.name());
						userProjectAppRelRequestList.add(userProjectAppRelRequest);
					}
				}
				UserProjectAppRelBatchRequest userProjectAppRelBatchRequest = new UserProjectAppRelBatchRequest(userProjectAppRelRequestList);
				UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = this.userProjectAppRelMgr.createBatch(userProjectAppRelBatchRequest, username);
				hotlineSaveResponse.setUserProjectAppRelBatchResponse(userProjectAppRelBatchResponse);
			}
		}
		
		if (hotlineUserProjectAppRelDelRequests != null && hotlineUserProjectAppRelDelRequests.size() > 0) {
			if (hotlineUserProjectAppRelDelRequests != null && hotlineUserProjectAppRelDelRequests.size() > 0) {
				final List<UserProjectAppRelRequest> userProjectAppRelRequestList = new ArrayList<>();
				for (final HotlineUserProjectAppRelRequest hotlineUserProjectAppRelRequest : hotlineUserProjectAppRelDelRequests) {
					String userId = hotlineUserProjectAppRelRequest.getUserId();
					String projectId = hotlineUserProjectAppRelRequest.getProjectId();
					String projectAppId = hotlineUserProjectAppRelRequest.getProjectAppId();
					String adminAreaId = hotlineUserProjectAppRelRequest.getAdminAreaId();
					String userRelationType = hotlineUserProjectAppRelRequest.getUserRelationType();
					String userProjectRelId = "";
					
					if (ApplicationRelationType.FIXED.name().equals(userRelationType) 
							|| ApplicationRelationType.PROTECTED.name().equals(userRelationType)) {
						
						List<UserProjectRelTbl> userProjRelTblList = null;
						if (null != projectId) {
							userProjRelTblList = new ArrayList<>();
							userProjRelTblList = this.userProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
							for (UserProjectRelTbl userProjectRelTbl : userProjRelTblList) {
								if (userProjectRelTbl.getProjectId().getProjectId().equals(projectId)
										&& userProjectRelTbl.getUserId().getUserId().equals(userId)) {
									userProjectRelId = userProjectRelTbl.getUserProjectRelId();
									break;
								}
							}
						}
						
						List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
						SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelTbls.get(0);
						String siteAdminAreaRelId = null;
						if (null != siteAdminAreaRelTbl) {
							siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
						}
						
						AdminAreaProjectRelTbl adminAreaProjRelTbl = adminAreaProjectRelMgr.findBySiteAdminAreaRelIdAndProjectId(siteAdminAreaRelId, projectId);
						String adminAreaProjectRelId = adminAreaProjRelTbl.getAdminAreaProjectRelId();
						
						UserProjectAppRelRequest userProjectAppRelRequest = new UserProjectAppRelRequest();
						userProjectAppRelRequest.setUserProjectRelId(userProjectRelId);
						userProjectAppRelRequest.setAdminAreaProjRelId(adminAreaProjectRelId);
						userProjectAppRelRequest.setProjectAppId(projectAppId);
						userProjectAppRelRequest.setUserRelationType(UserRelationType.NOTALLOWED.name());
						userProjectAppRelRequestList.add(userProjectAppRelRequest);
					} else if (ApplicationRelationType.NOTFIXED.name().equals(userRelationType)) {
						String userProjAppRelId = "";
						List<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelMgr.findByUserIdProjectIdAAId(userId, projectId, adminAreaId, validationRequest);
						if(userProjAppRelTbls.size() > 0){
							for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTbls) {
								if (userProjAppRelTbl.getProjectApplicationId().getProjectApplicationId().equals(projectAppId)) {
									userProjAppRelId = userProjAppRelTbl.getUserProjAppRelId();
									break;
								}
							}
						}
						this.userProjectAppRelMgr.delete(userProjAppRelId);
					}
					UserProjectAppRelBatchRequest userProjectAppRelBatchRequest = new UserProjectAppRelBatchRequest(userProjectAppRelRequestList);
					UserProjectAppRelBatchResponse userProjectAppRelBatchResponse = this.userProjectAppRelMgr.createBatch(userProjectAppRelBatchRequest, username);
				}
			}
		}
		return null;
	}

}
