package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.audit.mgr.SiteAuditMgr;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.SiteMgr;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.jpa.site.SiteTranslation;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import javax.servlet.http.HttpServletRequest;

@Component
public class SiteMgrImpl implements SiteMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteMgrImpl.class);

    @Autowired
    private SiteJpaDao siteJpaDao;

    @Autowired
    private AdminAreaProjectRelMgr adminAreaProjectRelMgr;

    @Autowired
    private ProjectMgr projectMgr;

    @Autowired
    private UserApplicationMgr userApplicationMgr;

    @Autowired
    private ProjectApplicationMgr projectApplicationMgr;

    @Autowired
    private StartApplicationMgr startApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    private UserTktJpaDao userTktJpaDao;

    @Autowired
    private SiteAuditMgr siteAuditMgr;

    /**
     *
     * @param validationRequest
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<SitesTbl> sitesTbls = this.siteJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        sitesTbls = validator.filterSiteResponse(isViewInactive, sitesTbls);
        final SiteResponse siteResponse = new SiteResponse(sitesTbls);
        LOG.info("<< findAll");
        return siteResponse;
    }

    /**
     *
     * @param siteRequest
     * @return SitesTbl
     */
    @Override
    public final SitesTbl create(final SiteRequest siteRequest) {
        LOG.info(">> create");
        final SitesTbl stIn = this.convert2Entity(siteRequest, false);
        final SitesTbl stOut = this.siteJpaDao.save(stIn);
        LOG.info("<< create");
        return stOut;
    }

    /**
     *
     * @param siteRequest
     * @param isUpdate
     * @return SitesTbl
     */
    private SitesTbl convert2Entity(final SiteRequest siteRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = siteRequest.getId();
        }

        final Date date = new Date();
        final String iconId = siteRequest.getIconId();
        final String siteName = siteRequest.getName();
        final String status = siteRequest.getStatus().toString();
        final List<SiteTranslation> translations = siteRequest.getSiteTranslations();

        if (!isUpdate
                && null != this.findSiteIdForName(siteName)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getSiteNameWithi18nCode(translations, siteName);
            throw new CannotCreateObjectException(
                    "Site name already found", "ERR0002", paramMap);
        }

        final SitesTbl sitesTbl = new SitesTbl(id);
        sitesTbl.setName(siteName);
        sitesTbl.setIconId(new IconsTbl(iconId));
        sitesTbl.setStatus(status);
        sitesTbl.setCreateDate(date);
        sitesTbl.setUpdateDate(date);
        final List<SiteTranslationTbl> stts = new ArrayList<>();

        for (SiteTranslation translation : translations) {
            String transId = UUID.randomUUID().toString();
            if (isUpdate) {
                transId = translation.getId();
            }
            final String languageCode = translation.getLanguageCode();
            final String description = translation.getDescription();
            final String remarks = translation.getRemarks();
            final SiteTranslationTbl stt = new SiteTranslationTbl(transId);
            stt.setCreateDate(date);
            stt.setUpdateDate(date);
            stt.setDescription(description);
            stt.setLanguageCode(new LanguagesTbl(languageCode));
            stt.setRemarks(remarks);
            stt.setSiteId(new SitesTbl(id));
            stts.add(stt);
        }
        sitesTbl.setSiteTranslationTblCollection(stts);
        return sitesTbl;
    }

    /**
     *
     * @param siteRequest
     * @return SitesTbl
     */
    @Override
    public final SitesTbl update(final SiteRequest siteRequest) {
        LOG.info(">> update");
        final SitesTbl stIn = this.convert2Entity(siteRequest, true);
        final SitesTbl stOut = this.siteJpaDao.save(stIn);
        LOG.info("<< update");

        return stOut;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateById(final String status, final String id) {
        LOG.info(">> updateById {}", id);
        final Date date = new Date();
        boolean isUpdated = false;
        final int out = this.siteJpaDao
                .setStatusAndUpdateDateForSiteTbl(status, date, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return SitesTbl
     */
    @Override
    public final SitesTbl findById(final String id) {
        LOG.info(">> findById");
        final SitesTbl st = this.siteJpaDao.findOne(id);
        LOG.info("<< findById");
        return st;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.siteJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Object not found", "P_ERR0010", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    @Override
    public SiteResponse multiDelete(Set<String> siteIds,
            HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");

        final List<Map<String, String>> statusMaps = new ArrayList<>();

        for (String siteId : siteIds) {
            String name = "";
            try {
                SitesTbl sitesTbl2Del = this.findById(siteId);
                if (null != sitesTbl2Del) {
                    name = sitesTbl2Del.getName();
                    this.deleteById(siteId);
                    this.siteAuditMgr.siteDeleteSuccessAudit(httpServletRequest,
                            siteId, name);
                } else {
                    throw new XMObjectNotFoundException("Object not found for "
                            + "Site id " + siteId, "ERR0003");
                }
            } catch (XMObjectNotFoundException objectNotFound) {
                String errorMsg;
                if (!"".equalsIgnoreCase(siteId)) {
                    errorMsg = "Site with id " + siteId + " cannot be deleted";
                } else {
                    errorMsg = "Site with id " + siteId + " and with name "
                            + name + "cannot be deleted";
                }
                this.siteAuditMgr.siteDeleteFailureAudit(httpServletRequest,
                        name, errorMsg);
                Map<String, String> statusMap
                        = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        }

        SiteResponse siteResponse = new SiteResponse(statusMaps);

        LOG.info(">> multiDelete");
        return siteResponse;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaById(final String id,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAdminAreaById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAdminAreaById isViewInactive={}", isViewInactive);
        final SitesTbl foundSitesTbl = validator.validateSite(id, isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaResponse response = this.getAdminAreas(foundSitesTbl, isViewInactive);
        LOG.info("<< findAdminAreaById");
        return response;
    }

    /**
     *
     * @param sitesTbl
     * @param isViewInactive
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaResponse getAdminAreas(final SitesTbl sitesTbl,
            final boolean isViewInactive) {
        Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls
                = sitesTbl.getSiteAdminAreaRelTblCollection();
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
        }
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        final List<SiteAdminAreaRelIdWithAdminArea> siteAdminAreaRelIdWithAdminAreas = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            final String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
            AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
            adminAreasTbl = validator.filterAdminAreaResponse(isViewInactive, adminAreasTbl);
            if (null != adminAreasTbl && null != adminAreasTbl.getAdminAreaId()) {
                final SiteAdminAreaRelIdWithAdminArea saariwaa = new SiteAdminAreaRelIdWithAdminArea();
                saariwaa.setAdminAreasTbl(adminAreasTbl);
                saariwaa.setSiteAdminAreaRelId(siteAdminAreaRelId);
                siteAdminAreaRelIdWithAdminAreas.add(saariwaa);
            }
        }
        final SiteAdminAreaRelIdWithAdminAreaResponse response
                = new SiteAdminAreaRelIdWithAdminAreaResponse(siteAdminAreaRelIdWithAdminAreas);
        return response;
    }

    /**
     *
     * @param name
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @Override
    public final SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaByName(final String name,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAdminAreaByName {}", name);
        final String siteId = this.findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAdminAreaByName isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(siteId, isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaResponse response = this.getAdminAreas(sitesTbl, isViewInactive);
        LOG.info("<< findAdminAreaByName");
        return response;
    }

    /**
     *
     * @param name
     * @param validationRequest
     * @return SitesTbl
     */
    @Override
    public final SitesTbl findByName(final String name, final String tkt,
            final ValidationRequest validationRequest) {
        LOG.info(">> findByName {}", name);
        final String siteId = findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByName isViewInactive={}", isViewInactive);
        if (!isViewInactive) {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(tkt,
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            }
        }
        final SitesTbl sitesTblByName = validator.validateSite(siteId, isViewInactive);
        LOG.info("<< findByName");
        return sitesTblByName;
    }

    /**
     *
     * @param name
     * @return SitesTbl
     */
    private SitesTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        String siteId;
        siteId = findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final SitesTbl sitesTblByName = this.findById(siteId);
        LOG.info("<< findByName");
        return sitesTblByName;
    }

    /**
     *
     * @param name
     * @return Site Name
     */
    private String findSiteIdForName(final String name) {
        SitesTbl sitesTbl = this.siteJpaDao.findByNameIgnoreCase(name);
        String siteId = null;
        if (sitesTbl != null) {
            siteId = sitesTbl.getSiteId();
        }
        LOG.info(">> findSiteIdForName - Site id for name is {} - {}",
                name, siteId);
        return siteId;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectById(final String id,
            final ValidationRequest validationRequest) {
        ProjectResponse projectResponse = null;
        LOG.info(">> findProjectById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectById isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(id, isViewInactive);
        if (null != sitesTbl) {
            projectResponse = this.getProjects(sitesTbl, isViewInactive);
        } else {
            final String[] param = {id};
            throw new XMObjectNotFoundException("Given SiteId not found", "S_ERR0002", param);
        }
        LOG.info("<< findProjectById");
        return projectResponse;
    }

    /**
     *
     * @param sitesTbl
     * @param isViewInactive
     * @return ProjectResponse
     */
    private ProjectResponse getProjects(final SitesTbl sitesTbl,
            final boolean isViewInactive) {
        ProjectResponse projectResponse;
        Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = sitesTbl.getSiteAdminAreaRelTblCollection();
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
        }
        // Filter site admin area relation by isViewInactive allowed
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        final List<ProjectsTbl> projectsTbls = new ArrayList<>();
        for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            AdminAreasTbl filterAdminAreaTbl = validator.filterAdminAreaResponse(isViewInactive, siteAdminAreaRelTbl.getAdminAreaId());
            if (null != filterAdminAreaTbl) {
                Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelMgr.findAdminAreaProjectRelTblsBySiteAdminAreaRelId(siteAdminAreaRelTbl);
                if (null != adminAreaProjectRelTbls) {
                    // Filter admin area project relation by isViewInactive allowed
                    adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
                    for (AdminAreaProjectRelTbl adminAreaProjectRelTbl
                            : adminAreaProjectRelTbls) {
                        ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
                        LOG.info("projectsTbl={}", projectsTbl.getProjectId());
                        projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
                        if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                            projectsTbls.add(projectsTbl);
                        }
                    }
                }
            }
        }
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No project relationship found for the site", "S_ERR0004");
        }
        projectResponse = new ProjectResponse(projectsTbls);
        return projectResponse;
    }

    /**
     *
     * @param name
     * @param validationRequest
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectByName(final String name,
            final ValidationRequest validationRequest) {
        LOG.info(">> findProjectByName {}", name);
        SitesTbl sitesTbl = this.findByName(name);
        if (null == sitesTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectByName isViewInactive={}", isViewInactive);
        sitesTbl = validator.validateSite(sitesTbl.getSiteId(), isViewInactive);
        final ProjectResponse projectResponse = this.getProjects(sitesTbl, isViewInactive);
        LOG.info("<< findProjectByName");
        return projectResponse;
    }

    /**
     *
     * @param projectId
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSiteByProjectId(final String projectId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSiteByProjectId {}", projectId);
        final ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
        if (null != projectsTbl) {
            final Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
            if (!adminAreaProjectRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Project relationship found for the Site Admin Area", "S_ERR0005");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Project relationship found for the project", "S_ERR0006");
            }
        } else {
            final String[] param = {projectId};
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0007", param);
        }
        LOG.info("<< findSiteByProjectId");
        return siteResponse;
    }

    /**
     *
     * @param userAppId
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByUserAppId(final String userAppId) {
        LOG.info(">> findSitesByUserAppId {}", userAppId);
        SiteResponse siteResponse = null;
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppId);
        if (null != userApplicationsTbl) {
            Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection = userApplicationsTbl.getAdminAreaUserAppRelTblCollection();
            if (!adminAreaUserAppRelTblCollection.isEmpty()) {
                Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaUserAppRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area User App relationship found for the Site Admin Area", "S_ERR0008");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area User App relationship found for the User App", "S_ERR0009");
            }
        } else {
            final String[] param = {userAppId};
            throw new XMObjectNotFoundException("No User App found", "S_ERR0010", param);
        }
        LOG.info("<< findSitesByUserAppId");

        return siteResponse;
    }

    /**
     *
     * @param projectAppId
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByProjectAppId(final String projectAppId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByProjectAppId {}", projectAppId);
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projectAppId);
        if (null != projectApplicationsTbl) {
            final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection = projectApplicationsTbl.getAdminAreaProjAppRelTblCollection();
            if (!adminAreaProjAppRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTblCollection) {
                    final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjAppRelTbl.getAdminAreaProjectRelId();
                    if (null != adminAreaProjectRelTbl) {
                        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                        if (null != siteAdminAreaRelTbl) {
                            final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                            sitesTbls.add(sitesTbl);
                        }
                    } else {
                        throw new XMObjectNotFoundException("No Admin Area Project App relationship found for the Admin Area Project Relation", "S_ERR0014");
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Project relationship found for the Site Admin Area", "S_ERR0015");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Proj App relationship found for the Project App", "S_ERR0016");
            }
        } else {
            final String[] param = {projectAppId};
            throw new XMObjectNotFoundException("No Project App found", "S_ERR0017", param);
        }
        LOG.info("<< findSitesByProjectAppId ");
        return siteResponse;
    }

    /**
     *
     * @param startAppId
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByStartAppId(final String startAppId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByStartAppId {}", startAppId);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationMgr.findById(startAppId);
        if (null != startApplicationsTbl) {
            final Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection = startApplicationsTbl.getAdminAreaStartAppRelTblCollection();
            if (!adminAreaStartAppRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaStartAppRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Start App relationship found for the Site Admin Area", "S_ERR0011");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Start App relationship found for the Start App", "S_ERR0012");
            }
        } else {
            final String[] param = {startAppId};
            throw new XMObjectNotFoundException("No Start App found", "S_ERR0013", param);
        }
        LOG.info("<< findSitesByStartAppId ");
        return siteResponse;
    }

    /**
     *
     * @param adminAreaId
     * @param validationRequest
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByAdminAreaId(final String adminAreaId,
            final ValidationRequest validationRequest) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByAdminAreaId {}", adminAreaId);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findSitesByAdminAreaId isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isViewInactive);
        if (null != adminAreasTbl) {
            Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection = adminAreasTbl.getSiteAdminAreaRelTblCollection();
            final Collection<SitesTbl> sitesTbls = new ArrayList<>();
            siteAdminAreaRelTblCollection = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTblCollection);
            for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTblCollection) {
                SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                sitesTbl = validator.filterSiteResponse(isViewInactive, sitesTbl);
                if (null != sitesTbl && null != sitesTbl.getSiteId()) {
                    sitesTbls.add(sitesTbl);
                }
            }
            siteResponse = new SiteResponse(sitesTbls);
        }
        LOG.info("<< findSitesByAdminAreaId ");
        return siteResponse;
    }

}
