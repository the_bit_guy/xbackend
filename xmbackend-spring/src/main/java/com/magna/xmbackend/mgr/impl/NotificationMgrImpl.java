/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.rel.dao.EmailNotifyToUserRelJpaDao;
import com.magna.xmbackend.mgr.NotificationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NotificationMgrImpl implements NotificationMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(NotificationMgrImpl.class);
    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;
    @Autowired
    private EmailNotifyToUserRelJpaDao emailNotifyToUserRelJpaDao;
    @Autowired
    private UserMgr userMgr;
    @Autowired
    private MessageMaker messageMaker;
    
    @Override
    public Iterable<EmailNotifyToUserRelTbl> findAllPendingNotification() {
        LOG.info(">> findAllPendingNotification");
        Iterable<EmailNotifyToUserRelTbl> enturts
                = this.emailNotifyToUserRelJpaDao.findAll();
        LOG.info("<< findAllPendingNotification");
        return enturts;
    }

    @Override
    public Iterable<EmailNotificationConfigTbl> findAllNotificationConfig() {
        LOG.info(">> findAllNotificationConfig");
        Iterable<EmailNotificationConfigTbl> findAll
                = this.emailNotificationConfigJpaDao.findAll();
        LOG.info("<< findAllNotificationConfig");
        return findAll;
    }

    @Override
    public NotificationResponse createNotification(
            NotificationRequest notificationRequest) {
        LOG.info(">> createNotification");
        Collection<EmailNotifyToUserRelTbl> convert2Entity
                = this.convert2Entity(notificationRequest);
        Iterable<EmailNotifyToUserRelTbl> outEntities
                = this.emailNotifyToUserRelJpaDao.save(convert2Entity);
        NotificationResponse notificationResponse
                = new NotificationResponse(outEntities);
        LOG.info("<< createNotification");
        return notificationResponse;
    }

	private Collection<EmailNotifyToUserRelTbl> convert2Entity(NotificationRequest notificationRequest) {
		String notificationEvent = notificationRequest.getNotificationEvent();
		EmailNotificationConfigTbl configTbl = this.emailNotificationConfigJpaDao.findByEvent(notificationEvent);

		if (null == configTbl) {
			LOG.error("Notification Event {} not supported", notificationEvent);
			// todo: throw err
		}
		List<String> usersToNotify = notificationRequest.getUsersToNotify();

		Date date = new Date();

		String includeRemarks = notificationRequest.getIncludeRemarks();
		String sendToAssignedUser = notificationRequest.getSendToAssignedUser();
		String message = notificationRequest.getMessage();
		String subject = notificationRequest.getSubject();
		String projectExpNoticePeriod = notificationRequest.getProjectExpNoticePeriod();

		Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = new ArrayList<>();
		List<String> invalidUserNames = new ArrayList<>();
		for (String user : usersToNotify) {
			UsersTbl usersTbl = this.userMgr.findByName(user);
			if (usersTbl != null) {
				Iterable<EmailNotifyToUserRelTbl> relTbls = this.emailNotifyToUserRelJpaDao.findByUserId(usersTbl);
				String uuid = UUID.randomUUID().toString();
				for (EmailNotifyToUserRelTbl relTbl : relTbls) {
					EmailNotificationConfigTbl emailNotificationConfigId = relTbl.getEmailNotificationConfigId();
					String eventGot = emailNotificationConfigId.getEvent();
					if (notificationEvent.equalsIgnoreCase(eventGot)) {
						uuid = relTbl.getEmailNotifyToUserRelId();
						break;
					}
				}
				EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = new EmailNotifyToUserRelTbl(uuid, date, date);

				// todo: check for user existence
				emailNotifyToUserRelTbl.setUserId(usersTbl);
				emailNotifyToUserRelTbl.setCreateDate(date);
				configTbl.setUpdateDate(date);
				configTbl.setIncludeRemarks(includeRemarks);
				configTbl.setMessage(message);
				configTbl.setSendToAssignedUser(sendToAssignedUser);
				configTbl.setProjectExpiryNoticePeriod(projectExpNoticePeriod);
				configTbl.setSubject(subject);
				emailNotifyToUserRelTbl.setEmailNotificationConfigId(configTbl);
				emailNotifyToUserRelTbls.add(emailNotifyToUserRelTbl);
			} else {
				invalidUserNames.add(user);
			}
		}
		if(invalidUserNames.size() > 0){
			String commaSeparatedUsernames = String.join(",", invalidUserNames);
			String[] param = {commaSeparatedUsernames};
			throw new XMObjectNotFoundException("No User Found", "USR_ERR0001", param);
		}	

		return emailNotifyToUserRelTbls;
	}

    @Override
    public NotificationResponse updateNotification(NotificationRequest notificationRequest) {
        LOG.info(">> updateNotification");
        NotificationResponse notificationResponse = null;
        final String event = notificationRequest.getNotificationEvent();
    	EmailNotificationConfigTbl notifTbl = this.emailNotificationConfigJpaDao.findByEvent(event);
    	final List<Map<String, String>> statusMaps = new ArrayList<>();
    	Collection<EmailNotifyToUserRelTbl> convert2EntityCollection = null;
		if (notificationRequest.getUsersToNotify().size() > 0) {
			try {
				List<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = this.emailNotifyToUserRelJpaDao
						.findByEmailNotificationConfigId(
								new EmailNotificationConfigTbl(notifTbl.getEmailNotificationConfigId()));
				this.emailNotifyToUserRelJpaDao.delete(emailNotifyToUserRelTbls);
				convert2EntityCollection = this.convert2Entity(notificationRequest);
			} catch (XMObjectNotFoundException objectNotFoundException) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFoundException);
				statusMaps.add(statusMap);
			}
			Iterable<EmailNotifyToUserRelTbl> outEntities = this.emailNotifyToUserRelJpaDao.save(convert2EntityCollection);
			notificationResponse = new NotificationResponse(outEntities, statusMaps);
			
		}else{
			List<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = this.emailNotifyToUserRelJpaDao
					.findByEmailNotificationConfigId(
							new EmailNotificationConfigTbl(notifTbl.getEmailNotificationConfigId()));
			this.emailNotifyToUserRelJpaDao.delete(emailNotifyToUserRelTbls);
			Date date = new Date();
			String notificationEvent = notificationRequest.getNotificationEvent();
	        String includeRemarks = notificationRequest.getIncludeRemarks();
	        String sendToAssignedUser = notificationRequest.getSendToAssignedUser();
	        String message = notificationRequest.getMessage();
	        String subject = notificationRequest.getSubject();
	        String projectExpNoticePeriod = notificationRequest.getProjectExpNoticePeriod();
	        EmailNotificationConfigTbl configTbl = this.emailNotificationConfigJpaDao.findByEvent(notificationEvent);
			configTbl.setUpdateDate(date);
            configTbl.setIncludeRemarks(includeRemarks);
            configTbl.setMessage(message);
            configTbl.setSendToAssignedUser(sendToAssignedUser);
            configTbl.setProjectExpiryNoticePeriod(projectExpNoticePeriod);
            configTbl.setSubject(subject);
            EmailNotificationConfigTbl emailNotificationConfigTbl = this.emailNotificationConfigJpaDao.save(configTbl);
			notificationResponse = new NotificationResponse(emailNotificationConfigTbl);
		}
        LOG.info("<< updateNotification");
        return notificationResponse;
    }

    @Override
    public NotificationResponse findByEvent(String event) {
        LOG.info(">> findByEvent");
        EmailNotificationConfigTbl notificationConfigTbl = this.emailNotificationConfigJpaDao.findByEvent(event);
        List<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = this.emailNotifyToUserRelJpaDao.findByEmailNotificationConfigId(notificationConfigTbl);
        final NotificationResponse notificationResponse = new NotificationResponse(notificationConfigTbl);
        notificationResponse.setEmailNotifyToUserRelTbls(emailNotifyToUserRelTbls);
        LOG.info("<< findByEvent");
        return notificationResponse;
    }

}
