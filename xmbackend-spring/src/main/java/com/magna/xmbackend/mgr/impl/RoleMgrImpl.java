package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.audit.mgr.RoleAuditMgr;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.RoleJpaDao;
import com.magna.xmbackend.jpa.dao.RolePermissionRelJpaDao;
import com.magna.xmbackend.mgr.RoleMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
@Component
public class RoleMgrImpl implements RoleMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(RoleMgrImpl.class);
    @Autowired
    private RoleJpaDao roleJpaDao;

    @Autowired
    private RolePermissionRelJpaDao rolePermissionRelJpaDao;
    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    RoleAuditMgr roleAuditMgr;

    /**
     *
     * @param roleRequestList
     * @return RolesTbl
     */
    @Override
    public final List<RolesTbl> create(final List<RoleRequest> roleRequestList) {
        LOG.info(">> create");
        final List<RolesTbl> rolesTbls = new ArrayList<>();
        if (null != roleRequestList) {
            roleRequestList.forEach(roleRequest -> {
                final RolesTbl rolesTblIn = this.create2Entity(roleRequest, false);
                final RolesTbl rolesTblOut = this.roleJpaDao.save(rolesTblIn);
                rolesTbls.add(rolesTblOut);
            });
        }
        LOG.info("<< create");
        return rolesTbls;
    }

    /**
     *
     * @param roleRequest
     * @param isUpdate
     * @return RolesTbl
     */
    private RolesTbl create2Entity(final RoleRequest roleRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = roleRequest.getRoleId();
        }
        final RolesTbl rolesTbl = new RolesTbl(id);
        final String roleName = roleRequest.getName();
        final String roleDescription = roleRequest.getDescription();
        final Date date = new Date();
        rolesTbl.setName(roleName);
        rolesTbl.setDescription(roleDescription);
        rolesTbl.setCreateDate(date);
        rolesTbl.setUpdateDate(date);

        final List<RolePermissionRelTbl> rolePermissionRelTblList = new ArrayList<>();

        final List<ObjectPermission> objectPermissionList = roleRequest.getObjectPermissionList();
        objectPermissionList.forEach(objectPermission -> {
            String rolePermissionRelationId = UUID.randomUUID().toString();
            final CreationType creationType = objectPermission.getCreationType();
            final String permissionId = objectPermission.getPermissionId();
            switch (creationType) {
                case DELETE:
                    rolePermissionRelJpaDao.deleteRolePermissionRelation(rolesTbl, new PermissionTbl(permissionId));
                    break;
                case ADD:
                    final RolePermissionRelTbl rolePermissionRelTbl = new RolePermissionRelTbl(rolePermissionRelationId);
                    rolePermissionRelTbl.setRoleId(rolesTbl);
                    rolePermissionRelTbl.setPermissionId(new PermissionTbl(permissionId));
                    rolePermissionRelTbl.setCreateDate(date);
                    rolePermissionRelTbl.setUpdateDate(date);
                    rolePermissionRelTblList.add(rolePermissionRelTbl);
                    break;
                default:
                    break;
            }

        });
        rolesTbl.setRolePermissionRelTblCollection(rolePermissionRelTblList);
        return rolesTbl;
    }

    /**
     *
     * @param roleRequestList
     * @return RolesTbl
     */
    @Override
    public List<RolesTbl> update(final List<RoleRequest> roleRequestList) {
        LOG.info(">> update");
        final List<RolesTbl> rolesTbls = new ArrayList<>();
        if (null != roleRequestList) {
            roleRequestList.forEach(roleRequest -> {
                final RolesTbl rolesTblIn = this.create2Entity(roleRequest, true);
                final RolesTbl rolesTblOut = this.roleJpaDao.save(rolesTblIn);
                rolesTbls.add(rolesTblOut);
            });
        }
        LOG.info("<< update");
        return rolesTbls;
    }

    /**
     *
     * @param roleId
     * @return RolesTbl
     */
    @Override
    public final RolesTbl findById(final String roleId) {
        LOG.info(">> findById");
        final RolesTbl rolesTbl = this.roleJpaDao.findOne(roleId);
        LOG.info("<< findById");
        return rolesTbl;
    }

    /**
     *
     * @return RoleResponse
     */
    @Override
    public final RoleResponse findAll() {
        LOG.info(">> findAll");
        final Iterable<RolesTbl> rolesTbls = this.roleJpaDao.findAll();
        final RoleResponse roleResponse = new RoleResponse(rolesTbls);
        LOG.info("<< findAll");
        return roleResponse;
    }

    /**
     *
     * @param roleId
     * @return boolean
     */
    @Override
    public boolean deleteById(final String roleId) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.roleJpaDao.delete(roleId);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {roleId};
                throw new XMObjectNotFoundException("User not found", "P_ERR0020", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param roleIds
     * @param httpServletRequest
     * @return RoleResponse
     */
    @Override
    public RoleResponse multiDelete(final Set<String> roleIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        roleIds.forEach(roleId -> {
            RolesTbl rolesTbl = null;
            try {
                rolesTbl = findById(roleId);
                this.deleteById(roleId);
                this.roleAuditMgr.roleDeleteStatusAuditor(roleId,
                        httpServletRequest, true, rolesTbl);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
                this.roleAuditMgr.roleDeleteStatusAuditor(roleId,
                        httpServletRequest, false, rolesTbl);
            }
        });
        RoleResponse roleResponse = new RoleResponse(statusMaps);
        LOG.info(">> multiDelete");
        return roleResponse;
    }
}
