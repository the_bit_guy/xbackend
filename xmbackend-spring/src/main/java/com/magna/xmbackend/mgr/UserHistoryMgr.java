/**
 * 
 */
package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserHistoryTbl;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserHistoryMgr {
	
	UserHistoryTbl create(final UserHistoryRequest userHistoryRequest, final String userName, final String tkt);
	
	boolean updateStatusToHistory(final String userName, final String tkt);
	
	Iterable<UserHistoryTbl> findAll();
	
	List<Map<String, Object>> findUserStatus(final UserHistoryRequest historyRequest);
	
	List<Map<String, Object>> findUserHistory(final UserHistoryRequest historyRequest);

}
