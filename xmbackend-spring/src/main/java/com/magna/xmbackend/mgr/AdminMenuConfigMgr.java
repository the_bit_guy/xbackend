package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponseWrapper;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponseWrapper;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminMenuConfigMgr {

    /**
     *
     * @param configRequest
     * @param userName
     * @return userName
     */
    AdminMenuConfigTbl save(final AdminMenuConfigRequest configRequest,
            final String userName);

    /**
     *
     * @param configRequestList
     * @param userName
     * @return userName
     */
    AdminMenuConfigResponse multiSave(final List<AdminMenuConfigRequest> configRequestList,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean deleteById(final String id);

    AdminMenuConfigResponse multiDelete(final Set<String> adminMenuConfigIds, HttpServletRequest httpServletRequest);

    /**
     *
     * @return AdminMenuConfigResponse
     */
    AdminMenuConfigResponse findAll();

    /**
     * *
     *
     * @param objectType
     * @param userName
     * @return AdminMenuConfigResponse
     */
    AdminMenuConfigCustomeResponseWrapper findByObjectType(final AdminMenuConfig objectType,
            final String userName);

    /**
     * @param userName
     * @return AdminMenuConfigResponseWrapper
     */
    AdminMenuConfigResponseWrapper findUserBasedConfiguration(final String userName);
}
