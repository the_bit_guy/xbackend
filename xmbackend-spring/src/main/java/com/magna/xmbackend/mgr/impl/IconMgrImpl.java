package com.magna.xmbackend.mgr.impl;

import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.IconJpaDao;
import com.magna.xmbackend.mgr.IconMgr;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.icon.IkonResponse;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class IconMgrImpl implements IconMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(IconMgrImpl.class);
    @Autowired
    private IconJpaDao iconJpaDao;

    @Override
    public final IkonResponse findAll() {
        LOG.info(">> findAll");
        final Iterable<IconsTbl> iconsTbls = this.iconJpaDao.findAll();
        final IkonResponse ikonResponse = new IkonResponse(iconsTbls);
        LOG.info("<< findAll");
        return ikonResponse;
    }

    /**
     *
     * @param ikonRequest
     * @return IconsTbl
     */
    @Override
    public final IconsTbl create(final IkonRequest ikonRequest) {
        LOG.info(">> create");
        final IconsTbl iTIn = this.create2Entity(ikonRequest, false);
        final IconsTbl iTOut = this.iconJpaDao.save(iTIn);
        LOG.info("<< create");
        return iTOut;
    }

    /**
     *
     * @param ikonRequest
     * @param isUpdate
     * @return IconsTbl
     */
    private IconsTbl create2Entity(final IkonRequest ikonRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = ikonRequest.getId();
        }
        final IconsTbl iconsTbl = new IconsTbl(id);
        final String iconName = ikonRequest.getIconName();
        String iconType = ikonRequest.getIconType();
        if (!iconType.equalsIgnoreCase("DEFAULT")) {
            iconType = "CUSTOM";
        }
        iconsTbl.setIconName(iconName);
        iconsTbl.setIconType(iconType);
        return iconsTbl;
    }

    /**
     *
     * @param ikonRequest
     * @return IconsTbl
     */
    @Override
    public final IconsTbl update(final IkonRequest ikonRequest) {
        LOG.info(">> update");
        final IconsTbl iTIn = this.create2Entity(ikonRequest, true);
        final IconsTbl iTOut = this.iconJpaDao.save(iTIn);
        LOG.info("<< update");
        return iTOut;
    }

    /**
     *
     * @param iconName
     * @return IconsTbl
     */
    @Override
    public final IconsTbl findByName(final String iconName) {
        LOG.info(">> findByName");
        final IconsTbl itOut = this.iconJpaDao.findByIconName(iconName);
        LOG.info("<< findByName");
        return itOut;
    }

    /**
     *
     * @param iconId
     * @return IconsTbl
     */
    @Override
    public final IconsTbl findById(final String iconId) {
        LOG.info(">> findById");
        final IconsTbl itOut = this.iconJpaDao.findOne(iconId);
        LOG.info("<< findById");
        return itOut;
    }

    /**
     *
     * @param iconType
     * @return IkonResponse
     */
    @Override
    public final IkonResponse findByType(final String iconType) {
        LOG.info(">> findByType");
        final Iterable<IconsTbl> iconsTbls = this.iconJpaDao.findByIconType(iconType);
        final IkonResponse ikonResponse = new IkonResponse(iconsTbls);
        LOG.info("<< findByType");
        return ikonResponse;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean isIconUsedById(final String id) {
        LOG.info(">> isIconUsedById");
        final IconsTbl iconsTbl = this.findById(id);
        final boolean isUsed = this.isIconUsed(iconsTbl);
        LOG.info("<< isIconUsedById");
        return isUsed;
    }

    /**
     *
     * @param name
     * @return boolean
     */
    @Override
    public boolean isIconUsedByName(final String name) {
        LOG.info(">> isIconUsedByName");
        final IconsTbl iconsTbl = this.findByName(name);
        final boolean isUsed = this.isIconUsed(iconsTbl);
        LOG.info("<< isIconUsedByName");
        return isUsed;
    }

    /**
     *
     * @param iconsTbl
     * @return boolean
     */
    private boolean isIconUsed(final IconsTbl iconsTbl) {
        boolean isUsed = true;
        if (iconsTbl.getBaseApplicationsTblCollection().isEmpty()
                && iconsTbl.getAdminAreasTblCollection().isEmpty()
                && iconsTbl.getDirectoryTblCollection().isEmpty()
                && iconsTbl.getProjectApplicationsTblCollection().isEmpty()
                && iconsTbl.getProjectsTblCollection().isEmpty()
                && iconsTbl.getSitesTblCollection().isEmpty()
                && iconsTbl.getStartApplicationsTblCollection().isEmpty()
                && iconsTbl.getUserApplicationsTblCollection().isEmpty()
                && iconsTbl.getUsersTblCollection().isEmpty()) {
            isUsed = false;
        }

        return isUsed;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean delete(final String id) {
        boolean isDeleted = true;
        LOG.info(">> delete {}", id);
        final IconsTbl iconsTbl = this.findById(id);
        if(iconsTbl == null){
        	throw new XMObjectNotFoundException("No Object Found", "ERR0022");
        }
        final String iconType = iconsTbl.getIconType();
        if (iconType.equals("DEFAULT") || this.isIconUsed(iconsTbl)) {
//            throw new RuntimeException("Icon cannot be deleted, Reason Default Type or already in use");
            final String[] param = {id};
            throw new XMObjectNotFoundException("Icon cannot be deleted, Reason Default Type or already in use", "I_ERR0001", param);
        }
        this.iconJpaDao.delete(id);
        LOG.info("<< delete");
        return isDeleted;
    }

}
