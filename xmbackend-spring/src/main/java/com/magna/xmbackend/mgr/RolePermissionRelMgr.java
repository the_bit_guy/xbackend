package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.RolePermissionRelTbl;

/**
 * The Interface RolePermissionRelMgr.
 *
 * @author shashwat.anand
 */
public interface RolePermissionRelMgr {
	

	/**
	 * Find permission by role id.
	 *
	 * @param roleId the role id
	 * @return the iterable
	 */
	Iterable<RolePermissionRelTbl> findPermissionByRoleId(String roleId);
}
