/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.mgr.PropertyConfigManager;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.enums.LdapKey;
import com.magna.xmbackend.vo.enums.ProjectExpiryKey;
import com.magna.xmbackend.vo.enums.ProjectPopupKey;
import com.magna.xmbackend.vo.enums.Singleton;
import com.magna.xmbackend.vo.enums.SmtpKey;
import com.magna.xmbackend.vo.propConfig.LdapConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectExpiryConfigRequest;
import com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest;
import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;
import com.magna.xmbackend.vo.propConfig.SingletonConfigRequest;
import com.magna.xmbackend.vo.propConfig.SmtpConfigRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class PropertyConfigManagerImpl implements PropertyConfigManager {

	private static final Logger LOG = LoggerFactory.getLogger(PropertyConfigManagerImpl.class);

	@Autowired
	private PropertyConfigJpaDao propertyConfigJpaDao;

	@Override
	public PropertyConfigResponse findLdapDetails() {
		Iterable<PropertyConfigTbl> ldapDetail = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.LDAP.toString());
		PropertyConfigResponse propertyConfigResponse = new PropertyConfigResponse(ldapDetail);
		return propertyConfigResponse;
	}

	@Override
	public PropertyConfigResponse findSmtpDetails() {
		Iterable<PropertyConfigTbl> smtpDetail = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.SMTP.toString());
		PropertyConfigResponse propertyConfigResponse = new PropertyConfigResponse(smtpDetail);
		return propertyConfigResponse;
	}
	
	
	@Override
	public PropertyConfigResponse findSingletonDetails() {
		Iterable<PropertyConfigTbl> singletonDetail = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.SINGLETON.toString());
		PropertyConfigResponse propertyConfigResponse = new PropertyConfigResponse(singletonDetail);
		return propertyConfigResponse;
	}

	@Override
	public PropertyConfigResponse findProjectPopupDetails() {
		Iterable<PropertyConfigTbl> projPopupDetail = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.PROJECTPOPUP.toString());
		PropertyConfigResponse propertyConfigResponse = new PropertyConfigResponse(projPopupDetail);
		return propertyConfigResponse;
	}

	@Override
	public PropertyConfigResponse findProjectExpiryDetails() {
		Iterable<PropertyConfigTbl> projExpiryDetail = this.propertyConfigJpaDao
				.findByCategory(ConfigCategory.PROJECTEXPIRYDAYS.toString());
		PropertyConfigResponse propertyConfigResponse = new PropertyConfigResponse(projExpiryDetail);
		return propertyConfigResponse;
	}

	@Override
	public PropertyConfigResponse updatePropertyConfigDetails(Object object) {
		Iterable<PropertyConfigTbl> configTblsOut = null;
		try {
			Iterable<PropertyConfigTbl> configTblsIn = convert2Entity(object);
				configTblsOut = this.propertyConfigJpaDao.save(configTblsIn);
		} catch (Exception e) {
			throw new RuntimeException("Exception occured while saving configuration");
		}
		PropertyConfigResponse configResponse = new PropertyConfigResponse(configTblsOut);
		return configResponse;
	}
	
	public Iterable<PropertyConfigTbl> convert2Entity(Object object)throws ClassCastException{
		LOG.info(">> convert2Entity");
		List<PropertyConfigTbl> listOfPropEntys = new ArrayList<>();
		final Date date = new Date();
		if(object instanceof LdapConfigRequest){
			LdapConfigRequest ldapConfigRequest = (LdapConfigRequest)object;
			EnumMap<LdapKey, String> ldapEnumMap = ldapConfigRequest.getLdapKeyValue();
			for(LdapKey ldapKey : ldapEnumMap.keySet()){
				PropertyConfigTbl pcTbl = this.propertyConfigJpaDao
						.findByCategoryAndProperty(ConfigCategory.LDAP.toString(),ldapKey.toString());
				if(null == pcTbl) {
					throw new XMObjectNotFoundException("No property config found with category "+ ConfigCategory.LDAP.toString()+" and property "+ldapKey.toString(), "ERR0022");
				}
				pcTbl.setValue(ldapEnumMap.get(ldapKey));
				pcTbl.setUpdateDate(date);
				listOfPropEntys.add(pcTbl);
			}
		}else if(object instanceof SmtpConfigRequest){
			SmtpConfigRequest smtpConfigRequest = (SmtpConfigRequest)object;
			EnumMap<SmtpKey, String> smtpEnumMap = smtpConfigRequest.getSmtpKeyValue();
			for(SmtpKey smtpKey : smtpEnumMap.keySet()){
				PropertyConfigTbl pcTbl = this.propertyConfigJpaDao
						.findByCategoryAndProperty(ConfigCategory.SMTP.toString(),smtpKey.toString());
				if(null == pcTbl) {
					throw new XMObjectNotFoundException("No property config found with category "+ ConfigCategory.SMTP.toString()+" and property "+smtpKey.toString(), "ERR0022");
				}
				pcTbl.setValue(smtpEnumMap.get(smtpKey));
				pcTbl.setUpdateDate(date);
				listOfPropEntys.add(pcTbl);
			}
		}else if(object instanceof SingletonConfigRequest){
			SingletonConfigRequest singletonConfigRequest = (SingletonConfigRequest)object;
			EnumMap<Singleton, String> singletonEnumMap = singletonConfigRequest.getSingletonKeyValue();
			for(Singleton singleton : singletonEnumMap.keySet()){
				PropertyConfigTbl pcTbl = this.propertyConfigJpaDao
						.findByCategoryAndProperty(ConfigCategory.SINGLETON.toString(),singleton.toString());
				if(null == pcTbl) {
					throw new XMObjectNotFoundException("No property config found with category "+ ConfigCategory.SINGLETON.toString()+" and property "+singleton.toString(), "ERR0022");
				}
				pcTbl.setValue(singletonEnumMap.get(singleton));
				pcTbl.setUpdateDate(date);
				listOfPropEntys.add(pcTbl);
			}
		}else if(object instanceof ProjectPopupConfigRequest){
			ProjectPopupConfigRequest projpopupConfigRequest = (ProjectPopupConfigRequest)object;
			EnumMap<ProjectPopupKey, String> projPopupEnumMap = projpopupConfigRequest.getProjectPopupKeyValue();
			for(ProjectPopupKey projectPopupKey : projPopupEnumMap.keySet()){
				PropertyConfigTbl pcTbl = this.propertyConfigJpaDao
						.findByCategoryAndProperty(ConfigCategory.PROJECTPOPUP.toString(),projectPopupKey.toString());
				if(null == pcTbl) {
					throw new XMObjectNotFoundException("No property config found with category "+ ConfigCategory.PROJECTPOPUP.toString()+" and property "+projectPopupKey.toString(), "ERR0022");
				}
				pcTbl.setValue(projPopupEnumMap.get(projectPopupKey));
				pcTbl.setUpdateDate(date);
				listOfPropEntys.add(pcTbl);
			}
		}else if(object instanceof ProjectExpiryConfigRequest){
			ProjectExpiryConfigRequest projexpiryConfigRequest = (ProjectExpiryConfigRequest)object;
			EnumMap<ProjectExpiryKey, String> projexpiryEnumMap = projexpiryConfigRequest.getProjectExpiryKeyValue();
			for(ProjectExpiryKey projectExpiryKey : projexpiryEnumMap.keySet()){
				PropertyConfigTbl pcTbl = this.propertyConfigJpaDao
						.findByCategoryAndProperty(ConfigCategory.PROJECTEXPIRYDAYS.toString(),projectExpiryKey.toString());
				if(null == pcTbl) {
					throw new XMObjectNotFoundException("No property config found with category "+ ConfigCategory.PROJECTEXPIRYDAYS.toString()+" and property "+projectExpiryKey.toString(), "ERR0022");
				}
				pcTbl.setValue(projexpiryEnumMap.get(projectExpiryKey));
				pcTbl.setUpdateDate(date);
				listOfPropEntys.add(pcTbl);
			}
		}
		
		LOG.info("<< convert2Entity");
		return listOfPropEntys;
	}

	
}
