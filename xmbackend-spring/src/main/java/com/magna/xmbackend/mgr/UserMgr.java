package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;

/**
 *
 * @author vijay
 */
public interface UserMgr {

    /**
     *
     * @param validationRequest
     * @return UserResponse
     */
    public UserResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return UsersTbl
     */
    public UsersTbl findById(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UsersTbl
     */
    public UsersTbl findById(final String id);

    /**
     *
     * @param startWith
     * @param validationRequest
     * @return UserResponse which matches startwith
     */
    public UserResponse findUserNameStartWith(final String startWith,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserResponse
     */
    UserResponse findUsersByProjectId(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserResponse
     */
    UserResponse findUsersByUserAppId(final String id);

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserResponse
     */
    UserResponse findUsersByProjectAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserResponse
     */
    UserResponse findUsersByStartAppId(final String id);

    /**
     * Find by name.
     *
     * @param name the name
     * @param validationRequest
     * @return the users tbl
     */
    UsersTbl findByName(final String name,
            final ValidationRequest validationRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the users tbl
     */
    UsersTbl findByName(final String name);

    /**
     * Creates the.
     *
     * @param userRequest the user request
     * @return the users tbl
     */
    UsersTbl create(final UserRequest userRequest);

    /**
     * Create Batch the.
     *
     * @param userRequests the user request
     * @return the users tbl
     */
    UserResponse createBatch(final List<UserRequest> userRequests);

    /**
     * Update.
     *
     * @param userRequest the user request
     * @return the users tbl
     */
    UsersTbl update(final UserRequest userRequest);

    /**
     * Update by id.
     *
     * @param status the status
     * @param id the id
     * @return true, if successful
     */
    boolean updateById(final String status, final String id);

    /**
     * Delete by id.
     *
     * @param id the id
     * @return true, if successful
     */
    boolean deleteById(final String id);
    
    /**
     * 
     * @param userIds
     * @return UserResponse
     */
    UserResponse multiDelete(final Set<String> userIds, HttpServletRequest httpServletRequest);
    

    /**
     *
     * @param adminAreaId
     * @return UserResponse
     */
    UserResponse findUsersByAAId(final String adminAreaId);

    /**
     * Find users by admin area id.
     *
     * @param adminAreaId the admin area id
     * @param validationRequest the validation request
     * @return the user response
     */
    UserResponse findUsersByAdminAreaId(final String adminAreaId,
            final ValidationRequest validationRequest);

    /**
     * Find user by site id.
     *
     * @param siteId the site id
     * @param validationRequest the validation request
     * @return the user response
     */
    UserResponse findUserBySiteId(final String siteId,
            final ValidationRequest validationRequest);
}
