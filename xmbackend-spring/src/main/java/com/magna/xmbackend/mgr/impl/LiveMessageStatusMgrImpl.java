/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.LiveMessageStatusTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.rel.dao.LiveMessageStatusJpaDao;
import com.magna.xmbackend.mgr.LiveMessageStatusMgr;
import com.magna.xmbackend.vo.enums.LiveMessageStatus;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class LiveMessageStatusMgrImpl implements LiveMessageStatusMgr {
	
	private static Logger LOG = LoggerFactory.getLogger(LiveMessageStatusMgrImpl.class);
	@Autowired
	LiveMessageStatusJpaDao liveMessageStatusJpaDao;

	
	@Override
	public LiveMessageStatusTbl create(LiveMessageStatusRequest liveMessageStatusRequest) {
		LOG.info(">> LiveMessageStatus create");
		final LiveMessageStatusTbl liveMsgStatusTblIn = this.convert2Entity(liveMessageStatusRequest, false);
		final LiveMessageStatusTbl liveMsgStatusOut = this.liveMessageStatusJpaDao.save(liveMsgStatusTblIn);
		LOG.info("<< LiveMessageStatus create");
		return liveMsgStatusOut;
	}

	
	private LiveMessageStatusTbl convert2Entity(LiveMessageStatusRequest liveMessageStatusRequest, boolean isUpdate){
		LOG.info(">> convert2Entity");
		String id = UUID.randomUUID().toString();
		if(isUpdate){
			id = liveMessageStatusRequest.getLiveMessageStatusId();
		}
		
		final String liveMessageId = liveMessageStatusRequest.getLiveMessageId();
		final String userId = liveMessageStatusRequest.getUserId();
		final String status = LiveMessageStatus.READ.name();
		
		Date date = new Date();
		LiveMessageStatusTbl liveMessageStatusTbl = new LiveMessageStatusTbl(id);
		liveMessageStatusTbl.setCreateDate(date);
		liveMessageStatusTbl.setUpdateDate(date);
		liveMessageStatusTbl.setLiveMessageId(new LiveMessageTbl(liveMessageId));
		liveMessageStatusTbl.setUserId(new UsersTbl(userId));
		liveMessageStatusTbl.setStatus(status);
		
		LOG.info("<< convert2Entity");
		return liveMessageStatusTbl;
	}
	
}
