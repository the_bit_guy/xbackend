package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dhana
 */
public interface BaseApplicationMgr {

    /**
     *
     * @param validationRequest
     * @return BaseApplicationResponse
     */
    BaseApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl findById(final String id);

    /**
     *
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl create(final BaseApplicationRequest baseApplicationRequest);

    /**
     *
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    BaseApplicationsTbl update(final BaseApplicationRequest baseApplicationRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean deleteById(final String id);

    /**
     *
     * @param baseAppIds
     * @param httpServletRequest
     * @return BaseApplicationResponse
     */
    BaseApplicationResponse multiDelete(final Set<String> baseAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the base applications tbl
     */
    BaseApplicationsTbl findByName(String name);
}
