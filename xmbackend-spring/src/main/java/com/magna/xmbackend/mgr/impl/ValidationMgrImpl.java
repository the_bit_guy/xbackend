package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.RolePermissionRelJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.mgr.ValidationMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.PermissionFilter;
import com.magna.xmbackend.validation.vo.ValidationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 *
 * @author vijay
 */
@Component
public class ValidationMgrImpl implements ValidationMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ValidationMgrImpl.class);

    @Autowired
    PermissionFilter permissionFilter;

    @Autowired
    PermissionMgr permissionMgr;
    @Autowired
    RoleUserRelJpaDao roleUserRelJpaDao;
    @Autowired
    UserJpaDao userJpaDao;
    @Autowired
    RolePermissionRelJpaDao rolePermissionRelJpaDao;
    @Autowired
    private MessageMaker messageMaker;

    /**
     *
     * @param validationRequest
     * @return boolean
     */
    @Override
    public boolean isObjectAccessAllowed(final ValidationRequest validationRequest) {
        LOG.info(">> isObjectAccessAllowed {}", validationRequest);
        final boolean isAccessAllowed = validateObject(validationRequest);
        LOG.info("<< isObjectAccessAllowed={}", isAccessAllowed);
        return isAccessAllowed;
    }

    /**
     * Invoke permission manager and validate the object
     *
     * @param validationRequest
     * @return boolean
     */
    private boolean validateObject(final ValidationRequest validationRequest) {
        boolean isAccessAllowed = false;
        final PermissionTbl permissionTbl = permissionMgr.isObjectAccessAllowed(validationRequest);
        if (null != permissionTbl
                && null != permissionTbl.getName()) {
            isAccessAllowed = true;
        }
        return isAccessAllowed;
    }

    /**
     * clears the cache
     */
    @Override
    public boolean clearCache() {
        boolean isCacheClear = false;
        if (!permissionFilter.getObjectPerMap().isEmpty()) {
            permissionFilter.clearCache();
            isCacheClear = true;
        }
        return isCacheClear;
    }

	/*@Override
	public ValidationResponse checkHotlinePermissions(String username) {
		
		boolean permissionCheck = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
		Set<String> permissisonNames = new HashSet<>();
		List<RoleUserRelTbl> roleUserTbls = this.roleUserRelJpaDao.findByUserId(usersTbl);
		Set<RolesTbl> roleTbls = new HashSet<>();
		for (RoleUserRelTbl roleUserRelTbl : roleUserTbls) {
			roleTbls.add(roleUserRelTbl.getRoleId());
		}

		Iterable<RolePermissionRelTbl> rolePermissionTbls = this.rolePermissionRelJpaDao.findByRoleIdIn(roleTbls);
		for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionTbls) {
			PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
			permissisonNames.add(permissionTbl.getName());
		}
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		if (permissisonNames.contains("VIEW_INACTIVE")) {
			try {
				checkFor9Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		} else {
			try {
				checkFor6Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		}
		ValidationResponse vr = new ValidationResponse(permissionCheck, statusMaps);
		return vr;
	}*/
	
	
	
	void checkFor6Permissions(Set<String> permissisonNames) {
		Set<String> requiredPermissions = new HashSet<>();
		Set<String> totPermissions = getInitial6Permissions();
		
		for (String permission : totPermissions) {
			if (!permissisonNames.contains(permission)) {
				requiredPermissions.add(permission);
			}
		}
		if(requiredPermissions.size() > 0) {
			String commaSeparatedUsernames = String.join(",", requiredPermissions);
			String[] param = {commaSeparatedUsernames};
			throw new XMObjectNotFoundException("Permissions not found", "PRM_ERR001", param);
		}
	}
	
	
	
	void checkFor9Permissions(Set<String> permissisonNames) {
		
		Set<String> requiredPermissions = new HashSet<>();
		Set<String> totPermissions = getInitial6Permissions();
		totPermissions.add("USER_TO_PROJECT-INACTIVE_ASSIGNMENT"); 
		totPermissions.add("USER_APPLICATION_TO_USER-INACTIVE_ASSIGNMENT"); 
		totPermissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-INACTIVE_ASSIGNMENT");
		
		for (String permission : totPermissions) {
			if (!permissisonNames.contains(permission)) {
				requiredPermissions.add(permission);
			}
		}
		
		if(requiredPermissions.size() > 0) {
			String commaSeparatedUsernames = String.join(",", requiredPermissions);
			String[] param = {commaSeparatedUsernames};
			throw new XMObjectNotFoundException("Permissions not found", "PRM_ERR001", param);
		}
	}
	
	
	
	Set<String> getInitial6Permissions() {
		Set<String> first6Permissions = new HashSet<String> ();
		first6Permissions.add("USER_TO_PROJECT-ASSIGN");
		first6Permissions.add("USER_TO_PROJECT-REMOVE");
		first6Permissions.add("USER_APPLICATION_TO_USER-REMOVE");
		first6Permissions.add("USER_APPLICATION_TO_USER-ASSIGN");
		first6Permissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-ASSIGN");
		first6Permissions.add("PROJECT_APPLICATION_TO_USER_PROJECT-REMOVE");
		return first6Permissions;
	}

	@Override
	public ValidationResponse checkHotlinePermissions(String username, String adminAreaId) {
		boolean permissionCheck = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
		Set<String> permissisonNames = new HashSet<>();
		List<RoleUserRelTbl> roleUserTbls = this.roleUserRelJpaDao.findByUserId(usersTbl);
		Set<RolesTbl> roleTbls = new HashSet<>();
		Set<RolesTbl> roleTblsWithoutRoleAA = new HashSet<>();
		
		
		for (RoleUserRelTbl roleUserRelTbl : roleUserTbls) {
			RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleUserRelTbl.getRoleAdminAreaRelId();
			//If user is in any role-admin-area then consider only those role-admin-area
			if(null != roleAdminAreaRelTbl) {
				String adminAreaIdfromTbl = roleAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
				if(adminAreaIdfromTbl.equals(adminAreaId)){
					roleTbls.add(roleUserRelTbl.getRoleId());
				}
			} else {
				roleTblsWithoutRoleAA.add(roleUserRelTbl.getRoleId());
			}
		}
		
		//getting all roles
		roleTbls.addAll(roleTblsWithoutRoleAA);
		Iterable<RolePermissionRelTbl> rolePermissionTblsWithoutRAA = this.rolePermissionRelJpaDao.findByRoleIdIn(roleTbls);
		for (RolePermissionRelTbl rolePermissionRelTbl : rolePermissionTblsWithoutRAA) {
			PermissionTbl permissionTbl = rolePermissionRelTbl.getPermissionId();
			permissisonNames.add(permissionTbl.getName());
		}
		
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		if (permissisonNames.contains("VIEW_INACTIVE")) {
			try {
				checkFor9Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		} else {
			try {
				checkFor6Permissions(permissisonNames);
				permissionCheck = true;
			} catch (XMObjectNotFoundException exception) {
				Map<String, String> statusMap = messageMaker.extractFromException(exception);
				statusMaps.add(statusMap);
			}
		}
		ValidationResponse vr = new ValidationResponse(permissionCheck, statusMaps);
		return vr;
		
	}
}
