package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.UserAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaUserAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserUserAppRelJpaDao;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;
import com.magna.xmbackend.vo.user.UserTranslation;

@Component
public class UserMgrImpl implements UserMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserMgrImpl.class);

    @Autowired
    private UserJpaDao userJpaDao;

    @Autowired
    private UserApplicationMgr userApplicationMgr;

    @Autowired
    private StartApplicationMgr startApplicationMgr;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private AdminAreaUserAppRelJpaDao adminAreaUserAppRelJpaDao;

    @Autowired
    private UserUserAppRelJpaDao userUserAppRelJpaDao;

    @Autowired
    AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private Validator validator;

    @Autowired
    private MessageMaker messageMaker;
    
    @Autowired
    private UserAuditMgr userAuditMgr;

    /**
     *
     * @param validationRequest
     * @return UserResponse
     */
    @Override
    public final UserResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<UsersTbl> usersTbls = this.userJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        usersTbls = validator.filterUserResponse(isViewInactive, usersTbls);
        final UserResponse userResponse = new UserResponse(usersTbls);
        LOG.info("<< findAll");
        return userResponse;
    }

    /**
     *
     * @param startWith
     * @param validationRequest
     * @return UserResponse
     */
    @Override
    public final UserResponse findUserNameStartWith(final String startWith,
            final ValidationRequest validationRequest) {
        LOG.info(">> findUserNameStartWith");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserNameStartWith isViewInactive={}", isViewInactive);
        Iterable<UsersTbl> usersTbls = this.userJpaDao.findUserNameStartWith(startWith.concat("%"));
        usersTbls = validator.filterUserResponse(isViewInactive, usersTbls);
        final UserResponse userResponse = new UserResponse(usersTbls);
        LOG.info("<< findUserNameStartWith");
        return userResponse;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserResponse
     */
    @Override
    public UserResponse findUsersByProjectId(final String id, final ValidationRequest validationRequest) {
        UserResponse userResponse = null;
        LOG.info(">> findUsersByProjectId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUsersByProjectId isViewInactive={}", isViewInactive);
        ProjectsTbl projectsTbl = validator.validateProject(id, isViewInactive);
        if (null != projectsTbl) {
            Collection<UserProjectRelTbl> userProjectRelTblCollection = projectsTbl.getUserProjectRelTblCollection();
            userProjectRelTblCollection = validator.filterUserProjectRel(isViewInactive, userProjectRelTblCollection);
            if (userProjectRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No User Project relationship found", "U_ERR0009");
            }
            final Collection<UsersTbl> usersTbls = new ArrayList<>();
            for (UserProjectRelTbl userProjectRelTbl : userProjectRelTblCollection) {
                UsersTbl usersTbl = userProjectRelTbl.getUserId();
                usersTbl = validator.filterUserResponse(isViewInactive, usersTbl);
                if (null != usersTbl && null != usersTbl.getUserId()) {
                    usersTbls.add(usersTbl);
                }
            }
            if (usersTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
            }
            userResponse = new UserResponse(usersTbls);
        } else {
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0007");
        }
        LOG.info("<< findUsersByProjectId");
        return userResponse;
    }

    /**
     *
     * @param id
     * @return UserResponse
     */
    @Override
    public final UserResponse findUsersByUserAppId(final String id) {
        UserResponse userResponse = null;
        LOG.info(">> findUsersByUserAppId {}", id);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(id);
        if (null != userApplicationsTbl) {
            final Collection<UserUserAppRelTbl> userUserAppRelTblCollection = userApplicationsTbl.getUserUserAppRelTblCollection();
            if (userUserAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No User User Application Relation found", "U_ERR0008");
            }
            final Set<UsersTbl> usersTbls = new HashSet<>();
            for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTblCollection) {
                final UsersTbl usersTbl = userUserAppRelTbl.getUserId();
                usersTbls.add(usersTbl);
            }
            if (usersTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
            }
            userResponse = new UserResponse(usersTbls);
        } else {
            throw new XMObjectNotFoundException("No User Applications found", "U_ERR0007");
        }
        LOG.info("<< findUsersByUserAppId ");
        return userResponse;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return UserResponse
     */
    @Override
    public final UserResponse findUsersByProjectAppId(final String id,
            final ValidationRequest validationRequest) {
        UserResponse userResponse = null;
        LOG.info(">> findUsersByProjectAppId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUsersByProjectAppId isViewInactive={}", isViewInactive);
        final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(id, isViewInactive);
        final Collection<UserProjAppRelTbl> userProjAppRelTblCollection = projectApplicationsTbl.getUserProjAppRelTblCollection();
        if (userProjAppRelTblCollection.isEmpty()) {
            throw new XMObjectNotFoundException("No User Project Application Relation found", "U_ERR0006");
        }
        final Collection<UsersTbl> usersTbls = new ArrayList<>();
        // Store the user id in the list - to avoid duplicate user id 
        final List<String> userIdList = new ArrayList<>();
        for (UserProjAppRelTbl userProjAppRelTbl : userProjAppRelTblCollection) {
            UserProjectRelTbl userProjectRelTbl = userProjAppRelTbl.getUserProjectRelId();
            userProjectRelTbl = validator.filterUserProjectResponse(isViewInactive, userProjectRelTbl);
            UsersTbl usersTbl = userProjectRelTbl.getUserId();
            if (null != usersTbl) {
                final String userId = usersTbl.getUserId();
                // Check user id is in the list, if it is dont add 
                if (null != userId && !userIdList.contains(userId)) {
                    userIdList.add(userId);
                    usersTbl = validator.filterUserResponse(isViewInactive, usersTbl);
                    if (null != usersTbl && null != usersTbl.getUserId()) {
                        usersTbls.add(usersTbl);
                    }
                }
            }
        }
        // Clear the list
        userIdList.clear();
        if (usersTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
        }
        userResponse = new UserResponse(usersTbls);
        LOG.info("<< findUsersByProjectAppId");
        return userResponse;
    }

    /**
     *
     * @param id
     * @return UserResponse
     */
    @Override
    public final UserResponse findUsersByStartAppId(final String id) {
        UserResponse userResponse = null;
        LOG.info(">> findUsersByStartAppId {}", id);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationMgr.findById(id);
        if (null != startApplicationsTbl) {
            final Collection<UserStartAppRelTbl> userStartAppRelTblCollection = startApplicationsTbl.getUserStartAppRelTblCollection();
            if (userStartAppRelTblCollection.isEmpty()) {
                throw new XMObjectNotFoundException("No User Start Application Relation found", "U_ERR0002");
            }
            final Collection<UsersTbl> usersTbls = new ArrayList<>();
            for (UserStartAppRelTbl userStartAppRelTbl : userStartAppRelTblCollection) {
                final UsersTbl usersTbl = userStartAppRelTbl.getUserId();
                usersTbls.add(usersTbl);
            }
            if (usersTbls.isEmpty()) {
                throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
            }
            userResponse = new UserResponse(usersTbls);
        } else {
            throw new XMObjectNotFoundException("No Start Applications found", "U_ERR0004");
        }
        LOG.info("<< findUsersByStartAppId");
        return userResponse;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return UsersTbl
     */
    @Override
    public UsersTbl findById(final String id,
            final ValidationRequest validationRequest) {
        LOG.info(">> findById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findById isViewInactive={}", isViewInactive);
        UsersTbl usersTbl = this.userJpaDao.findOne(id);
        usersTbl = validator.filterUserResponse(isViewInactive, usersTbl);
        LOG.info("<< findById");
        return usersTbl;
    }

    /**
     *
     * @param id
     * @return UsersTbl
     */
    @Override
    public UsersTbl findById(final String id) {
        LOG.info(">> findById {}", id);
        final UsersTbl usersTbl = this.userJpaDao.findOne(id);
        LOG.info("<< findById");
        return usersTbl;
    }

    /**
     * @param name
     * @param validationRequest
     * @return UsersTbl
     */
    @Override
    public final UsersTbl findByName(final String name,
            final ValidationRequest validationRequest) {
        LOG.info(">> findByName {}", name);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByName isViewInactive={}", isViewInactive);
        UsersTbl userTbls = this.userJpaDao.findByUsername(name);
        userTbls = validator.filterUserResponse(isViewInactive, userTbls);
        LOG.info("<< findByName");
        return userTbls;
    }

    /**
     * @param name
     * @return UsersTbl
     */
    @Override
    public final UsersTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        final UsersTbl userTbls = this.userJpaDao.findByUsername(name);
        if(null == userTbls){
    		String[] param = {name};
    		throw new XMObjectNotFoundException("User not found", "P_ERR0034", param);
    	}
        LOG.info("<< findByName");
        return userTbls;
    }

    /**
     * Creates the.
     *
     * @param userRequest the user request
     * @return the users tbl
     */
    @Override
    public final UsersTbl create(final UserRequest userRequest) {
        LOG.info(">> create");
        final UsersTbl uIn = this.convert2Entity(userRequest, false);
        final UsersTbl uOut = this.userJpaDao.save(uIn);
        LOG.info("<< create");
        return uOut;
    }

    /**
     * Convert 2 entity.
     *
     * @param userRequest the user request
     * @param isUpdate the is update
     * @return the users tbl
     */
    private UsersTbl convert2Entity(final UserRequest userRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = userRequest.getId();
        }
        final Date date = new Date();
        final String userName = userRequest.getUserName();
        final String fullName = userRequest.getFullName();
        final String iconId = userRequest.getIconId();
        final String status = userRequest.getStatus().toString();
        final String email = userRequest.getEmail();
        final String telephoneNumber = userRequest.getTelephoneNumber();
        final String department = userRequest.getDepartment();
        final List<UserTranslation> translations = userRequest.getUserTranslation();

        final UsersTbl usersTbl = new UsersTbl(id);

        usersTbl.setUsername(userName);
        usersTbl.setFullName(fullName);
        if (!isUpdate && null != this.findUserIdForName(userName)) {
            UsersTbl usersTblIn = this.userJpaDao.findByUsernameIgnoreCase(userName);
            Map<String, String> userNames = this.messageMaker.getUserNames(usersTblIn);
            final Map<String, String[]> paramMap = this.messageMaker.geti18nCodeMap(userNames);
            throw new CannotCreateObjectException("User name already found", "ERR0021", paramMap);
        }
        usersTbl.setIconId(new IconsTbl(iconId));
        usersTbl.setStatus(status);
        usersTbl.setEmailId(email);
        usersTbl.setDepartment(department);
        usersTbl.setTelephoneNumber(telephoneNumber);
        usersTbl.setCreateDate(date);
        usersTbl.setUpdateDate(date);
        final List<UserTranslationTbl> utts = new ArrayList<>();

        for (UserTranslation translation : translations) {
            String transId = UUID.randomUUID().toString();
            if (isUpdate) {
                transId = translation.getId();
            }
            final String languageCode = translation.getLanguageCode();
            final String description = translation.getDescription();
            final String remarks = translation.getRemarks();
            final UserTranslationTbl stt = new UserTranslationTbl(transId);
            stt.setCreateDate(date);
            stt.setUpdateDate(date);
            stt.setDescription(description);
            stt.setLanguageCode(new LanguagesTbl(languageCode));
            stt.setRemarks(remarks);
            stt.setUserId(new UsersTbl(id));
            utts.add(stt);
        }
        usersTbl.setUserTranslationTblCollection(utts);
        return usersTbl;
    }

    /**
     * Find user id for name.
     *
     * @param name the name
     * @return the string
     */
    private String findUserIdForName(final String name) {
        UsersTbl userTbl = this.userJpaDao.findByUsernameIgnoreCase(name);

        String userid = null;
        if (userTbl != null) {
            userid = userTbl.getUserId();
        }
        LOG.info(">> findUserIdForName - User id for name is {} - {}",
                name, userid);
        return userid;
    }

    /**
     * Gets the user name map.
     *
     * @param userTbl the user tbl
     * @return the user name map
     */
    @SuppressWarnings("unused")
	private Map<String, String[]> getUserNameMap(final UsersTbl userTbl) {
        final Map<String, String[]> paramMap = new HashMap<>();
        final String name = userTbl.getUsername();
        final String[] param = {name};
        paramMap.put("UserName", param);
        return paramMap;
    }

    /**
     *
     * @param userRequest
     * @return UsersTbl
     */
    @Override
    public final UsersTbl update(final UserRequest userRequest) {
        LOG.info(">> update");
        final UsersTbl uIn = this.convert2Entity(userRequest, true);
        final UsersTbl uOut = this.userJpaDao.save(uIn);
        LOG.info("<< update");

        return uOut;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateById(final String status, final String id) {
        LOG.info(">> updateById {}", id);
        final Date date = new Date();
        boolean isUpdated = false;
        final int out = this.userJpaDao.setStatusAndUpdateDateForUsersTbl(status, date, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.userJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("User not found", "P_ERR0009", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param userIds
     * @return UserResponse
     */
    @Override
    public UserResponse multiDelete(Set<String> userIds, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        userIds.forEach(userId -> {
        	String username = "";
            try {
            	UsersTbl usersTbl = this.findById(userId);
            	if(usersTbl != null){
            		username = usersTbl.getUsername();
            		this.deleteById(userId);
            		this.userAuditMgr.siteDeleteSuccessAudit(httpServletRequest,
            				userId, username);
            	}
                
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        UserResponse userResponse = new UserResponse(statusMaps);
        LOG.info(">> multiDelete");
        return userResponse;
    }

    /**
     *
     * @param adminAreaId
     * @return UserResponse
     */
    @Override
    public UserResponse findUsersByAAId(String adminAreaId) {
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(new AdminAreasTbl(adminAreaId));
        List<UserApplicationsTbl> userApplicationsTbls = new ArrayList<>();
        if (!siteAdminAreaRelTbls.isEmpty()) {
            List<AdminAreaUserAppRelTbl> aauartbls = this.adminAreaUserAppRelJpaDao.findBySiteAdminAreaRelIdIn(siteAdminAreaRelTbls);
            if (!aauartbls.isEmpty()) {
                for (AdminAreaUserAppRelTbl appRelTbl : aauartbls) {
                    userApplicationsTbls.add(appRelTbl.getUserApplicationId());
                }
            }
        }
        List<UserUserAppRelTbl> userUserAppRelTbls = this.userUserAppRelJpaDao.findByUserApplicationIdIn(userApplicationsTbls);
        if (userUserAppRelTbls == null) {
            throw new XMObjectNotFoundException("No Users Found", "U_ERR0003");
        }
        List<UsersTbl> usersTbls = new ArrayList<>();
        for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
            usersTbls.add(userUserAppRelTbl.getUserId());
        }
        UserResponse userResponse = new UserResponse(usersTbls);
        return userResponse;
    }

    /**
     *
     * @param userRequests
     * @return UserResponse
     */
    @Override
    public UserResponse createBatch(List<UserRequest> userRequests) {
        LOG.info(">> createBatch");
        List<UsersTbl> usersTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        for (UserRequest ur : userRequests) {
            try {
                UsersTbl usersTbl = this.convert2Entity(ur, false);
                usersTbls.add(usersTbl);
            } catch (CannotCreateObjectException ccoe) {
                Map<String, String> statusMap = messageMaker.extractFromException(ccoe);
                statusMaps.add(statusMap);
            }

        }
        Iterable<UsersTbl> usersTblsOut = this.userJpaDao.save(usersTbls);
        UserResponse userResponse = new UserResponse(usersTblsOut, statusMaps);
        LOG.info("<< createBatch");
        return userResponse;
    }

    /**
     * Find users by admin area id.
     *
     * @param adminAreaId the admin area id
     * @param validationRequest the validation request
     * @return the user response
     */
    @Override
    public UserResponse findUsersByAdminAreaId(final String adminAreaId,
            final ValidationRequest validationRequest) {
        LOG.info("<< findUsersByAdminAreaId");
        final SortedSet<UsersTbl> userSet = new TreeSet<>();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUsersByAdminAreaId isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isViewInactive);
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Site Admin Area Relation not found", "AA_ERR0016");
        }
        List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRel = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
			SitesTbl siteTbl = validator.filterSiteResponse(isViewInactive, siteAdminAreaRelTbl.getSiteId());
			if (null != siteTbl) {
				filteredSiteAdminAreaRel.add(siteAdminAreaRelTbl);
			}
		}
        List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdIn(filteredSiteAdminAreaRel);
        adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Projects not found", "S_ERR0018");
        }
        for (final AdminAreaProjectRelTbl aaprtbl : adminAreaProjectRelTbls) {
            ProjectsTbl projectsTbl = aaprtbl.getProjectId();
            projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
            if (null != projectsTbl) {
                Collection<UserProjectRelTbl> userProjectRelTblCollection = projectsTbl.getUserProjectRelTblCollection();
                if (!userProjectRelTblCollection.isEmpty()) {
                    userProjectRelTblCollection = validator.filterUserProjectRel(isViewInactive, userProjectRelTblCollection);
                    for (UserProjectRelTbl userProjectRelTbl : userProjectRelTblCollection) {
                        final UsersTbl usersTbl = userProjectRelTbl.getUserId();
                        userSet.add(usersTbl);
                    }
                }
            }
        }
        if (userSet.isEmpty()) {
            throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
        }
        Iterable<UsersTbl> usersTbls = validator.filterUserResponse(isViewInactive, userSet);
        final UserResponse userResponse = new UserResponse(usersTbls);
        LOG.info("<< findUsersByAdminAreaId");
        return userResponse;
    }

    /**
     * Find user by site id.
     *
     * @param siteId the site id
     * @param validationRequest the validation request
     * @return the user response
     */
    @Override
    public UserResponse findUserBySiteId(final String siteId, final ValidationRequest validationRequest) {
        LOG.info("<< findUserBySiteId");
        SortedSet<UsersTbl> userSet = new TreeSet<>();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserBySiteId isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(siteId, isViewInactive);
        Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findAdminAreaBySiteId(sitesTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRel = new ArrayList<>();
        for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
        	AdminAreasTbl adminAreasTbl = validator.filterAdminAreaResponse(isViewInactive, siteAdminAreaRelTbl.getAdminAreaId());
			if (null != adminAreasTbl) {
				filteredSiteAdminAreaRel.add(siteAdminAreaRelTbl);
			}
		}
        List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdIn(filteredSiteAdminAreaRel);
        adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Projects not found", "S_ERR0018");
        }
        for (final AdminAreaProjectRelTbl aaprtbl : adminAreaProjectRelTbls) {
            ProjectsTbl projectsTbl = aaprtbl.getProjectId();
            projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
            Collection<UserProjectRelTbl> userProjectRelTblCollection = projectsTbl.getUserProjectRelTblCollection();
            if (!userProjectRelTblCollection.isEmpty()) {
                userProjectRelTblCollection = validator.filterUserProjectRel(isViewInactive, userProjectRelTblCollection);
                for (UserProjectRelTbl userProjectRelTbl : userProjectRelTblCollection) {
                    final UsersTbl usersTbl = userProjectRelTbl.getUserId();
                    userSet.add(usersTbl);
                }
            }
        }
        if (userSet.isEmpty()) {
            throw new XMObjectNotFoundException("No Users found", "U_ERR0003");
        }
        Iterable<UsersTbl> usersTbls = validator.filterUserResponse(isViewInactive, userSet);
        final UserResponse userResponse = new UserResponse(usersTbls);
        LOG.info(">> findUserBySiteId");
        return userResponse;
    }

}
