package com.magna.xmbackend.mgr;

import java.util.Set;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import javax.servlet.http.HttpServletRequest;

public interface ProjectApplicationMgr {

    /**
     *
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl findById(final String id);

    /**
     *
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl create(final ProjectApplicationRequest projectApplicationRequest);

    /**
     *
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl update(final ProjectApplicationRequest projectApplicationRequest);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean deleteById(final String id);

    /**
     *
     * @param projAppIds
     * @param httpServletRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse multiDelete(final Set<String> projAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @param type
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectApplicationsByProjectId(final String id,
            final String type, final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     *
     * @param position
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByPosition(final String position,
            final ValidationRequest validationRequest);

    /**
     *
     * @param status
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAllProjectApplicationsByStatus(final String status);

    /**
     *
     * @param validationRequest
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAllProjectApplicationsByPositions(final ValidationRequest validationRequest);

    /**
     *
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByProjectSiteAAId(final String siteId,
            final String adminAreaId, final String projectId);

    /**
     *
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByUserProjectId(final String userId,
            final String projectId);

    /**
     *
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @param relType
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByProjectSiteAAIdAndRelType(final String siteId,
            final String adminAreaId, final String projectId, final String relType);

    /**
     *
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findFixedProjAppByUserProjectId(final String userId,
            final String projectId);

    /**
     *
     * @param aaId
     * @param projectId
     * @param userName
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserName(final String aaId,
            final String projectId, final String userName);

    /**
     *
     * @param aaId
     * @param projectId
     * @param userId
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(final String aaId,
            final String projectId, final String userId);

    /**
     *
     * @param aaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(final String aaId,
            final String projectId);

    /**
     *
     * @param aaId
     * @param projectId
     * @param userName
     * @param validationRequest
     * @return ProjectApplciationMenuWrapper
     */
    ProjectApplciationMenuWrapper findProjectApplicationsByAAIdAndProjIdAndUserName(final String aaId, final String tkt,
            final String projectId, final String userName,
            final ValidationRequest validationRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the project applications tbl
     */
    ProjectApplicationsTbl findByName(final String name);
}
