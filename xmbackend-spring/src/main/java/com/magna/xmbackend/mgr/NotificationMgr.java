/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;

/**
 *
 * @author dhana
 */
public interface NotificationMgr {

    Iterable<EmailNotifyToUserRelTbl> findAllPendingNotification();

    Iterable<EmailNotificationConfigTbl> findAllNotificationConfig();
    
    NotificationResponse createNotification(NotificationRequest notificationRequest);
    
    NotificationResponse updateNotification(NotificationRequest notificationRequest);
    
    NotificationResponse findByEvent(String event);
}
