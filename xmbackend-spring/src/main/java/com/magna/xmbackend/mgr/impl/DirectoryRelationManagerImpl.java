package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.DirectoryRelAuditMgr;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.DirectoryTranslationTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.DirectoryJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.DirectoryRelJpaDao;
import com.magna.xmbackend.mgr.DirectoryRelationManager;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class DirectoryRelationManagerImpl implements DirectoryRelationManager {

    private static final Logger LOG = LoggerFactory.getLogger(DirectoryRelationManagerImpl.class);

    @Autowired
    DirectoryRelJpaDao directoryRelJpaDao;

    @Autowired
    DirectoryJpaDao directoryJpaDao;

    @Autowired
    private ProjectJpaDao projectJpaDao;

    @Autowired
    private UserJpaDao userJpaDao;

    @Autowired
    private UserApplicationJpaDao userApplicationJpaDao;
    @Autowired

    private ProjectApplicationJpaDao projectApplicationJpaDao;
    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private DirectoryRelAuditMgr directoryRelAuditMgr;

    /**
     *
     * @param directoryRelationRequest
     * @param userName
     * @return DirectoryRelBatchResponse
     */
    @Override
    public DirectoryRelBatchResponse createBatch(final DirectoryRelBatchRequest directoryRelationRequest,
            final String userName) {
        LOG.info(">> createBatch ");
        final List<DirectoryRefTbl> directoryRefTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<DirectoryRelRequest> directoryRelRequests = directoryRelationRequest.getDirectoryRelRequests();
        for (DirectoryRelRequest relRequest : directoryRelRequests) {
            try {
                final DirectoryRefTbl drtOut = this.create(relRequest);
                directoryRefTbls.add(drtOut);

                /*final Map<String, Object> assignmentAllowedMap
                        = isInActiveAssignmentsAllowed(relRequest, userName);
                LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
                if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
                    boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
                    if (isAssignmentAllowed) {
                        final DirectoryRefTbl drtOut = this.create(relRequest);
                        directoryRefTbls.add(drtOut);
                    } else {
                        formInactiveAssignmentsErrorMessage(relRequest, assignmentAllowedMap);
                    }
                }*/
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final DirectoryRelBatchResponse directoryRelBatchResponse = new DirectoryRelBatchResponse(directoryRefTbls, statusMaps);
        LOG.info("<< createBatch ");
        return directoryRelBatchResponse;
    }

    /**
     *
     * @param relRequest
     * @param assignmentAllowedMap
     */
    @SuppressWarnings("unused")
	private void formInactiveAssignmentsErrorMessage(final DirectoryRelRequest relRequest,
            final Map<String, Object> assignmentAllowedMap) {
        final DirectoryObjectType directoryObjectType = relRequest.getObjectType();
        switch (directoryObjectType) {
            case PROJECT:
                if (assignmentAllowedMap.containsKey("projectsTbl")) {
                    final Map<String, String> projectTransMap
                            = messageMaker.getProjectNames((ProjectsTbl) assignmentAllowedMap.get("projectsTbl"));
                    final Map<String, String> directoryTransMap = getDirTransNames((DirectoryTbl) assignmentAllowedMap.get("directoryTbl"));
                    final Map<String, String[]> paramMap = this.messageMaker.getDirectoryProjectRelWithi18nCode(directoryTransMap, projectTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "DP_ERR0002", paramMap);
                }
                break;
            case USER:
                if (assignmentAllowedMap.containsKey("usersTbl")) {
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String> directoryTransMap = getDirTransNames((DirectoryTbl) assignmentAllowedMap.get("directoryTbl"));
                    final Map<String, String[]> paramMap = this.messageMaker.getDirectoryUserRelWithi18nCode(directoryTransMap, userTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "DU_ERR0002", paramMap);
                }
                break;
            case USERAPPLICATION:
                if (assignmentAllowedMap.containsKey("userApplicationsTbl")) {
                    final Map<String, String> userAppTransMap
                            = messageMaker.getUserAppNames((UserApplicationsTbl) assignmentAllowedMap.get("userApplicationsTbl"));
                    final Map<String, String> directoryTransMap = getDirTransNames((DirectoryTbl) assignmentAllowedMap.get("directoryTbl"));
                    final Map<String, String[]> paramMap = this.messageMaker.getDirectoryUserAppRelWithi18nCode(directoryTransMap, userAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "DUA_ERR0002", paramMap);
                }
                break;
            case PROJECTAPPLICATION:
                if (assignmentAllowedMap.containsKey("projectApplicationsTbl")) {
                    final Map<String, String> projectAppTransMap
                            = messageMaker.getProjectAppNames((ProjectApplicationsTbl) assignmentAllowedMap.get("projectApplicationsTbl"));
                    final Map<String, String> directoryTransMap = getDirTransNames((DirectoryTbl) assignmentAllowedMap.get("directoryTbl"));
                    final Map<String, String[]> paramMap = this.messageMaker.getDirectoryProjAppRelWithi18nCode(directoryTransMap, projectAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "DPA_ERR0002", paramMap);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param relRequest
     * @param userName
     * @return Map
     */
    
    @SuppressWarnings("unused")
	private Map<String, Object> isInActiveAssignmentsAllowed(final DirectoryRelRequest relRequest,
            final String userName) {
        Map<String, Object> assignmentAllowedMap = null;
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(relRequest, userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        assignmentAllowedMap
                = validator.isDirectoryInactiveAssignmentAllowed(relRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param relRequest
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final DirectoryRelRequest relRequest,
            final String userName) {
        ValidationRequest validationRequest = null;
        validationRequest = validator.formSaveRelationValidationRequest(userName, "DIRECTORY_RELATION");
        /*final DirectoryObjectType directoryObjectType = relRequest.getObjectType();
        switch (directoryObjectType) {
            case PROJECT:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "DIRECTORY_RELATION");
                break;
            case USER:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "DIRECTORY_RELATION");
                break;
            case USERAPPLICATION:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "DIRECTORY_RELATION");
                break;
            case PROJECTAPPLICATION:
                validationRequest = validator.formSaveRelationValidationRequest(userName, "DIRECTORY_RELATION");
                break;
            default:
                break;
        }*/
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param directoryRelRequest
     * @return DirectoryRefTbl
     */
    @Override
    public DirectoryRefTbl create(DirectoryRelRequest directoryRelRequest) {
        LOG.info(">> create");
        this.isDirectoryRelCreationValid(directoryRelRequest);
        final DirectoryRefTbl drtlIn = this.convert2Entity(directoryRelRequest);
        final DirectoryRefTbl drtOut = directoryRelJpaDao.save(drtlIn);
        LOG.info("<< create");
        return drtOut;
    }

    /**
     *
     * @param directoryRelRequest
     * @return DirectoryRefTbl
     */
    private DirectoryRefTbl convert2Entity(DirectoryRelRequest directoryRelRequest) {
        Date date = new Date();
        final String id = UUID.randomUUID().toString();
        final String directoryId = directoryRelRequest.getDirectoryId();
        DirectoryObjectType dirObjTypeEnum = directoryRelRequest.getObjectType();
        final String dirObjType = dirObjTypeEnum.toString();
        final String objectId = directoryRelRequest.getObjectId();

        final DirectoryRefTbl directoryRefTbl = new DirectoryRefTbl(id);
        directoryRefTbl.setDirectoryId(new DirectoryTbl(directoryId));
        directoryRefTbl.setObjectType(dirObjType);
        directoryRefTbl.setObjectId(objectId);
        directoryRefTbl.setCreateDate(date);
        directoryRefTbl.setUpdateDate(date);
        return directoryRefTbl;
    }

    /**
     *
     * @param directoryRelRequest
     */
    private void isDirectoryRelCreationValid(DirectoryRelRequest directoryRelRequest) {
        LOG.debug(">> isDirectoryRelCreationValid");
        final String objectId = directoryRelRequest.getObjectId();
        final String dirObjecType = directoryRelRequest.getObjectType().toString();
        DirectoryTbl directoryTbl = this.directoryJpaDao.findOne(directoryRelRequest.getDirectoryId());
        checkIfDirectoryRelAlreadyExist(directoryTbl, dirObjecType, objectId);
        LOG.debug("<< isDirectoryRelCreationValid");
    }

    /**
     *
     * @param dirTbl
     * @param dirObjecType
     * @param objId
     * @throws CannotCreateRelationshipException
     */
    private void checkIfDirectoryRelAlreadyExist(final DirectoryTbl dirTbl,
            String dirObjecType, String objId)
            throws CannotCreateRelationshipException {

        final Collection<DirectoryRefTbl> directoryRefTbls = dirTbl.getDirectoryRefTblCollection();
        if (!directoryRefTbls.isEmpty()) {
            final Map<String, String> directoryTransMap = getDirTransNames(dirTbl);
            LOG.debug("dirTransMap={}", directoryTransMap);
            for (DirectoryRefTbl refTbl : directoryRefTbls) {
                if (refTbl.getObjectType().equalsIgnoreCase(DirectoryObjectType.PROJECT.toString())
                        && refTbl.getObjectType().equalsIgnoreCase(dirObjecType)) {
                    final ProjectsTbl projectsTbl = this.projectJpaDao.findByProjectId(refTbl.getObjectId());
                    final String projectIdFromProjectsTbl = projectsTbl.getProjectId();
                    if (projectIdFromProjectsTbl.equalsIgnoreCase(objId)) {
                        final Map<String, String> projectTransMap = getProjectTransNames(projectsTbl);
                        LOG.debug("projectTransMap={}", projectTransMap);
                        final Map<String, String[]> paramMap = this.messageMaker.getDirectoryProjectRelWithi18nCode(directoryTransMap, projectTransMap);
                        LOG.debug("paramMap={}", paramMap);
                        throw new CannotCreateRelationshipException("Realtionship exists", "DP_ERR0001", paramMap);
                    }
                }
                if (refTbl.getObjectType().equalsIgnoreCase(DirectoryObjectType.USER.toString())
                        && refTbl.getObjectType().equalsIgnoreCase(dirObjecType)) {
                    final UsersTbl usersTbl = this.userJpaDao.findByUserId(refTbl.getObjectId());
                    final String userIdFromUsersTbl = usersTbl.getUserId();
                    if (userIdFromUsersTbl.equalsIgnoreCase(objId)) {
                        final Map<String, String> userTransMap = getUserTransNames(usersTbl);
                        LOG.debug("userTransMap={}", userTransMap);
                        final Map<String, String[]> paramMap = this.messageMaker.getDirectoryUserRelWithi18nCode(directoryTransMap, userTransMap);
                        LOG.debug("paramMap={}", paramMap);
                        throw new CannotCreateRelationshipException("Realtionship exists", "DU_ERR0001", paramMap);
                    }
                }
                if (refTbl.getObjectType().equalsIgnoreCase(DirectoryObjectType.PROJECTAPPLICATION.toString())
                        && refTbl.getObjectType().equalsIgnoreCase(dirObjecType)) {
                    final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findByProjectApplicationId(refTbl.getObjectId());
                    final String projAppIdFromProjAppTbl = projectApplicationsTbl.getProjectApplicationId();
                    if (projAppIdFromProjAppTbl.equalsIgnoreCase(objId)) {
                        final Map<String, String> projAppTransMap = getProjectAppTransNames(projectApplicationsTbl);
                        LOG.debug("projAppTransMap={}", projAppTransMap);
                        final Map<String, String[]> paramMap = this.messageMaker.getDirectoryProjAppRelWithi18nCode(directoryTransMap, projAppTransMap);
                        LOG.debug("paramMap={}", paramMap);
                        throw new CannotCreateRelationshipException("Realtionship exists", "DPA_ERR0001", paramMap);
                    }
                }
                if (refTbl.getObjectType().equalsIgnoreCase(DirectoryObjectType.USERAPPLICATION.toString())
                        && refTbl.getObjectType().equalsIgnoreCase(dirObjecType)) {
                    final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findByUserApplicationId(refTbl.getObjectId());
                    final String userApplicationIdFromUserAppTbl = userApplicationsTbl.getUserApplicationId();
                    if (userApplicationIdFromUserAppTbl.equalsIgnoreCase(objId)) {
                        final Map<String, String> userAppTransMap = getUserAppTransNames(userApplicationsTbl);
                        LOG.debug("userAppTransMap={}", userAppTransMap);
                        final Map<String, String[]> paramMap = this.messageMaker.getDirectoryUserAppRelWithi18nCode(directoryTransMap, userAppTransMap);
                        LOG.debug("paramMap={}", paramMap);
                        throw new CannotCreateRelationshipException("Realtionship exists", "DUA_ERR0001", paramMap);
                    }
                }
            }
        }
    }

    /**
     *
     * @param projectApplicationsTbl
     * @return Map
     */
    private Map<String, String> getProjectAppTransNames(final ProjectApplicationsTbl projectApplicationsTbl) {
        final Map<String, String> projAppTransMap = new HashMap<>();
        final String projAppName = projectApplicationsTbl.getName();
        final Collection<ProjectAppTranslationTbl> projectAppTranslationTbls = projectApplicationsTbl.getProjectAppTranslationTblCollection();
        for (ProjectAppTranslationTbl translationTbl : projectAppTranslationTbls) {
            final String languageCode = translationTbl.getLanguageCode().getLanguageCode();
            projAppTransMap.put(languageCode, projAppName);
        }
        return projAppTransMap;
    }

    /**
     *
     * @param userApplicationsTbl
     * @return Map
     */
    private Map<String, String> getUserAppTransNames(final UserApplicationsTbl userApplicationsTbl) {
        final Map<String, String> userAppTransMap = new HashMap<>();
        final String userAppName = userApplicationsTbl.getName();
        final Collection<UserAppTranslationTbl> userAppTranslationTbls = userApplicationsTbl.getUserAppTranslationTblCollection();
        for (UserAppTranslationTbl translationTbl : userAppTranslationTbls) {
            final String languageCode = translationTbl.getLanguageCode().getLanguageCode();
            userAppTransMap.put(languageCode, userAppName);
        }
        return userAppTransMap;
    }

    /**
     *
     * @param usersTbl
     * @return Map
     */
    private Map<String, String> getUserTransNames(final UsersTbl usersTbl) {
        final Map<String, String> userTransMap = new HashMap<>();
        final Collection<UserTranslationTbl> userTranslationTbls = usersTbl.getUserTranslationTblCollection();
        final String userName = usersTbl.getUsername();
        for (UserTranslationTbl translationTbl : userTranslationTbls) {
            final String languageCode = translationTbl.getLanguageCode().getLanguageCode();
            userTransMap.put(languageCode, userName);
        }
        return userTransMap;
    }

    /**
     *
     * @param projectsTbl
     * @return Map
     */
    private Map<String, String> getProjectTransNames(final ProjectsTbl projectsTbl) {
        final String projectName = projectsTbl.getName();
        final Map<String, String> projectTransMap = new HashMap<>();
        final Collection<ProjectTranslationTbl> projectTranslationTbls = projectsTbl.getProjectTranslationTblCollection();
        for (ProjectTranslationTbl translationTbl : projectTranslationTbls) {
            final String languageCode = translationTbl.getLanguageCode().getLanguageCode();
            projectTransMap.put(languageCode, projectName);
        }
        return projectTransMap;
    }

    /**
     *
     * @param dirTbl
     * @return Map
     */
    private Map<String, String> getDirTransNames(final DirectoryTbl dirTbl) {
        String dirName = dirTbl.getName();
        final Map<String, String> dirTransMap = new HashMap<>();
        final Collection<DirectoryTranslationTbl> directoryTranslationTbls = dirTbl.getDirectoryTranslationTblCollection();
        for (DirectoryTranslationTbl translationTbl : directoryTranslationTbls) {
            final String languageCode = translationTbl.getLanguageCode().getLanguageCode();
            dirTransMap.put(languageCode.toLowerCase(), dirName);
        }
        return dirTransMap;
    }

    /**
     *
     * @param dirRefIds
     * @return Boolean
     */
    @Override
    public Boolean multiDelete(Set<String> dirRefIds, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        if (dirRefIds.size() > 0) {
            for (String dirRefId : dirRefIds) {
            	DirectoryRefTbl directoryRefTbl = this.directoryRelJpaDao.findOne(dirRefId);
            	if(directoryRefTbl != null) {
            		directoryRelJpaDao.delete(dirRefId);
            		this.directoryRelAuditMgr.directoryRelMultiDeleteSuccessAudit(directoryRefTbl, httpServletRequest);
            	} else {
            		throw new XMObjectNotFoundException("Relation not found", "ERR0003");
            	}
                
            }
        }
        LOG.info("<< multiDelete");
        return true;
    }

    /**
     *
     * @param dirId
     * @param objectType
     * @param userName
     * @return DirectoryRelResponse
     */
    @Override
    public List<DirectoryRelResponse> findDirectoryRelByDirIdAndObjectType(final String dirId,
            final String objectType, final String userName) {
        final List<DirectoryRelResponse> directoryRelResponses = new ArrayList<>();
        final DirectoryObjectType directoryObjectType = DirectoryObjectType.valueOf(objectType);
        final ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setUserName(userName);
        validationRequest.setPermissionName("VIEW_INACTIVE");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findUserBasedConfiguration isInactiveAssignment={}", isInactiveAssignment);
        final List<DirectoryRefTbl> list = this.directoryRelJpaDao.findByDirectoryIdAndObjectType(new DirectoryTbl(dirId), objectType);
        switch (directoryObjectType) {
            case USER:
                //try {
                    for (DirectoryRefTbl directoryRefTbl : list) {
                    	try{
                    		final UsersTbl usersTbl = validator.validateUser(directoryRefTbl.getObjectId(), isInactiveAssignment);
                            final DirectoryRelResponse relResponse = new DirectoryRelResponse();
                            relResponse.setDirectoryRelId(directoryRefTbl.getDirectoryRefId());
                            relResponse.setUsersTbl(usersTbl);
                            directoryRelResponses.add(relResponse);
                    	} catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        
                    }
                /*} catch (XMObjectNotFoundException e) {
                    LOG.info("XMObjectNotFoundException={}", e);
                }*/
                break;
            case PROJECT:
                //try {
                    for (DirectoryRefTbl directoryRefTbl : list) {
                    	try{
                    		final ProjectsTbl projectsTbl = validator.validateProject(directoryRefTbl.getObjectId(), isInactiveAssignment);
                            final DirectoryRelResponse relResponse = new DirectoryRelResponse();
                            relResponse.setDirectoryRelId(directoryRefTbl.getDirectoryRefId());
                            relResponse.setProjectsTbl(projectsTbl);
                            directoryRelResponses.add(relResponse);
                    	} catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        
                    }
                /*} catch (XMObjectNotFoundException e) {
                    LOG.info("XMObjectNotFoundException={}", e);
                }*/
                break;

            case USERAPPLICATION:
               // try {
                    for (DirectoryRefTbl directoryRefTbl : list) {
                    	try{
                    		final UserApplicationsTbl userApplicationsTbl = validator.validateUserApp(directoryRefTbl.getObjectId(), isInactiveAssignment);
                            final DirectoryRelResponse relResponse = new DirectoryRelResponse();
                            relResponse.setDirectoryRelId(directoryRefTbl.getDirectoryRefId());
                            relResponse.setUserApplicationsTbl(userApplicationsTbl);
                            directoryRelResponses.add(relResponse);
                    	}  catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        
                    }
                    /*} catch (XMObjectNotFoundException e) {
                    LOG.info("XMObjectNotFoundException={}", e);
                }*/
                break;

            case PROJECTAPPLICATION:
                //try {
                    for (DirectoryRefTbl directoryRefTbl : list) {
                    	try{
                    		final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(directoryRefTbl.getObjectId(), isInactiveAssignment);
                            final DirectoryRelResponse relResponse = new DirectoryRelResponse();
                            relResponse.setDirectoryRelId(directoryRefTbl.getDirectoryRefId());
                            relResponse.setProjectApplicationsTbl(projectApplicationsTbl);
                            directoryRelResponses.add(relResponse);
                    	} catch (XMObjectNotFoundException e) {
                            LOG.info("XMObjectNotFoundException={}", e);
                        }
                        
                    }
               /* } catch (XMObjectNotFoundException e) {
                    LOG.info("XMObjectNotFoundException={}", e);
                }*/
                break;
            default:
                break;
        }
        return directoryRelResponses;
    }

}
