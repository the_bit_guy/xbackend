/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.PermissionJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.AppAuthorizationMgr;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AppAuthorizationMgrImpl implements AppAuthorizationMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppAuthorizationMgrImpl.class);
	
	@Autowired
	private UserJpaDao userJpaDao;
	@Autowired
	private RoleUserRelJpaDao roleUserRelJpaDao;
	@Autowired
	private PermissionJpaDao permissionJpaDao;
	

	
	
	@Override
	public boolean isAppUser(UserCredential userCredential) {
		LOG.info(">> isAppUser");
		boolean isAppUser = false;
		String username = userCredential.getUsername();
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
		if (usersTbl != null) {
			isAppUser = true;
		}
		LOG.info("<< isAppUser");
		return isAppUser;
	}

	@Override
	public boolean isAppUserActive(UserCredential userCredential) {
		LOG.info(">> isAppUserActive");
		boolean isActiveUser = false;
		String username = userCredential.getUsername();
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCaseAndStatus(username, Status.ACTIVE.name());
		if (usersTbl != null) {
			isActiveUser = true;
		}
		LOG.info("<< isAppUserActive");
		return isActiveUser;
	}
	
	@Override
	public boolean checkAdminHotlineBatchAccessPermission(UserCredential userCredential) {
		LOG.info(">> checkAdminHotlineAccessPermission");
		final boolean appUser = this.isAppUser(userCredential);
		Set<String> permissisonNames = new HashSet<>();
		if (appUser) {
			String username = userCredential.getUsername();
			UsersTbl userTbl = this.userJpaDao.findByUsernameIgnoreCase(username);

			if (null != userTbl) {
				List<RoleUserRelTbl> roleUserTbls = this.roleUserRelJpaDao.findByUserId(userTbl);
				roleUserTbls.forEach(roleUsrTbl -> {
					RolesTbl roleTbl = roleUsrTbl.getRoleId();
					Iterable<PermissionTbl> permissionTbls = this.permissionJpaDao
							.findObjectPermissionByRoleId(roleTbl);
					permissionTbls.forEach(permTbl -> {
						permissisonNames.add(permTbl.getName());
					});
				});
				if (!permissisonNames.isEmpty()) {
					if (userCredential.getAppName().equals(Application.CAX_START_ADMIN.name()) || userCredential.getAppName().equals(Application.CAX_ADMIN_MENU.name())) {
						if (permissisonNames.contains("LOGIN_CAX_START_ADMIN")) {
							return true;
						}
					}
					if (userCredential.getAppName().equals(Application.CAX_START_HOTLINE.name())) {
						if (permissisonNames.contains("LOGIN_CAX_START_HOTLINE")) {
							return true;
						}
					}
					if (userCredential.getAppName().equals(Application.CAX_START_BATCH.name())) {
						if (permissisonNames.contains("VIEW_INACTIVE")) {
							return true;
						}
					}
				}
			}
		}

		LOG.info("<< checkAdminHotlineAccessPermission");
		return false;
	}


	
	
	
}
