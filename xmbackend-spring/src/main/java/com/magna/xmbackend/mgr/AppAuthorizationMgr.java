/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.user.UserCredential;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AppAuthorizationMgr {

	public boolean isAppUser(final UserCredential userCredential);
	
	public boolean isAppUserActive(final UserCredential userCredential);
	
	public boolean checkAdminHotlineBatchAccessPermission(final UserCredential userCredential);
}
