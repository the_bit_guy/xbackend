/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.mail.mgr.UserProjectRelPreNotification;

/**
 *
 * @author dhana
 */
@Component
public class UserProjectRelPreNotificationImpl
        implements UserProjectRelPreNotification {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserProjectRelPreNotificationImpl.class);

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailSupportMgr mailSupportMgr;

    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;

    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;
    
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;
    
    
    private static final String STAGE_NEW = "new";

    @Override
    public void postToQueue(UserProjectRelTbl userProjectRelTbl, String event) {
        LOG.info(">>> postToQueue with event {}", event);
        List<UserEventConfigData> userEventConfigDatas
                = this.mailSupportMgr.getUserEventConfigData(event);
        Iterable<MailQueueTbl> mqtsOut = null;

        Map<String, String> eventAttributes = new HashMap<>();
        if (userEventConfigDatas.size() > 0) {
			/*ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
			    Collection<ProjectTranslationTbl> projectTranslationTblCollection
			    = projectsTbl.getProjectTranslationTblCollection();
			    String name = projectsTbl.getName();
			    projectTranslationTblCollection.forEach((projectTranslationTbl) -> {
			LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
			String languageCode = languagesTbl.getLanguageCode();
			
			eventAttributes.put("PROJECT_NAME_" + languageCode, name);
			    });*/
			UsersTbl usersTbl = userProjectRelTbl.getUserId();
			ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
			String projectName = projectsTbl.getName();
			String username = usersTbl.getUsername();
			eventAttributes.put("assignedUserName", username);
			eventAttributes.put("PROJECT_NAME_en", projectName);
			LOG.debug("eventAttribs{}", eventAttributes);
			this.bindTemplate2Data(userEventConfigDatas, eventAttributes, event);
			//store the userEventConfigDatas details in MAIL_QUEUE_TBL
			Collection<MailQueueTbl> mailQueueTbls = this.mailSupportMgr.convert2Entity(userEventConfigDatas);
			mailQueueTbls.addAll(this.addAssignedRemovedUser2QueueIfApplicable(usersTbl, eventAttributes, event));
			mqtsOut = this.mailQueueJpaDao.save(mailQueueTbls);
			LOG.debug("mqtsOut {}", mqtsOut);
		} else {
			final EmailNotificationConfigTbl configTbl = this.emailNotificationConfigJpaDao.findByEvent(event);
			if (configTbl == null) {
				LOG.info("No Message found for even: " + event);
				throw new XMObjectNotFoundException();
			}
			final String sendToAssignedUser = configTbl.getSendToAssignedUser();
			if (!"true".equalsIgnoreCase(sendToAssignedUser)) {
	            return;
	        }
			final UsersTbl usersTbl = userProjectRelTbl.getUserId();
			final ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
			final String emailId = usersTbl.getEmailId();
			if(emailId == null) {
				return;
			}
			String projectName = projectsTbl.getName();
			String username = usersTbl.getUsername();
			String fullName = usersTbl.getFullName();
			eventAttributes.put("assignedUserName", username);
			eventAttributes.put("PROJECT_NAME_en", projectName);
			LOG.debug("eventAttribs{}", eventAttributes);
            List<UserEventConfigData> userEventConfigDataList = new ArrayList<>();
			UserEventConfigData userEventConfigData = new UserEventConfigData();
			userEventConfigData = new UserEventConfigData();
            userEventConfigData.setEventType(event);
            userEventConfigData.setFromEmailId(emailId);
            userEventConfigData.setFullName(fullName);
            userEventConfigData.setIncludeRemarks(configTbl.getIncludeRemarks());
            userEventConfigData.setMessage(configTbl.getMessage());
            userEventConfigData.setProjectExpiryN(configTbl.getProjectExpiryNoticePeriod());
            userEventConfigData.setRetryCount("0");
            userEventConfigData.setStage(STAGE_NEW);
            userEventConfigData.setSubject(configTbl.getSubject());
            userEventConfigData.setToEmailId(emailId);
            userEventConfigData.setUsername(username);
            userEventConfigDataList.add(userEventConfigData);
			
            this.bindTemplate2Data(userEventConfigDataList, eventAttributes, event);
			Collection<MailQueueTbl> mailQueueTbls = this.mailSupportMgr.convert2Entity(userEventConfigDataList);
			mqtsOut = this.mailQueueJpaDao.save(mailQueueTbls);
		}
		LOG.info("<<< postToQueue with event ");
    }

    private Collection<MailQueueTbl> addAssignedRemovedUser2QueueIfApplicable(
            UsersTbl usersTbl, Map<String, String> eventAttributes,
            String event) {
        Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();
        EmailNotificationConfigTbl configTbl
                = this.emailNotificationConfigJpaDao.findByEvent(event);
        String sendToAssignedUser = configTbl.getSendToAssignedUser();
        if (!"true".equalsIgnoreCase(sendToAssignedUser)) {
            return mailQueueTbls;
        }
        Date date = new Date();
        String subject = configTbl.getSubject();
        String message = configTbl.getMessage();
        String userName = usersTbl.getUsername();
        String toMail = usersTbl.getEmailId();
		if (null != toMail) {
			String textMessage = this.mailSupportMgr.bindProjectTemplate2Data(userName,
					eventAttributes.get("PROJECT_NAME_en"), message, event);
			String fromEmailId = this.getSMTPValue("SMTP_EMAIL_ID");
			/*MailQueueTbl mailQueueTbl = new MailQueueTbl(UUID.randomUUID().toString(), userName, toMail, subject,
					textMessage, event, "new", date, date);*/
			MailQueueTbl mailQueueTbl = new MailQueueTbl(UUID.randomUUID().toString(), userName, toMail, fromEmailId, subject, textMessage, event, "new", date, date);
			mailQueueTbls.add(mailQueueTbl);
		}

		return mailQueueTbls;
	}

    private String getSMTPValue(String key) {
        PropertyConfigTbl propertyConfigTbl
                = this.propertyConfigJpaDao
                        .findByCategoryAndProperty("SMTP", key);
        String value = propertyConfigTbl.getValue();
        return value;
    }
    
    private void bindTemplate2Data(
            List<UserEventConfigData> userEventConfigDatas,
            Map<String, String> eventAttributes,
            String event) {
        userEventConfigDatas.forEach((userEventConfigData) -> {
            Context ctx = new Context();
            ctx.setVariable("fullName", userEventConfigData.getUsername());
            ctx.setVariable("assignedUserName",
                    eventAttributes.get("assignedUserName"));
            ctx.setVariable("PROJECT_NAME_en",
                    eventAttributes.get("PROJECT_NAME_en"));
            ctx.setVariable("message", userEventConfigData.getMessage());
            final String htmlContent
                    = this.templateEngine.process(event, ctx);
            LOG.debug("htmlContent {}", htmlContent);
            userEventConfigData.setTextMessage(htmlContent);
        });
    }
    
    
    @SuppressWarnings("unused")
	private void bindTemplate2Data(UserEventConfigData userEventConfigData,
            Map<String, String> eventAttributes,
            String event) {
        //userEventConfigDatas.forEach((userEventConfigData) -> {
            Context ctx = new Context();
            //ctx.setVariable("fullName", userEventConfigData.getUsername());
            ctx.setVariable("assignedUserName",
                    eventAttributes.get("assignedUserName"));
            ctx.setVariable("PROJECT_NAME_en",
                    eventAttributes.get("PROJECT_NAME_en"));
            
            ctx.setVariable("message", userEventConfigData.getMessage());
            final String htmlContent
                    = this.templateEngine.process(event, ctx);
            LOG.debug("htmlContent {}", htmlContent);
            userEventConfigData.setTextMessage(htmlContent);
        //});
    }

}
