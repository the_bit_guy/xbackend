/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import com.magna.xmbackend.vo.CreationInfo;

/**
 *
 * @author dhana
 */
public class UserEventConfigData {

    private String includeRemarks;
    private String message;
    private String textMessage;
    private String stage;
    private String retryCount;
    private String delaySend;
    private String projectExpiryN;
    private String sendToAssigned;
    private String subject;
    private String toEmailId;
    private String fromEmailId;
    private String fullName;
    private String username;
    private String eventType;

    private CreationInfo creationInfo;

    /**
     * @return the includeRemarks
     */
    public String getIncludeRemarks() {
        return includeRemarks;
    }

    /**
     * @param includeRemarks the includeRemarks to set
     */
    public void setIncludeRemarks(String includeRemarks) {
        this.includeRemarks = includeRemarks;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the textMessage
     */
    public String getTextMessage() {
        return textMessage;
    }

    /**
     * @param textMessage the textMessage to set
     */
    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    /**
     * @return the stage
     */
    public String getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(String stage) {
        this.stage = stage;
    }

    /**
     * @return the retryCount
     */
    public String getRetryCount() {
        return retryCount;
    }

    /**
     * @param retryCount the retryCount to set
     */
    public void setRetryCount(String retryCount) {
        this.retryCount = retryCount;
    }

    /**
     * @return the delaySend
     */
    public String getDelaySend() {
        return delaySend;
    }

    /**
     * @param delaySend the delaySend to set
     */
    public void setDelaySend(String delaySend) {
        this.delaySend = delaySend;
    }

    /**
     * @return the projectExpiryN
     */
    public String getProjectExpiryN() {
        return projectExpiryN;
    }

    /**
     * @param projectExpiryN the projectExpiryN to set
     */
    public void setProjectExpiryN(String projectExpiryN) {
        this.projectExpiryN = projectExpiryN;
    }

    /**
     * @return the sendToAssigned
     */
    public String getSendToAssigned() {
        return sendToAssigned;
    }

    /**
     * @param sendToAssigned the sendToAssigned to set
     */
    public void setSendToAssigned(String sendToAssigned) {
        this.sendToAssigned = sendToAssigned;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the toEmailId
     */
    public String getToEmailId() {
        return toEmailId;
    }

    /**
     * @param toEmailId the toEmailId to set
     */
    public void setToEmailId(String toEmailId) {
        this.toEmailId = toEmailId;
    }

    /**
     * @return the fromEmailId
     */
    public String getFromEmailId() {
        return fromEmailId;
    }

    /**
     * @param fromEmailId the fromEmailId to set
     */
    public void setFromEmailId(String fromEmailId) {
        this.fromEmailId = fromEmailId;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

}
