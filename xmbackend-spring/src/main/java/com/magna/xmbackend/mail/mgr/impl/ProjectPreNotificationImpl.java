/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import com.magna.xmbackend.mail.mgr.ProjectPreNotification;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author dhana
 */
@Component
public class ProjectPreNotificationImpl
        implements ProjectPreNotification {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectPreNotificationImpl.class);

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailSupportMgr mailSupportMgr;

    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;

    @Autowired
    private UserProjectRelMgr userProjectRelMgr;

    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;

    @Override
    public void postToQueue(ProjectsTbl projectsTbl, String event) {
        LOG.info(">>--postToQueue with event::{}", event);

        //Get the event config params from the table 
        //EMAIL_NOTIFICATION_CONFIG_TBL and the user data
        List<UserEventConfigData> userEventConfigDatas
                = this.mailSupportMgr.getUserEventConfigData(event);
        if (userEventConfigDatas == null) {
        	return;
        }
        if (userEventConfigDatas.isEmpty()) {
        	return;
        }

        Map<String, String> eventAttributes = new HashMap<>();

        String name = projectsTbl.getName();
        Collection<ProjectTranslationTbl> projectTranslationTblCollection
                = projectsTbl.getProjectTranslationTblCollection();
        projectTranslationTblCollection.forEach((projectTranslationTbl) -> {
            LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
            String languageCode = languagesTbl.getLanguageCode();

            eventAttributes.put("PROJECT_NAME_" + languageCode, name);
            String remarks = projectTranslationTbl.getRemarks();
            String remarksDecoded = null;
        	try {
    			if (remarks != null) {
    				remarksDecoded = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
    			}
    		} catch (UnsupportedEncodingException e) {
    			e.printStackTrace();
    		}
            eventAttributes.put("REMARKS_" + languageCode, remarksDecoded);
        });

        LOG.debug("eventAttribs{}", eventAttributes);
        this.bindProjectCreateTemplate2Data(userEventConfigDatas,
                eventAttributes, event);

        //store the userEventConfigDatas details in MAIL_QUEUE_TBL
        Collection<MailQueueTbl> mailQueueTbls
                = this.mailSupportMgr.convert2Entity(userEventConfigDatas);

        mailQueueTbls.addAll(this.addAssignedUsers2QueueIfApplicable(
                projectsTbl, eventAttributes, event));

        Iterable<MailQueueTbl> mqtsOut
                = this.mailQueueJpaDao.save(mailQueueTbls);

        LOG.debug("mqtsOut {}", mqtsOut);
        LOG.info("<<--postToQueue");
    }

    private Collection<MailQueueTbl> addAssignedUsers2QueueIfApplicable(
            ProjectsTbl projectsTbl, Map<String, String> eventAttributes, 
            String event) {
        Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();

        EmailNotificationConfigTbl configTbl
                = emailNotificationConfigJpaDao.findByEvent(event);
        String sendToAssignedUser = configTbl.getSendToAssignedUser();
        if (!"true".equalsIgnoreCase(sendToAssignedUser)) {
            return mailQueueTbls;
        }

        String projectId = projectsTbl.getProjectId();
        UserProjectRelResponse userProjectRelResponse;
        try {
            userProjectRelResponse
                    = this.userProjectRelMgr
                            .findUserProjectRelationByProjectId(projectId);
        } catch (RuntimeException rex) {
            return mailQueueTbls;
        }
        Iterable<UserProjectRelTbl> userProjectRelTbls
                = userProjectRelResponse.getUserProjectRelTbls();
        Date date = new Date();

        String subject = configTbl.getSubject();
        String message = configTbl.getMessage();

        for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
            UsersTbl usersTbl = userProjectRelTbl.getUserId();
            String userName = usersTbl.getUsername();
            String toMail = usersTbl.getEmailId();
            String textMessage
                    = this.mailSupportMgr.bindProjectTemplate2Data(userName,
                            eventAttributes.get("PROJECT_NAME_en"),
                            message, event);
            MailQueueTbl mailQueueTbl
                    = new MailQueueTbl(UUID.randomUUID().toString(),
                            userName, toMail, subject, textMessage,
                            event, "new", date, date);
            mailQueueTbls.add(mailQueueTbl);
        }
        return mailQueueTbls;
    }
    
    private void bindProjectCreateTemplate2Data(
            List<UserEventConfigData> userEventConfigDatas,
            Map<String, String> eventAttributes,
            String event) {
        userEventConfigDatas.forEach((userEventConfigData) -> {
            Context ctx = new Context();
            ctx.setVariable("fullName", userEventConfigData.getUsername());
            ctx.setVariable("PROJECT_NAME_en",
                    eventAttributes.get("PROJECT_NAME_en"));
            String includeRemarks = userEventConfigData.getIncludeRemarks();
            ctx.setVariable("INCLUDE_REMARKS", includeRemarks);
            eventAttributes.put("INCLUDE_REMARKS", includeRemarks);
            if ("true".equalsIgnoreCase(includeRemarks)) {
                ctx.setVariable("REMARKS_en", eventAttributes.get("REMARKS_en"));
                ctx.setVariable("REMARKS_de", eventAttributes.get("REMARKS_de"));
            }
            ctx.setVariable("message", userEventConfigData.getMessage());
            final String htmlContent
                    = this.templateEngine.process(event, ctx);
            LOG.debug("htmlContent {}", htmlContent);
            userEventConfigData.setTextMessage(htmlContent);
        });
    }

}
