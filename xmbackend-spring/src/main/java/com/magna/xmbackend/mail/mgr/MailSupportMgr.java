/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr;

import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.mail.mgr.impl.UserEventConfigData;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public interface MailSupportMgr {

    List<UserEventConfigData> getUserEventConfigData(String event);

    Collection<MailQueueTbl> convert2Entity(
            List<UserEventConfigData> userEventConfigDatas);

    String bindProjectTemplate2Data(String userName, String projectName,
            String message, String event);

    void bindTemplate2Data(
            List<UserEventConfigData> userEventConfigDatas,
            Map<String, String> eventAttributes,
            String event);

    String bindSendMailDirectTemplate2Data(
            Map<String, String> eventAttributes);
}
