/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.mail.mgr.ProjectAARelPreNotification;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author dhana
 */
@Component
public class ProjectAARelPreNotificationImpl 
        implements ProjectAARelPreNotification {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectAARelPreNotificationImpl.class);

/*    @Autowired
    private TemplateEngine templateEngine;*/

    @Autowired
    private MailSupportMgr mailSupportMgr;

    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;

    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;

    @Autowired
    private UserProjectRelMgr userProjectRelMgr;

    @Override
    public void postToQueue(
            AdminAreaProjectRelTbl adminAreaProjectRelTbl, String event) {
        LOG.info(">>> postToQueue");
        List<UserEventConfigData> userEventConfigDatas
                = this.mailSupportMgr.getUserEventConfigData(event);
        if (userEventConfigDatas == null) {
        	return;
        }
        if (userEventConfigDatas.isEmpty()) {
        	return;
        }

        ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
        SiteAdminAreaRelTbl siteAdminAreaRelTbl
                = adminAreaProjectRelTbl.getSiteAdminAreaRelId();

        Map<String, String> eventAttributes
                = this.getEventAttributes(siteAdminAreaRelTbl, projectsTbl);
        LOG.debug("eventAttributes {}", eventAttributes);
        this.mailSupportMgr
                .bindTemplate2Data(userEventConfigDatas,
                        eventAttributes, event);

        Collection<MailQueueTbl> mailQueueTbls
                = this.mailSupportMgr.convert2Entity(userEventConfigDatas);
        mailQueueTbls.addAll(this.addAssignedUsers2QueueIfApplicable(
                adminAreaProjectRelTbl, eventAttributes, event));

        Iterable<MailQueueTbl> mqtsOut
                = this.mailQueueJpaDao.save(mailQueueTbls);

        LOG.debug("mqtsOut {}", mqtsOut);

        LOG.info("<<< postToQueue");
    }

    private Collection<MailQueueTbl> addAssignedUsers2QueueIfApplicable(
            AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            Map<String, String> eventAttributes,
            String event) {
        Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();
        EmailNotificationConfigTbl configTbl
                = emailNotificationConfigJpaDao.findByEvent(event);
        String sendToAssignedUser = configTbl.getSendToAssignedUser();
        if (!"true".equalsIgnoreCase(sendToAssignedUser)) {
            return mailQueueTbls;
        }
        ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();

        String projectId = projectsTbl.getProjectId();
        UserProjectRelResponse userProjectRelResponse;
        try {
            userProjectRelResponse
                    = this.userProjectRelMgr
                            .findUserProjectRelationByProjectId(projectId);
        } catch (RuntimeException rex) {
            return mailQueueTbls;
        }
        Iterable<UserProjectRelTbl> userProjectRelTbls
                = userProjectRelResponse.getUserProjectRelTbls();
        Date date = new Date();

        String subject = configTbl.getSubject();
        String message = configTbl.getMessage();

        for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
            UsersTbl usersTbl = userProjectRelTbl.getUserId();
            String userName = usersTbl.getUsername();
            String toMail = usersTbl.getEmailId();
            String textMessage
                    = this.mailSupportMgr.bindProjectTemplate2Data(userName,
                            eventAttributes.get("PROJECT_NAME_en"),
                            message, event);
            MailQueueTbl mailQueueTbl
                    = new MailQueueTbl(UUID.randomUUID().toString(),
                            userName, toMail, subject, textMessage,
                            event, "new", date, date);
            mailQueueTbls.add(mailQueueTbl);
        }

        return mailQueueTbls;
    }

    private Map<String, String> getEventAttributes(
            SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            ProjectsTbl projectsTbl) {
        Map<String, String> eventAttributes = new HashMap<>();
        AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
        final String adminAreaName = adminAreasTbl.getName();
        final String projectName = projectsTbl.getName();
        Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection
                = adminAreasTbl.getAdminAreaTranslationTblCollection();
        adminAreaTranslationTblCollection.forEach(adminAreaTranslationTbl -> {
            LanguagesTbl languagesTbl
                    = adminAreaTranslationTbl.getLanguageCode();
            String languageCode = languagesTbl.getLanguageCode();
            String remarks = adminAreaTranslationTbl.getRemarks();
            eventAttributes.put("AA_NAME_" + languageCode, adminAreaName);
            eventAttributes.put("AA_REMARKS_" + languageCode, remarks);
        });
        Collection<ProjectTranslationTbl> projectTranslationTblCollection
                = projectsTbl.getProjectTranslationTblCollection();
        projectTranslationTblCollection.forEach(projectTranslationTbl -> {
            LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
            String languageCode = languagesTbl.getLanguageCode();
            String remarks = projectTranslationTbl.getRemarks();
            eventAttributes.put("PROJECT_NAME_" + languageCode, projectName);
            eventAttributes.put("PROJECT_REMARKS_" + languageCode, remarks);
        });

        return eventAttributes;
    }

}
