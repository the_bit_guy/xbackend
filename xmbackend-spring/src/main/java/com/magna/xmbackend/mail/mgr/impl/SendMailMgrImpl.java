/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.mail.mgr.SendMailMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.notification.SendMailRequest;
import com.magna.xmbackend.vo.notification.SendMailResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class SendMailMgrImpl implements SendMailMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SendMailMgrImpl.class);

    @Autowired
    private UserMgr userMgr;
    @Autowired
    private MailSupportMgr mailSupportMgr;
    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;
    @Autowired
    private MessageMaker messageMaker;

    @Override
	public SendMailResponse sendMail(SendMailRequest sendMailRequest) {
		LOG.info(">> sendMail");
		List<String> usersToNotify = sendMailRequest.getUsersToNotify();
		String subject = sendMailRequest.getSubject();
		String message = sendMailRequest.getMessage();
		Date date = new Date();
		List<String> invalidUserNames = new ArrayList<>();
		List<MailQueueTbl> mailQueueTbls = new ArrayList<>();
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		SendMailResponse ack = this.successAck(statusMaps);
		usersToNotify.forEach(userName -> {
			Map<String, String> eventAttributes = new HashMap<>();
			UsersTbl usersTbl = this.userMgr.findByName(userName);
			if (usersTbl == null) {
				invalidUserNames.add(userName);
			} else {
				eventAttributes.put("USERNAME", userName);
				eventAttributes.put("SUBJECT", subject);
				eventAttributes.put("MESSAGE", message);
				String fullName = usersTbl.getFullName();
				String emailId = usersTbl.getEmailId();
				eventAttributes.put("FULLNAME", fullName);
				eventAttributes.put("EMAILID", emailId);

				String textMsg = this.mailSupportMgr.bindSendMailDirectTemplate2Data(eventAttributes);

				MailQueueTbl mailQueueTbl = new MailQueueTbl(UUID.randomUUID().toString(), userName, emailId, subject,
						textMsg, "DIRECT_MAIL_SEND", "new", date, date);
				mailQueueTbl.setDelaySend("false");
				mailQueueTbl.setFromMail(this.getSMTPValue("SMTP_EMAIL_ID"));
				mailQueueTbl.setReTryCount("0");
				mailQueueTbls.add(mailQueueTbl);
			}
		});
		if (invalidUserNames.size() > 0) {
			try {
				String commaSeparatedUsernames = String.join(",", invalidUserNames);
				String[] param = { commaSeparatedUsernames };
				throw new XMObjectNotFoundException("No Users Found", "USR_ERR0001", param);
			} catch (XMObjectNotFoundException objectNotFoundException) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFoundException);
				statusMaps.add(statusMap);
			}
		} else {
			this.mailQueueJpaDao.save(mailQueueTbls);
			LOG.debug("mqtSaved");
		}
		LOG.info("<< sendMail");
		return ack;
	}

    private SendMailResponse successAck(List<Map<String, String>> statusMaps) {
        return new SendMailResponse("Mail posted to Queue - Success", statusMaps);
    }
    
    private String getSMTPValue(String key) {
        PropertyConfigTbl propertyConfigTbl
                = this.propertyConfigJpaDao
                        .findByCategoryAndProperty("SMTP", key);
        String value = propertyConfigTbl.getValue();
        return value;
    }

}
