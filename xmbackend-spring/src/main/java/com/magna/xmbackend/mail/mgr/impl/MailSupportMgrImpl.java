/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author dhana
 */
@Component
public class MailSupportMgrImpl implements MailSupportMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(MailSupportMgrImpl.class);

    @Autowired
    private TemplateEngine templateEngine;
    
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;

    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;

    private static final String STAGE_NEW = "new";

    private static final String DIRECT_MAIL_SEND_TEMPLATE = "DIRECT_MAIL_SEND_TEMPLATE";

    @Override
    public List<UserEventConfigData> getUserEventConfigData(String event) {
        //will add SUBJECT, MESSAGE, INCLUDE_REMARKS,
        //SEND_TO_ASSIGNED_USER, PROJECT_EXPIRY_NOTICE_PERIOD to the map
        EmailNotificationConfigTbl configTbl
                = this.emailNotificationConfigJpaDao.findByEvent(event);

        String includeRemarks = configTbl.getIncludeRemarks();
        String message = configTbl.getMessage();
        String projectExpiryNoticePeriod
                = configTbl.getProjectExpiryNoticePeriod();
        String sendToAssignedUser = configTbl.getSendToAssignedUser();
        String subject = configTbl.getSubject();

        Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTblCollection
                = configTbl.getEmailNotifyToUserRelTblCollection();

        List<UserEventConfigData> configDatas = new ArrayList<>();
        UserEventConfigData userEventConfigData = null;

        for (EmailNotifyToUserRelTbl emailNotifyToUserRelTbl
                : emailNotifyToUserRelTblCollection) {
            UsersTbl usersTbl = emailNotifyToUserRelTbl.getUserId();
            String emailId = usersTbl.getEmailId();
            String username = usersTbl.getUsername();
            if (emailId == null) {
                LOG.warn("username {} email id is null and so notification will not be sent", username);
                continue;
            }
            String fullName = usersTbl.getFullName();

            userEventConfigData = new UserEventConfigData();
            userEventConfigData.setEventType(event);
            userEventConfigData.setFromEmailId(emailId);
            userEventConfigData.setFullName(fullName);
            userEventConfigData.setIncludeRemarks(includeRemarks);
            userEventConfigData.setMessage(message);
            userEventConfigData.setProjectExpiryN(projectExpiryNoticePeriod);
            userEventConfigData.setRetryCount("0");
            userEventConfigData.setSendToAssigned(sendToAssignedUser);
            userEventConfigData.setStage(STAGE_NEW);
            userEventConfigData.setSubject(subject);
            userEventConfigData.setToEmailId(emailId);
            userEventConfigData.setUsername(username);
            configDatas.add(userEventConfigData);
        }
        return configDatas;
    }

    @Override
    public Collection<MailQueueTbl> convert2Entity(
            List<UserEventConfigData> userEventConfigDatas) {
        Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();
        Date date = new Date();
        for (UserEventConfigData userEventConfigData : userEventConfigDatas) {
            String eventType = userEventConfigData.getEventType();
            String fromEmailId = this.getSMTPValue("SMTP_EMAIL_ID");
            String retryCount = userEventConfigData.getRetryCount();
            String stage = userEventConfigData.getStage();
            String subject = userEventConfigData.getSubject();
            String toEmailId = userEventConfigData.getToEmailId();
            String username = userEventConfigData.getUsername();
            String textMessage = userEventConfigData.getTextMessage();

            MailQueueTbl mailQueueTbl
                    = new MailQueueTbl(UUID.randomUUID().toString(),
                            username, toEmailId, subject,
                            textMessage, eventType, stage, date, date);
            mailQueueTbl.setFromMail(fromEmailId);
            mailQueueTbl.setReTryCount(retryCount);
            mailQueueTbl.setDelaySend("false");

            mailQueueTbls.add(mailQueueTbl);
        }
        return mailQueueTbls;
    }

    @Override
    public String bindProjectTemplate2Data(String userName, String projectName,
            String message, String event) {
        Context ctx = new Context();
        ctx.setVariable("fullName", userName);
        ctx.setVariable("assignedUserName",userName);
        ctx.setVariable("PROJECT_NAME_en",
                projectName);
        ctx.setVariable("message", message);
        final String htmlContent
                = this.templateEngine.process(event, ctx);
        LOG.debug("htmlContent {}", htmlContent);

        return htmlContent;
    }

    @Override
    public void bindTemplate2Data(
            List<UserEventConfigData> userEventConfigDatas,
            Map<String, String> eventAttributes,
            String event) {
        userEventConfigDatas.forEach((userEventConfigData) -> {
            Context ctx = new Context();

            eventAttributes.keySet().forEach(attrib -> {
                String value = eventAttributes.get(attrib);
                ctx.setVariable(attrib, value);
            });
            ctx.setVariable("USERNAME", userEventConfigData.getUsername());
            ctx.setVariable("FULLNAME", userEventConfigData.getFullName());
            ctx.setVariable("MESSAGE", userEventConfigData.getMessage());
            String includeRemarks = userEventConfigData.getIncludeRemarks();
            if ("true".equalsIgnoreCase(includeRemarks)) {
                ctx.setVariable("REMARKS", userEventConfigData.getMessage());
            }

            final String htmlContent
                    = this.templateEngine.process(event, ctx);
            LOG.debug("htmlContent {}", htmlContent);
            userEventConfigData.setTextMessage(htmlContent);
        });
    }

    @Override
    public String bindSendMailDirectTemplate2Data(
            Map<String, String> eventAttributes) {
        LOG.info(">>> bindSendMailDirectTemplate2Data");
        Context ctx = new Context();
        eventAttributes.keySet().forEach(attrib -> {
            String value = eventAttributes.get(attrib);
            ctx.setVariable(attrib, value);
        });
        //DIRECT_MAIL_SEND_TEMPLATE
        final String htmlContent
                = this.templateEngine.process(DIRECT_MAIL_SEND_TEMPLATE, ctx);
        LOG.debug("htmlContent {}", htmlContent);
        LOG.info("<<< bindSendMailDirectTemplate2Data");
        return htmlContent;
    }
    
    private String getSMTPValue(String key) {
        PropertyConfigTbl propertyConfigTbl
                = this.propertyConfigJpaDao
                        .findByCategoryAndProperty("SMTP", key);
        String value = propertyConfigTbl.getValue();
        return value;
    }
}
