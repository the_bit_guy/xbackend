package com.magna.xmbackend.rel.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaUserAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.rel.mgr.AdminAreaUserAppRelMgr;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelWrapper;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/adminAreaUserAppRel")
public class AdminAreaUserAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaUserAppRelController.class);
    @Autowired
    private AdminAreaUserAppRelMgr adminAreaUserAppRelMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaUserAppAuditMgr adminAreaUserAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param adminAreaUserAppRelRequest
     * @return AdminAreaUserAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminAreaUserAppRelTbl aauarOut
                = adminAreaUserAppRelMgr.create(adminAreaUserAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aauarOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaUserAppRelBatchRequest
     * @return AdminAreaStartAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        AdminAreaUserAppRelBatchResponse aauarbr = null;
		try {
			aauarbr = adminAreaUserAppRelMgr.createBatch(adminAreaUserAppRelBatchRequest, userName);
			this.adminAreaUserAppAuditMgr.adminAreaUserAppMultiSaveAuditor(adminAreaUserAppRelBatchRequest, aauarbr, httpServletRequest);
		} catch (Exception ex) {
			this.adminAreaUserAppAuditMgr.adminAreaUserAppMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(aauarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final AdminAreaUserAppRelResponse adminAreaUserAppRelResponse = adminAreaUserAppRelMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(adminAreaUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaUserAppRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl = adminAreaUserAppRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(adminAreaUserAppRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = adminAreaUserAppRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> deleteById >> {}", ids);
        final AdminAreaUserAppRelResponse relResponse = adminAreaUserAppRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<AdminAreaUserAppRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.adminAreaUserAppRelMgr.updateStatusById(status, id);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaUserAppRelBatchRequest
     * @return AdminAreaUserAppRelBatchResponse
     */
    @RequestMapping(value = "/updateRelTypesByIds",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelBatchResponse> updateRelTypesByIds(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest) {
        LOG.info("> updateRelTypesByIds");
        final AdminAreaUserAppRelBatchResponse response = this.adminAreaUserAppRelMgr.updateRelTypesByIds(adminAreaUserAppRelBatchRequest);
        LOG.info("< updateRelTypesByIds");
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaUserAppRelResponse
     */
    @RequestMapping(value = "/findByUserAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelWrapper> findByUserAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findByUserAppId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaUserAppRelWrapper adminAreaUserAppRelRes
                = adminAreaUserAppRelMgr.findAAUARelByUserAppId(id, validationRequest);
        LOG.info("< findByUserAppId");
        return new ResponseEntity<>(adminAreaUserAppRelRes, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param aaId
     * @return AdminAreaUserAppRelWrapper
     */
    @RequestMapping(value = "/findAAUserAppRelsByAAId/{aaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaUserAppRelWrapper> findAAUserAppRelsByAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String aaId) {
        LOG.info("> findAAUserAppRelsByAAId {}", aaId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaUserAppRelWrapper adminAreaUserAppRelRes
                = adminAreaUserAppRelMgr.findAAUserAppRelsByAAId(aaId, validationRequest);
        LOG.info("< findAAUserAppRelsByAAId");
        return new ResponseEntity<>(adminAreaUserAppRelRes, HttpStatus.OK);
    }

}
