package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelationWrapper;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;

/**
 *
 * @author vijay
 */
public interface AdminAreaStartAppRelMgr {

    /**
     *
     * @return AdminAreaStartAppRelResponse
     */
    AdminAreaStartAppRelResponse findAll();

    /**
     *
     * @param id
     * @return AdminAreaStartAppRelTbl
     */
    AdminAreaStartAppRelTbl findById(final String id);

    /**
     *
     * @param adminAreaStartAppRelRequest
     * @param userName
     * @return AdminAreaStartAppRelTbl
     */
    AdminAreaStartAppRelTbl create(final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest,
            final String userName);

    /**
     *
     * @param adminAreaStartAppRelBatchRequest
     * @param userName
     * @return AdminAreaStartAppRelBatchResponse
     */
    AdminAreaStartAppRelBatchResponse createBatch(
            final AdminAreaStartAppRelBatchRequest adminAreaStartAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);
    
    
    
    AdminAreaStartAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param startAppId
     * @param validationRequest
     * @return AdminAreaStartAppRelationWrapper
     */
    AdminAreaStartAppRelationWrapper findAASARelByAtartAppId(final String startAppId,
            final ValidationRequest validationRequest);
}
