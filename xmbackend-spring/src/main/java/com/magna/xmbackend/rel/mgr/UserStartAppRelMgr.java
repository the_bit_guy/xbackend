package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelResponseWrapper;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelResponse;

/**
 *
 * @author dhana
 */
public interface UserStartAppRelMgr {

    /**
     *
     * @param userStartAppRelRequest
     * @param userName
     * @return UserStartAppRelTbl
     */
    UserStartAppRelTbl create(final UserStartAppRelRequest userStartAppRelRequest,
            final String userName);

    /**
     *
     * @param userStartAppRelBatchRequest
     * @param userName
     * @return UserStartAppRelBatchResponse
     */
    UserStartAppRelBatchResponse createBatch(final UserStartAppRelBatchRequest userStartAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param userId
     * @return UserStartAppRelResponse
     */
    UserStartAppRelResponse findUserStartAppRelByUserId(final String userId, final ValidationRequest validationRequest);

    /**
     *
     * @param status
     * @param relId
     * @return
     */
    Boolean updateStatusByUSARelId(final String status, final String relId);

    /**
     *
     * @param startAppId
     * @return
     */
    UserStartAppRelResponseWrapper findUserStartAppRelByStartAppId(final String startAppId);
    
    /**
     * 
     * @param startAppId
     * @param validationRequest
     * @return
     */
    UserStartAppRelResponseWrapper findUserStartAppRelByStartAppId(final String startAppId, final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return boolean
     */
    Boolean deleteById(final String id);
    
    
    UserStartAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);
}
