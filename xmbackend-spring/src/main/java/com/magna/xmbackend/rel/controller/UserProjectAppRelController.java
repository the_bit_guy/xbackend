package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.UserProjectAppRelAuditMgr;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.rel.mgr.UserProjectAppRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/userProjectAppRel")
public class UserProjectAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserProjectAppRelController.class);
    @Autowired
    private UserProjectAppRelMgr userProjectAppRelMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private UserProjectAppRelAuditMgr userProjAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param userProjectAppRelRequest
     * @return UserProjAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectAppRelRequest userProjectAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final UserProjAppRelTbl uparOut
                = userProjectAppRelMgr.create(userProjectAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(uparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userProjectAppRelBatchRequest
     * @return UserProjectAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectAppRelBatchRequest userProjectAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        UserProjectAppRelBatchResponse aaparbr = null;
		try {
			aaparbr = userProjectAppRelMgr.createBatch(userProjectAppRelBatchRequest, userName);
			this.userProjAppAuditMgr.userProjectAppMultiSaveAuditor(userProjectAppRelBatchRequest, aaparbr, httpServletRequest);
		} catch (Exception ex) {
			this.userProjAppAuditMgr.userProjectMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(aaparbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
        = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserProjAppRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjAppRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final UserProjAppRelTbl userProjAppRelTbl = userProjectAppRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(userProjAppRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = userProjectAppRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return UserProjectAppRelResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        final UserProjectAppRelResponse relResponse = userProjectAppRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<UserProjectAppRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /*@RequestMapping(value = "/findUserProjectAppRelsByUserProjectRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )*/
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findUserProjectAppRelsByUserProjectRelId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserProjectAppRelsByUserProjectRelId");
        final UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelMgr.findUserProjectAppRelsByUserProjectRelId(id);
        LOG.info("< findUserProjectAppRelsByUserProjectRelId");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param aaProjRelId
     * @return UserProjectAppRelResponse
     */
    @RequestMapping(value = "/findByUserProjectRelIdAndAAProjectRelId/{id}/{aaProjRelId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findByUserProjectRelIdAndAAProjectRelId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id, @PathVariable String aaProjRelId) {
        LOG.info("> findByUserProjectRelIdAndAAProjectRelId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectAppRelResponse userProjectAppRelResponse
                = userProjectAppRelMgr.findUserProjectAppRelsByUserProjRelIdAndAAPRelId(id, aaProjRelId, validationRequest);
        LOG.info("< findByUserProjectRelIdAndAAProjectRelId");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /*@RequestMapping(value = "/findUserProjectAppRelsByUserProjectRelIdAndUserRelType/{id}/{userRelType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )*/
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findUserProjectAppRelsByUserProjectRelIdAndRelationType(
            HttpServletRequest httpServletRequest,
            @PathVariable String id, @PathVariable String userRelType) {
        LOG.info("> findUserProjectAppRelsByUserProjectRelId");
        final UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelMgr.findUserProjectAppRelsByUserProjectRelIdAndUserRelType(id, userRelType);
        LOG.info("< findUserProjectAppRelsByUserProjectRelId");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param aaProjRelId
     * @param userRelType
     * @return UserProjectAppRelResponse
     */
    @RequestMapping(value = "/findByUserProjectRelIdAAProjRelIdUserRelType/{id}/{aaProjRelId}/{userRelType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<UserProjectAppRelResponse> findByUserProjectRelIdAAProjRelIdUserRelType(
			HttpServletRequest httpServletRequest, @PathVariable String id, @PathVariable String aaProjRelId,
			@PathVariable String userRelType) {
		LOG.info("> findUserProjectAppRelsByUserProjectRelIdAAProjRelIdUserRelType");
		final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");

		final UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelMgr
				.findUserProjectAppRelsByUserProjectRelIdAndAAPRelIdAndUserRelType(id, aaProjRelId, userRelType, validationRequest);
		LOG.info("< findUserProjectAppRelsByUserProjectRelIdAAProjRelIdUserRelType");
		return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
	}

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserProjectAppRelResponse
     */
    @RequestMapping(value = "/findUserProjectAppRelsByProjectAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findUserProjectAppRelsByProjectAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserProjectAppRelsByUserProjectRelId");
        final UserProjectAppRelResponse userProjectAppRelResponse = userProjectAppRelMgr.findUserProjectAppRelsByProjectAppId(id);
        LOG.info("< findUserProjectAppRelsByUserProjectRelId");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param projId
     * @param aaId
     * @return UserProjectAppRelResponse
     */
    @RequestMapping(value = "/findByUserIdProjectIdAAId/{userId}/{projId}/{aaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelResponse> findByUserIdProjectIdAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userId, @PathVariable String projId, @PathVariable String aaId) {
        LOG.info("> findByUserIdProjectIdAAId");
        final ValidationRequest validationRequest
        = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final List<UserProjAppRelTbl> userProjAppRelTbls = userProjectAppRelMgr.findByUserIdProjectIdAAId(userId, projId, aaId, validationRequest);
        UserProjectAppRelResponse userProjectAppRelResponse = new UserProjectAppRelResponse(userProjAppRelTbls);
        LOG.info("< findByUserIdProjectIdAAId");
        return new ResponseEntity<>(userProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userProjectAppRelBatchRequest
     * @return UserProjectAppRelBatchResponse
     */
    @RequestMapping(value = "/updateUserRelTypesByIds",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectAppRelBatchResponse> updateUserRelTypesByIds(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectAppRelBatchRequest userProjectAppRelBatchRequest) {
        LOG.info("> updateUserRelTypesByIds");
        final UserProjectAppRelBatchResponse response = userProjectAppRelMgr.updateUserRelsTypeByIds(userProjectAppRelBatchRequest);
        LOG.info("< updateUserRelTypesByIds");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
