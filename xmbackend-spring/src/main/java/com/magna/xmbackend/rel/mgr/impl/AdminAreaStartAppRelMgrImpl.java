package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaStartAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaStartAppRelJpaDao;
import com.magna.xmbackend.rel.mgr.AdminAreaStartAppRelMgr;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelation;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelationWrapper;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class AdminAreaStartAppRelMgrImpl implements AdminAreaStartAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaStartAppRelMgrImpl.class);

    @Autowired
    private AdminAreaStartAppRelJpaDao adminAreaStartAppRelJpaDao;

    @Autowired
    private SiteAdminAreaRelMgr siteAdminAreaRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaStartAppAuditMgr adminAreaStartAppAuditMgr;
    
    

    /**
     *
     * @return AdminAreaStartAppRelResponse
     */
    @Override
    public final AdminAreaStartAppRelResponse findAll() {
        final Iterable<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls = adminAreaStartAppRelJpaDao.findAll();
        final AdminAreaStartAppRelResponse adminAreaStartAppRelResponse = new AdminAreaStartAppRelResponse(adminAreaStartAppRelTbls);
        return adminAreaStartAppRelResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaStartAppRelTbl
     */
    @Override
    public final AdminAreaStartAppRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl = this.adminAreaStartAppRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return adminAreaStartAppRelTbl;
    }

    /**
     *
     * @param adminAreaStartAppRelRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    @Override
    public final AdminAreaStartAppRelTbl create(final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest,
            final String userName) {
        AdminAreaStartAppRelTbl aasarOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(adminAreaStartAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfStartAppExistsInAdminArea(adminAreaStartAppRelRequest);
                final AdminAreaStartAppRelTbl aasarIn = this.convert2Entity(adminAreaStartAppRelRequest);
                aasarOut = adminAreaStartAppRelJpaDao.save(aasarIn);
            } else {
                if (assignmentAllowedMap.containsKey("startApplicationsTbl")) {
                    final Map<String, String> adminAreaTransMap
                            = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                    final Map<String, String> startAppTransMap
                            = messageMaker.getStartAppNames((StartApplicationsTbl) assignmentAllowedMap.get("startApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(adminAreaTransMap, startAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AASA_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");

        return aasarOut;
    }

    /**
     *
     * @param adminAreaStartAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isStartAppAdminAreaInactiveAssignmentAllowed(adminAreaStartAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "STARTAPP_ADMINAREA");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param aasarr
     */
    private void checkIfStartAppExistsInAdminArea(final AdminAreaStartAppRelRequest aasarr) {
        String siteAdminAreaRelIdIn = aasarr.getSiteAdminAreaRelId();
        String startAppIdIn = aasarr.getStartAppId();

        LOG.debug("siteAdminAreaRelId {} and startAppId {}",
                siteAdminAreaRelIdIn, startAppIdIn);
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelMgr.findById(siteAdminAreaRelIdIn);
        Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection = siteAdminAreaRelTbl.getAdminAreaStartAppRelTblCollection();
        for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTblCollection) {
            StartApplicationsTbl startApplicationsTbl = adminAreaStartAppRelTbl.getStartApplicationId();
            String startApplicationIdFromTbl = startApplicationsTbl.getStartApplicationId();

            if (startApplicationIdFromTbl != null && startApplicationIdFromTbl.equalsIgnoreCase(startAppIdIn)) {
                //throw ex
                //Start Application 'name' is already assigned to Administrative Area 'name'
                AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                Map<String, String> startAppNames = this.messageMaker.getStartAppNames(startApplicationsTbl);
                Map<String, String> adminAreaNames = this.messageMaker.getAdminAreaNames(adminAreasTbl);

                Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(startAppNames, adminAreaNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0007", i18nCodeMap);
            }
        }
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.adminAreaStartAppRelJpaDao.setStatusForAdminAreaStartAppRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.adminAreaStartAppRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0029", param);
            }
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }

    @Override
    public AdminAreaStartAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	AdminAreaStartAppRelTbl adminAreaStartAppRelTbl = this.adminAreaStartAppRelJpaDao.findOne(id);
            try {
            	if(adminAreaStartAppRelTbl != null){
            		this.delete(id);
            		this.adminAreaStartAppAuditMgr.adminAreaStartAppMultiDeleteSuccessAudit(adminAreaStartAppRelTbl, httpServletRequest);
            	} else {
            		throw new XMObjectNotFoundException("Relation not found", "ERR0003");
            	}
                
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        AdminAreaStartAppRelResponse relResponse = new AdminAreaStartAppRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return relResponse;
    }

    /**
     *
     * @param adminAreaStartAppRelRequest
     * @return AdminAreaStartAppRelTbl
     */
    private AdminAreaStartAppRelTbl convert2Entity(final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = adminAreaStartAppRelRequest.getStatus();
        final String siteAdminAreaRelId = adminAreaStartAppRelRequest.getSiteAdminAreaRelId();
        final String startAppId = adminAreaStartAppRelRequest.getStartAppId();
        final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl = new AdminAreaStartAppRelTbl(id);
        adminAreaStartAppRelTbl.setSiteAdminAreaRelId(new SiteAdminAreaRelTbl(siteAdminAreaRelId));
        adminAreaStartAppRelTbl.setStatus(status);
        adminAreaStartAppRelTbl.setStartApplicationId(new StartApplicationsTbl(startAppId));
        return adminAreaStartAppRelTbl;
    }

    /**
     *
     * @param adminAreaStartAppRelBatchRequest
     * @param userName
     * @return AdminAreaStartAppRelBatchResponse
     */
    @Override
    public final AdminAreaStartAppRelBatchResponse createBatch(final AdminAreaStartAppRelBatchRequest adminAreaStartAppRelBatchRequest,
            final String userName) {
        final List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests
                = adminAreaStartAppRelBatchRequest.getAdminAreaStartAppRelRequests();
        for (final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest : adminAreaStartAppRelRequests) {
            try {
                final AdminAreaStartAppRelTbl aasartOut = this.create(adminAreaStartAppRelRequest, userName);
                adminAreaStartAppRelTbls.add(aasartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final AdminAreaStartAppRelBatchResponse adminAreaStartAppRelBatchResponse
                = new AdminAreaStartAppRelBatchResponse(adminAreaStartAppRelTbls, statusMaps);
        return adminAreaStartAppRelBatchResponse;
    }

    /**
     *
     * @param startAppId
     * @param validationRequest
     * @return AdminAreaStartAppRelationWrapper
     */
    @Override
    public final AdminAreaStartAppRelationWrapper findAASARelByAtartAppId(final String startAppId,
            final ValidationRequest validationRequest) {
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAASARelByAtartAppId isInactiveAssignment={}", isInactiveAssignment);
        final StartApplicationsTbl startApplicationsTbl = validator.validateStartApp(startAppId, isInactiveAssignment);
        List<AdminAreaStartAppRelTbl> aasartb
                = this.adminAreaStartAppRelJpaDao.findByStartApplicationId(startApplicationsTbl);
        aasartb = validator.filterAdminAreaStartAppRel(isInactiveAssignment, aasartb);
        if (aasartb.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Start Application relation found", "AA_ERR0009");
        }
        final List<AdminAreaStartAppRelation> adminAreaStartAppRelations = new ArrayList<>();
        for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : aasartb) {
            try {
                validateAdminAreaStartAppRel(adminAreaStartAppRelTbl, isInactiveAssignment);
                final AdminAreaStartAppRelation adminAreaStartAppRelation = new AdminAreaStartAppRelation();
                adminAreaStartAppRelation.setAdminAreaStartAppRelId(adminAreaStartAppRelTbl.getAdminAreaStartAppRelId());
                adminAreaStartAppRelation.setSiteAdminAreaRelId(adminAreaStartAppRelTbl.getSiteAdminAreaRelId());
                adminAreaStartAppRelation.setStartApplicationId(adminAreaStartAppRelTbl.getStartApplicationId());
                adminAreaStartAppRelation.setStatus(adminAreaStartAppRelTbl.getStatus());
                adminAreaStartAppRelations.add(adminAreaStartAppRelation);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        final AdminAreaStartAppRelationWrapper wrapper = new AdminAreaStartAppRelationWrapper(adminAreaStartAppRelations);
        return wrapper;
    }

    /**
     *
     * @param adminAreaStartAppRelTbl
     * @param isViewInactive
     */
    private void validateAdminAreaStartAppRel(final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl,
            final boolean isViewInactive) {
        final String siteAdminAreaRelId = adminAreaStartAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
        final String siteId = adminAreaStartAppRelTbl.getSiteAdminAreaRelId().getSiteId().getSiteId();
        final String adminAreaId = adminAreaStartAppRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getAdminAreaId();
        validator.validateSiteAdminArea(siteAdminAreaRelId, isViewInactive);
        validator.validateSite(siteId, isViewInactive);
        validator.validateAdminArea(adminAreaId, isViewInactive);
    }
}
