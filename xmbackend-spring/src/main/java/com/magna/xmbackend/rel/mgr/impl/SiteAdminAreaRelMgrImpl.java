package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.SiteAdminAreaAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.rel.mgr.AdminAreaUserAppRelMgr;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class SiteAdminAreaRelMgrImpl implements SiteAdminAreaRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteAdminAreaRelMgrImpl.class);

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private SiteJpaDao siteJpaDao;

    @Autowired
    private AdminAreaUserAppRelMgr adminAreaUserAppRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private SiteAdminAreaAuditMgr siteAdminAreaAuditMgr;

    /**
     * @param validationRequest
     * @return SiteAdminAreaRelResponse
     */
    @Override
    public final SiteAdminAreaRelResponse findAll(final ValidationRequest validationRequest) {
        final boolean isViewInActive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInActive={}", isViewInActive);
        Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = siteAdminAreaRelJpaDao.findAll();
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInActive, siteAdminAreaRelTbls);
        List<SiteAdminAreaRelTbl> filteredSiteAdminAreaTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            SitesTbl siteTbl = validator.filterSiteResponse(isViewInActive, siteAdminAreaRelTbl.getSiteId());
            if (null != siteTbl) {
                AdminAreasTbl adminAreasTbl = validator.filterAdminAreaResponse(isViewInActive, siteAdminAreaRelTbl.getAdminAreaId());
                if (null != adminAreasTbl) {
                    filteredSiteAdminAreaTbls.add(siteAdminAreaRelTbl);
                }
            }
        }
        SiteAdminAreaRelResponse siteAdminAreaRelResponse = new SiteAdminAreaRelResponse(filteredSiteAdminAreaTbls);
        return siteAdminAreaRelResponse;
    }

    /**
     *
     * @param id
     * @return SiteAdminAreaRelTbl
     */
    @Override
    public final SiteAdminAreaRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return siteAdminAreaRelTbl;
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @return SiteAdminAreaRelTbl
     */
    @Override
    public final SiteAdminAreaRelTbl create(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final ValidationRequest validationRequest) {
        SiteAdminAreaRelTbl saarelOut = null;
        LOG.info(">> create");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("create isInactiveAssignment={}", isInactiveAssignment);
        final Map<String, Object> assignmentAllowedMap
                = validator.isSiteAdminAreaInactiveAssignmentAllowed(siteAdminAreaRelRequest,
                        isInactiveAssignment);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.isSiteAdminAreaRelCreationValid(siteAdminAreaRelRequest);
                final SiteAdminAreaRelTbl saarelIn = this.convert2Entity(siteAdminAreaRelRequest);
                saarelOut = siteAdminAreaRelJpaDao.save(saarelIn);
            } else {
                if (assignmentAllowedMap.containsKey("sitesTbl")) {
                    final Map<String, String> adminAreaTransMap
                            = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                    final Map<String, String> siteTransMap
                            = messageMaker.getSiteNames((SitesTbl) assignmentAllowedMap.get("sitesTbl"));
                    final Map<String, String[]> paramMap = this.messageMaker.getSiteAdminAreaRelWithi18nCode(siteTransMap, adminAreaTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "SAA_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");
        return saarelOut;
    }

    /**
     *
     * @param siteAdminAreaRelBatchRequest
     * @param validationRequest
     * @return SiteAdminAreaRelBatchResponse
     */
    @Override
    public final SiteAdminAreaRelBatchResponse createBatch(final SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest,
            final ValidationRequest validationRequest) {
        final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests = siteAdminAreaRelBatchRequest.getSiteAdminAreaRelRequests();
        for (final SiteAdminAreaRelRequest siteAdminAreaRelRequest : siteAdminAreaRelRequests) {
            try {
                final SiteAdminAreaRelTbl saarelOut = this.create(siteAdminAreaRelRequest, validationRequest);
                siteAdminAreaRelTbls.add(saarelOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final SiteAdminAreaRelBatchResponse siteAdminAreaRelBatchResponse = new SiteAdminAreaRelBatchResponse(siteAdminAreaRelTbls, statusMaps);
        return siteAdminAreaRelBatchResponse;
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     */
    private void isSiteAdminAreaRelCreationValid(
            final SiteAdminAreaRelRequest siteAdminAreaRelRequest) {
        LOG.debug(">> isSiteAdminAreaRelCreationValid");
        final String adminAreaId = siteAdminAreaRelRequest.getAdminAreaId();
        //final String siteId = siteAdminAreaRelRequest.getSiteId();
        //has to check if AA is already assigned to any of the sites
        final Iterable<SitesTbl> sitesTbls = this.siteJpaDao.findAll();
        sitesTbls.forEach(sitesTbl -> checkIfAAPresentInSite(sitesTbl.getSiteId(), adminAreaId));
        //checkIfAAPresentInSite(siteId, adminAreaId);
        LOG.debug("<< isSiteAdminAreaRelCreationValid");
    }

    /**
     *
     * @param sitesTbl
     * @param adminAreaId
     * @throws CannotCreateRelationshipException
     */
    //method to check if AA is present in the given sitesTbl
    //if present it throws CannotCreateRelationshipException with paramMap
    private void checkIfAAPresentInSite(
            final String siteId, final String adminAreaId)
            throws CannotCreateRelationshipException {
        final SitesTbl sitesTbl = this.siteJpaDao.findOne(siteId);
        final Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection
                = sitesTbl.getSiteAdminAreaRelTblCollection();
        if (!siteAdminAreaRelTblCollection.isEmpty()) {
            final Map<String, String> siteTransMap = messageMaker.getSiteNames(sitesTbl);
            LOG.debug("siteTransMap={}", siteTransMap);
            for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTblCollection) {
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                final String adminAreaIdFromTbl = adminAreasTbl.getAdminAreaId();
                if (adminAreaIdFromTbl.equalsIgnoreCase(adminAreaId)) {
                    final Map<String, String> adminAreaTransMap = messageMaker.getAdminAreaNames(adminAreasTbl);
                    LOG.debug("adminAreaTransMap={}", adminAreaTransMap);
                    final Map<String, String[]> paramMap = this.messageMaker.getSiteAdminAreaRelWithi18nCode(siteTransMap, adminAreaTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Realtionship exists", "B_ERR0001", paramMap);
                }
            }
        }
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaProjRelResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaProjRelResponse
            findAllProjectsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllProjectsBySiteAdminAreaRelId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(id, isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaProjRelResponse response = this.getAllProjects(siteAdminAreaRelTbl, isViewInactive);
        LOG.info(">> findAllProjectsBySiteAdminAreaRelId");
        return response;
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param isInactiveAssignment
     * @return SiteAdminAreaRelIdWithAdminAreaProjRelResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaProjRelResponse getAllProjects(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final boolean isInactiveAssignment) {
        Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTbls
                = siteAdminAreaRelTbl.getAdminAreaProjectRelTblCollection();
        adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project Relation found", "AA_ERR0001");
        }
        final List<SiteAdminAreaRelIdWithAdminAreaProjRel> siteAdminAreaRelIdWithAdminAreaProjRels = new ArrayList<>();
        for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
            try {
                final String projectId = adminAreaProjectRelTbl.getProjectId().getProjectId();
                validator.validateProject(projectId, isInactiveAssignment);
                final SiteAdminAreaRelIdWithAdminAreaProjRel saariwaapr = new SiteAdminAreaRelIdWithAdminAreaProjRel();
                saariwaapr.setAdminAreaProjRelId(adminAreaProjectRelTbl.getAdminAreaProjectRelId());
                saariwaapr.setSiteAdminAreaRelId(adminAreaProjectRelTbl.getSiteAdminAreaRelId());
                saariwaapr.setProjectsTbl(adminAreaProjectRelTbl.getProjectId());
                saariwaapr.setStatus(adminAreaProjectRelTbl.getStatus());
                siteAdminAreaRelIdWithAdminAreaProjRels.add(saariwaapr);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        final SiteAdminAreaRelIdWithAdminAreaProjRelResponse saariwaaprr = new SiteAdminAreaRelIdWithAdminAreaProjRelResponse();
        saariwaaprr.setSiteAdminAreaRelIdWithAdminAreaProjRel(siteAdminAreaRelIdWithAdminAreaProjRels);
        return saariwaaprr;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
            findAllUserAppsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllUserAppsBySiteAdminAreaRelId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(id, isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse response = this.getAllUserApps(siteAdminAreaRelTbl, isViewInactive);
        LOG.info(">> findAllUserAppsBySiteAdminAreaRelId");
        return response;
    }

    /**
     *
     * @param id
     * @param relType
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
            findAllUserAppsBySiteAdminAreaRelId(final String id,
                    final String relType,
                    final ValidationRequest validationRequest) {
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(id, isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse response
                = this.getAllUserApps(siteAdminAreaRelTbl, relType, isViewInactive);
        return response;
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param relType
     * @param isInactiveAssignment
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse getAllUserApps(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final String relType, final boolean isInactiveAssignment) {
        Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls
                = adminAreaUserAppRelMgr.getAdminAreaUsrAppOnRelType(siteAdminAreaRelTbl, relType);
        adminAreaUserAppRelTbls = validator.filterAdminAreaUserAppRel(isInactiveAssignment, adminAreaUserAppRelTbls);
        final List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaProjRels = new ArrayList<>();
        formSiteAdminAreaUserAppRelResponse(adminAreaUserAppRelTbls,
                siteAdminAreaRelIdWithAdminAreaProjRels, isInactiveAssignment);
        if (siteAdminAreaRelIdWithAdminAreaProjRels.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area User Application relation found", "AA_ERR0003");
        }
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse saariwaauarr = new SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse();
        saariwaauarr.setSiteAdminAreaRelIdWithAdminAreaUsrAppRel(siteAdminAreaRelIdWithAdminAreaProjRels);
        return saariwaauarr;
    }

    /**
     *
     * @param adminAreaUserAppRelTbls
     * @param siteAdminAreaRelIdWithAdminAreaProjRels
     * @param isInactiveAssignment
     */
    private void formSiteAdminAreaUserAppRelResponse(final Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls,
            final List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaProjRels,
            final boolean isInactiveAssignment) {
        for (final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
            try {
                final String userAppId = adminAreaUserAppRelTbl.getUserApplicationId().getUserApplicationId();
                validator.validateUserApp(userAppId, isInactiveAssignment);
                final SiteAdminAreaRelIdWithAdminAreaUsrAppRel saariwaauar = new SiteAdminAreaRelIdWithAdminAreaUsrAppRel();
                saariwaauar.setAdminAreaUsrAppRelId(adminAreaUserAppRelTbl.getAdminAreaUserAppRelId());
                saariwaauar.setSiteAdminAreaRelId(adminAreaUserAppRelTbl.getSiteAdminAreaRelId());
                saariwaauar.setUserApplicationsTbl(adminAreaUserAppRelTbl.getUserApplicationId());
                saariwaauar.setStatus(adminAreaUserAppRelTbl.getStatus());
                saariwaauar.setRelationType(adminAreaUserAppRelTbl.getRelType());
                siteAdminAreaRelIdWithAdminAreaProjRels.add(saariwaauar);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param isInactiveAssignment
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse getAllUserApps(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final boolean isInactiveAssignment) {
        Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls
                = siteAdminAreaRelTbl.getAdminAreaUserAppRelTblCollection();
        adminAreaUserAppRelTbls = validator.filterAdminAreaUserAppRel(isInactiveAssignment, adminAreaUserAppRelTbls);
        if (adminAreaUserAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area User Application relation found", "AA_ERR0003");
        }
        final List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaProjRels = new ArrayList<>();
        formSiteAdminAreaUserAppRelResponse(adminAreaUserAppRelTbls,
                siteAdminAreaRelIdWithAdminAreaProjRels, isInactiveAssignment);
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse saariwaauarr = new SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse();
        saariwaauarr.setSiteAdminAreaRelIdWithAdminAreaUsrAppRel(siteAdminAreaRelIdWithAdminAreaProjRels);
        return saariwaauarr;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
            findAllStartAppsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllStartAppsBySiteAdminAreaRelId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(id, isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse response = this.getAllStartApps(siteAdminAreaRelTbl, isViewInactive);
        LOG.info(">> findAllStartAppsBySiteAdminAreaRelId");
        return response;
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param isInactiveAssignment
     * @return SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse getAllStartApps(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final boolean isInactiveAssignment) {
        Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls
                = siteAdminAreaRelTbl.getAdminAreaStartAppRelTblCollection();
        adminAreaStartAppRelTbls = validator.filterAdminAreaStartAppRel(isInactiveAssignment, adminAreaStartAppRelTbls);
        if (adminAreaStartAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Start Application relation found", "AA_ERR0009");
        }
        final List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> siteAdminAreaRelIdWithAdminAreaStartAppRels = new ArrayList<>();
        for (final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTbls) {
            try {
                final String startAppId = adminAreaStartAppRelTbl.getStartApplicationId().getStartApplicationId();
                validator.validateStartApp(startAppId, isInactiveAssignment);
                final SiteAdminAreaRelIdWithAdminAreaStartAppRel saariwaasar = new SiteAdminAreaRelIdWithAdminAreaStartAppRel();
                saariwaasar.setAdminAreaStartAppRelId(adminAreaStartAppRelTbl.getAdminAreaStartAppRelId());
                saariwaasar.setSiteAdminAreaRelId(adminAreaStartAppRelTbl.getSiteAdminAreaRelId());
                saariwaasar.setStartApplicationsTbl(adminAreaStartAppRelTbl.getStartApplicationId());
                saariwaasar.setStatus(adminAreaStartAppRelTbl.getStatus());
                siteAdminAreaRelIdWithAdminAreaStartAppRels.add(saariwaasar);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        final SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse saariwaasarr = new SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse();
        saariwaasarr.setSiteAdminAreaRelIdWithAdminAreaStartAppRel(siteAdminAreaRelIdWithAdminAreaStartAppRels);
        return saariwaasarr;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.siteAdminAreaRelJpaDao.setStatusForSiteAdminAreaRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.siteAdminAreaRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("Site Admin Area not found", "P_ERR0026", param);
            }
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param ids
     * @return SiteAdminAreaRelResponse
     */
    @Override
    public SiteAdminAreaRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(id);
            try {
            	if(siteAdminAreaRelTbl != null) {
            		this.delete(id);
            		this.siteAdminAreaAuditMgr.siteAdminAreaMultiDeleteSuccessAudit(siteAdminAreaRelTbl, httpServletRequest);
            	} else {
            		throw new XMObjectNotFoundException("Relation not found", "ERR0003");
            	}
                
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        SiteAdminAreaRelResponse relResponse = new SiteAdminAreaRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return relResponse;
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @return SiteAdminAreaRelTbl
     */
    private SiteAdminAreaRelTbl convert2Entity(final SiteAdminAreaRelRequest siteAdminAreaRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = siteAdminAreaRelRequest.getStatus();
        final String adminAreaId = siteAdminAreaRelRequest.getAdminAreaId();
        final String siteId = siteAdminAreaRelRequest.getSiteId();
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = new SiteAdminAreaRelTbl(id);
        siteAdminAreaRelTbl.setAdminAreaId(new AdminAreasTbl(adminAreaId));
        siteAdminAreaRelTbl.setStatus(status);
        siteAdminAreaRelTbl.setSiteId(new SitesTbl(siteId));
        return siteAdminAreaRelTbl;
    }

    /**
     *
     * @param siteId
     * @param validationRequest
     * @return SiteAdminAreaRelResponse
     */
    @Override
    public final SiteAdminAreaRelResponse findSiteAdminAreaRelBySiteId(final String siteId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findSiteAdminAreaRelBySiteId {}", siteId);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        final SitesTbl sitesTbl = validator.validateSite(siteId, isInactiveAssignment);
        Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findAdminAreaBySiteId(sitesTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbls);
        final List<SiteAdminAreaRelTbl> siteAdminAreaRelTblList
                = validateAdminArea(siteAdminAreaRelTbls, isInactiveAssignment);
        final SiteAdminAreaRelResponse siteAdminAreaRelResponse = new SiteAdminAreaRelResponse(siteAdminAreaRelTblList);
        LOG.info("<< findSiteAdminAreaRelBySiteId");
        return siteAdminAreaRelResponse;
    }

    /**
     *
     * @param siteAdminAreaRelTbls
     * @param isInactiveAssignment
     * @return SiteAdminAreaRelTbl
     */
    private List<SiteAdminAreaRelTbl> validateAdminArea(final Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls,
            final boolean isInactiveAssignment) {
        final List<SiteAdminAreaRelTbl> siteAdminAreaRelTblList = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            try {
                final String adminAreaId = siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
                validator.validateAdminArea(adminAreaId, isInactiveAssignment);
                siteAdminAreaRelTblList.add(siteAdminAreaRelTbl);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        return siteAdminAreaRelTblList;
    }

    /**
     *
     * @param aaId
     * @return SiteAdminAreaRelResponse
     */
    @Override
    public SiteAdminAreaRelResponse findSiteAdminAreaRelByAAId(final String aaId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findSiteAdminAreaRelByAAId {}", aaId);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(aaId, isInactiveAssignment);
        Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbls);
        final SiteAdminAreaRelResponse siteAdminAreaRelResponse = new SiteAdminAreaRelResponse(siteAdminAreaRelTbls);
        LOG.info("<< findSiteAdminAreaRelByAAId");
        return siteAdminAreaRelResponse;
    }

}
