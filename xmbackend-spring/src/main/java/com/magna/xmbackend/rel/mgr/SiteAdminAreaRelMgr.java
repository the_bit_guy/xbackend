package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;

/**
 *
 * @author vijay
 */
public interface SiteAdminAreaRelMgr {

    /**
     * @param validationRequest
     * @return SiteAdminAreaRelResponse
     */
    SiteAdminAreaRelResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return SiteAdminAreaRelTbl
     */
    SiteAdminAreaRelTbl findById(String id);

    /**
     *
     * @param siteAdminAreaRelBatchRequest
     * @param validationRequest
     * @return SiteAdminAreaRelBatchResponse
     */
    SiteAdminAreaRelBatchResponse createBatch(final SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest,
            final ValidationRequest validationRequest);

    /**
     *
     * @param siteAdminAreaRelRequest
     * @param validationRequest
     * @return SiteAdminAreaRelTbl
     */
    SiteAdminAreaRelTbl create(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);

    /**
     *
     * @param ids
     * @return SiteAdminAreaRelResponse
     */
    SiteAdminAreaRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaProjRelResponse
     */
    SiteAdminAreaRelIdWithAdminAreaProjRelResponse
            findAllProjectsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
            findAllUserAppsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param relType
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
            findAllUserAppsBySiteAdminAreaRelId(final String id,
                    final String relType,
                    final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
     */
    SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
            findAllStartAppsBySiteAdminAreaRelId(final String id,
                    final ValidationRequest validationRequest);

    /**
     * Find site admin area rel by site id.
     *
     * @param siteId the site id
     * @param validationRequest
     * @return the site admin area rel response
     */
    SiteAdminAreaRelResponse findSiteAdminAreaRelBySiteId(final String siteId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param aaId
     * @param validationRequest
     * @return SiteAdminAreaRelResponse
     */
    SiteAdminAreaRelResponse findSiteAdminAreaRelByAAId(final String aaId,
            final ValidationRequest validationRequest);
}
