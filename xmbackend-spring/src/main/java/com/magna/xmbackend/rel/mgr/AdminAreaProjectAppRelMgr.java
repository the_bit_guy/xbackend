package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;

/**
 *
 * @author vijay
 */
public interface AdminAreaProjectAppRelMgr {

    /**
     *
     * @return AdminAreaUserAppRelResponse
     */
    AdminAreaProjectAppRelResponse findAll();

    /**
     *
     * @param id
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjAppRelTbl findById(String id);

    /**
     *
     * @param adminAreaProjectAppRelRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    AdminAreaProjAppRelTbl create(final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest,
            final String userName);

    /**
     *
     * @param adminAreaProjectAppRelBatchRequest
     * @param userName
     * @return AdminAreaProjectAppRelBatchResponse
     */
    AdminAreaProjectAppRelBatchResponse createBatch(final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);
    
    
    AdminAreaProjectAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param relType
     * @return AdminAreaProjAppRelTbl
     */
    Iterable<AdminAreaProjAppRelTbl> getAdminAreaProjAppOnRelType(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final String relType);

    /**
     *
     * @param adminAreaProjectAppRelBatchRequest
     * @return AdminAreaProjectAppRelBatchResponse
     */
    AdminAreaProjectAppRelBatchResponse updateRelTypesByIds(final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest);

    /**
     * Find admin project app rel by AA pro rel.
     *
     * @param adminAreaProjectRelId the admin area project rel id
     * @return the admin area project app rel response
     */
    AdminAreaProjectAppRelResponse findAdminProjectAppRelByAAProRel(final String adminAreaProjectRelId);

    /**
     * Find admin area by project application id.
     *
     * @param projectApplicationId the project application id
     * @param validationRequest
     * @return the list
     */
    List<AdminAreaProjectAppResponse> findAdminAreaByProjectApplicationId(final String projectApplicationId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param aaId
     * @param pId
     * @param validationRequest
     * @return AdminAreaProjectAppRelResponse
     */
    AdminAreaProjectAppRelResponse findByAAIdAndProjectId(final String aaId,
            final String pId, final ValidationRequest validationRequest);
}
