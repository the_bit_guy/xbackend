package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.ProjectStartAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.ProjectStartAppRelJpaDao;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.rel.mgr.ProjectStartAppRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class ProjectStartAppRelMgrImpl implements ProjectStartAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectStartAppRelMgrImpl.class);

    @Autowired
    private ProjectStartAppRelJpaDao projectStartAppRelJpaDao;

    @Autowired
    private AdminAreaProjectRelMgr adminAreaProjectRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;

    @Autowired
    private ProjectStartAppAuditMgr projectStartAppAuditMgr;
    
    /**
     *
     * @return ProjectStartAppRelResponse
     */
    @Override
    public final ProjectStartAppRelResponse findAll() {
        final Iterable<ProjectStartAppRelTbl> adminAreaStartAppRelTbls = projectStartAppRelJpaDao.findAll();
        final ProjectStartAppRelResponse projectStartAppRelResponse = new ProjectStartAppRelResponse(adminAreaStartAppRelTbls);
        return projectStartAppRelResponse;
    }

    /**
     *
     * @param id
     * @return ProjectStartAppRelTbl
     */
    @Override
    public final ProjectStartAppRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final ProjectStartAppRelTbl projectStartAppRelTbl = this.projectStartAppRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return projectStartAppRelTbl;
    }

    /**
     *
     * @param projectStartAppRelRequest
     * @param userName
     * @return ProjectStartAppRelTbl
     */
    @Override
    public final ProjectStartAppRelTbl create(final ProjectStartAppRelRequest projectStartAppRelRequest,
            final String userName) {
        ProjectStartAppRelTbl psarOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(projectStartAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfStartAppExistsInProject(projectStartAppRelRequest);
                final ProjectStartAppRelTbl psarIn = this.convert2Entity(projectStartAppRelRequest);
                psarOut = projectStartAppRelJpaDao.save(psarIn);
            } else {
                if (assignmentAllowedMap.containsKey("startApplicationsTbl")) {
                    final Map<String, String> projectTransMap
                            = messageMaker.getProjectNames((ProjectsTbl) assignmentAllowedMap.get("projectsTbl"));
                    final Map<String, String> startAppTransMap
                            = messageMaker.getStartAppNames((StartApplicationsTbl) assignmentAllowedMap.get("startApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(projectTransMap, startAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "PSA_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");
        return psarOut;
    }

    /**
     *
     * @param projectStartAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final ProjectStartAppRelRequest projectStartAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isProjectStartAppInactiveAssignmentAllowed(projectStartAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "STARTAPP_PROJECT");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param psarr
     */
    private void checkIfStartAppExistsInProject(final ProjectStartAppRelRequest psarr) {
        final String adminAreaProjectRelIdIn = psarr.getAdminAreaProjectRelId();
        final String startAppIdIn = psarr.getStartAppId();

        LOG.debug("adminAreaProjectRelIdIn {} and startAppIdIn {}", adminAreaProjectRelIdIn, startAppIdIn);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjectRelMgr.findById(adminAreaProjectRelIdIn);
        final Collection<ProjectStartAppRelTbl> projectStartAppRelTblCollection = adminAreaProjectRelTbl.getProjectStartAppRelTblCollection();

        for (final ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTblCollection) {
            final StartApplicationsTbl startApplicationsTbl = projectStartAppRelTbl.getStartApplicationId();
            final String startApplicationIdFromTbl = startApplicationsTbl.getStartApplicationId();
            if (startApplicationIdFromTbl != null && startApplicationIdFromTbl.equalsIgnoreCase(startAppIdIn)) {
                //throw ex
                //Start Application 'name' is already assigned to Project 'name'
                final ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
                final Map<String, String> startAppNames = this.messageMaker.getStartAppNames(startApplicationsTbl);
                final Map<String, String> projectNames = this.messageMaker.getProjectNames(projectsTbl);

                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(startAppNames, projectNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0008", i18nCodeMap);
            }
        }
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.projectStartAppRelJpaDao.setStatusForProjectStartAppRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.projectStartAppRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0027", param);
            }
        }
        LOG.info("<< delete");
        return isDeleted;
    }

    @Override
    public ProjectStartAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	ProjectStartAppRelTbl projectStartAppRelTbl = this.projectStartAppRelJpaDao.findOne(id);
            try {
            	if(projectStartAppRelTbl != null) {
            		this.delete(id);
            		this.projectStartAppAuditMgr.projectStartAppMultiDeleteSuccessAudit(projectStartAppRelTbl, httpServletRequest);
            	} else {
            		throw new XMObjectNotFoundException("Relation not found", "ERR0003");
            	}
                
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        ProjectStartAppRelResponse projectStartAppRelResponse = new ProjectStartAppRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return projectStartAppRelResponse;
    }

    /**
     *
     * @param projectStartAppRelRequest
     * @return ProjectStartAppRelTbl
     */
    private ProjectStartAppRelTbl convert2Entity(final ProjectStartAppRelRequest projectStartAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = projectStartAppRelRequest.getStatus();
        final String adminAreaProjectRelId = projectStartAppRelRequest.getAdminAreaProjectRelId();
        final String startAppId = projectStartAppRelRequest.getStartAppId();
        final ProjectStartAppRelTbl projectStartAppRelTbl = new ProjectStartAppRelTbl(id);
        projectStartAppRelTbl.setAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjectRelId));
        projectStartAppRelTbl.setStatus(status);
        projectStartAppRelTbl.setStartApplicationId(new StartApplicationsTbl(startAppId));
        return projectStartAppRelTbl;
    }

    /**
     *
     * @param projectStartAppRelBatchRequest
     * @param userName
     * @return ProjectStartAppRelBatchResponse
     */
    @Override
    public final ProjectStartAppRelBatchResponse createBatch(final ProjectStartAppRelBatchRequest projectStartAppRelBatchRequest,
            final String userName) {
        final List<ProjectStartAppRelTbl> projectStartAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<ProjectStartAppRelRequest> projectStartAppRelRequests
                = projectStartAppRelBatchRequest.getProjectStartAppRelRequests();
        for (final ProjectStartAppRelRequest projectStartAppRelRequest : projectStartAppRelRequests) {
            try {
                final ProjectStartAppRelTbl aasartOut = this.create(projectStartAppRelRequest, userName);
                projectStartAppRelTbls.add(aasartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final ProjectStartAppRelBatchResponse projectStartAppRelBatchResponse
                = new ProjectStartAppRelBatchResponse(projectStartAppRelTbls, statusMaps);
        return projectStartAppRelBatchResponse;
    }

    /**
     *
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @param validationRequest
     * @return ProjectStartAppRelResponse
     */
    @Override
    public final ProjectStartAppRelResponse findProjectStartAppByProjectSiteAAId(final String siteId,
            final String adminAreaId, final String projectId,
            final ValidationRequest validationRequest) {
        LOG.info(">> findProjectStartAppByProjectSiteAAId");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectStartAppByProjectSiteAAId isInactiveAssignment={}", isInactiveAssignment);
        final SitesTbl sitesTbl = validator.validateSite(siteId, isInactiveAssignment);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isInactiveAssignment);
        final ProjectsTbl projectsTbl = validator.validateProject(projectId, isInactiveAssignment);
        Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls = projectStartAppRelJpaDao.findStartAppByProjectSiteAAId(sitesTbl, adminAreasTbl, projectsTbl);
        projectStartAppRelTbls = validator.filterProjectStartAppRel(isInactiveAssignment, projectStartAppRelTbls);
        final ProjectStartAppRelResponse projectApplicationResponse
                = new ProjectStartAppRelResponse();
        final List<ProjectStartAppRelTbl> projectStartAppRelTblList = new ArrayList<>();
        for (final ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
            try {
                validateProjectStartAppRel(projectStartAppRelTbl, isInactiveAssignment);
                projectStartAppRelTblList.add(projectStartAppRelTbl);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        projectApplicationResponse.setProjectStartAppRelTbls(projectStartAppRelTblList);
        LOG.info("<< findProjectStartAppByProjectSiteAAId");
        return projectApplicationResponse;
    }

    /**
     *
     * @param projectStartAppRelTbl
     * @param isViewInactive
     */
    private void validateProjectStartAppRel(final ProjectStartAppRelTbl projectStartAppRelTbl,
            final boolean isViewInactive) {
        final String siteAdminAreaId = projectStartAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId().getSiteAdminAreaRelId();
        final String adminAreaProjectRelId = projectStartAppRelTbl.getAdminAreaProjectRelId().getAdminAreaProjectRelId();
        validator.validateSiteAdminArea(siteAdminAreaId, isViewInactive);
        validator.validateAdminAreaProject(adminAreaProjectRelId, isViewInactive);
    }

    /**
     *
     * @param adminAreaProjectRelId
     * @return ProjectStartAppRelResponse
     */
    @Override
    public final ProjectStartAppRelResponse findStartAppByAdminAreaProjectRelId(String adminAreaProjectRelId) {
        LOG.info(">> findStartAppByAdminAreaProjectRelId {}", adminAreaProjectRelId);
        final Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls
                = this.projectStartAppRelJpaDao.findStartAppByAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjectRelId));
        final ProjectStartAppRelResponse projectStartAppRelResponse = new ProjectStartAppRelResponse(projectStartAppRelTbls);
        LOG.info("<< findStartAppByAdminAreaProjectRelId");
        return projectStartAppRelResponse;
    }

}
