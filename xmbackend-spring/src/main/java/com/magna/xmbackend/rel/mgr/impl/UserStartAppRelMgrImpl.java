package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.UserStartAppRelAuditMgr;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.UserStartAppRelJpaDao;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.rel.mgr.UserStartAppRelMgr;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelation;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelResponse;

/**
 *
 * @author dhana
 */
@Component
public class UserStartAppRelMgrImpl implements UserStartAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserStartAppRelMgrImpl.class);

    @Autowired
    private UserStartAppRelJpaDao userStartAppRelJpaDao;

    @Autowired
    private StartApplicationMgr startApplicationMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    
    @Autowired
    private UserStartAppRelAuditMgr userStartAppRelAuditMgr;
    

    /**
     *
     * @param userStartAppRelRequest
     * @param userName
     * @return UserStartAppRelTbl
     */
    @Override
    public UserStartAppRelTbl create(final UserStartAppRelRequest userStartAppRelRequest,
            final String userName) {
        UserStartAppRelTbl usartOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(userStartAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfStartAppExistInUser(userStartAppRelRequest);
                final UserStartAppRelTbl usartIn = this.convert2Entity(userStartAppRelRequest);
                usartOut = this.userStartAppRelJpaDao.save(usartIn);
            } else {
                if (assignmentAllowedMap.containsKey("startApplicationsTbl")) {
                    final Map<String, String> startAppTransMap
                            = messageMaker.getStartAppNames((StartApplicationsTbl) assignmentAllowedMap.get("startApplicationsTbl"));
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userTransMap, startAppTransMap);
                    LOG.debug("i18nCodeMap={}", i18nCodeMap);
                    throw new CannotCreateRelationshipException(
                            "Inactive Assignments not allowed", "USA_ERR0001", i18nCodeMap);
                }
            }
        }
        LOG.info("<< create");
        return usartOut;
    }

    /**
     *
     * @param userStartAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final UserStartAppRelRequest userStartAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isUserStartAppInactiveAssignmentAllowed(userStartAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "STARTAPPLICATION_USER");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param usarr
     */
    private void checkIfStartAppExistInUser(final UserStartAppRelRequest usarr) {
        final String startAppIdIn = usarr.getStartAppId();
        final String userIdIn = usarr.getUserId();

        LOG.debug("startAppIdIn {} and userIdIn {}", startAppIdIn, userIdIn);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationMgr.findById(startAppIdIn);
        final Collection<UserStartAppRelTbl> userStartAppRelTblCollection = startApplicationsTbl.getUserStartAppRelTblCollection();
        for (final UserStartAppRelTbl userStartAppRelTbl : userStartAppRelTblCollection) {
            final UsersTbl usersTbl = userStartAppRelTbl.getUserId();
            final String userIdFromTbl = usersTbl.getUserId();
            if (userIdFromTbl != null && userIdFromTbl.equalsIgnoreCase(userIdIn)) {
                //throw ex
                //Start Application 'name' is already assigned to User 'name'
                final Map<String, String> startAppNames = this.messageMaker.getStartAppNames(startApplicationsTbl);
                final Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(startAppNames, userNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0010", i18nCodeMap);
            }

        }
    }

    /**
     *
     * @param userStartAppRelRequest
     * @return UserStartAppRelTbl
     */
    private UserStartAppRelTbl convert2Entity(final UserStartAppRelRequest userStartAppRelRequest) {

        final String id = UUID.randomUUID().toString();

        final String startAppId = userStartAppRelRequest.getStartAppId();
        final String userId = userStartAppRelRequest.getUserId();
        final String status = userStartAppRelRequest.getStatus();

        final UserStartAppRelTbl userStartAppRelTbl = new UserStartAppRelTbl(id);
        userStartAppRelTbl.setStatus(status);
        userStartAppRelTbl.setUserId(new UsersTbl(userId));
        userStartAppRelTbl.setStartApplicationId(new StartApplicationsTbl(startAppId));

        return userStartAppRelTbl;
    }

    /**
     *
     * @param userStartAppRelBatchRequest
     * @param userName
     * @return UserStartAppRelBatchResponse
     */
    @Override
    public final UserStartAppRelBatchResponse createBatch(final UserStartAppRelBatchRequest userStartAppRelBatchRequest,
            final String userName) {
        final List<UserStartAppRelTbl> userStartAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<UserStartAppRelRequest> userStartAppRelRequests
                = userStartAppRelBatchRequest.getUserStartAppRelRequests();
        for (final UserStartAppRelRequest userStartAppRelRequest : userStartAppRelRequests) {
            try {
                final UserStartAppRelTbl aasartOut = this.create(userStartAppRelRequest, userName);
                userStartAppRelTbls.add(aasartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final UserStartAppRelBatchResponse userStartAppRelBatchResponse
                = new UserStartAppRelBatchResponse(userStartAppRelTbls, statusMaps);
        return userStartAppRelBatchResponse;
    }

    /**
     *
     * @param userId
     * @return UserStartAppRelResponse
     */
    @Override
    public UserStartAppRelResponse findUserStartAppRelByUserId(final String userId, final ValidationRequest validationRequest) {
        LOG.info(">> findUserStartAppRelByUserId");
        UserStartAppRelResponse startAppRelResponse = null;
        final boolean isViewInActive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findByUserIdProjectIdAAId isViewInActive={}", isViewInActive);
        if (null != userId) {
        	final UsersTbl validatedUser = validator.validateUser(userId, isViewInActive);
            final List<UserStartAppRelTbl> userStartAppTbls = this.userStartAppRelJpaDao.findByUserId(validatedUser);
            final List<UserStartAppRelTbl> newUserStartAppTbls = new ArrayList<>();
            for (UserStartAppRelTbl userStartAppRelTbl : userStartAppTbls) {
            	final StartApplicationsTbl startApplicationsTbl = userStartAppRelTbl.getStartApplicationId();
            	final StartApplicationsTbl filterStartApplicationsTbl = validator.filterStartApplicationResponse(isViewInActive, startApplicationsTbl);
            	if (null != filterStartApplicationsTbl) {
            		final UserStartAppRelTbl filterUserStartAppRelTbl = validator.filterUserStartApplicationResponse(isViewInActive, userStartAppRelTbl);
            		if (null != filterUserStartAppRelTbl) {
            			newUserStartAppTbls.add(userStartAppRelTbl);
            		}
            	}
			}
            if (newUserStartAppTbls.isEmpty()) {
                throw new RuntimeException("No User Start App Relation Found for given user id");
            }
            startAppRelResponse = new UserStartAppRelResponse(newUserStartAppTbls);
        }
        LOG.info("<< findUserStartAppRelByUserId");
        return startAppRelResponse;
    }

	/**
     *
     * @param status
     * @param relId
     * @return Boolean
     */
    @Override
    public Boolean updateStatusByUSARelId(final String status,
            final String relId) {
        LOG.info(">> updateStatusByUSARelId");
        if (null == status) {
            throw new RuntimeException("Status can not be null");
        }
        if (null == relId) {
            throw new RuntimeException("Relation id can not be null");
        }
        final int updateValue = this.userStartAppRelJpaDao.setStatusByUserStartAppRelId(status, relId);
        Boolean isUpdate = false;
        if (updateValue > 0) {
            isUpdate = true;
        }
        LOG.info(">> updateStatusByUSARelId");
        return isUpdate;
    }

    /**
     *
     * @param startAppId
     * @return UserStartAppRelResponseWrapper
     */
    @Override
    public UserStartAppRelResponseWrapper findUserStartAppRelByStartAppId(final String startAppId) {
        final List<UserStartAppRelTbl> userStartAppRelTbls = this.userStartAppRelJpaDao.findByStartApplicationId(new StartApplicationsTbl(startAppId));
        if (userStartAppRelTbls.isEmpty()) {
            throw new RuntimeException("No User Start App Relation Found");
        }
        final List<UserStartAppRelation> userStartAppRelations = new ArrayList<>();
        for (final UserStartAppRelTbl appRelTbl : userStartAppRelTbls) {
            final UserStartAppRelation userStartAppRelation = new UserStartAppRelation();
            userStartAppRelation.setUserStartAppRelId(appRelTbl.getUserStartAppRelId());
            userStartAppRelation.setStartApplicationId(appRelTbl.getStartApplicationId());
            userStartAppRelation.setStatus(appRelTbl.getStatus());
            userStartAppRelation.setUserId(appRelTbl.getUserId());
            userStartAppRelations.add(userStartAppRelation);
        }
        final UserStartAppRelResponseWrapper userStartAppRelResponse = new UserStartAppRelResponseWrapper(userStartAppRelations);
        return userStartAppRelResponse;
    }
    
    
    
    @Override
    public UserStartAppRelResponseWrapper findUserStartAppRelByStartAppId(final String startAppId, final ValidationRequest validationRequest) {
    	final boolean isViewInActive = validator.isViewInactiveAllowed(validationRequest);
		LOG.info("findByUserIdProjectIdAAId isViewInActive={}", isViewInActive);
    	final List<UserStartAppRelTbl> userStartAppRelTbls = this.userStartAppRelJpaDao.findByStartApplicationId(new StartApplicationsTbl(startAppId));
        if (userStartAppRelTbls.isEmpty()) {
            throw new RuntimeException("No User Start App Relation Found");
        }
        List<UserStartAppRelTbl> newUserStartAppTbls = new ArrayList<>();
        for (UserStartAppRelTbl userStartAppRelTbl : userStartAppRelTbls) {
        	final StartApplicationsTbl startApplicationsTbl = userStartAppRelTbl.getStartApplicationId();
        	final StartApplicationsTbl filterStartApplicationsTbl = validator.filterStartApplicationResponse(isViewInActive, startApplicationsTbl);
        	if (null != filterStartApplicationsTbl) {
        		final UserStartAppRelTbl filterUserStartAppRelTbl = validator.filterUserStartApplicationResponse(isViewInActive, userStartAppRelTbl);
        		if (null != filterUserStartAppRelTbl) {
        			newUserStartAppTbls.add(userStartAppRelTbl);
        		}
        	}
		}
        final List<UserStartAppRelation> userStartAppRelations = new ArrayList<>();
        for (final UserStartAppRelTbl appRelTbl : newUserStartAppTbls) {
        	final UsersTbl usersTbl = appRelTbl.getUserId();
        	final UsersTbl filterUserTbl = validator.filterUserResponse(isViewInActive, usersTbl);
        	if (null != filterUserTbl) {
        		final UserStartAppRelation userStartAppRelation = new UserStartAppRelation();
        		userStartAppRelation.setUserStartAppRelId(appRelTbl.getUserStartAppRelId());
        		userStartAppRelation.setStartApplicationId(appRelTbl.getStartApplicationId());
        		userStartAppRelation.setStatus(appRelTbl.getStatus());
        		userStartAppRelation.setUserId(usersTbl);
        		userStartAppRelations.add(userStartAppRelation);
        	}
        }
        final UserStartAppRelResponseWrapper userStartAppRelResponse = new UserStartAppRelResponseWrapper(userStartAppRelations);
        return userStartAppRelResponse;
    }

    /**
     *
     * @param id
     * @return Boolean
     */
    @Override
    public Boolean deleteById(String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
        	this.userStartAppRelJpaDao.delete(id);
        	isDeleted = true;
        } catch (Exception e) {
        	if (e instanceof EmptyResultDataAccessException) {
        		final String[] param = {id};
        		throw new XMObjectNotFoundException("User not found", "P_ERR0023", param);
        	}
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }

	@Override
	public UserStartAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
		LOG.info(">> multiDelete");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		ids.forEach(id -> {
			UserStartAppRelTbl userStartAppRelTbls = this.userStartAppRelJpaDao.findOne(id);
			try {
				if(userStartAppRelTbls != null) {
					this.deleteById(id);
					this.userStartAppRelAuditMgr.userStartAppMultiDeleteSuccessAudit(userStartAppRelTbls, httpServletRequest);
				} else {
					throw new XMObjectNotFoundException("Relation not found", "ERR0003");
				}
				
			} catch (XMObjectNotFoundException objectNotFound) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		});
		UserStartAppRelResponse userResponse = new UserStartAppRelResponse(statusMaps);
		LOG.info(">> multiDelete");
		return userResponse;
	}
}
