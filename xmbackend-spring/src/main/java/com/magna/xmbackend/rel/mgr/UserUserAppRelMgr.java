package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelResponse;

/**
 *
 * @author vijay
 */
public interface UserUserAppRelMgr {

    /**
     *
     * @param userUserAppRelRequest
     * @param userName
     * @return UserUserAppRelTbl
     */
    UserUserAppRelTbl create(final UserUserAppRelRequest userUserAppRelRequest,
            final String userName);

    /**
     *
     * @param userUserAppRelBatchRequest
     * @param userName
     * @return UserUserAppRelBatchResponse
     */
    UserUserAppRelBatchResponse createBatch(final UserUserAppRelBatchRequest userUserAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param userId
     * @return UserUserAppRelResponse
     */
    UserUserAppRelResponse findUserUserAppRelsByUserId(final String userId);

    /**
     *
     * @param userId
     * @param siteAARelId
     * @param validationRequest 
     * @return UserUserAppRelResponse
     */
    UserUserAppRelResponse findUserUserAppRelsByUserIdAndSiteAARelId(final String userId, final String siteAARelId, final ValidationRequest validationRequest);

    /**
     *
     * @param userId
     * @param relType
     * @return relType
     */
    UserUserAppRelResponse findUserUserAppRelsByUserIdAndRelationType(final String userId,
            final String relType);

    /**
     *
     * @param userId
     * @param siteAARelId
     * @param relType
     * @param validationRequest 
     * @return UserUserAppRelResponse
     */
    UserUserAppRelResponse findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType(final String userId,
            final String siteAARelId, final String relType, final ValidationRequest validationRequest);

    /**
     *
     * @param userAppId
     * @param validationRequest 
     * @return UserUserAppRelResponse
     */
    UserUserAppRelResponseWrapper findByUserAppId(final String userAppId, final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return boolean
     */
    Boolean deleteById(final String id);
    
    
    UserUserAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param userId
     * @param adminAreaId
     * @param validationRequest 
     * @return UserUserAppRelResponseWrapper
     */
    List<UserUserAppRelation> findUserUserAppRelsByUserIdAndAAId(final String userId,
            final String adminAreaId, ValidationRequest validationRequest);

    /**
     *
     * @param userUserAppRelBatchRequest
     * @return UserUserAppRelBatchResponse
     */
    UserUserAppRelBatchResponse updateUserRelTypesByIds(final UserUserAppRelBatchRequest userUserAppRelBatchRequest);

}
