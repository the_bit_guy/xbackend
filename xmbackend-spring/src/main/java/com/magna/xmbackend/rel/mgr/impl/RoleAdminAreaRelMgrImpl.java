package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.RoleAdminAreaAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.RoleAdminAreaRelJpaDao;
import com.magna.xmbackend.mgr.RoleMgr;
import com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;

/**
 * The Class RoleAdminAreaRelMgrImpl.
 *
 * @author vijay
 */
@Component
public class RoleAdminAreaRelMgrImpl implements RoleAdminAreaRelMgr {

    /** The Constant LOG. */
    private static final Logger LOG
            = LoggerFactory.getLogger(RoleAdminAreaRelMgrImpl.class);

    /** The role admin area rel jpa dao. */
    @Autowired
    private RoleAdminAreaRelJpaDao roleAdminAreaRelJpaDao;

    /** The role mgr. */
    @Autowired
    private RoleMgr roleMgr;

    /** The message maker. */
    @Autowired
    private MessageMaker messageMaker;
    
    /** The validator. */
    @Autowired
    private Validator validator;
    
    @Autowired
    private RoleAdminAreaAuditMgr adminAreaAuditMgr;

    /**
     * Creates the.
     *
     * @param roleAdminAreaRelRequestList the role admin area rel request list
     * @return RoleAdminAreaRelResponse
     */
    @Override
    public RoleAdminAreaRelResponse create(final List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList) {
        RoleAdminAreaRelResponse roleAdminAreaRelResponse = null;
        LOG.info(">> create");
        final List<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        if (null != roleAdminAreaRelRequestList) {
            final List<String> roleAdminAreaList = new ArrayList<>();
            roleAdminAreaRelRequestList.forEach(roleAdminAreaRelRequest -> {
                try {
                    boolean isValid = this.checkRoleIdAAIdExists(roleAdminAreaRelRequest, roleAdminAreaList);
                    if (isValid) {
                        final RoleAdminAreaRelTbl roleAdminAreaRelTblIn = this.convert2Entity(roleAdminAreaRelRequest);
                        final RoleAdminAreaRelTbl roleAdminAreaRelTblOut = this.roleAdminAreaRelJpaDao.save(roleAdminAreaRelTblIn);
                        roleAdminAreaRelTbls.add(roleAdminAreaRelTblOut);
                    }
                } catch (CannotCreateRelationshipException ccre) {
                    final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                    statusMaps.add(statusMap);
                }
            });
            roleAdminAreaList.clear();
            roleAdminAreaRelResponse = new RoleAdminAreaRelResponse(roleAdminAreaRelTbls, statusMaps);
        }
        LOG.info("<< create");
        return roleAdminAreaRelResponse;
    }

    /**
     * Check role id AA id exists.
     *
     * @param roleAdminAreaRelRequest the role admin area rel request
     * @param roleAdminAreaList the role admin area list
     * @return boolean
     */
    private boolean checkRoleIdAAIdExists(final RoleAdminAreaRelRequest roleAdminAreaRelRequest,
            final List<String> roleAdminAreaList) {
        boolean isValid = true;
        Map<String, String> adminAreaNameMap = null;
        String roleName = "";
        final String roleIdIn = roleAdminAreaRelRequest.getRoleId();
        final String adminAreaIdIn = roleAdminAreaRelRequest.getAdminAreaId();
        if (!adminAreaIdIn.equals("") && !roleIdIn.equals("")) {
            final String roleAdminAreaIn = roleIdIn.concat("-").concat(adminAreaIdIn);
            // This block checks duplicate entries in the request itself
            // This is done to avoid hitting database
            if (!roleAdminAreaList.contains(roleAdminAreaIn)) {
                roleAdminAreaList.add(roleAdminAreaIn);
            } else {
                LOG.debug("Duplicate role admin area in request");
                isValid = false;
            }
            if (isValid) {
                final RolesTbl rolesTblIn = this.roleMgr.findById(roleIdIn);
                if (null != rolesTblIn) {
                    roleName = rolesTblIn.getName();
                    final Collection<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = rolesTblIn.getRoleAdminAreaRelTblCollection();
                    LOG.debug("size {}", roleAdminAreaRelTbls.size());
                    for (final RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTbls) {
                        final String dbAdminAreaId = roleAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
                        final String dbRoleId = roleAdminAreaRelTbl.getRoleId().getRoleId();
                        LOG.debug("dbAdminAreaId={} dbRoleId={}", dbAdminAreaId, dbRoleId);
                        if (adminAreaIdIn.equalsIgnoreCase(dbAdminAreaId)
                                && dbRoleId.equalsIgnoreCase(roleIdIn)) {
                            roleName = roleAdminAreaRelTbl.getRoleId().getName();
                            adminAreaNameMap
                                    = messageMaker.getAdminAreaNames(roleAdminAreaRelTbl.getAdminAreaId());
                            isValid = false;
                        }
                    }
                }
            }
            if (!isValid && null != roleName && !roleName.equals("")
                    && null != adminAreaNameMap && !adminAreaNameMap.isEmpty()) {
                final Map<String, String> roleMap = new HashMap<>();
                roleMap.put("en", roleName);
                roleMap.put("de", roleName);
                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(roleMap, adminAreaNameMap);
                LOG.debug("paramMap={}", paramMap);
                throw new CannotCreateRelationshipException("Realtionship exists", "RAA_ERR0002", paramMap);
            }
        }
        return isValid;
    }

    /**
     * Convert 2 entity.
     *
     * @param roleAdminAreaRelRequest the role admin area rel request
     * @return RoleAdminAreaRelTbl
     */
    private RoleAdminAreaRelTbl convert2Entity(final RoleAdminAreaRelRequest roleAdminAreaRelRequest) {

        final String roleAdminAreaRelationId = UUID.randomUUID().toString();

        final String adminAreaId = roleAdminAreaRelRequest.getAdminAreaId();
        final String roleId = roleAdminAreaRelRequest.getRoleId();
        final Date date = new Date();

        final RoleAdminAreaRelTbl roleAdminAreaRelTbl = new RoleAdminAreaRelTbl(roleAdminAreaRelationId);
        roleAdminAreaRelTbl.setAdminAreaId(new AdminAreasTbl(adminAreaId));
        roleAdminAreaRelTbl.setRoleId(new RolesTbl(roleId));
        roleAdminAreaRelTbl.setCreateDate(date);
        roleAdminAreaRelTbl.setUpdateDate(date);

        return roleAdminAreaRelTbl;
    }

    /**
     * Delete by id.
     *
     * @param roleAdminAreaRelId the role admin area rel id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String roleAdminAreaRelId) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
        	this.roleAdminAreaRelJpaDao.delete(roleAdminAreaRelId);
        	isDeleted = true;
        } catch (Exception e) {
        	if (e instanceof EmptyResultDataAccessException) {
        		final String[] param = {roleAdminAreaRelId};
        		throw new XMObjectNotFoundException("User not found", "P_ERR0032", param);
        	}
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }
    
    
    /* (non-Javadoc)
     * @see com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr#multiDelete(java.util.Set)
     */
    @Override
	public RoleAdminAreaRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
    	LOG.info(">> multiDelete");
    	final List<Map<String, String>> statusMaps = new ArrayList<>();
    	ids.forEach(id -> {
    		RoleAdminAreaRelTbl roleAdminAreaRelTbl = this.roleAdminAreaRelJpaDao.findOne(id);
    		try {
    			if(roleAdminAreaRelTbl != null){
    				this.deleteById(id);
    				this.adminAreaAuditMgr.roleAdminAreaMultiDeleteSuccess(roleAdminAreaRelTbl, httpServletRequest);
    			} else {
    				throw new XMObjectNotFoundException("Relation not found", "ERR0003");
    			}
    			
    		} catch (XMObjectNotFoundException objectNotFound) {
    			Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
    			statusMaps.add(statusMap);
    		}
    	});
    	RoleAdminAreaRelResponse roleAdminAreaRelResponse = new RoleAdminAreaRelResponse(statusMaps);
    	LOG.info(">> multiDelete");
    	return roleAdminAreaRelResponse;
	}

    /**
     * Find by id.
     *
     * @param roleAdminAreaId the role admin area id
     * @return RoleAdminAreaRelTbl
     */
    @Override
    public RoleAdminAreaRelTbl findById(final String roleAdminAreaId) {
        LOG.info(">> findById {}", roleAdminAreaId);
        final RoleAdminAreaRelTbl roleAdminAreaRelTbl = this.roleAdminAreaRelJpaDao.findOne(roleAdminAreaId);
        LOG.info("<< findById");
        return roleAdminAreaRelTbl;
    }

    /* (non-Javadoc)
     * @see com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr#findRoleAdminAreaRelTblByRoleId(java.lang.String, com.magna.xmbackend.vo.permission.ValidationRequest)
     */
    @Override
    public RoleAdminAreaRelResponse findRoleAdminAreaRelTblByRoleId(final String roleId, final ValidationRequest validationRequest) {
        RoleAdminAreaRelResponse roleAdminAreaRelResponse = null;
        LOG.info(">> findById {}", roleId);
        final RolesTbl rolesTbl = roleMgr.findById(roleId);
        final List<RoleAdminAreaRelTbl> responseList = new ArrayList<>();
        if (null != rolesTbl) {
        	final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            final Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls = this.roleAdminAreaRelJpaDao.findRoleAdminAreaRelTblByRoleId(rolesTbl);
            AdminAreasTbl adminAreaTbl;
            for (final RoleAdminAreaRelTbl roleAdminAreaRelTbl : roleAdminAreaRelTbls) {
				if (null != roleAdminAreaRelTbl && (adminAreaTbl = roleAdminAreaRelTbl.getAdminAreaId()) != null) {
					AdminAreasTbl adminAreasTbl = validator.filterAdminAreaResponse(isViewInactive, adminAreaTbl);
					if (null != adminAreasTbl && null != adminAreasTbl.getAdminAreaId()) {
						responseList.add(roleAdminAreaRelTbl);
	                }
				}
			}
            roleAdminAreaRelResponse = new RoleAdminAreaRelResponse(responseList, null);
        } else {
            throw new XMObjectNotFoundException("RoleId not found", "P_ERR0001");
        }
        LOG.info("<< findById");
        return roleAdminAreaRelResponse;
    }

    /**
     * Find role admin area id by AA id.
     *
     * @param adminAreaId the admin area id
     * @return RoleAdminAreaRelTbl
     */
    @Override
    public RoleAdminAreaRelTbl findRoleAdminAreaIdByAAId(final String adminAreaId) {
        RoleAdminAreaRelTbl roleAdminAreaRelTbl = null;
        LOG.info(">> findRoleAdminAreaIdByAAId {}", adminAreaId);
        if (null != adminAreaId) {
            roleAdminAreaRelTbl = this.roleAdminAreaRelJpaDao.findRoleAdminAreaRelTblByAAId(new AdminAreasTbl(adminAreaId));
            LOG.info("<< findRoleAdminAreaIdByAAId");
        }
        return roleAdminAreaRelTbl;
    }
}
