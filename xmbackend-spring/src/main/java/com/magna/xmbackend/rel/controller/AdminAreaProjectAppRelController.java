package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectAppRelMgr;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponseWrapper;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/adminAreaProjectAppRel")
public class AdminAreaProjectAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaProjectAppRelController.class);
    @Autowired
    private AdminAreaProjectAppRelMgr adminAreaProjectAppRelMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaProjectAppAuditMgr adminAreaProjectAppAuditMgr;
    

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectAppRelRequest
     * @return AdminAreaProjAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminAreaProjAppRelTbl aaparOut
                = adminAreaProjectAppRelMgr.create(adminAreaProjectAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aaparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectAppRelBatchRequest
     * @return AdminAreaProjectAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        AdminAreaProjectAppRelBatchResponse aaparbr = null;
		try {
			aaparbr = adminAreaProjectAppRelMgr.createBatch(adminAreaProjectAppRelBatchRequest, userName);
			this.adminAreaProjectAppAuditMgr.adminAreaProjectAppMultiSaveAuditor(adminAreaProjectAppRelBatchRequest, aaparbr, httpServletRequest);
		} catch (Exception ex) {
			this.adminAreaProjectAppAuditMgr.adminAreaProjectAppMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(aaparbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = adminAreaProjectAppRelMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(adminAreaProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaProjAppRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjAppRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = adminAreaProjectAppRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(adminAreaProjAppRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = adminAreaProjectAppRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }
    
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        final AdminAreaProjectAppRelResponse appRelResponse = adminAreaProjectAppRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<AdminAreaProjectAppRelResponse> responseEntity
                = new ResponseEntity<>(appRelResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.adminAreaProjectAppRelMgr.updateStatusById(status, id);
        this.adminAreaProjectAppAuditMgr.adminAreaProjectAppUpdateAuditor(updateStatusById, status, id, httpServletRequest);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectAppRelBatchRequest
     * @return AdminAreaProjectAppRelBatchResponse
     */
    @RequestMapping(value = "/updateRelTypesByIds",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelBatchResponse> updateRelTypesByIds(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest) {
        LOG.info("> updateRelTypesByIds");
        final AdminAreaProjectAppRelBatchResponse batchResp = this.adminAreaProjectAppRelMgr.updateRelTypesByIds(adminAreaProjectAppRelBatchRequest);
        this.adminAreaProjectAppAuditMgr.adminAreaProjectAppRelUpdateAuditor(batchResp, httpServletRequest);
        LOG.info("< updateRelTypesByIds");
        return new ResponseEntity<>(batchResp, HttpStatus.ACCEPTED);
    }

    /**
     * Find admin project app rel by AA pro rel.
     *
     * @param httpServletRequest the http servlet request
     * @param adminAreaProjectRelId the admin area project rel id
     * @return the response entity
     */
    @RequestMapping(value = "/findAdminProjectAppRelByAAProRel/{adminAreaProjectRelId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelResponse> findAdminProjectAppRelByAAProRel(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaProjectRelId) {
        LOG.info("> findAdminProjectAppRelByAAProRel {}", adminAreaProjectRelId);
        final AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = adminAreaProjectAppRelMgr.findAdminProjectAppRelByAAProRel(adminAreaProjectRelId);
        LOG.info("< findAdminProjectAppRelByAAProRel");
        return new ResponseEntity<>(adminAreaProjectAppRelResponse, HttpStatus.OK);
    }

    /**
     * Find admin area by project application id.
     *
     * @param httpServletRequest the http servlet request
     * @param projectApplicationId the project application id
     * @return the response entity
     */
    @RequestMapping(value = "/findAdminAreaByProjectApplicationId/{projectApplicationId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppResponseWrapper> findAdminAreaByProjectApplicationId(
            HttpServletRequest httpServletRequest,
            @PathVariable String projectApplicationId) {
        LOG.info("> findAdminAreaByProjectApplicationId {}", projectApplicationId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final List<AdminAreaProjectAppResponse> adminAreaProjectAppResponseList
                = adminAreaProjectAppRelMgr.findAdminAreaByProjectApplicationId(projectApplicationId, validationRequest);
        LOG.info("< findAdminAreaByProjectApplicationId");
        return new ResponseEntity<>(new AdminAreaProjectAppResponseWrapper(adminAreaProjectAppResponseList), HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param aaId
     * @param pId
     * @return AdminAreaProjectAppRelResponse
     */
    @RequestMapping(value = "/findByAAIdAndProjectId/{aaId}/{pId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectAppRelResponse> findByAAIdAndProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String aaId, @PathVariable String pId) {
        LOG.info("> findByAAIdAndProjectId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaProjectAppRelResponse response
                = adminAreaProjectAppRelMgr.findByAAIdAndProjectId(aaId, pId,
                        validationRequest);
        LOG.info("< findByAAIdAndProjectId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
