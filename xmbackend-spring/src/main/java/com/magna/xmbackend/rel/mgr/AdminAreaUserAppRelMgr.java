package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelWrapper;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelResponse;

/**
 *
 * @author vijay
 */
public interface AdminAreaUserAppRelMgr {

    /**
     *
     * @return AdminAreaUserAppRelResponse
     */
    AdminAreaUserAppRelResponse findAll();

    /**
     *
     * @param id
     * @return AdminAreaUserAppRelTbl
     */
    AdminAreaUserAppRelTbl findById(String id);

    /**
     *
     * @param adminAreaUserAppRelRequest
     * @param userName
     * @return AdminAreaUserAppRelTbl
     */
    AdminAreaUserAppRelTbl create(final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest,
            final String userName);

    /**
     *
     * @param adminAreaUserAppRelBatchRequest
     * @param userName
     * @return AdminAreaUserAppRelBatchResponse
     */
    AdminAreaUserAppRelBatchResponse createBatch(final AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);

    AdminAreaUserAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param siteAdminAreaRelTbl
     * @param relType
     * @return AdminAreaUserAppRelTbl
     */
    Iterable<AdminAreaUserAppRelTbl> getAdminAreaUsrAppOnRelType(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final String relType);

    /**
     *
     * @param adminAreaUserAppRelBatchRequest
     * @return AdminAreaUserAppRelBatchResponse
     */
    AdminAreaUserAppRelBatchResponse updateRelTypesByIds(final AdminAreaUserAppRelBatchRequest adminAreaUserAppRelBatchRequest);

    /**
     *
     * @param userAppId
     * @param validationRequest
     * @return AdminAreaUserAppRelResponse
     */
    AdminAreaUserAppRelWrapper findAAUARelByUserAppId(final String userAppId,
            final ValidationRequest validationRequest);

    /**
     *
     * @param aaId
     * @param validationRequest
     * @return AdminAreaUserAppRelWrapper
     */
    AdminAreaUserAppRelWrapper findAAUserAppRelsByAAId(final String aaId,
            final ValidationRequest validationRequest);

}
