package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectAppRelMgr;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class AdminAreaProjectAppRelMgrImpl implements AdminAreaProjectAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaProjectAppRelMgrImpl.class);

    @Autowired
    private AdminAreaProjectAppRelJpaDao adminAreaProjectAppRelJpaDao;

    @Autowired
    private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaProjectAppAuditMgr adminAreaProjectAppAuditMgr;

    /**
     *
     * @return AdminAreaUserAppRelResponse
     */
    @Override
    public final AdminAreaProjectAppRelResponse findAll() {
        final Iterable<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = adminAreaProjectAppRelJpaDao.findAll();
        final AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = new AdminAreaProjectAppRelResponse(adminAreaProjectAppRelTbls);
        return adminAreaProjectAppRelResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaProjAppRelTbl
     */
    @Override
    public final AdminAreaProjAppRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return adminAreaProjAppRelTbl;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequest
     * @param userName
     * @return AdminAreaProjAppRelTbl
     */
    @Override
    public final AdminAreaProjAppRelTbl create(
            final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest,
            final String userName) {
        AdminAreaProjAppRelTbl aaparOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(adminAreaProjectAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfProjAppExistsInProject(adminAreaProjectAppRelRequest);
                final AdminAreaProjAppRelTbl aaparIn
                        = this.convert2Entity(adminAreaProjectAppRelRequest);
                aaparOut = adminAreaProjectAppRelJpaDao.save(aaparIn);
            } else {
                if (assignmentAllowedMap.containsKey("projectApplicationsTbl")) {
                    final Map<String, String> adminAreaTransMap
                            = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                    final Map<String, String> projectAppTransMap
                            = messageMaker.getProjectAppNames((ProjectApplicationsTbl) assignmentAllowedMap.get("projectApplicationsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(adminAreaTransMap, projectAppTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AAPA_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");
        return aaparOut;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isProjectAppAdminAreaInactiveAssignmentAllowed(adminAreaProjectAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "PROJECTAPP_ADMINAREA");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequest
     */
    private void checkIfProjAppExistsInProject(
            final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest) {
        final String adminAreaProjectRelIdIn
                = adminAreaProjectAppRelRequest.getAdminAreaProjectRelId();
        final String projectAppIdIn = adminAreaProjectAppRelRequest.getProjectAppId();

        final AdminAreaProjectRelTbl adminAreaProjectRelTbl
                = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjectRelIdIn);

        final ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
        final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection
                = adminAreaProjectRelTbl.getAdminAreaProjAppRelTblCollection();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl
                : adminAreaProjAppRelTblCollection) {
            final ProjectApplicationsTbl projectApplicationsTbl
                    = adminAreaProjAppRelTbl.getProjectApplicationId();
            final String projectApplicationIdFromTbl
                    = projectApplicationsTbl.getProjectApplicationId();

            if (projectApplicationIdFromTbl != null
                    && projectApplicationIdFromTbl.equalsIgnoreCase(projectAppIdIn)) {
                //throw exception
                //Error Message: Project Application 'name' already exists in Project 'name'.
                final Map<String, String> projectNames
                        = this.getProjectNames(projectsTbl);
                final Map<String, String> projectAppTransNames
                        = this.getProjectAppTransNames(projectApplicationsTbl);
                final Map<String, String[]> i18nCodeMap
                        = messageMaker.geti18nCodeMap(projectAppTransNames, projectNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0005", i18nCodeMap);
            }
        }
    }

    /**
     *
     * @param projectsTbl
     * @return Map
     */
    private Map<String, String> getProjectNames(ProjectsTbl projectsTbl) {
        final String name = projectsTbl.getName();
        final Map<String, String> projectNamesMap = new HashMap<>();
        final Collection<ProjectTranslationTbl> projectTranslationTblCollection
                = projectsTbl.getProjectTranslationTblCollection();
        for (final ProjectTranslationTbl projectTranslationTbl
                : projectTranslationTblCollection) {
            final LanguagesTbl languagesTbl
                    = projectTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            projectNamesMap.put(languageCode, name);
        }
        return projectNamesMap;
    }

    /**
     *
     * @param projectApplicationsTbl
     * @return Map
     */
    private Map<String, String> getProjectAppTransNames(final ProjectApplicationsTbl projectApplicationsTbl) {
        final String name = projectApplicationsTbl.getName();
        final Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectApplicationsTbl.getProjectAppTranslationTblCollection();
        final Map<String, String> projAppNameMap = new HashMap<>();
        for (final ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
            final LanguagesTbl languagesTbl = projectAppTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            projAppNameMap.put(languageCode, name);
        }
        return projAppNameMap;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.adminAreaProjectAppRelJpaDao.setStatusForAdminAreaProjectAppRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        boolean isDeleted = false;
        try {
            this.adminAreaProjectAppRelJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                throw new XMObjectNotFoundException("User not found", "P_ERR0031", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /**
     *
     * @param ids
     * @return AdminAreaProjectAppRelResponse
     */
    @Override
    public AdminAreaProjectAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
            try {
            	AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findOne(id);
            	if (adminAreaProjAppRelTbl != null) {
            		this.delete(id);
            		this.adminAreaProjectAppAuditMgr.adminAreaProjectAppMultiDeleteAuditor(adminAreaProjAppRelTbl, httpServletRequest);
				} else {
					throw new XMObjectNotFoundException("Relation not found", "ERR0003");
				}
                
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = new AdminAreaProjectAppRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return adminAreaProjectAppRelResponse;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequest
     * @return AdminAreaProjAppRelTbl
     */
    private AdminAreaProjAppRelTbl convert2Entity(final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = adminAreaProjectAppRelRequest.getStatus();
        final String adminAreaProjectRelId = adminAreaProjectAppRelRequest.getAdminAreaProjectRelId();
        final String projectAppId = adminAreaProjectAppRelRequest.getProjectAppId();
        final String relationType = adminAreaProjectAppRelRequest.getRelationType();
        final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = new AdminAreaProjAppRelTbl(id);
        adminAreaProjAppRelTbl.setAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjectRelId));
        adminAreaProjAppRelTbl.setStatus(status);
        adminAreaProjAppRelTbl.setRelType(relationType);
        adminAreaProjAppRelTbl.setProjectApplicationId(new ProjectApplicationsTbl(projectAppId));
        return adminAreaProjAppRelTbl;
    }

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param relType
     * @return AdminAreaProjAppRelTbl
     */
    @Override
    public final Iterable<AdminAreaProjAppRelTbl> getAdminAreaProjAppOnRelType(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final String relType) {
        LOG.info(">> getAdminAreaProjAppOnRelType {} {}", adminAreaProjectRelTbl, relType);
        final Iterable<AdminAreaProjAppRelTbl> aauart = this.adminAreaProjectAppRelJpaDao.getAdminAreaProjAppOnRelType(adminAreaProjectRelTbl, relType);
        LOG.info("<< getAdminAreaProjAppOnRelType");
        return aauart;
    }

    /**
     *
     * @param adminAreaProjectAppRelBatchRequest
     * @return AdminAreaProjectAppRelBatchResponse
     */
    @Override
    public AdminAreaProjectAppRelBatchResponse updateRelTypesByIds(final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest) {
        LOG.info(">> updateRelTypesByIds ");
        List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests = adminAreaProjectAppRelBatchRequest.getAdminAreaProjectAppRelRequests();
        List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = null;
        if (!adminAreaProjectAppRelRequests.isEmpty()) {
            adminAreaProjAppRelTbls = new ArrayList<>();
            for (AdminAreaProjectAppRelRequest request : adminAreaProjectAppRelRequests) {
                AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjAppRelId(request.getId());
                adminAreaProjAppRelTbl.setRelType(request.getRelationType());
                adminAreaProjAppRelTbls.add(this.adminAreaProjectAppRelJpaDao.save(adminAreaProjAppRelTbl));
            }

        }
        List<Map<String, String>> statusMaps = null;
        final AdminAreaProjectAppRelBatchResponse batchResponse = new AdminAreaProjectAppRelBatchResponse(adminAreaProjAppRelTbls, statusMaps);
        LOG.info("<< updateRelTypesByIds ");
        return batchResponse;
    }

    /**
     *
     * @param adminAreaProjectAppRelBatchRequest
     * @param userName
     * @return AdminAreaProjectAppRelBatchResponse
     */
    @Override
    public final AdminAreaProjectAppRelBatchResponse createBatch(final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest,
            final String userName) {
        final List<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests
                = adminAreaProjectAppRelBatchRequest.getAdminAreaProjectAppRelRequests();
        for (final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest : adminAreaProjectAppRelRequests) {
            try {
                final AdminAreaProjAppRelTbl aapartOut = this.create(adminAreaProjectAppRelRequest, userName);
                adminAreaProjectAppRelTbls.add(aapartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final AdminAreaProjectAppRelBatchResponse adminAreaProjectAppRelBatchResponse
                = new AdminAreaProjectAppRelBatchResponse(adminAreaProjectAppRelTbls, statusMaps);
        return adminAreaProjectAppRelBatchResponse;
    }

    /**
     *
     * @param adminAreaProjectRelId
     * @return AdminAreaProjectAppRelResponse
     */
    @Override
    public final AdminAreaProjectAppRelResponse findAdminProjectAppRelByAAProRel(String adminAreaProjectRelId) {
        LOG.info(">> findAdminProjectAppRelByAAProRel {}", adminAreaProjectRelId);
        final Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findProjectApplicationIdByAdminAreaProjectRelId(new AdminAreaProjectRelTbl(adminAreaProjectRelId));
        final AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = new AdminAreaProjectAppRelResponse(adminAreaProjAppRelTbls);
        LOG.info("<< findAdminProjectAppRelByAAProRel");
        return adminAreaProjectAppRelResponse;
    }

    /**
     *
     * @param projectApplicationId
     * @param validationRequest
     * @return AdminAreaProjectAppResponse
     */
    @Override
    public final List<AdminAreaProjectAppResponse> findAdminAreaByProjectApplicationId(String projectApplicationId,
            final ValidationRequest validationRequest) {
        final List<AdminAreaProjectAppResponse> adminAreaProjectAppResponseList = new ArrayList<>();
        LOG.info(">> findAdminAreaByProjectApplicationId {}", projectApplicationId);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAdminAreaByProjectApplicationId isInactiveAssignment={}", isInactiveAssignment);
        final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(projectApplicationId, isInactiveAssignment);
        Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection = projectApplicationsTbl.getAdminAreaProjAppRelTblCollection();
        adminAreaProjAppRelTblCollection = validator.filterAdminAreaProjAppRel(isInactiveAssignment, adminAreaProjAppRelTblCollection);
        if (adminAreaProjAppRelTblCollection.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project App Relation found", "ERR0003");
        }
        for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTblCollection) {
            AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjAppRelTbl.getAdminAreaProjectRelId();
            adminAreaProjectRelTbl = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTbl);
            if (null != adminAreaProjectRelTbl) {
                SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                siteAdminAreaRelTbl = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbl);
                if (null != siteAdminAreaRelTbl) {
                    AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    adminAreasTbl = validator.filterAdminAreaResponse(isInactiveAssignment, adminAreasTbl);
                    if (null != adminAreasTbl && !checkIfExist(adminAreaProjectAppResponseList, adminAreasTbl)) {
                        adminAreaProjectAppResponseList.add(new AdminAreaProjectAppResponse(adminAreaProjAppRelTbl, adminAreasTbl));
                    }
                }
            }
        }
        LOG.info("<< findAdminAreaByProjectApplicationId");
        return adminAreaProjectAppResponseList;
    }

    /**
     *
     * @param adminAreaProjectAppResponseList
     * @param adminAreasTblP
     * @return boolean
     */
    private boolean checkIfExist(List<AdminAreaProjectAppResponse> adminAreaProjectAppResponseList, AdminAreasTbl adminAreasTblP) {
        for (AdminAreaProjectAppResponse adminAreaProjectAppResponse : adminAreaProjectAppResponseList) {
            AdminAreasTbl adminAreasTbl = adminAreaProjectAppResponse.getAdminAreasTbl();
            if (adminAreasTbl.getAdminAreaId().equals(adminAreasTblP.getAdminAreaId())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param aaId
     * @param pId
     * @param validationRequest
     * @return AdminAreaProjectAppRelResponse
     */
    @Override
    public AdminAreaProjectAppRelResponse findByAAIdAndProjectId(final String aaId,
            final String pId, final ValidationRequest validationRequest) {
        List<AdminAreaProjAppRelTbl> filteredAdminAreaProjAppRelTbls = new ArrayList<>();
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByAAIdAndProjectId isInactiveAssignment={}", isInactiveAssignment);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(aaId, isInactiveAssignment);
        final ProjectsTbl projectsTbl = validator.validateProject(pId, isInactiveAssignment);
        List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(adminAreasTbl);
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isInactiveAssignment, siteAdminAreaRelTbls);
        if (!siteAdminAreaRelTbls.isEmpty()) {
        	List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRelTbls = new ArrayList<>();
        	for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
        		SitesTbl siteTbl = validator.filterSiteResponse(isInactiveAssignment, siteAdminAreaRelTbl.getSiteId());
        		if (null != siteTbl) {
        			filteredSiteAdminAreaRelTbls.add(siteAdminAreaRelTbl);
        		}
        	}
            List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdInAndProjectId(filteredSiteAdminAreaRelTbls, projectsTbl);
            adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTbls);
            if (!adminAreaProjectRelTbls.isEmpty()) {
            	List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjectRelIdIn(adminAreaProjectRelTbls);
                adminAreaProjAppRelTbls = validator.filterAdminAreaProjAppRel(isInactiveAssignment, adminAreaProjAppRelTbls);
                for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
                	ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
                	ProjectApplicationsTbl filterProjectApplicationTbl = validator.filterProjectApplicationResponse(isInactiveAssignment, projectApplicationsTbl);
                	if (null != filterProjectApplicationTbl) {
                		filteredAdminAreaProjAppRelTbls.add(adminAreaProjAppRelTbl);
                	}
				}
            } else {
                throw new XMObjectNotFoundException("No Relation Found", "S_ERR0014");
            }
        }
        final AdminAreaProjectAppRelResponse response = new AdminAreaProjectAppRelResponse(filteredAdminAreaProjAppRelTbls);
        return response;
    }

}
