package com.magna.xmbackend.rel.controller;

import com.magna.xmbackend.audit.rel.mgr.SiteAdminAreaAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/siteAdminAreaRel")
public class SiteAdminAreaRelController {
    
    private static final Logger LOG
            = LoggerFactory.getLogger(SiteAdminAreaRelController.class);
    @Autowired
    private SiteAdminAreaRelMgr siteAdminAreaRelMgr;
    
    @Autowired
    private Validator validator;
    
    @Autowired
    private SiteAdminAreaAuditMgr siteAdminAreaAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param siteAdminAreaRelRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final SiteAdminAreaRelRequest siteAdminAreaRelRequest) {
        SiteAdminAreaRelTbl saarelOut = null;
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "AREA_SITE");
        try {
            saarelOut = siteAdminAreaRelMgr.create(siteAdminAreaRelRequest, validationRequest);
            this.siteAdminAreaAuditMgr.siteAdminAreaAuditor(saarelOut,
                    httpServletRequest, "SiteAdminArea", "SiteAdminArea Create", "Success");
        } catch (CannotCreateRelationshipException ex) {
            LOG.error("CannotCreateRelationshipException={}", ex.getMessage());
            this.siteAdminAreaAuditMgr.siteAdminAreaCreateFailAuditor(siteAdminAreaRelRequest,
                    ex, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(saarelOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteAdminAreaRelBatchRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "AREA_SITE");
        final SiteAdminAreaRelBatchResponse saarbr
                = siteAdminAreaRelMgr.createBatch(siteAdminAreaRelBatchRequest, validationRequest);
        this.siteAdminAreaAuditMgr.siteAdminAreaMultiSaveAuditor(siteAdminAreaRelBatchRequest, saarbr, httpServletRequest);
        LOG.info("< batchSave");
        return new ResponseEntity<>(saarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelResponse siteAdminAreaRelResponse = siteAdminAreaRelMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(siteAdminAreaRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteAdminAreaRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(siteAdminAreaRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        boolean isDeleted = false;
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = null;
        LOG.info("> deleteById >> {}", id);
        try {
            siteAdminAreaRelTbl = siteAdminAreaRelMgr.findById(id);
            isDeleted = siteAdminAreaRelMgr.delete(id);
            this.siteAdminAreaAuditMgr.siteAdminAreaDeleteSuccessAuditor(id, httpServletRequest, siteAdminAreaRelTbl);
        } catch (XMObjectNotFoundException ex) {
            LOG.error("XMObjectNotFoundException {}", ex.getMessage());
            this.siteAdminAreaAuditMgr.siteAdminAreaDeleteFailureAuditor(id, ex.getMessage(),
                    httpServletRequest, siteAdminAreaRelTbl);
            throw ex;
        } catch (Exception ex) {
            LOG.error("Exception occurred {}", ex.getMessage());
            this.siteAdminAreaAuditMgr.siteAdminAreaDeleteFailureAuditor(id, ex.getMessage(),
                    httpServletRequest, siteAdminAreaRelTbl);
            final String[] param = {id};
            throw new XMObjectNotFoundException("Site Admin Area not found", "P_ERR0026", param);
        }
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return SiteAdminAreaRelResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> deleteById >> {}", ids);
        final SiteAdminAreaRelResponse relResponse = siteAdminAreaRelMgr.multiDelete(ids, httpServletRequest);
        //this.siteAdminAreaAuditMgr.siteAdminAreaMultiDeleteAuditor(relResponse, ids, httpServletRequest);
        final ResponseEntity<SiteAdminAreaRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        boolean updateStatusById = false;
        try {
            updateStatusById = this.siteAdminAreaRelMgr.updateStatusById(status, id);
            this.siteAdminAreaAuditMgr.siteAdminAreaUpdateSuccessAuditor(id, httpServletRequest);
        } catch (Exception ex) {
            LOG.error("Exception occurred in updating status for id={} with Status={}",
                    id, status);
            final StringBuilder errMsg = new StringBuilder("Updating status for id=");
            errMsg.append(id).append(" with status=").append(status).append(" failed");
            this.siteAdminAreaAuditMgr.siteAdminAreaUpdateFailAuditor(id,
                    errMsg.toString(), httpServletRequest);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteAdminAreaRelIdWithAdminAreaProjRelResponse
     */
    @RequestMapping(value = "/findAllProjectsBySiteAdminAreaRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaProjRelResponse>
            findAllProjectsBySiteAdminAreaRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAllProjectsBySiteAdminAreaRelId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelIdWithAdminAreaProjRelResponse response
                = this.siteAdminAreaRelMgr.findAllProjectsBySiteAdminAreaRelId(id, validationRequest);
        LOG.info("< findAllProjectsBySiteAdminAreaRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    @RequestMapping(value = "/findAllUserAppsBySiteAdminAreaRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse>
            findAllUsrAppsBySiteAdminAreaRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAllUsrAppsBySiteAdminAreaRelId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse response
                = this.siteAdminAreaRelMgr.findAllUserAppsBySiteAdminAreaRelId(id, validationRequest);
        LOG.info("< findAllUsrAppsBySiteAdminAreaRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param relType
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    @RequestMapping(value = "/findAllUserAppsBySiteAdminAreaRelId/{id}/{relType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse>
            findAllUsrAppsBySiteAdminAreaRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id, @PathVariable String relType) {
        LOG.info("> findAllUsrAppsBySiteAdminAreaRelId {} {}", id, relType);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse response
                = this.siteAdminAreaRelMgr.findAllUserAppsBySiteAdminAreaRelId(id, relType, validationRequest);
        LOG.info("< findAllUsrAppsBySiteAdminAreaRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse
     */
    @RequestMapping(value = "/findAllStartAppsBySiteAdminAreaRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse>
            findAllStartAppsBySiteAdminAreaRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAllStartAppsBySiteAdminAreaRelId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse response
                = this.siteAdminAreaRelMgr.findAllStartAppsBySiteAdminAreaRelId(id, validationRequest);
        LOG.info("< findAllStartAppsBySiteAdminAreaRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Find site admin area rel by site id.
     *
     * @param httpServletRequest the http servlet request
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findSiteAdminAreaRelBySiteId/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelResponse> findSiteAdminAreaRelBySiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String siteId) {
        LOG.info("> findSiteAdminAreaRelBySiteId {}", siteId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelResponse siteAdminAreaRelResponse
                = siteAdminAreaRelMgr.findSiteAdminAreaRelBySiteId(siteId, validationRequest);
        LOG.info("< findSiteAdminAreaRelBySiteId");
        return new ResponseEntity<>(siteAdminAreaRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return findSiteAdminAreaRelByAAId
     */
    @RequestMapping(value = "/findSiteAdminAreaRelByAAId/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelResponse> findSiteAdminAreaRelByAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findSiteAdminAreaRelByAAId {}", adminAreaId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteAdminAreaRelResponse siteAdminAreaRelResponse
                = siteAdminAreaRelMgr.findSiteAdminAreaRelByAAId(adminAreaId, validationRequest);
        LOG.info("< findSiteAdminAreaRelByAAId");
        return new ResponseEntity<>(siteAdminAreaRelResponse, HttpStatus.OK);
    }
}
