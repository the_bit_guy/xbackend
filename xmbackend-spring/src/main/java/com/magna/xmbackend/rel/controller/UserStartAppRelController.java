package com.magna.xmbackend.rel.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.UserStartAppRelAuditMgr;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.rel.mgr.UserStartAppRelMgr;
import com.magna.xmbackend.response.rel.userstartapp.UserStartAppRelResponseWrapper;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/userStartAppRel")
public class UserStartAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserStartAppRelController.class);

    @Autowired
    private UserStartAppRelMgr userStartAppRelMgr;
    @Autowired
    Validator validator;
    @Autowired
    private UserStartAppRelAuditMgr userStartAppRelAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param userStartAppRelRequest
     * @return UserStartAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserStartAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserStartAppRelRequest userStartAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final UserStartAppRelTbl usartOut
                = this.userStartAppRelMgr.create(userStartAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(usartOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userStartAppRelBatchRequest
     * @return UserStartAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserStartAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserStartAppRelBatchRequest userStartAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        UserStartAppRelBatchResponse usarbr = null;
		try {
			usarbr = userStartAppRelMgr.createBatch(userStartAppRelBatchRequest, userName);
			this.userStartAppRelAuditMgr.userStartAppMultiSaveAuditor(userStartAppRelBatchRequest, usarbr, httpServletRequest);
		} catch (Exception ex) {
			this.userStartAppRelAuditMgr.userStartAppMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(usarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @return UserStartAppRelResponse
     */
    @RequestMapping(value = "/findUserStartAppRelByUserId/{userId}", method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    final ResponseEntity<UserStartAppRelResponse> findUserStartAppRelByUserId(HttpServletRequest httpServletRequest,
            @PathVariable String userId) {
        LOG.info("> findUserStartAppRelByUserId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserStartAppRelResponse usarbr
                = userStartAppRelMgr.findUserStartAppRelByUserId(userId, validationRequest);
        LOG.info("< findUserStartAppRelByUserId");
        return new ResponseEntity<>(usarbr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param relId
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatusByUSARelId/{status}/{relId}", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    final ResponseEntity<Boolean> updateStatusByUSARelId(HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String relId) {
        LOG.info("> updateStatusByUSARelId");
        final Boolean isUpdated
                = userStartAppRelMgr.updateStatusByUSARelId(status, relId);
        LOG.info("< updateStatusByUSARelId");
        return new ResponseEntity<>(isUpdated, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param startAppId
     * @return UserStartAppRelResponseWrapper
     */
    @RequestMapping(value = "/findUserStartAppRelByStartAppId/{startAppId}", method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    final ResponseEntity<UserStartAppRelResponseWrapper> findUserStartAppRelByStartAppId(HttpServletRequest httpServletRequest,
            @PathVariable String startAppId) {
        LOG.info("> findUserStartAppRelByUserId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserStartAppRelResponseWrapper startAppRelResponseWrapper
                = userStartAppRelMgr.findUserStartAppRelByStartAppId(startAppId, validationRequest);
        LOG.info("< findUserStartAppRelByUserId");
        return new ResponseEntity<>(startAppRelResponseWrapper, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    final ResponseEntity<Boolean> delete(HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        final Boolean isDeleted = this.userStartAppRelMgr.deleteById(id);
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/multiDelete", method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    final ResponseEntity<UserStartAppRelResponse> multiDelete(HttpServletRequest httpServletRequest,
    		@RequestBody Set<String> ids) {
        LOG.info("> multiDelete");
        final UserStartAppRelResponse relResponse = this.userStartAppRelMgr.multiDelete(ids, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(relResponse, HttpStatus.OK);
    }
}
