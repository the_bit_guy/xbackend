package com.magna.xmbackend.rel.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.UserProjectAuditMgr;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/userProjectRel")
public class UserProjectRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserProjectRelController.class);
    @Autowired
    private UserProjectRelMgr userProjectRelMgr;
    
    @Autowired
    private UserMgr userMgr;
    @Autowired
    private ProjectMgr projectMgr;
    @Autowired
    private Validator validator;
    @Autowired
    private UserProjectAuditMgr userProjectAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param userProjectRelRequest
     * @return UserProjectRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectRelRequest userProjectRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final UserProjectRelTbl aaparOut = userProjectRelMgr.create(userProjectRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aaparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userProjectRelBatchRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectRelBatchRequest userProjectRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        UserProjectRelBatchResponse uprbr = null;
		try {
			uprbr = userProjectRelMgr.createBatch(userProjectRelBatchRequest, userName);
			this.userProjectAuditMgr.userProjectMultiSaveAuditor(userProjectRelBatchRequest, uprbr, httpServletRequest);
		} catch (Exception ex) {
			this.userProjectAuditMgr.userProjectMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(uprbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
        = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectRelResponse userProjectRelResponse = userProjectRelMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(userProjectRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserProjectRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final UserProjectRelTbl userProjectRelTbl = userProjectRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(userProjectRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = userProjectRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        UserProjectRelResponse relResponse = null;
		try {
			relResponse = userProjectRelMgr.multiDelete(ids, httpServletRequest);
		} catch (Exception ex) {
			
		}
        final ResponseEntity<UserProjectRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.userProjectRelMgr.updateStatusById(status, id);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserProjectRelTbl
     */
    @RequestMapping(value = "/findUserProjectRelationByUserId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelResponse> findUserProjectRelationByUserId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectRelResponse projectRelResponse = userProjectRelMgr.findUserProjectRelationByUserId(id, validationRequest);
        LOG.info("< find");
        return new ResponseEntity<>(projectRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserProjectRelTbl
     */
    @RequestMapping(value = "/findUserProjectRelationByProjectId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelResponse> findUserProjectRelationByProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectRelResponse projectRelResponse = userProjectRelMgr.findUserProjectRelationByProjectId(id, validationRequest);
        LOG.info("< find");
        return new ResponseEntity<>(projectRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param projectId
     * @return String
     */
    @RequestMapping(value = "/findUserProjectExpiryDaysByUserProjectId/{userId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<String> findProjectExpiryDaysByUserProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userId, @PathVariable String projectId) {
        LOG.info("> findUserProjectExpiryDaysByUserProjectId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final String proExpiryDays = userProjectRelMgr.findProjectExpiryDaysByUserProjectId(userId, projectId, validationRequest);
        LOG.info("< findUserProjectExpiryDaysByUserProjectId");
        return new ResponseEntity<>(proExpiryDays, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userProjRelId
     * @param expDays
     * @return Boolean
     */
    @RequestMapping(value = "/updateProjectExpiryDaysById/{userProjRelId}/{expDays}",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> updateProjectExpiryDaysById(
            HttpServletRequest httpServletRequest,
            @PathVariable String userProjRelId, @PathVariable String expDays) {
        LOG.info("> findUserProjectExpiryDaysByUserProjectId");
        final boolean isUpdated = userProjectRelMgr.updateProjectExpiryDaysByUserProjectId(userProjRelId, expDays);
        LOG.info("< findUserProjectExpiryDaysByUserProjectId");
        return new ResponseEntity<>(isUpdated, HttpStatus.OK);
    }
    
    /**
     * Find user project rel by user project id.
     *
     * @param httpServletRequest the http servlet request
     * @param userId the user id
     * @param projectId the project id
     * @return the response entity
     */
    @RequestMapping(value = "/findUserProjectRelByUserIdProjectId/{userId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserProjectRelTbl> findUserProjectRelByUserProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userId, @PathVariable String projectId) {
        LOG.info("> findUserProjectRelByUserProjectId");
        final UserProjectRelTbl userProjectRelTbl = userProjectRelMgr.findUserProjectRelByUserIdProjectId(userId, projectId);
        LOG.info("< findUserProjectRelByUserProjectId");
        return new ResponseEntity<>(userProjectRelTbl, HttpStatus.OK);
    }
    
    /**
     * Save from cax start batch.
     *
     * @param httpServletRequest the http servlet request
     * @param userProjectRelRequest the user project rel request
     * @return the response entity
     */
    @RequestMapping(value = "/caxstartbatch/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> saveFromCaxStartBatch(HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectRelRequest userProjectRelRequest) {
        LOG.info("> saveFromCaxStartBatch");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        if (null == userProjectRelRequest.getProjectId()) {
        	String projectName = userProjectRelRequest.getProjectName();
        	final ProjectsTbl projectsTbl = this.projectMgr.findByName(userProjectRelRequest.getProjectName());
        	if(null == projectsTbl){
        		String[] param = {projectName};
        		throw new XMObjectNotFoundException("Project not found", "P_ERR0035", param);
        	}
        	userProjectRelRequest.setProjectId(projectsTbl.getProjectId());
        }
        if (null == userProjectRelRequest.getUserId()) {
        	String userName2 = userProjectRelRequest.getUserName();
        	final UsersTbl usersTbl = this.userMgr.findByName(userName2);
        	if(null == usersTbl){
        		if(null == usersTbl){
            		String[] param = {userName2};
            		throw new XMObjectNotFoundException("User not found", "P_ERR0034", param);
            	}
        	}
        	userProjectRelRequest.setUserId(usersTbl.getUserId());
        }
        LOG.info("userName={}", userName);
        final UserProjectRelTbl aaparOut = userProjectRelMgr.create(userProjectRelRequest, userName);
        LOG.info("< saveFromCaxStartBatch");
        if (aaparOut != null) {
        	return new ResponseEntity<>(Boolean.TRUE, HttpStatus.ACCEPTED);
        } else {
        	return new ResponseEntity<>(Boolean.FALSE, HttpStatus.ACCEPTED);
        }
    }
    
    /**
     * Delete by username and project.
     *
     * @param httpServletRequest the http servlet request
     * @param request the request
     * @return the response entity
     */
    @RequestMapping(value = "/caxstartbatch/delete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteByUsernameAndProject(final HttpServletRequest httpServletRequest,
            @RequestBody final UserProjectRelRequest request) {
        LOG.info("> deleteByUsernameAndProject >> {}");
        String userName;
		String projectName;
		if ((projectName = request.getProjectName()) != null && (userName = request.getUserName()) != null) {
			final ProjectsTbl projectsTbl = this.projectMgr.findByName(projectName);
			final UsersTbl usersTbl = this.userMgr.findByName(userName);
			final UserProjectRelTbl userProjectRelTbl = userProjectRelMgr.findUserProjectRelByUserIdProjectId(usersTbl.getUserId(), projectsTbl.getProjectId());
			final boolean isDeleted = userProjectRelMgr.delete(userProjectRelTbl.getUserProjectRelId());
			LOG.info("< deleteByUsernameAndProject");
			return new ResponseEntity<>(isDeleted, HttpStatus.OK);
        }
        LOG.info("< deleteByUsernameAndProject");
        return new ResponseEntity<>(Boolean.FALSE, HttpStatus.OK);
    }
}
