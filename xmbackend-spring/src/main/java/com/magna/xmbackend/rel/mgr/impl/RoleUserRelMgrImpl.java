package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.RoleUserAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.rel.dao.RoleUserRelJpaDao;
import com.magna.xmbackend.mgr.RoleMgr;
import com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr;
import com.magna.xmbackend.rel.mgr.RoleUserRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class RoleUserRelMgrImpl implements RoleUserRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(RoleUserRelMgrImpl.class);

    @Autowired
    private RoleUserRelJpaDao roleUserRelJpaDao;

    @Autowired
    private RoleMgr roleMgr;

    @Autowired
    private RoleAdminAreaRelMgr roleAdminAreaRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private Validator validator;
    @Autowired
    private RoleUserAuditMgr roleUserAuditMgr;

    /**
     *
     * @param roleUserRelRequestList
     * @param userName
     * @return RoleUserRelResponse
     */
    @Override
    public RoleUserRelResponse create(final List<RoleUserRelRequest> roleUserRelRequestList,
            final String userName) {
        RoleUserRelResponse roleUserRelResponse = null;
        LOG.info(">> create");
        final List<RoleUserRelTbl> roleUserRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        if (null != roleUserRelRequestList) {
            final List<String> roleUserList = new ArrayList<>();
            roleUserRelRequestList.forEach(roleUserRelRequest -> {
                try {
                    final Map<String, Object> assignmentAllowedMap
                            = isInActiveAssignmentsAllowed(roleUserRelRequest, userName);
                    LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
                    if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
                        boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
                        if (isAssignmentAllowed) {
                            boolean isValid = this.checkRoleIdUserIdExists(roleUserRelRequest, roleUserList);
                            if (isValid) {
                                final RoleUserRelTbl roleUserRelTblIn = this.convert2Entity(roleUserRelRequest);
                                final RoleUserRelTbl roleUserRelTblOut = this.roleUserRelJpaDao.save(roleUserRelTblIn);
                                roleUserRelTbls.add(roleUserRelTblOut);
                            }
                        } else {
                            if (assignmentAllowedMap.containsKey("usersTbl")
                                    && !assignmentAllowedMap.containsKey("adminAreasTbl")) {
                                final Map<String, String> userTransMap
                                        = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(userTransMap);
                                LOG.debug("paramMap={}", paramMap);
                                throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "RUI_ERR0001", paramMap);
                            } else if (assignmentAllowedMap.containsKey("usersTbl")
                                    && assignmentAllowedMap.containsKey("adminAreasTbl")) {
                                final Map<String, String> adminAreaTransMap
                                        = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                                final Map<String, String> userTransMap
                                        = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                                final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(adminAreaTransMap, userTransMap);
                                LOG.debug("paramMap={}", paramMap);
                                throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "RUAA_ERR0001", paramMap);
                            }
                        }
                    }
                } catch (CannotCreateRelationshipException ccre) {
                    final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                    statusMaps.add(statusMap);
                }
            });
            roleUserList.clear();
            roleUserRelResponse = new RoleUserRelResponse(roleUserRelTbls, statusMaps);
        }
        LOG.info("<< create");
        return roleUserRelResponse;
    }

    /**
     *
     * @param roleUserRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final RoleUserRelRequest roleUserRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isRoleUserInactiveAssignmentAllowed(roleUserRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "ROLE_USER");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param roleUserRelRequest
     * @param roleUserList
     * @return boolean
     */
    private boolean checkRoleIdUserIdExists(final RoleUserRelRequest roleUserRelRequest,
			List<String> roleUserList) throws CannotCreateRelationshipException {
		boolean isValid = true;
		String userName = "";
		String roleName = "";
		final String roleIdIn = roleUserRelRequest.getRoleId();
		final String userIdIn = roleUserRelRequest.getUserId();
		String roleAdminAreaRelIdIn = roleUserRelRequest.getRoleAdminAreaRelId();
		if (!userIdIn.equals("") && !roleIdIn.equals("")) {
			final String roleUserIn = roleIdIn.concat("-").concat(userIdIn);
			// This block checks duplicate entries in the request itself
			// This is done to avoid hitting database
			if (!roleUserList.contains(roleUserIn)) {
				roleUserList.add(roleUserIn);
			} else {
				LOG.debug("Duplicate role user in request");
				isValid = false;
			}
			if (isValid) {
				List<RoleUserRelTbl> roleUserRelTbls = this.roleUserRelJpaDao
						.findByRoleIdAndUserId(new RolesTbl(roleIdIn), new UsersTbl(userIdIn));
				if (roleUserRelTbls.size() > 0) {
					for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
						String userId = roleUserRelTbl.getUserId().getUserId();
						String roleId = roleUserRelTbl.getRoleId().getRoleId();
						RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleUserRelTbl.getRoleAdminAreaRelId();
						if (roleAdminAreaRelIdIn != null) {

							if (roleAdminAreaRelTbl == null) {
								if (userIdIn.equalsIgnoreCase(userId) && roleIdIn.equalsIgnoreCase(roleId)) {
									userName = roleUserRelTbl.getUserId().getUsername();
									roleName = roleUserRelTbl.getRoleId().getName();
									isValid = false;
								}
							} else {
								RoleUserRelTbl roleUserTbl = this.roleUserRelJpaDao.findByRoleAdminAreaRelIdAndUserId(
										new RoleAdminAreaRelTbl(roleAdminAreaRelIdIn), new UsersTbl(userIdIn));
								if (roleUserTbl != null) {
									userName = roleUserRelTbl.getUserId().getUsername();
									roleName = roleUserRelTbl.getRoleId().getName();
									isValid = false;
								}
							}
						} else if (roleAdminAreaRelIdIn == null) {
							if (roleAdminAreaRelTbl != null) {
								if (userIdIn.equalsIgnoreCase(userId) && roleIdIn.equalsIgnoreCase(roleId)) {
									userName = roleUserRelTbl.getUserId().getUsername();
									roleName = roleUserRelTbl.getRoleId().getName();
									isValid = false;
								} else {
									RoleUserRelTbl roleUserTbl = this.roleUserRelJpaDao.findByRoleAdminAreaRelIdAndUserId(
											new RoleAdminAreaRelTbl(roleAdminAreaRelIdIn), new UsersTbl(userIdIn));
									if (roleUserTbl != null) {
										userName = roleUserRelTbl.getUserId().getUsername();
										roleName = roleUserRelTbl.getRoleId().getName();
										isValid = false;
									}
								}
							} else {
								userId = roleUserRelTbl.getUserId().getUserId();
								roleId = roleUserRelTbl.getRoleId().getRoleId();
								/*RoleUserRelTbl roleUserTbl = this.roleUserRelJpaDao.findByRoleAdminAreaRelIdAndUserId(
										new RoleAdminAreaRelTbl(roleAdminAreaRelIdIn), new UsersTbl(userIdIn));
								if (roleUserTbl != null) {
									userName = roleUserRelTbl.getUserId().getUsername();
									roleName = roleUserRelTbl.getRoleId().getName();
									isValid = false;
								} else*/ if(userId.equalsIgnoreCase(userIdIn) && (roleId.equalsIgnoreCase(roleIdIn))){
									userName = roleUserRelTbl.getUserId().getUsername();
									roleName = roleUserRelTbl.getRoleId().getName();
									isValid = false;
								}
							}
						}
					}
				}
			}
			if (!isValid && null != userName && !userName.equals("")) {
				final Map<String, String> userRoleMap = new HashMap<>();
				userRoleMap.put(userName, roleName);
				final Map<String, String[]> paramMap = this.messageMaker.getUserRoleRelWithi18nCode(userRoleMap);
				LOG.debug("paramMap={}", paramMap);
				throw new CannotCreateRelationshipException("Realtionship exists", "RU_ERR0002", paramMap);
			}
		}
		return isValid;
	}

    /**
     *
     * @param roleUserRelRequestList
     * @param userName
     * @return RoleUserRelResponse
     */
    @Override
    public RoleUserRelResponse createAll(final List<RoleUserRelRequest> roleUserRelRequestList,
            final String userName) {
        RoleUserRelResponse roleUserRelResponse = null;
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        LOG.info(">> createAll");
        final List<RoleUserRelTbl> roleUserRelTbls = new ArrayList<>();
        if (null != roleUserRelRequestList) {
            final List<String> userRoleAdminList = new ArrayList<>();
            roleUserRelRequestList.forEach(roleUserRelRequest -> {
                try {
                    boolean isValid = this.checkIfRoleAdminAreaExistInRoleUserRel(roleUserRelRequest, userRoleAdminList);
                    if (isValid) {
                        final RoleUserRelTbl roleUserRelTblIn = this.convert2Entity(roleUserRelRequest);
                        final RoleUserRelTbl roleUserRelTblOut = this.roleUserRelJpaDao.save(roleUserRelTblIn);
                        roleUserRelTbls.add(roleUserRelTblOut);
                    }
                } catch (CannotCreateRelationshipException ccre) {
                    final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                    statusMaps.add(statusMap);
                }
            });
            roleUserRelResponse = new RoleUserRelResponse(roleUserRelTbls, statusMaps);
            userRoleAdminList.clear();
        }
        LOG.info("<< createAll");
        return roleUserRelResponse;
    }

    /**
     *
     * @param roleUserRelRequest
     * @param userRoleAdminList
     * @return boolean
     */
    private boolean checkIfRoleAdminAreaExistInRoleUserRel(final RoleUserRelRequest roleUserRelRequest,
            final List<String> userRoleAdminList) throws CannotCreateRelationshipException {
        boolean isValid = true;
        final String roleIdIn = roleUserRelRequest.getRoleId();
        final String userIdIn = roleUserRelRequest.getUserId();
        final String roleAdminAreaRelIdIn = roleUserRelRequest.getRoleAdminAreaRelId();
        if (!userIdIn.equals("") && !roleIdIn.equals("")) {
            final String userRoleAdminIn = roleIdIn.concat("-").concat(userIdIn).concat("-").concat(roleAdminAreaRelIdIn);
            // This block checks duplicate entries in the request itself
            // This is done to avoid hitting database
            if (!userRoleAdminList.contains(userRoleAdminIn)) {
                userRoleAdminList.add(userRoleAdminIn);
            } else {
                LOG.debug("Duplicate user role roleAdminAreaRelIdIn in request");
                isValid = false;
            }
            if (isValid) {
                LOG.debug("roleIdIn {} and roleAdminAreaRelIdIn {}", roleIdIn, roleAdminAreaRelIdIn);
                final RolesTbl rolesTblIn = this.roleMgr.findById(roleIdIn);
                if (null != rolesTblIn) {
                    final Collection<RoleUserRelTbl> roleUserRelTbls = rolesTblIn.getRoleUserRelTblCollection();
                    LOG.debug("size {}", roleUserRelTbls.size());
                    for (final RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
                        final RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleUserRelTbl.getRoleAdminAreaRelId();
                        checkUserRoleIdWoAA(roleUserRelRequest, roleUserRelTbl, roleAdminAreaRelTbl);
                    }
                } else {
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    /**
     *
     * @param userName
     * @param roleName
     * @return Map
     */
    private Map<String, String> formUserRoleMap(final String userName,
            final String roleName) {
        final Map<String, String> userRoleMap = new HashMap<>();
        userRoleMap.put("en", "User ".concat(userName).concat(" with Role ").concat(roleName));
        userRoleMap.put("de", "User ".concat(userName).concat(" with Role ").concat(roleName));
        return userRoleMap;
    }

    /**
     *
     * @param roleUserRelRequest
     * @param roleUserRelTbl
     * @param roleAdminAreaRelTbl
     */
    private void checkUserRoleIdWoAA(final RoleUserRelRequest roleUserRelRequest,
            final RoleUserRelTbl roleUserRelTbl,
            final RoleAdminAreaRelTbl roleAdminAreaRelTbl)
            throws CannotCreateRelationshipException {
        final String roleIdIn = roleUserRelRequest.getRoleId();
        final String userIdIn = roleUserRelRequest.getUserId();
        final String roleAdminAreaIdIn = roleUserRelRequest.getRoleAdminAreaRelId();
        final String dbUserId = roleUserRelTbl.getUserId().getUserId();
        final String dbRoleId = roleUserRelTbl.getRoleId().getRoleId();
        if (roleIdIn.equalsIgnoreCase(dbRoleId)
                && userIdIn.equalsIgnoreCase(dbUserId)) {
            final String userName = roleUserRelTbl.getUserId().getUsername();
            final String roleName = roleUserRelTbl.getRoleId().getName();
            // Avoid assigning the same user again to the same role
            if (null == roleAdminAreaRelTbl) {
                final Map<String, String> userRoleMap = new HashMap<>();
                userRoleMap.put(userName, roleName);
                final Map<String, String[]> paramMap = this.messageMaker.getUserRoleRelWithi18nCode(userRoleMap);
                LOG.debug("paramMap={}", paramMap);
                throw new CannotCreateRelationshipException("Realtionship exists", "RU_ERR0003", paramMap);
            } else {
                // Avoid assigning the same user again to the same role &
                // same role admin area id
                final String dbRoleAdminAreaId = roleAdminAreaRelTbl.getRoleAdminAreaRelId();
                if (null != roleAdminAreaIdIn && !roleAdminAreaIdIn.equals("")
                        && null != dbRoleAdminAreaId && !dbRoleAdminAreaId.equals("")
                        && roleAdminAreaIdIn.equalsIgnoreCase(dbRoleAdminAreaId)) {
                    final Map<String, String[]> paramMap = formMessage(userName,
                            roleName, roleAdminAreaRelTbl.getAdminAreaId());
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Realtionship exists", "RU_ERR0004", paramMap);
                } else if (null == roleAdminAreaIdIn || roleAdminAreaIdIn.equals("")) {
                    // Throw message as the user is already assigned to the role with role admin area id
                    // so user to role direct assignment is not possible
                    final Map<String, String[]> paramMap = formMessage(userName,
                            roleName, roleAdminAreaRelTbl.getAdminAreaId());
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Realtionship exists", "RU_ERR0005", paramMap);
                }
            }
        }
    }

    /**
     *
     * @param userName
     * @param roleName
     * @param areasTbl
     * @return Map
     */
    private Map<String, String[]> formMessage(final String userName,
            final String roleName, final AdminAreasTbl areasTbl) {
        final Map<String, String> userRoleMap
                = formUserRoleMap(userName, roleName);
        final Map<String, String> adminAreaNameMap
                = messageMaker.getAdminAreaNames(areasTbl);
        final Map<String, String[]> paramMap
                = messageMaker.geti18nCodeMap(userRoleMap, adminAreaNameMap);
        return paramMap;
    }

    /**
     *
     * @param roleUserRelRequest
     * @return RoleUserRelTbl
     */
    private RoleUserRelTbl convert2Entity(final RoleUserRelRequest roleUserRelRequest) {

        final String roleUserRelationId = UUID.randomUUID().toString();

        final String userId = roleUserRelRequest.getUserId();
        final String roleId = roleUserRelRequest.getRoleId();
        final String roleAdminAreaRelId = roleUserRelRequest.getRoleAdminAreaRelId();
        final Date date = new Date();

        final RoleUserRelTbl roleUserRelTbl = new RoleUserRelTbl(roleUserRelationId);
        roleUserRelTbl.setUserId(new UsersTbl(userId));
        roleUserRelTbl.setRoleId(new RolesTbl(roleId));
        if (null != roleAdminAreaRelId && !roleAdminAreaRelId.equals("")) {
            roleUserRelTbl.setRoleAdminAreaRelId(new RoleAdminAreaRelTbl(roleAdminAreaRelId));
        }
        roleUserRelTbl.setCreateDate(date);
        roleUserRelTbl.setUpdateDate(date);

        return roleUserRelTbl;
    }

    /**
     *
     * @param roleUserRelId
     * @return boolean
     */
    @Override
    public boolean deleteById(final String roleUserRelId) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
        	this.roleUserRelJpaDao.delete(roleUserRelId);
        	isDeleted = true;
        } catch (Exception e) {
        	if (e instanceof EmptyResultDataAccessException) {
        		final String[] param = {roleUserRelId};
        		throw new XMObjectNotFoundException("User not found", "P_ERR0033", param);
        	}
        }
        LOG.info("<< deleteById");
        return isDeleted;
    }
    
    
	@Override
	public RoleUserRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
		LOG.info(">> multiDelete");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		ids.forEach(id -> {
			RoleUserRelTbl roleUserRelTbl = this.roleUserRelJpaDao.findOne(id);
			try {
				if(roleUserRelTbl != null) {
					this.deleteById(id);
					this.roleUserAuditMgr.roleUserMultiDeleteSuccessAudit(roleUserRelTbl, httpServletRequest);
				}
				
			} catch (XMObjectNotFoundException objectNotFound) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		});
		RoleUserRelResponse userRelResponse = new RoleUserRelResponse(statusMaps);
		LOG.info(">> multiDelete");
		return userRelResponse;
	}

    /**
     *
     * @param roleId
     * @return RoleUserRelResponse
     */
    @Override
    public RoleUserRelResponse findRoleUserRelTblByRoleId(final String roleId, final ValidationRequest validationRequest) {
        RoleUserRelResponse roleUserRelResponse = null;
        final RolesTbl rolesTbl = roleMgr.findById(roleId);
        if (null != rolesTbl) {
            final Iterable<RoleUserRelTbl> roleUserRelTbls = roleUserRelJpaDao.findRoleUserRelTblByRoleId(rolesTbl);
            if (null != roleUserRelTbls) {
            	final List<RoleUserRelTbl> responseList = new ArrayList<>();
            	final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            	UsersTbl userTbl;
            	for (final RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
					if (null != roleUserRelTbl && (userTbl = roleUserRelTbl.getUserId()) != null) {
    					UsersTbl usersTbl = validator.filterUserResponse(isViewInactive, userTbl);
    					if (null != usersTbl && null != usersTbl.getUserId()) {
							responseList.add(roleUserRelTbl);
    	                }
    				}
				}
                roleUserRelResponse = new RoleUserRelResponse(responseList, null);
            } else {
                throw new XMObjectNotFoundException("No Role User Relationship found for the role id", "RU_ERR0001");
            }
        } else {
            throw new XMObjectNotFoundException("RoleId not found", "P_ERR0001");
        }
        return roleUserRelResponse;
    }

    /**
     *
     * @param roleId
     * @param roleAdminAreaRelId
     * @return RoleUserRelResponse
     */
    @Override
    public RoleUserRelResponse findRoleUserRelTblByRoleIdRoleAARelId(final String roleId,
            final String roleAdminAreaRelId, final ValidationRequest validationRequest) {
        RoleUserRelResponse roleUserRelResponse = null;
        final RolesTbl rolesTbl = roleMgr.findById(roleId);
        if (null != rolesTbl) {
            final RoleAdminAreaRelTbl roleAdminAreaRelTbl = roleAdminAreaRelMgr.findById(roleAdminAreaRelId);
            if (null != roleAdminAreaRelTbl) {
            	final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
                final Iterable<RoleUserRelTbl> roleUserRelTbls = roleUserRelJpaDao.findRoleUserRelTblByRoleIdRoleAARelId(rolesTbl, roleAdminAreaRelTbl);
                if (null != roleUserRelTbls) {
                	List<RoleUserRelTbl> responseList = new ArrayList<>();
                	UsersTbl userTbl;
                	for (final RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
    					if (null != roleUserRelTbl && (userTbl = roleUserRelTbl.getUserId()) != null) {
        					UsersTbl usersTbl = validator.filterUserResponse(isViewInactive, userTbl);
        					if (null != usersTbl && null != usersTbl.getUserId()) {
    							responseList.add(roleUserRelTbl);
        	                }
        				}
    				}
                    roleUserRelResponse = new RoleUserRelResponse(responseList, null);
                } else {
                    throw new XMObjectNotFoundException("No Role User Relationship found for the role id", "RU_ERR0001");
                }
            } else {
                throw new XMObjectNotFoundException("RoleAdminArea not found", "RAA_ERR0001");
            }
        } else {
            throw new XMObjectNotFoundException("RoleId not found", "P_ERR0001");
        }
        return roleUserRelResponse;
    }


}
