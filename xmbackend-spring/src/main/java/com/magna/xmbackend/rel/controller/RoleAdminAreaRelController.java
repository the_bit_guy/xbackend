package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.RoleAdminAreaAuditMgr;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.rel.mgr.RoleAdminAreaRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/roleAdminAreaRelation")
public class RoleAdminAreaRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(RoleAdminAreaRelController.class);

    @Autowired
    private RoleAdminAreaRelMgr roleAdminAreaRelMgr;
    
    @Autowired
    private RoleAdminAreaAuditMgr roleAdminAreaAuditMgr;

	/** The validator. */
	@Autowired
    private Validator validator;

    /**
     *
     * @param httpServletRequest
     * @param roleAdminAreaRelRequestList
     * @return RoleAdminAreaRelResponse
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleAdminAreaRelResponse> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList) {
        LOG.info("> save");
        final RoleAdminAreaRelResponse roleAdminAreaRelResponse
                = this.roleAdminAreaRelMgr.create(roleAdminAreaRelRequestList);
        this.roleAdminAreaAuditMgr.roleAdminAreaCreateSuccessAudit(roleAdminAreaRelResponse,roleAdminAreaRelRequestList, httpServletRequest);
        
        LOG.info("< save");
        return new ResponseEntity<>(roleAdminAreaRelResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleAdminAreaRelId
     * @return boolean
     */
    @RequestMapping(value = "/delete/{roleAdminAreaRelId}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleAdminAreaRelId) {
        LOG.info("> delete");
        final boolean isDeleted = this.roleAdminAreaRelMgr.deleteById(roleAdminAreaRelId);
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }
    
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleAdminAreaRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete");
        final RoleAdminAreaRelResponse relResponse = this.roleAdminAreaRelMgr.multiDelete(ids, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(relResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleAdminAreaId
     * @return RoleAdminAreaRelTbl
     */
    @RequestMapping(value = "/findById/{roleAdminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleAdminAreaRelTbl> findById(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleAdminAreaId) {
        LOG.info("> findById");
        final RoleAdminAreaRelTbl roleAdminAreaRelTbl
                = this.roleAdminAreaRelMgr.findById(roleAdminAreaId);
        LOG.info("< findById");
        return new ResponseEntity<>(roleAdminAreaRelTbl, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return RoleAdminAreaRelResponse
     */
    @RequestMapping(value = "/findRoleAdminAreaRelTblByRoleId/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleAdminAreaRelResponse> findRoleAdminAreaRelTblByRoleId(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findRoleAdminAreaRelTblByRoleId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final RoleAdminAreaRelResponse roleAdminAreaRelResponse = this.roleAdminAreaRelMgr.findRoleAdminAreaRelTblByRoleId(roleId, validationRequest);
        LOG.info("< findRoleAdminAreaRelTblByRoleId");
        return new ResponseEntity<>(roleAdminAreaRelResponse, HttpStatus.ACCEPTED);
    }
}
