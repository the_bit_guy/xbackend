package com.magna.xmbackend.rel.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.ProjectStartAppAuditMgr;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.rel.mgr.ProjectStartAppRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/projectStartAppRel")
public class ProjectStartAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectStartAppRelController.class);
    @Autowired
    private ProjectStartAppRelMgr projectStartAppRelMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private ProjectStartAppAuditMgr projectStartAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param projectStartAppRelRequest
     * @return ProjectStartAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final ProjectStartAppRelRequest projectStartAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final ProjectStartAppRelTbl aaparOut
                = projectStartAppRelMgr.create(projectStartAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aaparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectStartAppRelBatchRequest
     * @return ProjectStartAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final ProjectStartAppRelBatchRequest projectStartAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        ProjectStartAppRelBatchResponse psarbr = null;
		try {
			psarbr = projectStartAppRelMgr.createBatch(projectStartAppRelBatchRequest, userName);
			this.projectStartAppAuditMgr.projectStartAppMultiSaveAuditor(projectStartAppRelBatchRequest, psarbr, httpServletRequest);
		} catch (Exception ex) {
			this.projectStartAppAuditMgr.projectStartAppMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(psarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ProjectStartAppRelResponse projectStartAppRelResponse = projectStartAppRelMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(projectStartAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectStartAppRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final ProjectStartAppRelTbl projectStartAppRelTbl = projectStartAppRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(projectStartAppRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = projectStartAppRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return ProjectStartAppRelResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        final ProjectStartAppRelResponse relResponse = projectStartAppRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<ProjectStartAppRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.projectStartAppRelMgr.updateStatusById(status, id);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     * Find project start app by project site AA id.
     *
     * @param httpServletRequest the http servlet request
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectId the project id
     * @return the response entity
     */
    @RequestMapping(value = "/findProjectStartAppByProjectSiteAAId/{siteId}/{adminAreaId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelResponse> findProjectStartAppByProjectSiteAAId(
            HttpServletRequest httpServletRequest, @PathVariable String siteId,
            @PathVariable String adminAreaId, @PathVariable String projectId) {
        LOG.info("> findProjectStartAppByProjectSiteAAId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectStartAppRelResponse par = projectStartAppRelMgr.findProjectStartAppByProjectSiteAAId(siteId,
                adminAreaId, projectId, validationRequest);
        LOG.info("< findProjectStartAppByProjectSiteAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectRelId
     * @return ProjectStartAppRelResponse
     */
    @RequestMapping(value = "/findStartAppByAdminAreaProjectRelId/{adminAreaProjectRelId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectStartAppRelResponse> findStartAppByAdminAreaProjectRelId(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaProjectRelId) {
        LOG.info("> findStartAppByAdminAreaProjectRelId {}", adminAreaProjectRelId);
        final ProjectStartAppRelResponse projectStartAppRelResponse = projectStartAppRelMgr.findStartAppByAdminAreaProjectRelId(adminAreaProjectRelId);
        LOG.info("< findStartAppByAdminAreaProjectRelId");
        return new ResponseEntity<>(projectStartAppRelResponse, HttpStatus.OK);
    }
}
