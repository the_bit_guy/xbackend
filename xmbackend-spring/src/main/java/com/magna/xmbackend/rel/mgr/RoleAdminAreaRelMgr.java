package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;

/**
 *
 * @author vijay
 */
public interface RoleAdminAreaRelMgr {

    /**
     *
     * @param roleAdminAreaRelRequestList
     * @return RoleAdminAreaRelResponse
     */
    public RoleAdminAreaRelResponse create(List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList);

    /**
     *
     * @param roleAdminAreaRelId
     * @return boolean
     */
    public boolean deleteById(final String roleAdminAreaRelId);
    
    
    RoleAdminAreaRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param roleAdminAreaId
     * @return RoleAdminAreaRelTbl
     */
    public RoleAdminAreaRelTbl findById(final String roleAdminAreaId);

    /**
     *
     * @param roleId
     * @param validationRequest 
     * @return RoleAdminAreaRelResponse
     */
    public RoleAdminAreaRelResponse findRoleAdminAreaRelTblByRoleId(final String roleId, final ValidationRequest validationRequest);
    
    /**
     *
     * @param adminAreaId
     * @return RoleAdminAreaRelTbl
     */
    public RoleAdminAreaRelTbl findRoleAdminAreaIdByAAId(final String adminAreaId);

}
