package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author vijay
 */
public interface UserProjectRelMgr {

    /**
     *
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserProjectRelTbl
     */
    UserProjectRelTbl findById(String id);

    /**
     *
     * @param userProjectRelRequest
     * @param userName
     * @return UserProjectRelTbl
     */
    UserProjectRelTbl create(final UserProjectRelRequest userProjectRelRequest,
            final String userName);

    /**
     *
     * @param userProjectRelBatchRequest
     * @param userName
     * @return UserProjectRelBatchResponse
     */
    UserProjectRelBatchResponse createBatch(final UserProjectRelBatchRequest userProjectRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);
    
    
    UserProjectRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     *
     * @param userId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByUserId(final String userId, final ValidationRequest validationRequest);

    /**
     *
     * @param projectId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId, final ValidationRequest validationRequest);

    /**
     * 
     * @param projectId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId);
    /**
     *
     * @param userId
     * @param projectId
     * @return UserProjectExpiryDaysResponse
     */
    String findProjectExpiryDaysByUserProjectId(final String userId,
            final String projectId, final ValidationRequest validationRequest);

    /**
     *
     * @param userProjRelId
     * @param expDays
     * @return boolean
     */
    boolean updateProjectExpiryDaysByUserProjectId(final String userProjRelId,
            final String expDays);
    
    
    UserProjectRelTbl findUserProjectRelByUserIdProjectId(final String userId,
            final String projectId);

}
