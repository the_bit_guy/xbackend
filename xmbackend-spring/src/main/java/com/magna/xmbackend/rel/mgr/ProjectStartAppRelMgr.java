package com.magna.xmbackend.rel.mgr;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;

/**
 *
 * @author vijay
 */
public interface ProjectStartAppRelMgr {

    /**
     *
     * @return ProjectStartAppRelResponse
     */
    ProjectStartAppRelResponse findAll();

    /**
     *
     * @param id
     * @return ProjectStartAppRelTbl
     */
    ProjectStartAppRelTbl findById(String id);

    /**
     *
     * @param projectStartAppRelRequest
     * @param userName
     * @return ProjectStartAppRelTbl
     */
    ProjectStartAppRelTbl create(final ProjectStartAppRelRequest projectStartAppRelRequest,
            final String userName);

    /**
     *
     * @param projectStartAppRelBatchRequest
     * @param userName
     * @return ProjectStartAppRelBatchResponse
     */
    ProjectStartAppRelBatchResponse createBatch(final ProjectStartAppRelBatchRequest projectStartAppRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    boolean delete(final String id);

    /**
     *
     * @param ids
     * @return ProjectStartAppRelResponse
     */
    ProjectStartAppRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Find project start app by project site AA id.
     *
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectId the project id
     * @param validationRequest
     * @return the project start app rel response
     */
    ProjectStartAppRelResponse findProjectStartAppByProjectSiteAAId(final String siteId,
            final String adminAreaId, final String projectId,
            final ValidationRequest validationRequest);

    /**
     * Find start app by admin area project rel id.
     *
     * @param adminAreaProjectRelId the admin area project rel id
     * @return the project start app rel response
     */
    ProjectStartAppRelResponse findStartAppByAdminAreaProjectRelId(final String adminAreaProjectRelId);
}
