package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.UserProjectAuditMgr;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.mail.mgr.UserProjectRelPreNotification;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.enums.ProjectExpiryKey;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class UserProjectRelMgrImpl implements UserProjectRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserProjectRelMgrImpl.class);

    @Autowired
    private UserProjectRelJpaDao userProjectRelJpaDao;

    @Autowired
    private UserMgr userMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;

    @Autowired
    private UserProjectRelPreNotification userProjectRelPreNotification;

    @Autowired
    private Validator validator;
    
    @Autowired
    private UserProjectAuditMgr userProjectAuditMgr;
    

    /**
     *
     * @return UserProjectRelResponse
     */
    @Override
    public final UserProjectRelResponse findAll(ValidationRequest validationRequest) {
    	LOG.info(">> findAll ");
    	final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
    	LOG.info("findAll isInactiveAssignment={}", isInactiveAssignment);
        Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelJpaDao.findAll();
        userProjectRelTbls = this.validator.filterUserProjectRel(isInactiveAssignment, userProjectRelTbls);
        final UserProjectRelResponse userProjectRelResponse = new UserProjectRelResponse(userProjectRelTbls);
        LOG.info(">> findAll ");
        return userProjectRelResponse;
    }

    /**
     *
     * @param id
     * @return UserProjectRelTbl
     */
    @Override
    public final UserProjectRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return userProjectRelTbl;
    }

    /**
     *
     * @param userProjectRelRequest
     * @param userName
     * @return UserProjectRelTbl
     */
    @Override
    public final UserProjectRelTbl create(final UserProjectRelRequest userProjectRelRequest,
            final String userName) {
        UserProjectRelTbl uparOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(userProjectRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfUserExistsInProject(userProjectRelRequest);
                final UserProjectRelTbl uparIn = this.convert2Entity(userProjectRelRequest);
                uparOut = userProjectRelJpaDao.save(uparIn);

                //send email notification
                this.userProjectRelPreNotification.postToQueue(uparOut,
                        "USER_PROJECT_RELATION_ASSIGN");
            } else {
                if (assignmentAllowedMap.containsKey("projectsTbl")) {
                    final Map<String, String> projectTransMap
                            = messageMaker.getProjectNames((ProjectsTbl) assignmentAllowedMap.get("projectsTbl"));
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userTransMap, projectTransMap);
                    LOG.debug("i18nCodeMap={}", i18nCodeMap);
                    throw new CannotCreateRelationshipException(
                            "Inactive Assignments not allowed", "UP_ERR0001", i18nCodeMap);
                }
            }
        }
        LOG.info("<< create");
        return uparOut;
    }

    /**
     *
     * @param uprr
     */
    //Error Message: User 'name' already exists in Project 'name'.
    private void checkIfUserExistsInProject(final UserProjectRelRequest uprr) {
        final String userIdIn = uprr.getUserId();
        final String projectIdIn = uprr.getProjectId();

        LOG.debug("userId {} and projectId {}", userIdIn, projectIdIn);

        final UsersTbl usersTbl = this.userMgr.findById(userIdIn);
        final Collection<UserProjectRelTbl> userProjectRelTblCollection = usersTbl.getUserProjectRelTblCollection();
        for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTblCollection) {
            final ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
            final String projectIdFromTbl = projectsTbl.getProjectId();
            if (projectIdFromTbl != null && projectIdFromTbl.equalsIgnoreCase(projectIdIn)) {
                //throw ex
                //User 'name' already exists in Project 'name'
                final Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
                final Map<String, String> projectNames = this.messageMaker.getProjectNames(projectsTbl);
                final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userNames, projectNames);
                throw new CannotCreateRelationshipException(
                        "Realtionship exists", "ERR0009", i18nCodeMap);
            }
        }
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.userProjectRelJpaDao.setStatusForUserProjectRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
    public final boolean delete(final String id) {
        LOG.info(">> delete {}", id);
        UserProjectRelTbl uparOut = this.findById(id);
        boolean isDeleted = false;
        try {
        	this.userProjectRelJpaDao.delete(id);
        	isDeleted = true;
        } catch (Exception e) {
        	if (e instanceof EmptyResultDataAccessException) {
        		final String[] param = {id};
        		throw new XMObjectNotFoundException("User not found", "P_ERR0024", param);
        	}
        }
      //send email notification
        this.userProjectRelPreNotification.postToQueue(uparOut,
                "USER_PROJECT_RELATION_REMOVE");
        LOG.info("<< deleteById");
        return isDeleted;
        
    }
    
    
    @Override
	public UserProjectRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
    	LOG.info(">> multiDelete");
    	final List<Map<String, String>> statusMaps = new ArrayList<>();
    	ids.forEach(id -> {
    		UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(id);
    		try {
    			if(userProjectRelTbl != null) {
    				this.delete(id);
    				this.userProjectAuditMgr.userProjectMultiDeleteSuccessAudit(userProjectRelTbl, httpServletRequest);
    			}
    			
    		} catch (XMObjectNotFoundException objectNotFound) {
    			Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
    			statusMaps.add(statusMap);
    		}
    	});
    	UserProjectRelResponse userResponse = new UserProjectRelResponse(statusMaps);
    	LOG.info(">> multiDelete");
    	return userResponse;
	}

    /**
     *
     * @param userProjectRelRequest
     * @return UserProjectRelTbl
     */
    private UserProjectRelTbl convert2Entity(final UserProjectRelRequest userProjectRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = userProjectRelRequest.getStatus();
        final String userId = userProjectRelRequest.getUserId();
        final String projectId = userProjectRelRequest.getProjectId();
        final PropertyConfigTbl propertyConfigTbl = this.propertyConfigJpaDao.findByCategoryAndProperty(ConfigCategory.PROJECTEXPIRYDAYS.toString(), ProjectExpiryKey.PROJECT_EXPIRY_DAYS.toString());
        final String projectExpiryDays = propertyConfigTbl.getValue();

        final UserProjectRelTbl userProjectRelTbl = new UserProjectRelTbl(id);
        userProjectRelTbl.setUserId(new UsersTbl(userId));
        userProjectRelTbl.setStatus(status);
        userProjectRelTbl.setProjectExpiryDays(projectExpiryDays);
        userProjectRelTbl.setProjectId(new ProjectsTbl(projectId));
        return userProjectRelTbl;
    }

    /**
     *
     * @param userProjectRelBatchRequest
     * @param userName
     * @return UserProjectRelBatchResponse
     */
    @Override
    public final UserProjectRelBatchResponse createBatch(final UserProjectRelBatchRequest userProjectRelBatchRequest,
            final String userName) {
        final List<UserProjectRelTbl> userProjectRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<UserProjectRelRequest> userProjectRelRequests
                = userProjectRelBatchRequest.getUserProjectRelRequests();
        for (final UserProjectRelRequest userProjectRelRequest : userProjectRelRequests) {
            try {
                final UserProjectRelTbl aaprtOut = this.create(userProjectRelRequest, userName);
                userProjectRelTbls.add(aaprtOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final UserProjectRelBatchResponse userProjectRelBatchResponse
                = new UserProjectRelBatchResponse(userProjectRelTbls, statusMaps);
        return userProjectRelBatchResponse;
    }

    /**
     *
     * @param userProjectRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final UserProjectRelRequest userProjectRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isUserProjectInactiveAssignmentAllowed(userProjectRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "USER_PROJECT");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param userId
     * @return UserProjectRelResponse
     */
    @Override
    public UserProjectRelResponse findUserProjectRelationByUserId(final String userId, final ValidationRequest validationRequest) {
    	LOG.info(">> findUserProjectRelationByUserId");
    	final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
    	UsersTbl validateUser = validator.validateUser(userId, isInactiveAssignment);
        List<UserProjectRelTbl> userProjRelTblList = this.userProjectRelJpaDao.findByUserId(validateUser);
        List<UserProjectRelTbl> filterUserProjectRels = validator.filterUserProjectRels(isInactiveAssignment, userProjRelTblList);
        List<UserProjectRelTbl> newFilterUserProjectRels = new ArrayList<>();
        for (UserProjectRelTbl userProjectRelTbl : filterUserProjectRels) {
			try{
				this.validateUserProject(userProjectRelTbl, isInactiveAssignment);
				newFilterUserProjectRels.add(userProjectRelTbl);
			}catch (XMObjectNotFoundException e) {
				LOG.info("XMObjectNotFoundException", e);
			}
        	
		}
        if (newFilterUserProjectRels.isEmpty()) {
            throw new RuntimeException("No User Project relationship found");
        }
        final UserProjectRelResponse projectRelResponse = new UserProjectRelResponse(newFilterUserProjectRels);
        LOG.info("<< findUserProjectRelationByUserId");
        return projectRelResponse;
    }
    
    
    

    private void validateUserProject(UserProjectRelTbl userProjectRelTbl, boolean isInactiveAssignment) {
    	String userProjectRelId = userProjectRelTbl.getUserProjectRelId();
		validator.validateUserProjectRel(userProjectRelId, isInactiveAssignment);
	}

	/**
     *
     * @param projectId
     * @return UserProjectRelResponse
     */
    @Override
    public UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId, final ValidationRequest validationRequest) {
    	final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
    	final ProjectsTbl validateProject = validator.validateProject(projectId, isInactiveAssignment);
        final List<UserProjectRelTbl> userProjRelTblList = this.userProjectRelJpaDao.findByProjectId(validateProject);
        List<UserProjectRelTbl> filterUserProjectRels = validator.filterUserProjectRels(isInactiveAssignment, userProjRelTblList);
        List<UserProjectRelTbl> newFilterUserProjectRels = new ArrayList<>();
        for (UserProjectRelTbl userProjectRelTbl : filterUserProjectRels) {
			try{
				String userId = userProjectRelTbl.getUserId().getUserId();
				validator.validateUser(userId, isInactiveAssignment);
				this.validateUserProject(userProjectRelTbl, isInactiveAssignment);
				newFilterUserProjectRels.add(userProjectRelTbl);
			}catch (XMObjectNotFoundException e) {
				LOG.info("XMObjectNotFoundException", e);
			}
        	
		}
        if (newFilterUserProjectRels.isEmpty()) {
            throw new RuntimeException("No User Project relationship found");
        }
        final UserProjectRelResponse projectRelResponse = new UserProjectRelResponse(newFilterUserProjectRels);
        return projectRelResponse;
    }
    
    
    @Override
    public UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId) {
        final List<UserProjectRelTbl> userProjRelTblList = this.userProjectRelJpaDao.findByProjectId(new ProjectsTbl(projectId));
        if (userProjRelTblList.isEmpty()) {
            throw new RuntimeException("No User Project relationship found");
        }
        final UserProjectRelResponse projectRelResponse = new UserProjectRelResponse(userProjRelTblList);
        return projectRelResponse;
    }

    /**
     *
     * @param userId
     * @param projectId
     * @return String
     */
    @Override
    public String findProjectExpiryDaysByUserProjectId(final String userId,
            final String projectId, final ValidationRequest validationRequest) {
        LOG.info(">> findProjectExpiryDaysByUserProjectId");
        String projectExpiryDays = null;
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        final UsersTbl validatedUser = validator.validateUser(userId, isInactiveAssignment);
        final ProjectsTbl validatedProject = validator.validateProject(projectId, isInactiveAssignment);
        UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findByUserIdAndProjId(validatedUser, validatedProject);
        if(userProjectRelTbl != null){
        	validator.validateUserProjectRel(userProjectRelTbl.getUserProjectRelId(), isInactiveAssignment);
        	projectExpiryDays = userProjectRelTbl.getProjectExpiryDays();
        }
        //final String projectExpiryDays = this.userProjectRelJpaDao.findProjectExpiryDaysByUserIdAndProjectId(validatedUser, validatedProject);
        LOG.info("<< findProjectExpiryDaysByUserProjectId");
        return projectExpiryDays;
    }

    /**
     *
     * @param userProjRelId
     * @param expDays
     * @return boolean
     */
    @Override
    public boolean updateProjectExpiryDaysByUserProjectId(final String userProjRelId,
            final String expDays) {
        LOG.info(">> updateProjectExpiryDaysByUserProjectId");
        final int updateVal = this.userProjectRelJpaDao.updateProjectExpiryDays(userProjRelId, expDays);
        boolean isUpdated = false;
        if (updateVal > 0) {
            isUpdated = true;
        }
        LOG.info(">> updateProjectExpiryDaysByUserProjectId");
        return isUpdated;
    }

	@Override
	public UserProjectRelTbl findUserProjectRelByUserIdProjectId(String userId, String projectId) {
		LOG.info(">> findUserProjectRelByUserIdProjectId");
		final UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findByUserIdAndProjId(new UsersTbl(userId), new ProjectsTbl(projectId));
		if(null == userProjectRelTbl){
			throw new XMObjectNotFoundException("User Project relation not found", "U_ERR0009");
		}
		LOG.info("<< findUserProjectRelByUserIdProjectId");
		return userProjectRelTbl;
	}

	
}
