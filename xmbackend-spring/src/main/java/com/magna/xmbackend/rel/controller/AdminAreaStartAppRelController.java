package com.magna.xmbackend.rel.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaStartAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.rel.mgr.AdminAreaStartAppRelMgr;
import com.magna.xmbackend.response.rel.adminareastartapp.AdminAreaStartAppRelationWrapper;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/adminAreaStartAppRel")
public class AdminAreaStartAppRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaStartAppRelController.class);
    @Autowired
    private AdminAreaStartAppRelMgr adminAreaStartAppRelMgr;
    
    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaStartAppAuditMgr adminAreaStartAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param adminAreaStartAppRelRequest
     * @return AdminAreaStartAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminAreaStartAppRelTbl aaparOut
                = adminAreaStartAppRelMgr.create(adminAreaStartAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aaparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaStartAppRelBatchRequest
     * @return AdminAreaStartAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaStartAppRelBatchRequest adminAreaStartAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        AdminAreaStartAppRelBatchResponse aasarbr = null;
		try {
			aasarbr = adminAreaStartAppRelMgr.createBatch(adminAreaStartAppRelBatchRequest, userName);
			this.adminAreaStartAppAuditMgr.adminAreaStartAppMultiSaveAuditor(adminAreaStartAppRelBatchRequest, aasarbr, httpServletRequest);
		} catch (Exception ex) {
			this.adminAreaStartAppAuditMgr.adminAreaStartAppMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(aasarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final AdminAreaStartAppRelResponse adminAreaStartAppRelResponse = adminAreaStartAppRelMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(adminAreaStartAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaStartAppRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl = adminAreaStartAppRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(adminAreaStartAppRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = adminAreaStartAppRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> deleteById >> {}", ids);
        final AdminAreaStartAppRelResponse relResponse = adminAreaStartAppRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<AdminAreaStartAppRelResponse> responseEntity
                = new ResponseEntity<>(relResponse, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.adminAreaStartAppRelMgr.updateStatusById(status, id);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     * 
     * @param httpServletRequest
     * @param startAppId
     * @return AdminAreaStartAppRelationWrapper
     */
    @RequestMapping(value = "/findAdminAreaStartAppRelByStartAppId/{startAppId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaStartAppRelationWrapper> findAdminAreaStartAppRelByStartAppId(
            HttpServletRequest httpServletRequest, @PathVariable String startAppId) {
        LOG.info("> findAdminAreaStartAppRelByStartAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaStartAppRelationWrapper adminAreaStartAppRelRespWrapper 
                = adminAreaStartAppRelMgr.findAASARelByAtartAppId(startAppId, validationRequest);
        LOG.info("< findAdminAreaStartAppRelByStartAppId");
        return new ResponseEntity<>(adminAreaStartAppRelRespWrapper, HttpStatus.OK);
    }
}
