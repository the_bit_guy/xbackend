package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.AdminAreaAudiMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.AdminAreaManager;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

/**
 * The Class AdminAreaController.
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/area")
public class AdminAreaController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaController.class);

    @Autowired
    private AdminAreaManager adminAreaManager;

    @Autowired
    private Validator validator;

    @Autowired
    private AdminAreaAudiMgr adminAreaAudiMgr;

    /**
     *
     * @param httpServletRequest
     * @return XMDunkinResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaResponse aar = adminAreaManager.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreasTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreasTbl> findById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findById");
        final AdminAreasTbl st = this.adminAreaManager.findById(id);
        LOG.info("< findById");
        return new ResponseEntity<>(st, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreasTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody AdminAreaRequest adminAreaRequest) {
        AdminAreasTbl aat = null;
        LOG.info("> save");
        try {
            aat = this.adminAreaManager.save(adminAreaRequest);
            this.adminAreaAudiMgr.adminAreaCreateSuccessAuditor(aat,
                    httpServletRequest);
        } catch (Exception ex) {
            String name = adminAreaRequest.getName();
            LOG.error("Exception / Error occured in creating "
                    + "Admin area with name {}", name);
            this.adminAreaAudiMgr.adminAreaCreateFailureAuditor(
                    adminAreaRequest, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(aat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaRequest
     * @return AdminAreasTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreasTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody AdminAreaRequest adminAreaRequest) {
        LOG.info("> update");
        AdminAreasTbl aaOut = null;
        try {
            aaOut = this.adminAreaManager.update(adminAreaRequest);
            this.adminAreaAudiMgr
                    .adminAreaUpdateSuccessAuditor(aaOut, httpServletRequest);
        } catch (Exception ex) {
            String id = adminAreaRequest.getId();
            LOG.error("Exception / Error occured in updating "
                    + "Admin Area with id {}", id);
            this.adminAreaAudiMgr
                    .adminAreaUpdateFailureAuditor(adminAreaRequest,
                            httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(aaOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        boolean updateStatusById = false;
        AdminAreasTbl aat = null;
        try {
            aat = this.adminAreaManager.findById(id);
            if (null != aat) {
                updateStatusById
                        = this.adminAreaManager.updateStatusById(status, id);
                this.adminAreaAudiMgr.admainAreaUpdateStatusAuditor(status, id,
                        httpServletRequest, updateStatusById, aat);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.info("Exception / Error in updating admin area status for id {}",
                    id);
            this.adminAreaAudiMgr.admainAreaUpdateStatusAuditor(status, id,
                    httpServletRequest, updateStatusById, aat);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean value
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        boolean isDeleted = false;
        AdminAreasTbl aat = null;
        try {
            aat = this.adminAreaManager.findById(id);
            isDeleted = this.adminAreaManager.deleteById(id);
            this.adminAreaAudiMgr.adminAreaDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, aat);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting admin "
                    + "area with id {}", id);
            this.adminAreaAudiMgr.adminAreaDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, aat);
            throw ex;
        }
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param aaIds
     * @return AdminAreaResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> delete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> aaIds) {
        LOG.info("> multiDelete");
        final AdminAreaResponse adminAreaResponse
                = this.adminAreaManager.multiDelete(aaIds, httpServletRequest);
//        this.adminAreaAudiMgr.adminAreaMultiDeleteAuditor(adminAreaResponse, aaIds, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(adminAreaResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaResponse
     */
    @RequestMapping(value = "/findAAByProjectId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findAAByProjectId");
        final AdminAreaResponse aar = adminAreaManager.findAAByProjectId(id);
        LOG.info("< findAAByProjectId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaResponse
     */
    @RequestMapping(value = "/findAAByUserAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByUserAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findAAByUserAppId");
        final AdminAreaResponse aar = adminAreaManager.findAAByUserAppId(id);
        LOG.info("< findAAByUserAppId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaResponse
     */
    @RequestMapping(value = "/findAAByUserProjectAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByUserProjectAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findAAByUserProjectAppId");
        final AdminAreaResponse aar = adminAreaManager.findAAByUserProjectAppId(id);
        LOG.info("< findAAByUserProjectAppId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaResponse
     */
    @RequestMapping(value = "/findAAByStartAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByStartAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findAAByStartAppId");
        final AdminAreaResponse aar = adminAreaManager.findAAByStartAppId(id);
        LOG.info("< findAAByStartAppId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @return AdminAreasTbl
     */
    @RequestMapping(value = "/findByName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreasTbl> findByName(
            HttpServletRequest httpServletRequest,
            @PathVariable String name) {
        LOG.info("> findByName");
        final AdminAreasTbl st = this.adminAreaManager.findByName(name);
        LOG.info("< findByName");
        return new ResponseEntity<>(st, HttpStatus.OK);
    }

    /**
     * Find AA by project id and site id.
     *
     * @param httpServletRequest the http servlet request
     * @param projectId the project id
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findAAByProjectIdAndSiteId/{projectId}/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByProjectIdAndSiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String projectId, @PathVariable String siteId) {
        LOG.info("> findAAByProjectIdAndSiteId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        String tkt = httpServletRequest.getHeader("TKT");
        final AdminAreaResponse aar
                = adminAreaManager.findAAByProjectIdAndSiteId(projectId, tkt,
                        siteId, validationRequest);
        LOG.info("< findAAByProjectIdAndSiteId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     * Find AA by start app id and site id.
     *
     * @param httpServletRequest the http servlet request
     * @param startAppId the start app id
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findAAByStartAppIdAndSiteId/{startAppId}/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByStartAppIdAndSiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String startAppId, @PathVariable String siteId) {
        LOG.info("> findAAByStartAppIdAndSiteId");
        final AdminAreaResponse aar = adminAreaManager.findAAByStartAppIdAndSiteId(startAppId, siteId);
        LOG.info("< findAAByStartAppIdAndSiteId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     * Find AA by user app id and site id.
     *
     * @param httpServletRequest the http servlet request
     * @param userAppId the user app id
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findAAByUserAppIdAndSiteId/{userAppId}/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByUserAppIdAndSiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userAppId, @PathVariable String siteId) {
        LOG.info("> findAAByUserAppIdAndSiteId");
        final AdminAreaResponse aar = adminAreaManager.findAAByUserAppIdAndSiteId(userAppId, siteId);
        LOG.info("< findAAByUserAppIdAndSiteId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }

    /**
     * Find AA by project app id and site id.
     *
     * @param httpServletRequest the http servlet request
     * @param projectAppId the project App id
     * @param siteId the site id
     * @return the response entity
     */
    @RequestMapping(value = "/findAAByProjectAppIdAndSiteId/{projectAppId}/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaResponse> findAAByProjectAppIdAndSiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String projectAppId, @PathVariable String siteId) {
        LOG.info("> findAAByProjectAppIdAndSiteId");
        final AdminAreaResponse aar = adminAreaManager.findAAByProjectAppIdAndSiteId(projectAppId, siteId);
        LOG.info("< findAAByProjectAppIdAndSiteId");
        return new ResponseEntity<>(aar, HttpStatus.OK);
    }
}
