/**
 * 
 */
package com.magna.xmbackend.controller;

import java.sql.SQLSyntaxErrorException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.XMBatchMgr;
import com.magna.xmbackend.xmbatch.XmbatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/xmbatch")
public class XMBatchController {
	
	private static final Logger LOG
    = LoggerFactory.getLogger(XMBatchController.class);
	
	@Autowired
	private XMBatchMgr xmBatchMgr;
	
	
	@RequestMapping(value = "/findResultSet",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<XmbatchResponse> findAll(HttpServletRequest httpServletRequest,
    		@RequestBody String query) throws SQLSyntaxErrorException {
        LOG.info("> findResultSet");
        XmbatchResponse xmbatchResponse = this.xmBatchMgr.findResultSet(query);
        LOG.info("< findResultSet");
        return new ResponseEntity<>(xmbatchResponse, HttpStatus.OK);
    }

}
