package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.mgr.IconMgr;
import com.magna.xmbackend.vo.icon.IconRespWrapper;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.icon.IkonResponse;

@RestController
@RequestMapping(value = "/icon")
public class IconController {

    private static final Logger LOG
            = LoggerFactory.getLogger(IconController.class);

    @Autowired
    private IconMgr iconMgr;

    /**
     *
     * @param httpServletRequest
     * @return IconResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IkonResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        IkonResponse ir = this.iconMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(ir, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return IconsTbl
     */
    @RequestMapping(value = "/findById/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IconsTbl> findById(
            HttpServletRequest httpServletRequest, @PathVariable String id) {
        LOG.info("> findById");
        IconsTbl it = this.iconMgr.findById(id);
        LOG.info("< findById");
        return new ResponseEntity<>(it, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param ikonRequest
     * @return IconsTbl
     */
    @RequestMapping(value = "/findByName",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IconsTbl> findByName(
            HttpServletRequest httpServletRequest,
            @RequestBody IkonRequest ikonRequest) {
        LOG.info("> findByName");
        String iconName = ikonRequest.getIconName();
        IconsTbl it = this.iconMgr.findByName(iconName);
        LOG.info("< findByName");
        return new ResponseEntity<>(it, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param ikonRequest
     * @return IconsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IconsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody IkonRequest ikonRequest) {
        LOG.info("> save");
        IconsTbl it = this.iconMgr.create(ikonRequest);
        LOG.info("< save");
        return new ResponseEntity<>(it, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param ikonRequest
     * @return IconsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IconsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody IkonRequest ikonRequest) {
        LOG.info("> update");
        IconsTbl it = this.iconMgr.update(ikonRequest);
        LOG.info("< update");
        return new ResponseEntity<>(it, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param type
     * @return IkonResponse
     */
    @RequestMapping(value = "/findByType/{type}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IkonResponse> findByType(
            HttpServletRequest httpServletRequest, @PathVariable String type) {
        LOG.info("> findByType");
        IkonResponse ir = this.iconMgr.findByType(type);
        LOG.info("< findByType");
        return new ResponseEntity<>(ir, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return IkonResponse
     */
    @RequestMapping(value = "/type/default",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )

    public @ResponseBody
    final ResponseEntity<IkonResponse> findDefaultIcons(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findDefaultIcons");
        IkonResponse ir = this.iconMgr.findByType("DEFAULT");
        LOG.info("< findDefaultIcons");
        return new ResponseEntity<>(ir, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return IkonResponse
     */
    @RequestMapping(value = "/type/non-default",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IkonResponse> findNonDfaultIcons(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findNonDfaultIcons");
        IkonResponse ir = this.iconMgr.findByType("CUSTOM");
        LOG.info("< findNonDfaultIcons");
        return new ResponseEntity<>(ir, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/usedById/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> isIconUsedById(
            HttpServletRequest httpServletRequest, @PathVariable String id) {
        LOG.info("> isIconUsedById");
        boolean iconUsed = this.iconMgr.isIconUsedById(id);
        LOG.info("< isIconUsedById");
        return new ResponseEntity<>(iconUsed, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param ikonRequest
     * @return Boolean
     */
    @RequestMapping(value = "/usedByName",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> isIconUsedByName(
            HttpServletRequest httpServletRequest,
            @RequestBody IkonRequest ikonRequest) {
        LOG.info("> findByName");
        String iconName = ikonRequest.getIconName();
        boolean iconUsed = this.iconMgr.isIconUsedByName(iconName);
        LOG.info("< isIconUsedByName");
        return new ResponseEntity<>(iconUsed, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        boolean isDeleted = this.iconMgr.delete(id);
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }
    
    /**
     * Find icon id by name.
     *
     * @param httpServletRequest the http servlet request
     * @param ikonRequest the ikon request
     * @return the response entity
     */
    @RequestMapping(value = "/findIconIdByName",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<IconRespWrapper> findIconIdByName(HttpServletRequest httpServletRequest,
            @RequestBody IkonRequest ikonRequest) {
        LOG.info("> findIconIdByName");
        final String iconName = ikonRequest.getIconName();
        final IconsTbl it = this.iconMgr.findByName(iconName);
        LOG.info("< findIconIdByName");
        final String iconId = it.getIconId();
        IconRespWrapper iconRespWrapper = new IconRespWrapper(iconId, iconName);
        return new ResponseEntity<>(iconRespWrapper, HttpStatus.OK);
    }
}
