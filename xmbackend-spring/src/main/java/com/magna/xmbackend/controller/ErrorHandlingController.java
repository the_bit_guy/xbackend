/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.sql.SQLSyntaxErrorException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.CannotDeleteRelationException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.LanguageJpaDao;
import com.magna.xmbackend.utils.ExceptionResponse;
import com.magna.xmbackend.utils.MultiLingualExceptionResponse;

/**
 * The Class ErrorHandlingController.
 *
 * @author dhana
 */
@ControllerAdvice
public class ErrorHandlingController {

    private static final Logger LOG
            = LoggerFactory.getLogger(ErrorHandlingController.class);

    @Autowired
    private LanguageJpaDao languageJpaDao;

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionResponse> generalException(Exception ex) throws Exception {
        LOG.info("Inside generalException with exception {}", ex.getMessage());
        ExceptionResponse exceptionResponse
                = new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(SQLSyntaxErrorException.class)
    public ResponseEntity<ExceptionResponse> sqlSyntaxErrorException(SQLSyntaxErrorException ex) throws Exception {
        LOG.info("Inside SQLSyntaxErrorException with exception {}", ex.getMessage());
        ExceptionResponse exceptionResponse
                = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);

    }
    
    
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ExceptionResponse> emptyResultDataAccessException(EmptyResultDataAccessException ex) throws Exception {
        LOG.info("Inside emptyResultDataAccessException with exception {}", ex.getMessage());
        ExceptionResponse exceptionResponse
                = new ExceptionResponse(HttpStatus.UNPROCESSABLE_ENTITY.value(), ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @ExceptionHandler(CannotCreateRelationshipException.class)
    public ResponseEntity<MultiLingualExceptionResponse>
            cannotCreateRelationshipException(CannotCreateRelationshipException ex)
            throws Exception {
        String errCode = ex.getErrCode();
        String message = ex.getMessage();
        Map<String, String[]> paramMap = ex.getParamMap();
        LOG.info("Inside CannotCreateRelationshipException with exception {}",
                message);

        Map<String, String> lingualMessages = makeMessageMap(errCode, paramMap);
        MultiLingualExceptionResponse exceptionResponse
                = new MultiLingualExceptionResponse(
                        HttpStatus.PRECONDITION_FAILED.value(),
                        errCode, message, lingualMessages);
        return new ResponseEntity<>(exceptionResponse,
                HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @ExceptionHandler(CannotCreateObjectException.class)
    public ResponseEntity<MultiLingualExceptionResponse>
            cannotCreateObjectException(CannotCreateObjectException ex)
            throws Exception {
        String errCode = ex.getErrCode();
        String message = ex.getMessage();
        Map<String, String[]> paramMap = ex.getParamMap();
        String[] param = ex.getParam();
        LOG.info("Inside CannotCreateObjectException with exception {}",
                message);
        Map<String, String> lingualMessages = null;
        if(paramMap != null){
        	lingualMessages = makeMessageMap(errCode, paramMap);
        }else if(param != null){
        	lingualMessages = makeMessageMap(errCode, param);
        }
        MultiLingualExceptionResponse exceptionResponse
                = new MultiLingualExceptionResponse(
                        HttpStatus.PRECONDITION_FAILED.value(),
                        errCode, message, lingualMessages);
        return new ResponseEntity<>(exceptionResponse,
                HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @ExceptionHandler(XMObjectNotFoundException.class)
    public ResponseEntity<MultiLingualExceptionResponse>
            xMObjectNotFoundException(XMObjectNotFoundException ex)
            throws Exception {
        String errCode = ex.getErrCode();
        String message = ex.getMessage();
        String[] param = ex.getParam();
        LOG.info("Inside XMObjectNotFoundException with exception {}",
                message);

        Map<String, String> lingualMessages = makeMessageMap(errCode, param);
        MultiLingualExceptionResponse exceptionResponse
                = new MultiLingualExceptionResponse(
                        HttpStatus.PRECONDITION_FAILED.value(),
                        errCode, message, lingualMessages);
        return new ResponseEntity<>(exceptionResponse,
                HttpStatus.UNPROCESSABLE_ENTITY);

    }
    
    
    @ExceptionHandler(CannotDeleteRelationException.class)
    public ResponseEntity<MultiLingualExceptionResponse>
    	cannotDeleteRelationException(CannotDeleteRelationException ex)
            throws Exception {
        String errCode = ex.getErrCode();
        String message = ex.getMessage();
        String[] param = ex.getParam();
        LOG.info("Inside CannotDeleteRelationException with exception {}",
                message);

        Map<String, String> lingualMessages = makeMessageMap(errCode, param);
        MultiLingualExceptionResponse exceptionResponse
                = new MultiLingualExceptionResponse(
                        HttpStatus.PRECONDITION_FAILED.value(),
                        errCode, message, lingualMessages);
        return new ResponseEntity<>(exceptionResponse,
                HttpStatus.UNPROCESSABLE_ENTITY);

    }

//    private Map<String, String>
//            makeMessageMap(String errCode)
//            throws NoSuchMessageException {
//        Map<String, String> lingualMessages = new HashMap<>();
//        Iterable<LanguagesTbl> languagesTbls = languageJpaDao.findAll();
//        Iterator<LanguagesTbl> iterator = languagesTbls.iterator();
//        while (iterator.hasNext()) {
//            LanguagesTbl languagesTbl = iterator.next();
//            String languageCode = languagesTbl.getLanguageCode();
//            java.util.Locale local = new Locale(languageCode);
//            String message
//                    = messageSource.getMessage(errCode, null, local);
//            lingualMessages.put(languageCode, message);
//        }
//        return lingualMessages;
//    }
    private Map<String, String>
            makeMessageMap(String errCode, String[] param)
            throws NoSuchMessageException {
        Map<String, String> lingualMessages = new HashMap<>();
        Iterable<LanguagesTbl> languagesTbls = languageJpaDao.findAll();
        Iterator<LanguagesTbl> iterator = languagesTbls.iterator();
        while (iterator.hasNext()) {
            LanguagesTbl languagesTbl = iterator.next();
            String languageCode = languagesTbl.getLanguageCode();
            java.util.Locale local = new Locale(languageCode);
            String message
                    = messageSource.getMessage(errCode, param, local);
            lingualMessages.put(languageCode, message);
        }
        return lingualMessages;
    }

    private Map<String, String>
            makeMessageMap(String errCode, Map<String, String[]> paramMap)
            throws NoSuchMessageException {
        Map<String, String> lingualMessages = new HashMap<>();

        Iterable<LanguagesTbl> languagesTbls = languageJpaDao.findAll();
        Iterator<LanguagesTbl> iterator = languagesTbls.iterator();
        while (iterator.hasNext()) {
            LanguagesTbl languagesTbl = iterator.next();
            String languageCode = languagesTbl.getLanguageCode();
            String[] param = paramMap.get(languageCode);
            java.util.Locale local = new Locale(languageCode);
            String message
                    = messageSource.getMessage(errCode, param, local);
            lingualMessages.put(languageCode, message);
        }
        return lingualMessages;
    }
}
