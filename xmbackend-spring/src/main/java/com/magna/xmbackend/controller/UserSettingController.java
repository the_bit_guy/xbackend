/**
 * 
 */
package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.mgr.UserSettingsMgr;
import com.magna.xmbackend.vo.userSettings.UserSettingsRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/userSettings")
public class UserSettingController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserSettingController.class);
	
	@Autowired
	UserSettingsMgr userSettingsMgr;
	
    @RequestMapping(value = "/findUserSettingsBySiteId/{siteId}", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
        public @ResponseBody
        final ResponseEntity<UserSettingsTbl> findAll(HttpServletRequest httpServletRequest, 
        		@PathVariable String siteId) {
            LOG.info("> findUserSettingsBySiteId");
            LOG.info("siteId={}", siteId);
            final String userName = httpServletRequest.getHeader("USER_NAME");
            UserSettingsTbl userSettingsTbls = this.userSettingsMgr.findUserSettingsBySiteId(userName, siteId);
            LOG.info("< findUserSettingsBySiteId");
            return new ResponseEntity<>(userSettingsTbls, HttpStatus.OK);
        }
    
    @RequestMapping(value = "/saveOrUpdateProject", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
        public @ResponseBody
        final ResponseEntity<UserSettingsTbl> saveOrUpdate(HttpServletRequest httpServletRequest, 
        		@RequestBody UserSettingsRequest userSettingsRequest) {
            LOG.info("> saveOrUpdateProject");
            final String userName = (String)httpServletRequest.getAttribute("userName");
		    String tkt = httpServletRequest.getHeader("TKT");
            UserSettingsTbl userSettingsTbl = this.userSettingsMgr.saveOrUpdateProject(userName, userSettingsRequest, tkt);
            LOG.info("< saveOrUpdateProject");
            return new ResponseEntity<>(userSettingsTbl, HttpStatus.ACCEPTED);
        }
    
    
    @RequestMapping(value = "/saveOrUpdateSettings", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
        public @ResponseBody
        final ResponseEntity<UserSettingsTbl> saveOrUpdateSettings(HttpServletRequest httpServletRequest, 
        		@RequestBody UserSettingsRequest userSettingsRequest) {
            LOG.info("> saveOrUpdateSettings");
            final String userName = (String)httpServletRequest.getAttribute("userName");
		    String tkt = httpServletRequest.getHeader("TKT");
            UserSettingsTbl userSettingsTbl = this.userSettingsMgr.saveOrUpdateSettings(userName, userSettingsRequest, tkt);
            LOG.info("< saveOrUpdateSettings");
            return new ResponseEntity<>(userSettingsTbl, HttpStatus.ACCEPTED);
        }

}
