package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.UserAppAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/userApplication")
public class UserApplicationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserApplicationController.class);

    @Autowired
    private UserApplicationMgr userApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    UserAppAuditMgr userAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationResponse uar = userApplicationMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserApplicationsTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationsTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationsTbl uat = userApplicationMgr.findById(id, validationRequest);
        LOG.info("< find");
        return new ResponseEntity<>(uat, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        boolean isDeleted = false;
        UserApplicationsTbl uat = null;
        LOG.info("> deleteById >> {}", id);
        httpServletRequest.setAttribute("userAppId", id);
        try {
            uat = userApplicationMgr.findById(id);
            isDeleted = userApplicationMgr.delete(id);
            this.userAppAuditMgr.userAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, uat);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting user app={}", id);
            this.userAppAuditMgr.userAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, uat);
            throw ex;
        }
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete ");
        httpServletRequest.setAttribute("userAppIds", ids);
        final UserApplicationResponse userApplicationResponse = userApplicationMgr.multiDelete(ids,
                httpServletRequest);
//        this.userAppAuditMgr.userAppMultiDeleteAuditor(userApplicationResponse, ids, httpServletRequest);
        final ResponseEntity<UserApplicationResponse> responseEntity
                = new ResponseEntity<>(userApplicationResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param userApplicationRequest
     * @return UserApplicationsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody UserApplicationRequest userApplicationRequest) {
        UserApplicationsTbl uat = null;
        LOG.info("> save");
        try {
            uat = userApplicationMgr.createOrUpdate(userApplicationRequest,
                    false);
            this.userAppAuditMgr.userAppCreateSuccessAuditor(uat,
                    httpServletRequest);
        } catch (Exception ex) {
            final String name = userApplicationRequest.getName();
            LOG.error("Exception / Error occured in creating "
                    + "User App with name {}", name);
            this.userAppAuditMgr.userAppCreateFailureAuditor(
                    userApplicationRequest, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(uat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userApplicationRequest
     * @return UserApplicationsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody UserApplicationRequest userApplicationRequest) {
        UserApplicationsTbl uat = null;
        LOG.info("> update");
        try {
            uat = this.userApplicationMgr.createOrUpdate(userApplicationRequest,
                    true);
            this.userAppAuditMgr.userAppUpdateSuccessAuditor(uat, httpServletRequest);
        } catch (Exception ex) {
            final String id = userApplicationRequest.getId();
            LOG.error("Exception / Error occured in updating "
                    + "User App with id {}", id);
            this.userAppAuditMgr.userAppUpdateFailureAuditor(userApplicationRequest,
                    httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(uat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        boolean updateStatusById = false;
        UserApplicationsTbl uat = null;
        LOG.info("> updateStatus");
        try {
            uat = userApplicationMgr.findById(id);
            if (null != uat) {
                updateStatusById = this.userApplicationMgr.updateStatusById(status, id);
                this.userAppAuditMgr.userAppUpdateStatusAuditor(status, id,
                        httpServletRequest, updateStatusById, uat);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.info("Exception / Error in updating User app status for id {}",
                    id);
            this.userAppAuditMgr.userAppUpdateStatusAuditor(status, id,
                    httpServletRequest, updateStatusById, uat);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findUserApplicationsByBaseAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findUserApplicationsByBaseAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserApplicationsByBaseAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationResponse uar
                = userApplicationMgr.findUserApplicationsByBaseAppId(id, validationRequest);
        LOG.info("< findUserApplicationsByBaseAppId");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param position
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findUserAppByPosition/{position}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findUserAppByPosition(
            HttpServletRequest httpServletRequest,
            @PathVariable("position") String position) {
        LOG.info("> findUserAppByPosition {}", position);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationResponse uar
                = userApplicationMgr.findUserAppByPosition(position, validationRequest);
        LOG.info("< findUserAppByPosition");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userAppId
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findChildUserAppByUserAppId/{userAppId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findChildUserAppByUserAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable("userAppId") String userAppId) {
        LOG.info("> findChildUserAppByUserAppId {}", userAppId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationResponse uar
                = userApplicationMgr.findUserAppByPosition(userAppId, validationRequest);
        LOG.info("< findChildUserAppByUserAppId");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findAllUserAppByStatus/{status}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findAllUserAppByStatus(
            HttpServletRequest httpServletRequest,
            @PathVariable("status") String status) {
        LOG.info("> findAllUserAppByStatus {}", status);
        final UserApplicationResponse uar
                = userApplicationMgr.findAllUserAppByStatus(status);
        LOG.info("< findAllUserAppByStatus");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findAllUserAppByPositions",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findAllUserAppByPositions(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllUserAppByPositions");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserApplicationResponse uar
                = userApplicationMgr.findAllUserAppByPositions(validationRequest);
        LOG.info("< findAllUserAppByPositions");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @return UserApplicationsTbl
     */
    @RequestMapping(value = "/findByName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationsTbl> findByName(
            HttpServletRequest httpServletRequest,
            @PathVariable String name) {
        LOG.info("> findByName");
        final UserApplicationsTbl userApplication = this.userApplicationMgr.findByName(name);
        LOG.info("< findByName");
        return new ResponseEntity<>(userApplication, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @param langCode
     * @return UserApplicationsTbl
     */
    @RequestMapping(value = "/findByNameAndLangCode/{name}/{langCode}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationsTbl> findByNameAndLanguageCode(
            HttpServletRequest httpServletRequest,
            @PathVariable String name, @PathVariable String langCode) {
        LOG.info("> findByNameAndLanguageCode");
        final UserApplicationsTbl userApplication = this.userApplicationMgr.findByUserNameAndLanguageCode(name, langCode);
        LOG.info("< findByNameAndLanguageCode");
        return new ResponseEntity<>(userApplication, HttpStatus.OK);
    }

    /**
     * Find User Applications based on User id
     *
     * @param httpServletRequest
     * @param id
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findUserApplicationsByUserId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findUserApplicationsByUserId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserApplicationsByUserId");
        final UserApplicationResponse uar = userApplicationMgr.findUserApplicationsByUserId(id);
        LOG.info("< findUserApplicationsByUserId");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return UserApplicationResponse
     */
    @RequestMapping(value = "/findAllUserAppsByAAIdAndActive/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> findAllUserAppsByAAIdAndActive(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findAllUserAppsByAAIdAndActive");
        final UserApplicationResponse uar = userApplicationMgr.findAllUserAppsByAAIdAndActive(adminAreaId);
        LOG.info("< findAllUserAppsByAAIdAndActive");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param adminAreaId
     * @return UserApplicationResponse
     *//*
    @RequestMapping(value = "/getAllUserAppsByUserAAId/{userId}/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationResponse> getAllUserAppsByUserAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userId, @PathVariable String adminAreaId) {
        LOG.info("> getAllUserAppsByUserAAId");
        final UserApplicationResponse uar = userApplicationMgr.getAllUserAppsByUserAAId(userId, adminAreaId);
        LOG.info("< getAllUserAppsByUserAAId");
        return new ResponseEntity<>(uar, HttpStatus.OK);
    }*/

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return UserApplicationMenuWrapper XMenu API
     */
    @RequestMapping(value = "/findUserAppByAAIdAndUserName/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserApplicationMenuWrapper> findUserAppByAAIdAndUserName(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findUserAppByAAIdAndUserName");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        final UserApplicationMenuWrapper userApplicationMenuWrapper
                = userApplicationMgr.findUserAppByAAIdAndUserName(adminAreaId, tkt,
                        userName, validationRequest);
        LOG.info("< findUserAppByAAIdAndUserName");
        return new ResponseEntity<>(userApplicationMenuWrapper, HttpStatus.OK);
    }
}
