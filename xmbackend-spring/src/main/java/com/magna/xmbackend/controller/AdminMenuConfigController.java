package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.AdminMenuConfigAuditMgr;
import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.mgr.AdminMenuConfigMgr;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigCustomeResponseWrapper;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponseWrapper;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/adminMenuConfig")
public class AdminMenuConfigController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminMenuConfigController.class);

    @Autowired
    AdminMenuConfigMgr adminMenuConfigMgr;
    
    @Autowired
    AdminMenuConfigAuditMgr adminMenuConfigAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return AdminMenuConfigResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final AdminMenuConfigResponse adminMenuConfigResponse = adminMenuConfigMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(adminMenuConfigResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminMenuConfigRequest
     * @return AdminMenuConfigTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody AdminMenuConfigRequest adminMenuConfigRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminMenuConfigTbl adminMenuConfigTbl
                = adminMenuConfigMgr.save(adminMenuConfigRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(adminMenuConfigTbl, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminMenuConfigRequestList
     * @return AdminMenuConfigResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigResponse> multiSave(
            HttpServletRequest httpServletRequest,
            @RequestBody List<AdminMenuConfigRequest> adminMenuConfigRequestList) {
        LOG.info("> multiSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminMenuConfigResponse response
                = adminMenuConfigMgr.multiSave(adminMenuConfigRequestList, userName);
        this.adminMenuConfigAuditMgr.amcCreateSuccessAudit(httpServletRequest, adminMenuConfigRequestList, response);
        LOG.info("< multiSave");
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteAdminMenuById >>{} ", id);
        final boolean isDeleted = adminMenuConfigMgr.deleteById(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return AdminMenuConfigResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> deleteAdminMenuById >>{} ", ids);
        final AdminMenuConfigResponse configResponse = adminMenuConfigMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<AdminMenuConfigResponse> responseEntity
                = new ResponseEntity<>(configResponse, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param objectType
     * @return AdminMenuConfigResponse
     */
    @RequestMapping(value = "/findByObjectType/{objectType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigCustomeResponseWrapper> findByObjectType(
            HttpServletRequest httpServletRequest, @PathVariable final AdminMenuConfig objectType) {
        LOG.info("> findByObjectType");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminMenuConfigCustomeResponseWrapper adminMenuConfigResponse = adminMenuConfigMgr.findByObjectType(objectType, userName);
        LOG.info("< findByObjectType");
        return new ResponseEntity<>(adminMenuConfigResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return AdminMenuConfigResponseWrapper
     */
    @RequestMapping(value = "/findUserAdminsMenuConfiguration",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminMenuConfigResponseWrapper> findUserBasedConfiguration(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findUserBasedConfiguration");
        final String userName = httpServletRequest.getHeader("USER_NAME");
        final AdminMenuConfigResponseWrapper configResponseWrapper = adminMenuConfigMgr.findUserBasedConfiguration(userName);
        LOG.info("< findUserBasedConfiguration");
        return new ResponseEntity<>(configResponseWrapper, HttpStatus.OK);
    }
}
