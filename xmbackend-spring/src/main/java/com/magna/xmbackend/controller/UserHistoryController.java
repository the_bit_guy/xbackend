/**
 * 
 */
package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.UserHistoryTbl;
import com.magna.xmbackend.mgr.UserHistoryMgr;
import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;
import com.magna.xmbackend.vo.userHistory.UserHistoryResponse;
import com.magna.xmbackend.vo.userHistory.UserHistoryResponseWrapper;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/userHistory")
public class UserHistoryController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserHistoryController.class);

	@Autowired
	private UserHistoryMgr userHistoryMgr; 
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<UserHistoryTbl> save(
			HttpServletRequest httpServletRequest,
			@RequestBody UserHistoryRequest userHistoryRequest) {
		LOG.info("> save");
		final String userName = (String)httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
		UserHistoryTbl userHistoryTbl = this.userHistoryMgr.create(userHistoryRequest, userName, tkt);
		LOG.info("< save");
		return new ResponseEntity<>(userHistoryTbl, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/updateStatusToHistory", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<Boolean> updateStatusToHistory(
			HttpServletRequest httpServletRequest) {
		LOG.info("> updateStatus");
		final String userName = (String)httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
		Boolean isUpdated = this.userHistoryMgr.updateStatusToHistory(userName, tkt);
		LOG.info("< updateStatus");
		return new ResponseEntity<>(isUpdated, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/findAll", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<UserHistoryResponse> findAll(
			HttpServletRequest httpServletRequest) {
		LOG.info("> updateStatus");
		Iterable<UserHistoryTbl> userHistoryTbls = this.userHistoryMgr.findAll();
		UserHistoryResponse historyResponse = new UserHistoryResponse(userHistoryTbls);
		LOG.info("< updateStatus");
		return new ResponseEntity<>(historyResponse, HttpStatus.ACCEPTED);
	}
	
	
	
	@RequestMapping(value = "/findUserStatus", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<UserHistoryResponseWrapper> findUserStatus(
			HttpServletRequest httpServletRequest, @RequestBody UserHistoryRequest historyRequest) {
		LOG.info("> findUserStatus");
		List<Map<String, Object>> resultSet = this.userHistoryMgr.findUserStatus(historyRequest);
		UserHistoryResponseWrapper response = new UserHistoryResponseWrapper(resultSet);
		LOG.info("< findUserStatus");
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}
	
	
	@RequestMapping(value = "/findUserHistory", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<UserHistoryResponseWrapper> findUserHistory(
			HttpServletRequest httpServletRequest, @RequestBody UserHistoryRequest historyRequest) {
		LOG.info("> findUserStatus");
		List<Map<String, Object>> resultSet = this.userHistoryMgr.findUserHistory(historyRequest);
		UserHistoryResponseWrapper response = new UserHistoryResponseWrapper(resultSet);
		LOG.info("< findUserStatus");
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

}
