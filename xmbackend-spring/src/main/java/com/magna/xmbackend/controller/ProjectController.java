package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.ProjectAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/project")
public class ProjectController {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectMgr projectMgr;

    @Autowired
    private Validator validator;

    @Autowired
    private ProjectAuditMgr projectAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.projectMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectsTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectsTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find");
        final ProjectsTbl pt = this.projectMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(pt, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectRequest
     * @return ProjectsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody ProjectRequest projectRequest) {
        LOG.info("> save");
        ProjectsTbl pt = null;
        try {
            pt = this.projectMgr.save(projectRequest);
            this.projectAuditMgr.projectCreateAuditor(pt, httpServletRequest);
        } catch (CannotCreateObjectException ex) {
            LOG.error("CannotCreateObjectException {}", ex.getMessage());
            this.projectAuditMgr.projectCreateFailAuditor(projectRequest,
                    ex.getMessage(), httpServletRequest);
            throw ex;
        } catch (Exception ex) {
            LOG.error("Exception {}", ex.getMessage());
            this.projectAuditMgr.projectCreateFailAuditor(projectRequest,
                    ex.getMessage(), httpServletRequest);
            throw new CannotCreateObjectException(ex.toString(), "ERR0000");
        }
        LOG.info("< save");
        return new ResponseEntity<>(pt, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectRequest
     * @return ProjectsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody ProjectRequest projectRequest) {
        LOG.info("> update");
        ProjectsTbl pt = null;
        try {
            pt = this.projectMgr.update(projectRequest);
            this.projectAuditMgr.projectUpdateSuccessAuditor(pt, httpServletRequest);
        } catch (Exception ex) {
            String projectId = projectRequest.getId();
            this.projectAuditMgr.projectUpdateFailAuditor(projectId, ex.getMessage(), httpServletRequest);
            LOG.error("Exception in updating ProjectTbl");
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(pt, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> update");
        boolean updateStatusById = true;
        try {
            ProjectsTbl projectTbl2Update = this.projectMgr.findById(id);
            if (null == projectTbl2Update) {
                throw new XMObjectNotFoundException("project with id " + id
                        + "not found for update", "ERR0003");
            }
            String name = projectTbl2Update.getName();
            updateStatusById = this.projectMgr.upateStatusById(status, id);
            this.projectAuditMgr.projectUpdateStautsSuccessAuditor(id, name,
                    status, httpServletRequest);
        } catch (Exception ex) {
            LOG.error("Exception occured in updating Project with id {} for Stauts {}", id, status);
            String errMsg = "Updating project with id " + id
                    + " for status " + status + " failed";
            this.projectAuditMgr.projectUpdateFailAuditor(id, errMsg, httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        httpServletRequest.setAttribute("projectId", id);
        boolean isDeleted = false;
        try {
            ProjectsTbl projectsTbl2Del = this.projectMgr.findById(id);
            if (null == projectsTbl2Del) {
                throw new XMObjectNotFoundException("Object not found", "ERR0003");
            }
            String name = projectsTbl2Del.getName();
            isDeleted = this.projectMgr.delete(id);
            this.projectAuditMgr.projectDeleteSuccessAuditor(id, name, httpServletRequest);
        } catch (XMObjectNotFoundException ex) {
            LOG.error("XMObjectNotFoundException {}", ex.getMessage());
            this.projectAuditMgr.projectDeleteFailureAuditor(id, ex.getMessage(), httpServletRequest);
            throw ex;
        } catch (Exception ex) {
            LOG.error("Exception occured {}", ex.getMessage());
            this.projectAuditMgr.projectDeleteFailureAuditor(id, ex.getMessage(), httpServletRequest);
            final String[] param = {id};
            throw new XMObjectNotFoundException("Delete failed for Project ID", "P_ERR0011", param);
        }
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> projectIds) {
        LOG.info("> multiDelete");
        httpServletRequest.setAttribute("projectIds", projectIds);
        ProjectResponse projectResponse = this.projectMgr.multiDelete(httpServletRequest, projectIds);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(projectResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findProjectsByUserId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findProjectsByUserId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectsByUserId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.projectMgr.findProjectsByUserId(id, validationRequest);
        LOG.info("< findProjectsByUserId");
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findProjectsByStartAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findProjectsByStartAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectsByStartAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.projectMgr.findProjectsByStartAppId(id, validationRequest);
        LOG.info("< findProjectsByStartAppId");
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /**
     * Find all projects by project app site AA id.
     *
     * @param httpServletRequest the http servlet request
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectAppId the project app id
     * @return the response entity
     */
    @RequestMapping(value = "/findAllProjectsByProjectAppSiteAAId/{siteId}/{adminAreaId}/{projectAppId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findAllProjectsByProjectAppSiteAAId(
            HttpServletRequest httpServletRequest, @PathVariable String siteId,
            @PathVariable String adminAreaId, @PathVariable String projectAppId) {
        LOG.info("> findAllProjectsByProjectAppSiteAAId");
        final ProjectResponse par = projectMgr.findAllProjectsByProjectAppSiteAAId(projectAppId, siteId, adminAreaId);
        LOG.info("< findAllProjectsByProjectAppSiteAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findAllProjectsByAAId/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findAllProjectsByAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findAllProjectsByAAId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse par = projectMgr.findAllProjectsByAAId(adminAreaId, validationRequest);
        LOG.info("< findAllProjectsByAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return ProjectResponse
     */
    /*@RequestMapping(value = "/findAllProjectsByAAIdAndActive/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findAllProjectsByAAIdAndActive(
            HttpServletRequest httpServletRequest,
            @PathVariable String adminAreaId) {
        LOG.info("> findAllProjectsByAAId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse par = projectMgr.findAllProjectsByAAIdAndActive(adminAreaId, validationRequest);
        LOG.info("< findAllProjectsByAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }*/
    /**
     * XMenu
     *
     * @param httpServletRequest
     * @param siteId
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findUserProjectsBySiteId/{siteId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findUserProjectsBySiteId(
            HttpServletRequest httpServletRequest,
            @PathVariable String siteId) {
        LOG.info("> findUserProjectsBySiteIdAndUsername");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        final ProjectResponse par
                = projectMgr.findUserProjectsBySiteIdAndUsername(siteId, tkt,
                        userName, validationRequest);
        LOG.info("< findUserProjectsBySiteIdAndUsername");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

}
