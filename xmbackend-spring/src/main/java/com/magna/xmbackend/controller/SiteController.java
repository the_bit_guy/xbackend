package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.SiteAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.SiteMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;

@RestController
@RequestMapping(value = "/site")
public class SiteController {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteController.class);

    @Autowired
    private SiteMgr siteMgr;

    @Autowired
    private Validator validator;

    @Autowired
    private SiteAuditMgr siteAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return SiteResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteResponse sr = this.siteMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SitesTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SitesTbl> findById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findById");
        final SitesTbl st = this.siteMgr.findById(id);
        LOG.info("< findById");
        return new ResponseEntity<>(st, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteRequest
     * @return SitesTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SitesTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody SiteRequest siteRequest) {
        LOG.info("> save");
        SitesTbl st = null;
        try {
            st = this.siteMgr.create(siteRequest);
            this.siteAuditMgr.siteCreateSuccessAudit(httpServletRequest, st);
        } catch (Exception ex) {
            String name = siteRequest.getName();
            LOG.error(
                    "Exception / Error occured while creating site with name {}",
                    name);
            this.siteAuditMgr.siteCreateFailureAudit(httpServletRequest,
                    siteRequest, ex.getMessage());
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(st, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteRequest
     * @return SitesTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SitesTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody SiteRequest siteRequest) {
        LOG.info("> update");
        SitesTbl st = null;
        try {
            st = this.siteMgr.update(siteRequest);
            this.siteAuditMgr.siteUpdateSuccessAudit(
                    httpServletRequest, siteRequest);
        } catch (Exception ex) {
            String name = siteRequest.getName();
            LOG.error("Exception / Error in updating site with name {} "
                    + "and exception msg {}", name, ex.getMessage());
            this.siteAuditMgr.siteUpdateFailureAudit(httpServletRequest,
                    siteRequest, ex.getMessage());
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(st, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> update");
        boolean updateStatusById = true;
        String siteName = "";
        try {
            SitesTbl sitesTbl2Update = this.siteMgr.findById(id);

            if (null != sitesTbl2Update) {
                siteName = sitesTbl2Update.getName();
                updateStatusById = this.siteMgr.updateById(status, id);
                this.siteAuditMgr.siteUpdateSuccessAudit(httpServletRequest,
                        id, siteName);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }

            /*updateStatusById = this.siteMgr.updateById(status, id);
            this.siteAuditMgr.siteUpdateSuccessAudit(httpServletRequest,
                    id, siteName);*/
        } catch (Exception ex) {
            LOG.error("Exception / Error {}", ex.getMessage());
            String errorMsg = "Site update failed for id " + id + "with name "
                    + siteName + " ex: " + ex.getMessage();
            this.siteAuditMgr.siteUpdateFailureAudit(httpServletRequest, id,
                    errorMsg);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> delete");
        boolean isDeleted = true;
        String name = "";
        try {
            SitesTbl sitesTbl2Delete = this.siteMgr.findById(id);
            if (null != sitesTbl2Delete) {
                name = sitesTbl2Delete.getName();
                isDeleted = this.siteMgr.deleteById(id);
                this.siteAuditMgr.siteDeleteSuccessAudit(httpServletRequest, id,
                        name);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.error("Exception / Error in deleting site with id " + id);
            String errMsg = "Error in deleting site with id " + id
                    + " and name " + name + " : "
                    + ex.getMessage();
            this.siteAuditMgr.siteDeleteFailureAudit(httpServletRequest, id,
                    errMsg);
            throw ex;
        }
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> siteIds) {
        LOG.info("> multiDelete");
        final SiteResponse siteResponse = this.siteMgr.multiDelete(siteIds, 
                httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(siteResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @RequestMapping(value = "/findAdminAreaBySiteId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaResponse>
            findAdminAreasBySiteId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAdminAreasById {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final SiteAdminAreaRelIdWithAdminAreaResponse response
                = this.siteMgr.findAdminAreaById(id, validationRequest);
        LOG.info("< findAdminAreasById");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @RequestMapping(value = "/findAdminAreaBySiteName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteAdminAreaRelIdWithAdminAreaResponse>
            findAdminAreaBySiteName(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String name) {
        LOG.info("> findAdminAreaByName {}", name);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final SiteAdminAreaRelIdWithAdminAreaResponse response
                = this.siteMgr.findAdminAreaByName(name, validationRequest);
        LOG.info("< findAdminAreaByName");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @return SitesTbl
     */
    @RequestMapping(value = "/findByName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SitesTbl> findByName(
            HttpServletRequest httpServletRequest,
            @PathVariable String name) {
        LOG.info("> findByName");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        String tkt = httpServletRequest.getHeader("TKT");
        final SitesTbl st = this.siteMgr.findByName(name, tkt, validationRequest);
        LOG.info("< findByName");
        return new ResponseEntity<>(st, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findProjectsById/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findProjectsById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectsById {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.siteMgr.findProjectById(id, validationRequest);
        LOG.info("< findProjectsById");
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param name
     * @return ProjectResponse
     */
    @RequestMapping(value = "/findProjectsByName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectResponse> findProjectsByName(
            HttpServletRequest httpServletRequest,
            @PathVariable String name) {
        LOG.info("> findProjectsByName {}", name);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.siteMgr.findProjectByName(name, validationRequest);
        LOG.info("< findProjectsByName");
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteResponse
     */
    @RequestMapping(value = "/findSitesByProjectId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findSitesByProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findSitesByProjectId");
        final SiteResponse sr = this.siteMgr.findSiteByProjectId(id);
        LOG.info("< findSitesByProjectId");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteResponse
     */
    @RequestMapping(value = "/findSitesByUserAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findSitesByUserAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findSitesByUserAppId");
        final SiteResponse sr = this.siteMgr.findSitesByUserAppId(id);
        LOG.info("< findSitesByUserAppId");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteResponse
     */
    @RequestMapping(value = "/findSitesByProjectAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findSitesByProjectAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findSitesByProjectAppId");
        final SiteResponse sr = this.siteMgr.findSitesByProjectAppId(id);
        LOG.info("< findSitesByProjectAppId");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteResponse
     */
    @RequestMapping(value = "/findSitesByStartAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findSitesByStartAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findSitesByStartAppId");
        final SiteResponse sr = this.siteMgr.findSitesByStartAppId(id);
        LOG.info("< findSitesByStartAppId");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return SiteResponse
     */
    @RequestMapping(value = "/findSitesByAdminAreaId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SiteResponse> findSitesByAdminAreaId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findSitesByAdminAreaId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "SITE");
        final SiteResponse sr = this.siteMgr.findSitesByAdminAreaId(id, validationRequest);
        LOG.info("< findSitesByAdminAreaId");
        return new ResponseEntity<>(sr, HttpStatus.OK);
    }
}
