package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.RolePermissionRelTbl;
import com.magna.xmbackend.mgr.RolePermissionRelMgr;
import com.magna.xmbackend.vo.rel.RolePermissionRelResponse;

/**
 * The Class RolePermissionRelController.
 * 
 * @author shashwat.anand
 */
@RestController
@RequestMapping(value = "/rolePermissionRel")
public class RolePermissionRelController {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(RolePermissionRelController.class);
	
	/** The role permission rel mgr. */
	@Autowired
    private RolePermissionRelMgr rolePermissionRelMgr;
	
	/**
	 * Find permission by role id.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param roleId the role id
	 * @return the response entity
	 */
	@RequestMapping(value = "/findPermissionByRoleId/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RolePermissionRelResponse> findPermissionByRoleId (
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findPermissionByRoleId");
        final Iterable<RolePermissionRelTbl> rolePermissionRelTbls = this.rolePermissionRelMgr.findPermissionByRoleId(roleId);
        final RolePermissionRelResponse rolePermissionRelResponse = new RolePermissionRelResponse(rolePermissionRelTbls);
        LOG.info("< findPermissionByRoleId");
        return new ResponseEntity<>(rolePermissionRelResponse, HttpStatus.ACCEPTED);
    }
}
