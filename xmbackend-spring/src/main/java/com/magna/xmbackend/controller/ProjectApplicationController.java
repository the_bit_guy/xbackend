package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.ProjectAppAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;

@RestController
@RequestMapping(value = "/projectApplication")
public class ProjectApplicationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(ProjectApplicationController.class);

    @Autowired
    private ProjectApplicationMgr projectApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    ProjectAppAuditMgr projectAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par = projectApplicationMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectApplicationsTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationsTbl> findById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findById");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationsTbl pat = projectApplicationMgr.findById(id, validationRequest);
        LOG.info("< findById");
        return new ResponseEntity<>(pat, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )

    public @ResponseBody
    final ResponseEntity<ProjectApplicationsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody ProjectApplicationRequest projectApplicationRequest) {
        ProjectApplicationsTbl pat = null;
        LOG.info("> update");
        try {
            pat = projectApplicationMgr.update(projectApplicationRequest);
            this.projectAppAuditMgr.projectAppUpdateSuccessAuditor(pat, httpServletRequest);
        } catch (Exception ex) {
            final String id = projectApplicationRequest.getId();
            LOG.error("Exception / Error occured in updating "
                    + "Project App with id {}", id);
            this.projectAppAuditMgr.projectAppUpdateFailureAuditor(projectApplicationRequest,
                    httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(pat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectApplicationRequest
     * @return ProjectApplicationsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody ProjectApplicationRequest projectApplicationRequest) {
        ProjectApplicationsTbl pat = null;
        LOG.info("> save");
        try {
            pat = projectApplicationMgr.create(projectApplicationRequest);
            this.projectAppAuditMgr.projectAppCreateSuccessAuditor(pat,
                    httpServletRequest);
        } catch (Exception ex) {
            final String name = projectApplicationRequest.getName();
            LOG.error("Exception / Error occured in creating "
                    + "Project App with name {}", name);
            this.projectAppAuditMgr.projectAppCreateFailureAuditor(
                    projectApplicationRequest, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(pat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        ProjectApplicationsTbl pat = null;
        boolean isDeleted = false;
        LOG.info("> deleteIconById >>{} and {}", id);
        httpServletRequest.setAttribute("projectAppId", id);
        try {
            pat = projectApplicationMgr.findById(id);
            isDeleted = projectApplicationMgr.deleteById(id);
            this.projectAppAuditMgr.projectAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, pat);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting project app={}", id);
            this.projectAppAuditMgr.projectAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, pat);
            throw ex;
        }
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >>{} and {}", ids);
        httpServletRequest.setAttribute("projectAppIds", ids);
        final ProjectApplicationResponse projectApplicationResponse
                = projectApplicationMgr.multiDelete(ids, httpServletRequest);
//        this.projectAppAuditMgr.projectAppMultiDeleteAuditor(projectApplicationResponse, ids, httpServletRequest);
        final ResponseEntity<ProjectApplicationResponse> responseEntity
                = new ResponseEntity<>(projectApplicationResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        boolean updateStatusById = false;
        ProjectApplicationsTbl pat = null;
        LOG.info("> updateStatus");
        try {
            pat = projectApplicationMgr.findById(id);
            if (null != pat) {
                updateStatusById = this.projectApplicationMgr.updateStatusById(status, id);
                this.projectAppAuditMgr.projectAppUpdateStatusAuditor(status, id,
                        httpServletRequest, updateStatusById, pat);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.info("Exception / Error in updating Project app status for id {}",
                    id);
            this.projectAppAuditMgr.projectAppUpdateStatusAuditor(status, id,
                    httpServletRequest, updateStatusById, pat);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjectApplicationsByProjectId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectApplicationsByProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectApplicationsByProjectId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par = projectApplicationMgr.findProjectApplicationsByProjectId(id, null, validationRequest);
        LOG.info("< findProjectApplicationsByProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param type
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjectApplicationsByProjectIdAndType/{id}/{type}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectApplicationsByProjectIdAndType(
            HttpServletRequest httpServletRequest,
            @PathVariable String id,
            @PathVariable String type) {
        LOG.info("> findProjectApplicationsByProjectId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par = projectApplicationMgr.findProjectApplicationsByProjectId(id, type, validationRequest);
        LOG.info("< findProjectApplicationsByProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjectApplicationsByBaseAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectApplicationsByBaseAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectApplicationsByBaseAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par = projectApplicationMgr.findProjectApplicationsByBaseAppId(id, validationRequest);
        LOG.info("< findProjectApplicationsByBaseAppId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param position
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjectAppByPosition/{position}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectAppByPosition(
            HttpServletRequest httpServletRequest,
            @PathVariable String position) {
        LOG.info("> findProjectAppByPosition");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjectAppByPosition(position, validationRequest);
        LOG.info("< findProjectAppByPosition");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjectAppByProjectAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectAppByProjectAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findProjectAppByProjectAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjectAppByPosition(id, validationRequest);
        LOG.info("< findProjectAppByProjectAppId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findAllProjectApplicationsByStatus/{status}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse>
            findAllProjectApplicationsByStatus(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String status) {
        LOG.info("> findAllProjectApplicationsByStatus");
        final ProjectApplicationResponse par
                = projectApplicationMgr
                        .findAllProjectApplicationsByStatus(status);
        LOG.info("< findAllProjectApplicationsByStatus");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findAllProjectApplicationsByPositions",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse>
            findAllProjectApplicationsByPositions(HttpServletRequest httpServletRequest) {
        LOG.info("> findAllProjectApplicationsByPositions");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final ProjectApplicationResponse appPosResp
                = this.projectApplicationMgr.findAllProjectApplicationsByPositions(validationRequest);
        LOG.info("< findAllProjectApplicationsByPositions");
        return new ResponseEntity<>(appPosResp, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjAppByProjectSiteAAId/{siteId}/{adminAreaId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjAppByProjectSiteAAId(
            HttpServletRequest httpServletRequest, @PathVariable String siteId,
            @PathVariable String adminAreaId, @PathVariable String projectId) {
        LOG.info("> findProjAppByProjectSiteAAId");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjAppByProjectSiteAAId(siteId,
                        adminAreaId, projectId);
        LOG.info("< findProjAppByProjectSiteAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteId
     * @param adminAreaId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjAppByProjectSiteAAIdAndRelType/{siteId}/{adminAreaId}/{projectId}/{relType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjAppByProjectSiteAAIdAndRelType(
            HttpServletRequest httpServletRequest, @PathVariable String siteId,
            @PathVariable String adminAreaId, @PathVariable String projectId, @PathVariable String relType) {
        LOG.info("> findProjAppByProjectSiteAAId");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjAppByProjectSiteAAIdAndRelType(siteId, adminAreaId, projectId, relType);
        LOG.info("< findProjAppByProjectSiteAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findProjAppByUserProjectId/{userId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjAppByUserProjectId(
            HttpServletRequest httpServletRequest, @PathVariable String userId, @PathVariable String projectId) {
        LOG.info("> findProjAppByUserProjectId");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjAppByUserProjectId(userId, projectId);
        LOG.info("< findProjAppByUserProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param projectId
     * @return ProjectApplicationResponse
     */
    @RequestMapping(value = "/findFixedProjAppByUserProjectId/{userId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findFixedProjAppByUserProjectId(
            HttpServletRequest httpServletRequest, @PathVariable String userId, @PathVariable String projectId) {
        LOG.info("> findProjAppByUserProjectId");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findFixedProjAppByUserProjectId(userId, projectId);
        LOG.info("< findProjAppByUserProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param aaId
     * @param projectId
     * @param userName
     * @return ProjectApplicationResponse
     *//*
    @RequestMapping(value = "/findProjectAppByAAIdProjectIdUserName/{aaId}/{projectId}/{userName}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectAppByAAIdProjectIdUserName(
            HttpServletRequest httpServletRequest, @PathVariable String aaId, @PathVariable String projectId,
            @PathVariable String userName) {
        LOG.info("> findProjectAppByAAIdProjectIdUserName");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjectAppByAAIdProjectIdUserName(aaId, projectId, userName);
        LOG.info("< findProjectAppByAAIdProjectIdUserName");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

     */
    /**
     *
     * @param httpServletRequest
     * @param aaId
     * @param projectId
     * @param userId
     * @return ProjectApplicationResponse
     *//*
    @RequestMapping(value = "/findProjectAppByAAIdProjectIdUserId/{aaId}/{projectId}/{userId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectAppByAAIdProjectIdUserId(
            HttpServletRequest httpServletRequest, @PathVariable String aaId, @PathVariable String projectId,
            @PathVariable String userId) {
        LOG.info("> findProjectAppByAAIdProjectIdUserId");
        final ProjectApplicationResponse par
                = projectApplicationMgr.findProjectAppByAAIdProjectIdUserId(aaId, projectId, userId);
        LOG.info("< findProjectAppByAAIdProjectIdUserId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

     */
    /**
     *
     * @param httpServletRequest
     * @param aaId
     * @param projId
     * @return ProjectApplicationResponse
     *//*
    @RequestMapping(value = "/findProjectApplicationsByAAIdProjectId/{aaId}/{projId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplicationResponse> findProjectApplicationsByAAIdProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String aaId, @PathVariable String projId) {
        LOG.info("> findProjectApplicationsByAAIdProjectId");
        final ProjectApplicationResponse par = projectApplicationMgr.findProjectAppByAAIdProjectIdUserId(aaId, projId);
        LOG.info("< findProjectApplicationsByAAIdProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }*/



    /**
     * XMenu
     *
     * @param httpServletRequest
     * @param aaId
     * @param projId
     * @return ProjectApplciationMenuWrapper
     */
    @RequestMapping(value = "/findProjectAppsByAAIdProjectIdAndUserName/{aaId}/{projId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<ProjectApplciationMenuWrapper> findProjectApplicationsByAAIdAndUserName(
            HttpServletRequest httpServletRequest,
            @PathVariable String aaId, @PathVariable String projId) {
        LOG.info("> findProjectApplicationsByAAIdAndUserName");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECTAPPLICATION");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        final ProjectApplciationMenuWrapper projectApplciationMenuWrapper
                = projectApplicationMgr.findProjectApplicationsByAAIdAndProjIdAndUserName(aaId, tkt,
                        projId, userName, validationRequest);
        LOG.info("< findProjectApplicationsByAAIdAndUserName");
        return new ResponseEntity<>(projectApplciationMenuWrapper, HttpStatus.OK);
    }
}
