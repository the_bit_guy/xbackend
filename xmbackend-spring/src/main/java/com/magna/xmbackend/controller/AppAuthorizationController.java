/**
 * 
 */
package com.magna.xmbackend.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value="/appauth")
public class AppAuthorizationController {
	/*	
	private static final Logger LOG = LoggerFactory.getLogger(AppAuthorizationController.class);
	
	@Autowired
	private AppAuthorizationMgr appAuthMgr; 
	@Autowired
	private UserTktJpaDao userTktJpaDao;
	
	
	@RequestMapping(value = "/xmenuUserAccessCheck", method = RequestMethod.POST, consumes = {
	        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
	        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
	        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	    public @ResponseBody
	    final ResponseEntity<Boolean> xmenuUserAccessCheck(HttpServletRequest httpServletRequest,
			@RequestBody UserCredential userCredential) {
		LOG.info("> app login");
		HttpHeaders headers = new HttpHeaders();
		boolean appUser = false;
		if (userCredential.getAppName().equals(Application.CAX_START_MENU.name())) {
			appUser = this.appAuthMgr.isAppUser(userCredential);
			if (appUser) {
				String userTktId = String.valueOf(UUID.randomUUID());
				Date createdDate = new Date();
				Date updatedDate = new Date();
				String username = userCredential.getUsername();
				String appName = userCredential.getAppName();
				UserTkt userTktFound = this.userTktJpaDao.findByUsernameAndApplicationName(username, appName);
				String tkt = "TKT" + userTktId;

				if (null != userTktFound) {
					userTktId = userTktFound.getUserTktId();
					tkt = userTktFound.getTkt();
					createdDate = userTktFound.getCreateDate();
				}

				UserTkt userTkt = new UserTkt(userTktId, username, appName, tkt, createdDate, updatedDate);
				this.userTktJpaDao.save(userTkt);

				headers.add("Content-Type", "application/json; charset=UTF-8");
				headers.add("TKT", tkt);
			}
		}

		ResponseEntity<Boolean> responseEntity = new ResponseEntity<Boolean>(appUser, headers, HttpStatus.OK);
		LOG.info("< app login");
		return responseEntity;
	}
	
	
	@RequestMapping(value = "/appUserAccessCheck", method = RequestMethod.POST, consumes = {
	        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
	        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
	        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	    public @ResponseBody
	    final ResponseEntity<Boolean> checkAdminHotlineAccessPermission(HttpServletRequest httpServletRequest,
	            @RequestBody UserCredential userCredential) {
	        LOG.info("> app login");
	        final boolean accessCheck = this.appAuthMgr.checkAdminHotlineBatchAccessPermission(userCredential);
	        ResponseEntity<Boolean> responseEntity = new ResponseEntity<>(accessCheck, HttpStatus.OK);
	        LOG.info("< app login");
	        return responseEntity;
	    }
*/
}
