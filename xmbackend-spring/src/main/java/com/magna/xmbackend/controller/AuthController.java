/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.AppAuthorizationMgr;
import com.magna.xmbackend.mgr.LdapMgr;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private LdapMgr ldapMgr;

    @Autowired
    private UserTktJpaDao userTktJpaDao;
    @Autowired
    private AppAuthorizationMgr appAuthorizationMgr;

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = {
        MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE,
        MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public @ResponseBody
    final ResponseEntity<AuthResponse> login(HttpServletRequest httpServletRequest,
			@RequestBody UserCredential userCredential) {
		LOG.info("> login");
		AuthResponse authResponse;
		HttpHeaders headers = new HttpHeaders();
		String username = userCredential.getUsername();
		String applicationName = userCredential.getAppName();
		// boolean isAuthorized = false;
		String[] ldapResp = new String[2];

		if (applicationName.equals(Application.CAX_START_MENU.name())) {
			boolean isCAXUser = this.appAuthorizationMgr.isAppUser(userCredential);
			if (!isCAXUser) {
				authResponse = new AuthResponse(username, isCAXUser, "Invalid CAXMenu user.", ldapResp[1]);
				return new ResponseEntity<>(authResponse, HttpStatus.OK);
			} else {
				boolean isCAXUserActive = this.appAuthorizationMgr.isAppUserActive(userCredential);
				if (!isCAXUserActive) {
					authResponse = new AuthResponse(username, isCAXUserActive,
							"User account deactive! Please contact administrator.", ldapResp[1]);
					return new ResponseEntity<>(authResponse, HttpStatus.OK);
				}
				if (applicationName.equals(Application.CAX_START_BATCH.name())) {
					boolean isHavingPermission = this.appAuthorizationMgr
							.checkAdminHotlineBatchAccessPermission(userCredential);
					if (!isHavingPermission) {
						authResponse = new AuthResponse(username, isHavingPermission, "Invalid CAXMenu User.", ldapResp[1]);
						return new ResponseEntity<>(authResponse, HttpStatus.OK);
					}
				}
			}
		} else {
			ldapResp = this.ldapMgr.isUserValid(userCredential);
			boolean isLDAPUser = Boolean.valueOf(ldapResp[0]);
			if (!isLDAPUser) {
				authResponse = new AuthResponse(username, isLDAPUser, "Incorrect username or password.", ldapResp[1]);
				return new ResponseEntity<>(authResponse, HttpStatus.OK);
			} else {
				boolean isCAXUser = this.appAuthorizationMgr.isAppUser(userCredential);
				if (!isCAXUser) {
					authResponse = new AuthResponse(username, isCAXUser, "Invalid CAXMenu user.", ldapResp[1]);
					return new ResponseEntity<>(authResponse, HttpStatus.OK);
				} else {
					boolean isCAXUserActive = this.appAuthorizationMgr.isAppUserActive(userCredential);
					if (!isCAXUserActive && !applicationName.equals(Application.CAX_START_BATCH.name())) {
						authResponse = new AuthResponse(username, isCAXUserActive,
								"User account deactivated! Please contact administrator.", ldapResp[1]);
						return new ResponseEntity<>(authResponse, HttpStatus.OK);
					} else {
						boolean isAdmin = this.appAuthorizationMgr
								.checkAdminHotlineBatchAccessPermission(userCredential);
						if (!isAdmin) {
							authResponse = new AuthResponse(username, isAdmin, "Invalid administrator access.", ldapResp[1]);
							return new ResponseEntity<>(authResponse, HttpStatus.OK);
						}
					}
				}
			}
		}

		String userTktId = String.valueOf(UUID.randomUUID());
		Date createdDate = new Date();
		Date updatedDate = new Date();
		// check if user with tkt exists
		// UserTkt userTktFound = this.userTktJpaDao.findByUsername(username);
		UserTkt userTktFound = this.userTktJpaDao.findByUsernameAndApplicationName(username, applicationName);
		String tkt = "TKT" + userTktId;

		if (null != userTktFound) {
			userTktId = userTktFound.getUserTktId();
			tkt = userTktFound.getTkt();
			createdDate = userTktFound.getCreateDate();
		}
		UserTkt userTkt = new UserTkt(userTktId, username, applicationName, tkt, createdDate, updatedDate);
		this.userTktJpaDao.save(userTkt);

		headers.add("Content-Type", "application/json; charset=UTF-8");
		headers.add("TKT", tkt);

		authResponse = new AuthResponse(username, true, Status.VALID_USER.name(), ldapResp[1]);
		ResponseEntity<AuthResponse> responseEntity = new ResponseEntity<>(authResponse, headers, HttpStatus.OK);

		LOG.info("< login");
		return responseEntity;
	}

}
