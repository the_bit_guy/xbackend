/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.mgr.LdapMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.opendxm.vo.OpenDXMProjectVO;
import com.magna.xmbackend.opendxm.vo.OpenDXMUserProjectVO;
import com.magna.xmbackend.opendxm.vo.OpenDXMUserVO;
import com.magna.xmbackend.rel.mgr.UserProjectRelMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;
import com.magna.xmbackend.vo.user.UserCredential;
import com.magna.xmbackend.vo.user.UserResponse;

/**
 * The Class OpenDXMController.
 * 
 * @author shashwat.anand
 */
@Controller
@RequestMapping(value = "/opendxm")
public class OpenDXMController {

    /** The Constant LOG. */
    private static final Logger LOG
            = LoggerFactory.getLogger(OpenDXMController.class);

    /** The project mgr. */
    @Autowired
    private ProjectMgr projectMgr;
    
    /** The user mgr. */
    @Autowired
    private UserMgr userMgr;
    
    /** The user project rel mgr. */
    @Autowired
    private UserProjectRelMgr userProjectRelMgr;

    /** The validator. */
    @Autowired
    private Validator validator;
    
    /** The ldap mgr. */
    @Autowired
    private LdapMgr ldapMgr;
    
    

    /**
     * Active projects by user id.
     *
     * @param httpServletRequest the http servlet request
     * @param id the id
     * @param model the model
     * @return the string
     */
    @Deprecated
    @RequestMapping(value = "/activeProjectsByUserId/{id}",
            method = RequestMethod.GET)
    public String activeProjectsByUserId(HttpServletRequest httpServletRequest,
            @PathVariable String id,
            ModelMap model) {
        LOG.info("> activeProjectsByUserId");

        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.projectMgr.findProjectsByUserId(id, validationRequest);
        List<OpenDXMProjectVO> activeProjectVOs = getProjectVOs(pr);
        model.put("activeProjectVOs", activeProjectVOs);
        return "activeProject";
    }

    /**
     * Active projects.
     *
     * @param httpServletRequest the http servlet request
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/activeProjects",
            method = RequestMethod.GET)
    public String activeProjects(HttpServletRequest httpServletRequest,
            ModelMap model) {
        LOG.info("> activeProjects");

        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "PROJECT");
        final ProjectResponse pr = this.projectMgr.findAll(validationRequest);

        List<OpenDXMProjectVO> activeProjectVOs = getProjectVOs(pr);
        Collections.sort(activeProjectVOs, new Comparator<OpenDXMProjectVO>() {

			@Override
			public int compare(OpenDXMProjectVO o1, OpenDXMProjectVO o2) {
				if (o1 != null && o2 != null) {
					String o1Name;
					String o2Name;
					if ((o1Name = o1.getName()) != null && (o2Name = o2.getName()) != null) {
						return o1Name.compareToIgnoreCase(o2Name);
					}
				}
				return 0;
			}
		});
        model.put("activeProjectVOs", activeProjectVOs);
        return "activeProject";
    }
    
    
    /**
     * Gets the projects.
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param username the username
     * @param password the password
     * @param model the model
     * @return the projects
     */
    @RequestMapping(value = "/getProjects", method = RequestMethod.GET)
    public String getProjects(HttpServletRequest httpServletRequest,
    		HttpServletResponse httpServletResponse,
    		@RequestParam("user") String username,
    		@RequestParam("password") String password,
            ModelMap model) {
        LOG.info("> getProjects");
        List<OpenDXMProjectVO> projectVOs = null;
        String[] userValid = this.ldapMgr.isUserValid(new UserCredential(username, password));
        boolean isLDAPUser = Boolean.valueOf(userValid[0]);
		if (isLDAPUser) {
			httpServletRequest.setAttribute("userName", username);
			final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest,
					"PROJECT");
			final ProjectResponse pr = this.projectMgr.findAll(validationRequest);
			
			projectVOs = getProjectVOs(pr);
			Collections.sort(projectVOs, new Comparator<OpenDXMProjectVO>() {

				@Override
				public int compare(OpenDXMProjectVO o1, OpenDXMProjectVO o2) {
					if (o1 != null && o2 != null) {
						String o1Name;
						String o2Name;
						if ((o1Name = o1.getName()) != null && (o2Name = o2.getName()) != null) {
							return o1Name.compareToIgnoreCase(o2Name);
						}
					}
					return 0;
				}
			});
		} else {
			model.put("message", "Unauthorized Access!");
			return "error";
			
		}
        
        model.put("projectVOs", projectVOs);
        return "project";
    }
    
    
    
    /**
     * Active users.
     *
     * @param httpServletRequest the http servlet request
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/activeUsers",
            method = RequestMethod.GET)
    public String activeUsers(HttpServletRequest httpServletRequest,
            ModelMap model) {
        LOG.info("> activeUsers");

        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserResponse userResponse = this.userMgr.findAll(validationRequest);

        List<OpenDXMUserVO> activeUserVOs = getUserVOs(userResponse);
        Collections.sort(activeUserVOs, new Comparator<OpenDXMUserVO>() {

			@Override
			public int compare(OpenDXMUserVO o1, OpenDXMUserVO o2) {
				if (o1 != null && o2 != null) {
					String o1Name;
					String o2Name;
					if ((o1Name = o1.getName()) != null && (o2Name = o2.getName()) != null) {
						return o1Name.compareToIgnoreCase(o2Name);
					}
				}
				return 0;
			}
		});
        model.put("activeUserVOs", activeUserVOs);
        return "activeUser";
    }
    
    /**
     * Gets the users.
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param username the username
     * @param password the password
     * @param model the model
     * @return the users
     */
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public String getUsers(HttpServletRequest httpServletRequest,
    		HttpServletResponse httpServletResponse,
    		@RequestParam("user") String username,
    		@RequestParam("password") String password,
            ModelMap model) {
        LOG.info("> getUsers");
        List<OpenDXMUserVO> userVOs = null;
        String[] userValid = this.ldapMgr.isUserValid(new UserCredential(username, password));
        boolean isLDAPUser = Boolean.valueOf(userValid[0]);
		if (isLDAPUser) {
			httpServletRequest.setAttribute("userName", username);
			final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
			final UserResponse userResponse = this.userMgr.findAll(validationRequest);
			
			userVOs = getUserVOs(userResponse);
			Collections.sort(userVOs, new Comparator<OpenDXMUserVO>() {

				@Override
				public int compare(OpenDXMUserVO o1, OpenDXMUserVO o2) {
					if (o1 != null && o2 != null) {
						String o1Name;
						String o2Name;
						if ((o1Name = o1.getName()) != null && (o2Name = o2.getName()) != null) {
							return o1Name.compareToIgnoreCase(o2Name);
						}
					}
					return 0;
				}
			});
		} else {
			model.put("message", "Unauthorized Access!");
			return "error";
		}
        model.put("userVOs", userVOs);
        return "user";
    }
    
    /**
     * Active user projects.
     *
     * @param httpServletRequest the http servlet request
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/activeUserProjects",
            method = RequestMethod.GET)
    public String activeUserProjects(HttpServletRequest httpServletRequest,
            ModelMap model) {
        LOG.info("> activeUserProjects");

        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserProjectRelResponse userProjectRelResponse = this.userProjectRelMgr.findAll(validationRequest);

        List<OpenDXMUserProjectVO> activeUserProjectVO = getUserProjectVOs(userProjectRelResponse);
        model.put("activeUserProjectVOs", activeUserProjectVO);
        return "activeUserProject";
    }
    
    
    /**
     * Gets the user projects.
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param username the username
     * @param password the password
     * @param model the model
     * @return the user projects
     */
    @RequestMapping(value = "/getUserProjects",
            method = RequestMethod.GET)
    public String getUserProjects(HttpServletRequest httpServletRequest,
    		HttpServletResponse httpServletResponse,
    		@RequestParam("user") String username,
    		@RequestParam("password") String password,
            ModelMap model) {
        LOG.info("> getUserProjects");
        List<OpenDXMUserProjectVO> userProjectVOs = null;
        String[] userValid = this.ldapMgr.isUserValid(new UserCredential(username, password));
        boolean isLDAPUser = Boolean.valueOf(userValid[0]);
        if (isLDAPUser) {
        	httpServletRequest.setAttribute("userName", username);
			final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
			final UserProjectRelResponse userProjectRelResponse = this.userProjectRelMgr.findAll(validationRequest);
			
			userProjectVOs = getUserProjectVOs(userProjectRelResponse);
        } else {
			model.put("message", "Unauthorized Access!");
			return "error";
		}
        model.put("userProjectVOs", userProjectVOs);
        return "userproject";
    }

    /**
     * Gets the user project V os.
     *
     * @param userProjectRelResponse the user project rel response
     * @return the user project V os
     */
    private List<OpenDXMUserProjectVO> getUserProjectVOs(final UserProjectRelResponse userProjectRelResponse) {
    	List<OpenDXMUserProjectVO> userProjectVOs = new ArrayList<>();
    	final Iterable<UserProjectRelTbl> userProjectRelTbls = userProjectRelResponse.getUserProjectRelTbls();
    	for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
			UsersTbl userTbl = userProjectRelTbl.getUserId();
			String userName = userTbl.getUsername();
			ProjectsTbl projectsTbl = userProjectRelTbl.getProjectId();
			String projectName = projectsTbl.getName();
			String status = userProjectRelTbl.getStatus();
			OpenDXMUserProjectVO openDXMUserProjectVO = new OpenDXMUserProjectVO(userName, projectName, status);
			userProjectVOs.add(openDXMUserProjectVO);
		}
    	LOG.debug("getUserProjectVOs::{}", userProjectVOs);
		return userProjectVOs;
	}

	/**
	 * Gets the user V os.
	 *
	 * @param userResponse the user response
	 * @return the user V os
	 */
	private List<OpenDXMUserVO> getUserVOs(UserResponse userResponse) {
    	Iterable<UsersTbl> usersTbl = userResponse.getUserTbls();
        List<OpenDXMUserVO> userVOs = new ArrayList<>();
        for (final UsersTbl userTbl : usersTbl) {
            final Collection<UserTranslationTbl> userTranslationTblCollection = userTbl.getUserTranslationTblCollection();
            final String name = userTbl.getUsername();
            final String status = userTbl.getStatus();
            for (final UserTranslationTbl userTranslationTbl : userTranslationTblCollection) {
                LanguagesTbl languagesTbl = userTranslationTbl.getLanguageCode();
                String languageCode = languagesTbl.getLanguageCode();
                if ("en".equalsIgnoreCase(languageCode)) {
                    final String description = userTranslationTbl.getDescriptionWithoutDecoding();
                    final String remarks = userTranslationTbl.getRemarkWithoutDecoding();
                    OpenDXMUserVO openDXMUserVO = new OpenDXMUserVO(name, status, description, remarks);
                    userVOs.add(openDXMUserVO);
                }
            }
        }
        LOG.debug("getUserVOs::{}", userVOs);
        return userVOs;
	}

	/**
	 * Gets the project V os.
	 *
	 * @param pr the pr
	 * @return the project V os
	 */
	private List<OpenDXMProjectVO> getProjectVOs(final ProjectResponse pr) {
        Iterable<ProjectsTbl> projectsTbls = pr.getProjectsTbls();
        List<OpenDXMProjectVO> projectVOs = new ArrayList<>();
        for (ProjectsTbl projectsTbl : projectsTbls) {
            Collection<ProjectTranslationTbl> projectTranslationTblCollection
                    = projectsTbl.getProjectTranslationTblCollection();
            final String name = projectsTbl.getName();
            final String status = projectsTbl.getStatus();
            for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
                LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
                String languageCode = languagesTbl.getLanguageCode();
                if ("en".equalsIgnoreCase(languageCode)) {
                    final String description = projectTranslationTbl.getDescriptionWithoutDecoding();
                    final String remarks = projectTranslationTbl.getRemarksWithoutDecoding();
                    OpenDXMProjectVO openDXMProjectVO = new OpenDXMProjectVO(name, status, description, remarks);
                    projectVOs.add(openDXMProjectVO);
                }
            }
        }
        LOG.debug("getProjectVOs::{}", projectVOs);
        return projectVOs;
    }

}
