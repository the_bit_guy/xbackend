package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.StartAppAuditMgr;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/startApplication")
public class StartApplicationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(StartApplicationController.class);

    @Autowired
    private StartApplicationMgr startApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    StartAppAuditMgr startAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return StartApplicationResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "STARTAPPLICATION");
        final StartApplicationResponse sar = startApplicationMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(sar, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return StartApplicationsTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationsTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final StartApplicationsTbl sat = startApplicationMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(sat, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        boolean isDeleted = false;
        StartApplicationsTbl sat = null;
        LOG.info("> deleteById >> {}", id);
        try {
            sat = startApplicationMgr.findById(id);
            isDeleted = startApplicationMgr.delete(id);
            this.startAppAuditMgr.startAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, sat);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting start app={}", id);
            this.startAppAuditMgr.startAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, sat);
            throw ex;
        }
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return StartApplicationResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        final StartApplicationResponse startApplicationResponse
                = startApplicationMgr.multiDelete(ids, httpServletRequest);
//        this.startAppAuditMgr.startAppMultiDeleteAuditor(startApplicationResponse, ids, httpServletRequest);
        final ResponseEntity<StartApplicationResponse> responseEntity
                = new ResponseEntity<>(startApplicationResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param startApplicationRequest
     * @return StartApplicationsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody StartApplicationRequest startApplicationRequest) {
        StartApplicationsTbl sat = null;
        LOG.info("> save");
        try {
            sat = startApplicationMgr.createOrUpdate(startApplicationRequest,
                    false);
            this.startAppAuditMgr.startAppCreateSuccessAuditor(sat,
                    httpServletRequest);
        } catch (Exception ex) {
            final String name = startApplicationRequest.getName();
            LOG.error("Exception / Error occured in creating "
                    + "Start App with name {}", name);
            this.startAppAuditMgr.startAppCreateFailureAuditor(
                    startApplicationRequest, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(sat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param startApplicationRequest
     * @return StartApplicationsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody StartApplicationRequest startApplicationRequest) {
        StartApplicationsTbl sat = null;
        LOG.info("> update");
        try {
            sat = startApplicationMgr.createOrUpdate(startApplicationRequest,
                    true);
            this.startAppAuditMgr.startAppUpdateSuccessAuditor(sat, httpServletRequest);
        } catch (Exception ex) {
            final String id = startApplicationRequest.getId();
            LOG.error("Exception / Error occured in updating "
                    + "Start App with id {}", id);
            this.startAppAuditMgr.startAppUpdateFailureAuditor(startApplicationRequest,
                    httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(sat, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        boolean updateStatusById = false;
        LOG.info("> updateStatus");
        StartApplicationsTbl sat = null;
        try {
            sat = startApplicationMgr.findById(id);
            if (null != sat) {
                updateStatusById = this.startApplicationMgr.updateStatusById(status, id);
                this.startAppAuditMgr.startAppUpdateStatusAuditor(status, id,
                        httpServletRequest, updateStatusById, sat);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.info("Exception / Error in updating Start app status for id {}",
                    id);
            this.startAppAuditMgr.startAppUpdateStatusAuditor(status, id,
                    httpServletRequest, updateStatusById, sat);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return StartApplicationResponse
     */
    @RequestMapping(value = "/findStartApplicationsByBaseAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findStartApplicationsByBaseAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findStartApplicationsByBaseAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "STARTAPPLICATION");
        final StartApplicationResponse par
                = startApplicationMgr.findStartApplicationsByBaseAppId(id,
                        validationRequest);
        LOG.info("< findStartApplicationsByBaseAppId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return StartApplicationResponse XMenu API
     */
    @RequestMapping(value = "/findUserStartApplications",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findUserStartApplicationsByUserName(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findStartApplicationsByUserId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "STARTAPPLICATION");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        final StartApplicationResponse par
                = startApplicationMgr.findUserStartApplicationsByUserName(userName, tkt,
                        validationRequest);
        LOG.info("< findStartApplicationsByUserId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @param projectId
     * @return StartApplicationResponse XMenu API
     */
    @RequestMapping(value = "/findStartAppByAAIdProjectId/{adminAreaId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findStartAppByAAIdProjectId(
            HttpServletRequest httpServletRequest, @PathVariable String adminAreaId,
            @PathVariable String projectId) {
        LOG.info("> findStartAppByProjectSiteAAId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "STARTAPPLICATION");
        final String tkt = httpServletRequest.getHeader("TKT");
        final StartApplicationResponse par
                = startApplicationMgr.findStartAppByAAIdProjectId(adminAreaId, tkt,
                        projectId, validationRequest);
        LOG.info("< findStartAppByProjectSiteAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaId
     * @return StartApplicationResponse XMenu API
     */
    @RequestMapping(value = "/findStartAppsByAAId/{adminAreaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findStartAppsByAAId(
            HttpServletRequest httpServletRequest, @PathVariable String adminAreaId) {
        LOG.info("> findStartAppsByAAId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "STARTAPPLICATION");
        final String tkt = httpServletRequest.getHeader("TKT");
        final StartApplicationResponse par
                = startApplicationMgr.findStartAppsByAAId(adminAreaId, tkt,
                        validationRequest);
        LOG.info("< findStartAppsByAAId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

    /**
     * XMHotline
     *
     * @param httpServletRequest
     * @param projectId
     * @return StartApplicationResponse
     */
    @RequestMapping(value = "/findStartAppsByProjectId/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<StartApplicationResponse> findStartAppsByProjectId(
            HttpServletRequest httpServletRequest, @PathVariable String projectId) {
        LOG.info("> findStartAppsByProjectId");
        final StartApplicationResponse par = startApplicationMgr.findStartAppsByProjectId(projectId);
        LOG.info("< findStartAppsByProjectId");
        return new ResponseEntity<>(par, HttpStatus.OK);
    }

}
