/**
 * 
 */
package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.GroupAuditMgr;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.GroupsMgr;
import com.magna.xmbackend.vo.group.GroupCreateRequest;
import com.magna.xmbackend.vo.group.GroupResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/groups")
public class GroupController {
	
	private static final Logger LOG =  LoggerFactory.getLogger(GroupController.class);
	
	@Autowired
	private GroupsMgr groupsMgr;
	@Autowired
	private GroupAuditMgr groupAuditMgr;
	
	/**
	 * 
	 * @param request
	 * @param groupCreateRequest
	 * @return GroupsTbl
	 */
	@RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<GroupsTbl> save(HttpServletRequest request,
			@RequestBody GroupCreateRequest groupCreateRequest){
		LOG.info("> group save");
        GroupsTbl groupsTbl = null;
		try {
			groupsTbl = this.groupsMgr.create(groupCreateRequest);
			this.groupAuditMgr.groupCreateSuccessAudit(request, groupsTbl);
		} catch (Exception ex) {
			String groupName = groupCreateRequest.getGroupName();
			LOG.error(
                    "Exception / Error occured while creating group with name {}",
                    groupName);
			this.groupAuditMgr.groupCreateFailureAudit(request, groupCreateRequest, ex.getMessage().split("with ErrorCode")[0]);
		}
        LOG.info("< group save");
        return new ResponseEntity<>(groupsTbl, HttpStatus.ACCEPTED);
	}

	
	@RequestMapping(value = "/findAll", method = RequestMethod.GET, 
			consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE }, 
			produces = {MediaType.APPLICATION_JSON_VALUE, 
					MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<GroupResponse> findAll(HttpServletRequest httpServletRequest) {
		LOG.info("> findAll");
		final List<GroupsTbl> groupTbls = this.groupsMgr.findAll();
		GroupResponse gr = new GroupResponse(groupTbls);
		LOG.info("< findAll");
		return new ResponseEntity<>(gr, HttpStatus.OK);
	}
	
	
	  @RequestMapping(value = "/update",
	          method = RequestMethod.POST,
	          consumes = {MediaType.APPLICATION_JSON_VALUE,
	              MediaType.APPLICATION_XML_VALUE,
	              MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	          produces = {MediaType.APPLICATION_JSON_VALUE,
	              MediaType.APPLICATION_XML_VALUE,
	              MediaType.APPLICATION_FORM_URLENCODED_VALUE}
	  )
	  public @ResponseBody
	  final ResponseEntity<Boolean> update(
	          HttpServletRequest httpServletRequest,
			@RequestBody GroupCreateRequest groupCreateRequest) {
		LOG.info("> group update");
		boolean isUpdated = false;
		try {
			GroupsTbl groupsTbl = this.groupsMgr.update(groupCreateRequest);
			this.groupAuditMgr.groupUpdateSuccessAudit(httpServletRequest, groupsTbl);
		} catch (Exception ex) {
			final String name = groupCreateRequest.getGroupName();
			LOG.error("Exception / Error in updating group with name {} "
                    + "and exception msg {}", groupCreateRequest.getGroupName(), ex.getMessage());
			String errMsg = "Error in updating group with name " + name + " : " + ex.getMessage().split("with ErrorCode")[0];
			this.groupAuditMgr.groupUpdateFailureAudit(httpServletRequest, groupCreateRequest, errMsg);
			throw ex;
		}
		LOG.info("< group update");
		return new ResponseEntity<>(isUpdated, HttpStatus.ACCEPTED);
	}
	  
	  
	  @RequestMapping(value = "/delete/{id}",
		         method = RequestMethod.DELETE,
		         consumes = {MediaType.APPLICATION_JSON_VALUE,
		             MediaType.APPLICATION_XML_VALUE,
		             MediaType.APPLICATION_FORM_URLENCODED_VALUE},
		         produces = {MediaType.APPLICATION_JSON_VALUE,
		             MediaType.APPLICATION_XML_VALUE,
		             MediaType.APPLICATION_FORM_URLENCODED_VALUE}
		 )
		 public @ResponseBody
		 final ResponseEntity<Boolean> deleteById(
		         HttpServletRequest httpServletRequest,
		         @PathVariable String id) {
		     LOG.info("> group delete");
		     boolean isDeleted = false;
		     String name = "";
			try {
				GroupsTbl groupsTbl = this.groupsMgr.findById(id);
				if(groupsTbl != null) {
					isDeleted = this.groupsMgr.deleteById(id);
					this.groupAuditMgr.groupDeleteSuccessAudit(httpServletRequest, id, name, groupsTbl.getGroupType());
				} else {
					throw new XMObjectNotFoundException("Group not found with id "+id, "ERR0022");
				}
				
			} catch (Exception ex) {

				LOG.error("Exception / Error in deleting group with id " + id);
				String errMsg = "Error in deleting group with id " + id + " and name " + name +" reason" +" : " + ex.getMessage().split("with ErrorCode")[0];
				this.groupAuditMgr.groupDeleteFailureAudit(httpServletRequest, id, errMsg);
				throw ex;
			}
		     LOG.info("< group delete");
		     return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
		 }
	  
	  
	  @RequestMapping(value = "/multiDelete",
		         method = RequestMethod.DELETE,
		         consumes = {MediaType.APPLICATION_JSON_VALUE,
		             MediaType.APPLICATION_XML_VALUE,
		             MediaType.APPLICATION_FORM_URLENCODED_VALUE},
		         produces = {MediaType.APPLICATION_JSON_VALUE,
		             MediaType.APPLICATION_XML_VALUE,
		             MediaType.APPLICATION_FORM_URLENCODED_VALUE}
		 )
		 public @ResponseBody
		 final ResponseEntity<GroupResponse> multiDelete(
		         HttpServletRequest httpServletRequest,
		         @RequestBody Set<String> ids) {
		     LOG.info("> group multiDelete");
		     final GroupResponse groupResponse = this.groupsMgr.multiDelete(ids, httpServletRequest);
		     LOG.info("< group multiDelete");
		     return new ResponseEntity<>(groupResponse, HttpStatus.ACCEPTED);
		 }
	  
	  
		@RequestMapping(value = "/findByGroupType/{groupType}", method = RequestMethod.GET, 
				consumes = { MediaType.APPLICATION_JSON_VALUE,
						MediaType.APPLICATION_XML_VALUE,
						MediaType.APPLICATION_FORM_URLENCODED_VALUE }, 
				produces = {MediaType.APPLICATION_JSON_VALUE, 
						MediaType.APPLICATION_XML_VALUE,
						MediaType.APPLICATION_FORM_URLENCODED_VALUE })
		public @ResponseBody final ResponseEntity<GroupResponse> findByGroupType(HttpServletRequest httpServletRequest,
						@PathVariable String groupType) {
			LOG.info("> findByGroupType");
			final List<GroupsTbl> groupTbls = this.groupsMgr.findByGroupType(groupType);
			GroupResponse gr = new GroupResponse(groupTbls);
			LOG.info("< findByGroupType");
			return new ResponseEntity<>(gr, HttpStatus.OK);
		}
}
