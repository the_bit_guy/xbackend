/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.mail.mgr.SendMailMgr;
import com.magna.xmbackend.mgr.NotificationMgr;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmbackend.vo.notification.SendMailRequest;
import com.magna.xmbackend.vo.notification.SendMailResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/notify")
public class NotificationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    private NotificationMgr notificationMgr;

    @Autowired
    private SendMailMgr sendMailMgr;

    @RequestMapping(value = "/findAll/pendingNotification",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Iterable<EmailNotifyToUserRelTbl>> findAllPendingNotification(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllPendingNotification");
        Iterable<EmailNotifyToUserRelTbl> enturts
                = this.notificationMgr.findAllPendingNotification();
        LOG.info("< findAllPendingNotification");
        return new ResponseEntity<>(enturts, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAll/config",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Iterable<EmailNotificationConfigTbl>> findAllNotificationConfig(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllNotificationConfig");
        Iterable<EmailNotificationConfigTbl> encts
                = this.notificationMgr.findAllNotificationConfig();
        LOG.info("< findAllNotificationConfig");
        return new ResponseEntity<>(encts, HttpStatus.OK);
    }

    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<NotificationResponse> save(
            HttpServletRequest httpServletRequest,
            @RequestBody NotificationRequest notificationRequest) {
        LOG.info("> save");
        NotificationResponse nr
                = this.notificationMgr.createNotification(notificationRequest);
        LOG.info("< save");
        return new ResponseEntity<>(nr, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/updateNotification",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<NotificationResponse> updateNotification(
            HttpServletRequest httpServletRequest,
            @RequestBody NotificationRequest notificationRequest) {
        LOG.info("> updateNotification");
        NotificationResponse nr
                = this.notificationMgr.updateNotification(notificationRequest);
        LOG.info("< updateNotification");
        return new ResponseEntity<>(nr, HttpStatus.OK);
    }

    @RequestMapping(value = "/findByEvent/{event}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<NotificationResponse> findByEvent(
            HttpServletRequest httpServletRequest, @PathVariable String event) {
        LOG.info("> findByEvent");
        NotificationResponse encts
                = this.notificationMgr.findByEvent(event);
        LOG.info("< findByEvent");
        return new ResponseEntity<>(encts, HttpStatus.OK);
    }

    @RequestMapping(value = "/sendMail",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SendMailResponse> sendMail(
            HttpServletRequest httpServletRequest,
            @RequestBody SendMailRequest sendMailRequest) {
        LOG.info("> sendMail");
        SendMailResponse ack = this.sendMailMgr.sendMail(sendMailRequest);
        LOG.info("< sendMail");
        return new ResponseEntity<>(ack, HttpStatus.ACCEPTED);
    }

}
