package com.magna.xmbackend.utils;

import com.magna.xmbackend.entities.UserAccountStatusTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.UserAccountStatusJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mgr.impl.LdapMgrImpl;
import com.magna.xmbackend.mgr.impl.UserMgrImpl;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.ldap.LdapUser;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class LdapUserSyncTask {

    private static final Logger LOG
            = LoggerFactory.getLogger(LdapUserSyncTask.class);

    @Autowired
    private UserJpaDao userJpaDao;

    @Autowired
    UserMgrImpl userMgrImpl;

    @Autowired
    LdapMgrImpl ldapMgrImpl;

    @Autowired
    private UserAccountStatusJpaDao userAccountStatusJpaDao;

    @Scheduled(cron = "${ldap.cron.expression}")
    public void ldapSyncUser() {
        LOG.info("ldapSync invoked");
        final Iterable<UsersTbl> usersTbls = this.userJpaDao.findAll();
        if (null != usersTbls) {
            for (final UsersTbl usersTbl : usersTbls) {
                final String userName = usersTbl.getUsername();
                final String userId = usersTbl.getUserId();
                LOG.info("userName={} userId={}", userName, userId);
                if (null != userName) {
                    try {
                        final LdapUser ldapUser = ldapMgrImpl.getLdapUserDetails(userName);
                        if (null != ldapUser) {
                            String email = ldapUser.getEmail() == null ? ldapUser.getUserPrincipalName() : ldapUser.getEmail();
                            usersTbl.setEmailId(email);
                            final String telephoneNumber = ldapUser.getTelephoneNumber() == null ? "" : ldapUser.getTelephoneNumber();
                            usersTbl.setTelephoneNumber(telephoneNumber);
                            final String givenName = ldapUser.getGivenName() == null ? "" : ldapUser.getGivenName();
                            final String sn = ldapUser.getSn() == null ? "" : ldapUser.getSn();
                            final String fullName = givenName + " " + sn;
                            usersTbl.setFullName(fullName);
                            usersTbl.setDepartment(ldapUser.getDepartment());
                            Date date = new Date();
                            usersTbl.setUpdateDate(date);
                            userJpaDao.save(usersTbl);

                            //check for account deactivation
                            final String userAccountControl = ldapUser.getUserAccountControl() == null ? "" : ldapUser.getUserAccountControl();
                            if ("AccountDisabled".equalsIgnoreCase(userAccountControl) && !usersTbl.getStatus().equals(Status.INACTIVE.name())) {
                                this.actOnDeactivatedOrDeletedUsers(usersTbl, true);
                            }
                        } else {
                            this.actOnDeactivatedOrDeletedUsers(usersTbl, false);
                        }
                    } catch (Exception e) {
                        LOG.info("In method ldapSyncUser, ldap execption occured for user : " + userName + " exception is " + e);
                    }
                }
            }
        }
    }

    //if deactivated is false then its a deleted user
    private void actOnDeactivatedOrDeletedUsers(UsersTbl usersTbl, boolean isDeactivated) {
        String username = usersTbl.getUsername();

        String accountStatus = Status.DELETED.name();
        if (isDeactivated) {
            accountStatus = Status.DEACTIVATED.name();
        }
        LOG.debug("user with user name {} account status is {}", username, accountStatus);

        UserAccountStatusTbl userAccountStatusTbl = userAccountStatusJpaDao.findOne(username);
        Date updatedDate = new Date();
        if (userAccountStatusTbl == null) {
            Date createdDate = new Date();
            userAccountStatusTbl = new UserAccountStatusTbl(username);
            userAccountStatusTbl.setCreateDate(createdDate);
        }
        userAccountStatusTbl.setAccountStatus(accountStatus);
        userAccountStatusTbl.setRemarks("Status changed on -" + updatedDate);
        userAccountStatusTbl.setUpdateDate(updatedDate);
        this.userAccountStatusJpaDao.save(userAccountStatusTbl);
    }
}
