package com.magna.xmbackend.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UserStartAppRelTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.BaseApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.DirectoryJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.RoleJpaDao;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.dao.StartApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.RoleAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserStartAppRelJpaDao;
import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelRequest;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;
import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;

/**
 *
 * @author vijay
 */
@Component
public class Validator {

    private static final Logger LOG = LoggerFactory.getLogger(Validator.class);

    @Autowired
    private PermissionMgr permissionMgr;

    @Autowired
    private SiteJpaDao siteJpaDao;

    @Autowired
    private AdminAreaJpaDao adminAreaJpaDao;

    @Autowired
    private ProjectJpaDao projectJpaDao;

    @Autowired
    private ProjectApplicationJpaDao projectApplicationJpaDao;

    @Autowired
    private UserJpaDao userJpaDao;

    @Autowired
    private UserApplicationJpaDao userApplicationJpaDao;

    @Autowired
    private StartApplicationJpaDao startApplicationJpaDao;

    @Autowired
    private BaseApplicationJpaDao baseApplicationJpaDao;

    @Autowired
    private DirectoryJpaDao directoryJpaDao;

    @Autowired
    private UserProjectRelJpaDao userProjectRelJpaDao;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private RoleJpaDao roleJpaDao;

    @Autowired
    private RoleAdminAreaRelJpaDao roleAdminAreaRelJpaDao;
    
    @Autowired
    private UserStartAppRelJpaDao userStartAppRelJpaDao;

    /**
     *
     * @param httpServletRequest
     * @param objectName
     * @return ValidationRequest
     */
    public final ValidationRequest formObjectValidationRequest(final HttpServletRequest httpServletRequest,
            final String objectName) {
        final ValidationRequest validationRequest = new ValidationRequest();
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        validationRequest.setUserName(userName);
        switch (objectName) {
            case "SITE":
            case "AREA":
            case "PROJECT":
            case "BASEAPPLICATION":
            case "USERAPPLICATION":
            case "PROJECTAPPLICATION":
            case "STARTAPPLICATION":
            case "USER":
                validationRequest.setPermissionName("VIEW_INACTIVE");
                break;
            default:
                break;
        }
//        switch (objectName) {
//            case "SITE":
//                validationRequest.setPermissionName("SITE-VIEW_INACTIVE");
//                break;
//            case "AREA":
//                validationRequest.setPermissionName("ADMINISTRATION_AREA-VIEW_INACTIVE");
//                break;
//            case "PROJECT":
//                validationRequest.setPermissionName("PROJECT-VIEW_INACTIVE");
//                break;
//            case "BASEAPPLICATION":
//                validationRequest.setPermissionName("BASE_APPLICATION-VIEW_INACTIVE");
//                break;
//            case "USERAPPLICATION":
//                validationRequest.setPermissionName("USER_APPLICATION-VIEW_INACTIVE");
//                break;
//            case "PROJECTAPPLICATION":
//                validationRequest.setPermissionName("PROJECT_APPLICATION-VIEW_INACTIVE");
//                break;
//            case "STARTAPPLICATION":
//                validationRequest.setPermissionName("START_APPLICATION-VIEW_INACTIVE");
//                break;
//            case "USER":
//                validationRequest.setPermissionName("USER-VIEW_INACTIVE");
//                break;
//            default:
//                break;
//        }
        return validationRequest;
    }

    /**
     *
     * @param userName
     * @param objectName
     * @return ValidationRequest
     */
    public final ValidationRequest formSaveRelationValidationRequest(final String userName,
            final String objectName) {
        final ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setPermissionType(PermissionType.RELATION_PERMISSION.name());
        validationRequest.setUserName(userName);
        switch (objectName) {
            case "AREA_SITE":
                validationRequest.setPermissionName("ADMINISTRATION_AREA_TO_SITE-INACTIVE_ASSIGNMENT");
                break;
            case "DIRECTORY_RELATION":
                validationRequest.setPermissionName("DIRECTORY_RELATION-ASSIGN");
                break;

            case "ADMIN_MENU_CONFIG":
                validationRequest.setPermissionName("ADMIN_MENU_CONFIG-INACTIVE_ASSIGNMENT");
                break;
//            case "PROJECT_GROUP":
//                validationRequest.setObjectType("PROJECT_TO_GROUP-INACTIVE_ASSIGNMENT");
//                break;
            /*  case "PROJECT_DIRECTORY":
                validationRequest.setPermissionName("PROJECT_TO_DIRECTORY-INACTIVE_ASSIGNMENT");
                break;*/
            case "USERAPPLICATION_USER":
                validationRequest.setPermissionName("USER_APPLICATION_TO_USER-INACTIVE_ASSIGNMENT");
                break;
//            case "USERAPPLICATION_GROUP":
//                validationRequest.setObjectType("USER_APPLICATION_TO_GROUP-INACTIVE_ASSIGNMENT");
//                break;
            /* case "USERAPPLICATION_DIRECTORY":
                validationRequest.setPermissionName("USER_APPLICATION_TO_DIRECTORY-INACTIVE_ASSIGNMENT");
                break;*/
 /* case "USERAPPLICATION_ADMIN_MENU":
                validationRequest.setPermissionName("USER_APPLICATION_TO_ADMIN_MENU-INACTIVE_ASSIGNMENT");
                break;*/
            case "PROJECTAPPLICATION_USER":
                validationRequest.setPermissionName("PROJECT_APPLICATION_TO_USER_PROJECT-INACTIVE_ASSIGNMENT");
                break;
//            case "PROJECTAPPLICATION_GROUP":
//                validationRequest.setObjectType("PROJECT_APPLICATION_TO_GROUP-INACTIVE_ASSIGNMENT");
//                break;
            /*  case "PROJECTAPPLICATION_DIRECTORY":
                validationRequest.setPermissionName("PROJECT_APPLICATION_TO_DIRECTORY-INACTIVE_ASSIGNMENT");
                break;*/
 /*case "PROJECTAPPLICATION_ADMIN_MENU":
                validationRequest.setPermissionName("PROJECT_APPLICATION_TO_ADMIN_MENU-INACTIVE_ASSIGNMENT");
                break;*/
            case "STARTAPPLICATION_USER":
                validationRequest.setPermissionName("START_APPLICATION_TO_USER-INACTIVE_ASSIGNMENT");
                break;
//            case "USER_GROUP":
//                validationRequest.setObjectType("USER_TO_GROUP-INACTIVE_ASSIGNMENT");
//                break;
            /* case "USER_DIRECTORY":
                validationRequest.setPermissionName("USER_TO_DIRECTORY-INACTIVE_ASSIGNMENT");
                break;*/
 /* case "USER_ADMIN_MENU":
                validationRequest.setPermissionName("USER_TO_ADMIN_MENU-INACTIVE_ASSIGNMENT");
                break;*/
            case "USER_PROJECT":
                validationRequest.setPermissionName("USER_TO_PROJECT-INACTIVE_ASSIGNMENT");
                break;
            case "PROJECT_ADMINAREA":
                validationRequest.setPermissionName("PROJECT_TO_ADMINISTRATION_AREA-INACTIVE_ASSIGNMENT");
                break;
            case "USERAPP_ADMINAREA":
                validationRequest.setPermissionName("USER_APPLICATION_TO_ADMINISTRATION_AREA-INACTIVE_ASSIGNMENT");
                break;
            case "STARTAPP_ADMINAREA":
                validationRequest.setPermissionName("START_APPLICATION_TO_ADMINISTRATION_AREA-INACTIVE_ASSIGNMENT");
                break;
            case "STARTAPP_PROJECT":
                validationRequest.setPermissionName("START_APPLICATION_TO_PROJECT-INACTIVE_ASSIGNMENT");
                break;
            case "PROJECTAPP_ADMINAREA":
                validationRequest.setPermissionName("PROJECT_APPLICATION_TO_PROJECT-INACTIVE_ASSIGNMENT");
                break;
            case "ROLE_USER":
                validationRequest.setPermissionName("USER_TO_ROLE-INACTIVE_ASSIGNMENT");
                break;
            default:
                break;
        }
        return validationRequest;
    }

    /**
     *
     * @param validationRequest
     * @return boolean
     */
    public final boolean isViewInactiveAllowed(final ValidationRequest validationRequest) {
        boolean isViewInactiveAllowed = false;
        final PermissionTbl permissionTbl = permissionMgr.isObjectAccessAllowed(validationRequest);
        if (null != permissionTbl && null != permissionTbl.getName()) {
            isViewInactiveAllowed = true;
        }
        return isViewInactiveAllowed;
    }

    /**
     *
     * @param isViewInActive
     * @param sitesTbls
     * @return SitesTbl
     */
    public final Iterable<SitesTbl> filterSiteResponse(final boolean isViewInActive,
            final Iterable<SitesTbl> sitesTbls) {
        final List<SitesTbl> filteredSitesTbls = new ArrayList<>();
        for (final SitesTbl sitesTbl : sitesTbls) {
            final SitesTbl activeSitesTbl = filterSiteResponse(isViewInActive, sitesTbl);
            if (null != activeSitesTbl) {
                filteredSitesTbls.add(activeSitesTbl);
            }
        }
        final Iterable<SitesTbl> newSitesTbls = filteredSitesTbls;
        return newSitesTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param sitesTbl
     * @return SitesTbl
     */
    public final SitesTbl filterSiteResponse(final boolean isViewInActive,
            final SitesTbl sitesTbl) {
        SitesTbl activeSitesTbl = null;
        final Status dbStatusValue = Status.valueOf(sitesTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeSitesTbl = sitesTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeSitesTbl = sitesTbl;
                }
                break;
            default:
                break;
        }
        return activeSitesTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreasTbl
     * @return AdminAreasTbl
     */
    public final AdminAreasTbl filterAdminAreaResponse(final boolean isViewInActive,
            final AdminAreasTbl adminAreasTbl) {
        AdminAreasTbl activeadminAreasTbl = null;
        final Status dbStatusValue = Status.valueOf(adminAreasTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeadminAreasTbl = adminAreasTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeadminAreasTbl = adminAreasTbl;
                }
                break;
            default:
                break;
        }
        return activeadminAreasTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param siteAdminAreaRelTbls
     * @return SiteAdminAreaRelTbl
     */
    public final Collection<SiteAdminAreaRelTbl> filterSiteAdminAreaRel(final boolean isViewInActive,
            final Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        final List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRelTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            processSiteAdminAreaRel(filteredSiteAdminAreaRelTbls,
                    siteAdminAreaRelTbl, isViewInActive);
        }
        final Collection<SiteAdminAreaRelTbl> newSiteAdminAreaRelTbls = filteredSiteAdminAreaRelTbls;
        return newSiteAdminAreaRelTbls;
    }

    /**
     *
     * @param filteredSiteAdminAreaRelTbls
     * @param siteAdminAreaRelTbl
     * @param isViewInActive
     */
    private void processSiteAdminAreaRel(final List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRelTbls,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(siteAdminAreaRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredSiteAdminAreaRelTbls.add(siteAdminAreaRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredSiteAdminAreaRelTbls.add(siteAdminAreaRelTbl);
                }
                break;
            default:
                break;
        }
    }
    
    
    private void processUserProjectRel(final List<UserProjectRelTbl> filteredUserProjectRelTbls,
            final UserProjectRelTbl userProjectRelTbl,
            final boolean isViewInActive) {
    	final Status dbStatusValue = Status.valueOf(userProjectRelTbl.getStatus());
    	switch (dbStatusValue) {
        case ACTIVE:
        	filteredUserProjectRelTbls.add(userProjectRelTbl);
            break;
        case INACTIVE:
            if (isViewInActive) {
            	filteredUserProjectRelTbls.add(userProjectRelTbl);
            }
            break;
        default:
            break;
    }
    } 

    /**
     *
     * @param isViewInActive
     * @param siteAdminAreaRelTbls
     * @return SiteAdminAreaRelTbl
     */
    public final List<SiteAdminAreaRelTbl> filterSiteAdminAreaRel(final boolean isViewInActive,
            final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        final List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRelTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            processSiteAdminAreaRel(filteredSiteAdminAreaRelTbls,
                    siteAdminAreaRelTbl, isViewInActive);
        }
        final List<SiteAdminAreaRelTbl> newSiteAdminAreaRelTbls = filteredSiteAdminAreaRelTbls;
        return newSiteAdminAreaRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param siteAdminAreaRelTbls
     * @return SiteAdminAreaRelTbl
     */
    public final Iterable<SiteAdminAreaRelTbl> filterSiteAdminAreaRel(final boolean isViewInActive,
            final Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        final List<SiteAdminAreaRelTbl> filteredSiteAdminAreaRelTbls = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            processSiteAdminAreaRel(filteredSiteAdminAreaRelTbls,
                    siteAdminAreaRelTbl, isViewInActive);
        }
        final Iterable<SiteAdminAreaRelTbl> newSiteAdminAreaRelTbls = filteredSiteAdminAreaRelTbls;
        return newSiteAdminAreaRelTbls;
    }
    
    
    /**
     * 
     * @param isViewInActive
     * @param userProjectRelTbls
     * @return UserProjectRelTbl
     */
    public final Iterable<UserProjectRelTbl> filterUserProjectRel(final boolean isViewInActive,
            final Iterable<UserProjectRelTbl> userProjectRelTbls) {
    	final List<UserProjectRelTbl> filteredUserProjectRelTbls = new ArrayList<>();
    	for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
			processUserProjectRel(filteredUserProjectRelTbls, userProjectRelTbl, isViewInActive);
		}
    	
    	Iterable<UserProjectRelTbl> newUserProjectRelTbls = filteredUserProjectRelTbls;
    	return newUserProjectRelTbls;
    }
    
    
    public Iterable<UserProjAppRelTbl> filterUserProjectAppRel(boolean isInactiveAssignment,
			Iterable<UserProjAppRelTbl> userProjectAppRelTbls) {
		final List<UserProjAppRelTbl> filteredUserProjectAppRels = new ArrayList<>();
		for (UserProjAppRelTbl userProjAppRelTbl : userProjectAppRelTbls) {
			processUserProjectAppRel(filteredUserProjectAppRels, userProjAppRelTbl, isInactiveAssignment);
		}
		Iterable<UserProjAppRelTbl> newUserProjAppRelTbl = filteredUserProjectAppRels;
		return newUserProjAppRelTbl;
	}

    private void processUserProjectAppRel(List<UserProjAppRelTbl> filteredUserProjectAppRels,
			UserProjAppRelTbl userProjAppRelTbl, boolean isInactiveAssignment) {

    	
    
		
	}

	/**
     *
     * @param isViewInActive
     * @param adminAreaProjectRelTbls
     * @return AdminAreaProjectRelTbl
     */
    public final List<AdminAreaProjectRelTbl> filterAdminAreaProjectRel(final boolean isViewInActive,
            final List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls) {
        final List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRelTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
            processAdminAreaProjectRel(filteredAdminAreaProjectRelTbls,
                    adminAreaProjectRelTbl, isViewInActive);
        }
        final List<AdminAreaProjectRelTbl> newAdminAreaProjectRelTbls = filteredAdminAreaProjectRelTbls;
        return newAdminAreaProjectRelTbls;
    }

    /**
     *
     * @param filteredAdminAreaProjectRelTbls
     * @param adminAreaProjectRelTbl
     * @param isViewInActive
     */
    private void processAdminAreaProjectRel(final List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRelTbls,
            final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(adminAreaProjectRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredAdminAreaProjectRelTbls.add(adminAreaProjectRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredAdminAreaProjectRelTbls.add(adminAreaProjectRelTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param siteAdminAreaRelTbl
     * @return SiteAdminAreaRelTbl
     */
    public final SiteAdminAreaRelTbl filterSiteAdminAreaRel(final boolean isViewInActive,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl) {
        SiteAdminAreaRelTbl activeSiteAdminAreaRelTbl = null;
        final Status dbStatusValue = Status.valueOf(siteAdminAreaRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeSiteAdminAreaRelTbl = siteAdminAreaRelTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeSiteAdminAreaRelTbl = siteAdminAreaRelTbl;
                }
                break;
            default:
                break;
        }
        return activeSiteAdminAreaRelTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaProjectRelTbls
     * @return SiteAdminAreaRelTbl
     */
    public final Collection<AdminAreaProjectRelTbl> filterAdminAreaProjectRel(final boolean isViewInActive,
            final Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls) {
        final List<AdminAreaProjectRelTbl> filteredAdminAreaProjectRelTbls = new ArrayList<>();
        for (final AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
            processAdminAreaProjectRel(filteredAdminAreaProjectRelTbls,
                    adminAreaProjectRelTbl, isViewInActive);
        }
        final Collection<AdminAreaProjectRelTbl> newAdminAreaProjectRelTbls = filteredAdminAreaProjectRelTbls;
        return newAdminAreaProjectRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaProjectRelTbl
     * @return AdminAreaProjectRelTbl
     */
    public final AdminAreaProjectRelTbl filterAdminAreaProjectRel(final boolean isViewInActive,
            final AdminAreaProjectRelTbl adminAreaProjectRelTbl) {
        AdminAreaProjectRelTbl activeAdminAreaProjectRelTbl = null;
        final Status dbStatusValue = Status.valueOf(adminAreaProjectRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeAdminAreaProjectRelTbl = adminAreaProjectRelTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeAdminAreaProjectRelTbl = adminAreaProjectRelTbl;
                }
                break;
            default:
                break;
        }
        return activeAdminAreaProjectRelTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param userProjectRelTblTbls
     * @return UserProjectRelTbl
     */
    public final Collection<UserProjectRelTbl> filterUserProjectRel(final boolean isViewInActive,
            final Collection<UserProjectRelTbl> userProjectRelTblTbls) {
        final List<UserProjectRelTbl> filteredUserProjectRelTbls = new ArrayList<>();
        for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTblTbls) {
            final Status dbStatusValue = Status.valueOf(userProjectRelTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredUserProjectRelTbls.add(userProjectRelTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredUserProjectRelTbls.add(userProjectRelTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Collection<UserProjectRelTbl> newUserProjectRelTbls = filteredUserProjectRelTbls;
        return newUserProjectRelTbls;
    }
    
    
    /**
     * 
     * @param isViewInActive
     * @param userProjectRelTblTbls
     * @return UserProjectRelTbl
     */
    public final List<UserProjectRelTbl> filterUserProjectRels(final boolean isViewInActive,
            final Collection<UserProjectRelTbl> userProjectRelTblTbls) {
        final List<UserProjectRelTbl> filteredUserProjectRelTbls = new ArrayList<>();
        for (final UserProjectRelTbl userProjectRelTbl : userProjectRelTblTbls) {
            final Status dbStatusValue = Status.valueOf(userProjectRelTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredUserProjectRelTbls.add(userProjectRelTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredUserProjectRelTbls.add(userProjectRelTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final List<UserProjectRelTbl> newUserProjectRelTbls = filteredUserProjectRelTbls;
        return newUserProjectRelTbls;
    }
    
    /**
     * Filter user user app rel.
     *
     * @param isViewInactive the is view inactive
     * @param userUserAppRelTbls the user user app rel tbls
     * @return the list
     */
    public List<UserUserAppRelTbl> filterUserUserAppRel(final boolean isViewInactive, final List<UserUserAppRelTbl> userUserAppRelTbls) {
    	final List<UserUserAppRelTbl> filteredUserUserAppRelTbls = new ArrayList<>();
        for (final UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
            final Status dbStatusValue = Status.valueOf(userUserAppRelTbl.getUserApplicationId().getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                	filteredUserUserAppRelTbls.add(userUserAppRelTbl);
                    break;
                case INACTIVE:
                    if (isViewInactive) {
                    	filteredUserUserAppRelTbls.add(userUserAppRelTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final List<UserUserAppRelTbl> newUserUserAppRelTbls = filteredUserUserAppRelTbls;
        return newUserUserAppRelTbls;
	}

    /**
     *
     * @param isViewInActive
     * @param userProjectRelTbl
     * @return ProjectsTbl
     */
    public final UserProjectRelTbl filterUserProjectResponse(final boolean isViewInActive,
            final UserProjectRelTbl userProjectRelTbl) {
        UserProjectRelTbl activeUserProjectRelTbl = null;
        final Status dbStatusValue = Status.valueOf(userProjectRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeUserProjectRelTbl = userProjectRelTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeUserProjectRelTbl = userProjectRelTbl;
                }
                break;
            default:
                break;
        }
        return activeUserProjectRelTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param projectsTbl
     * @return ProjectsTbl
     */
    public final ProjectsTbl filterProjectResponse(final boolean isViewInActive,
            final ProjectsTbl projectsTbl) {
        ProjectsTbl activeProjectsTbl = null;
        final Status dbStatusValue = Status.valueOf(projectsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeProjectsTbl = projectsTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeProjectsTbl = projectsTbl;
                }
                break;
            default:
                break;
        }
        return activeProjectsTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param projectStartAppRelTbls
     * @return ProjectStartAppRelTbl
     */
    public final Iterable<ProjectStartAppRelTbl> filterProjectStartAppRel(final boolean isViewInActive,
            final Iterable<ProjectStartAppRelTbl> projectStartAppRelTbls) {
        final List<ProjectStartAppRelTbl> filteredProjectStartAppRelTbls = new ArrayList<>();
        for (final ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
            processProjectStartAppRel(filteredProjectStartAppRelTbls,
                    projectStartAppRelTbl, isViewInActive);
        }
        final Iterable<ProjectStartAppRelTbl> newProjectStartAppRelTbls = filteredProjectStartAppRelTbls;
        return newProjectStartAppRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param projectStartAppRelTbls
     * @return ProjectStartAppRelTbl
     */
    public final Collection<ProjectStartAppRelTbl> filterProjectStartAppRel(final boolean isViewInActive,
            final Collection<ProjectStartAppRelTbl> projectStartAppRelTbls) {
        final List<ProjectStartAppRelTbl> filteredProjectStartAppRelTbls = new ArrayList<>();
        for (final ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
            processProjectStartAppRel(filteredProjectStartAppRelTbls,
                    projectStartAppRelTbl, isViewInActive);
        }
        final Collection<ProjectStartAppRelTbl> newProjectStartAppRelTbls = filteredProjectStartAppRelTbls;
        return newProjectStartAppRelTbls;
    }

    /**
     *
     * @param filteredProjectStartAppRelTbls
     * @param projectStartAppRelTbl
     * @param isViewInActive
     */
    private void processProjectStartAppRel(final List<ProjectStartAppRelTbl> filteredProjectStartAppRelTbls,
            final ProjectStartAppRelTbl projectStartAppRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(projectStartAppRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredProjectStartAppRelTbls.add(projectStartAppRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredProjectStartAppRelTbls.add(projectStartAppRelTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaProjAppRelTbls
     * @return AdminAreaProjAppRelTbl
     */
    public final Collection<AdminAreaProjAppRelTbl> filterAdminAreaProjAppRel(final boolean isViewInActive,
            final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls) {
        final List<AdminAreaProjAppRelTbl> filteredAdminAreaProjAppRelTbls = new ArrayList<>();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
            processAdminAreaProjAppRel(filteredAdminAreaProjAppRelTbls,
                    adminAreaProjAppRelTbl, isViewInActive);
        }
        final Collection<AdminAreaProjAppRelTbl> newAdminAreaProjAppRelTbls = filteredAdminAreaProjAppRelTbls;
        return newAdminAreaProjAppRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaProjAppRelTbls
     * @return AdminAreaProjAppRelTbl
     */
    public final List<AdminAreaProjAppRelTbl> filterAdminAreaProjAppRel(final boolean isViewInActive,
            final List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls) {
        final List<AdminAreaProjAppRelTbl> filteredAdminAreaProjAppRelTbls = new ArrayList<>();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
            processAdminAreaProjAppRel(filteredAdminAreaProjAppRelTbls,
                    adminAreaProjAppRelTbl, isViewInActive);
        }
        final List<AdminAreaProjAppRelTbl> newAdminAreaProjAppRelTbls = filteredAdminAreaProjAppRelTbls;
        return newAdminAreaProjAppRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaProjAppRelTbls
     * @return AdminAreaProjAppRelTbl
     */
    public final Iterable<AdminAreaProjAppRelTbl> filterAdminAreaProjAppRel(final boolean isViewInActive,
            final Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls) {
        final List<AdminAreaProjAppRelTbl> filteredAdminAreaProjAppRelTbls = new ArrayList<>();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
            processAdminAreaProjAppRel(filteredAdminAreaProjAppRelTbls,
                    adminAreaProjAppRelTbl, isViewInActive);
        }
        final Iterable<AdminAreaProjAppRelTbl> newAdminAreaProjAppRelTbls = filteredAdminAreaProjAppRelTbls;
        return newAdminAreaProjAppRelTbls;
    }

    /**
     *
     * @param filteredAdminAreaProjAppRelTbls
     * @param adminAreaProjAppRelTbl
     * @param isViewInActive
     */
    private void processAdminAreaProjAppRel(final List<AdminAreaProjAppRelTbl> filteredAdminAreaProjAppRelTbls,
            final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(adminAreaProjAppRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredAdminAreaProjAppRelTbls.add(adminAreaProjAppRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredAdminAreaProjAppRelTbls.add(adminAreaProjAppRelTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaUserAppRelTbls
     * @return AdminAreaUserAppRelTbl
     */
    public final List<AdminAreaUserAppRelTbl> filterAdminAreaUserAppRel(final boolean isViewInActive,
            final List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls) {
        final List<AdminAreaUserAppRelTbl> filteredAdminAreaUserAppRelTbls = new ArrayList<>();
        for (final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
            processAdminAreaUserAppRel(filteredAdminAreaUserAppRelTbls,
                    adminAreaUserAppRelTbl, isViewInActive);
        }
        final List<AdminAreaUserAppRelTbl> newAdminAreaProjAppRelTbls = filteredAdminAreaUserAppRelTbls;
        return newAdminAreaProjAppRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaUserAppRelTbls
     * @return AdminAreaUserAppRelTbl
     */
    public final List<AdminAreaUserAppRelTbl> filterAdminAreaUserAppRel(final boolean isViewInActive,
            final Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls) {
        final List<AdminAreaUserAppRelTbl> filteredAdminAreaUserAppRelTbls = new ArrayList<>();
        for (final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
            processAdminAreaUserAppRel(filteredAdminAreaUserAppRelTbls,
                    adminAreaUserAppRelTbl, isViewInActive);
        }
        final List<AdminAreaUserAppRelTbl> newAdminAreaProjAppRelTbls = filteredAdminAreaUserAppRelTbls;
        return newAdminAreaProjAppRelTbls;
    }

    /**
     *
     * @param filteredAdminAreaUserAppRelTbls
     * @param adminAreaUserAppRelTbl
     * @param isViewInActive
     */
    private void processAdminAreaUserAppRel(final List<AdminAreaUserAppRelTbl> filteredAdminAreaUserAppRelTbls,
            final AdminAreaUserAppRelTbl adminAreaUserAppRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(adminAreaUserAppRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredAdminAreaUserAppRelTbls.add(adminAreaUserAppRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredAdminAreaUserAppRelTbls.add(adminAreaUserAppRelTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaStartAppRelTbls
     * @return AdminAreaStartAppRelTbl
     */
    public final List<AdminAreaStartAppRelTbl> filterAdminAreaStartAppRel(final boolean isViewInActive,
            final List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls) {
        final List<AdminAreaStartAppRelTbl> filteredAdminAreaStartAppRelTbls = new ArrayList<>();
        for (final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTbls) {
            processAdminAreaStartAppRel(filteredAdminAreaStartAppRelTbls,
                    adminAreaStartAppRelTbl, isViewInActive);
        }
        final List<AdminAreaStartAppRelTbl> newAdminAreaStartAppRelTbls = filteredAdminAreaStartAppRelTbls;
        return newAdminAreaStartAppRelTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param adminAreaStartAppRelTbls
     * @return AdminAreaStartAppRelTbl
     */
    public final List<AdminAreaStartAppRelTbl> filterAdminAreaStartAppRel(final boolean isViewInActive,
            final Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls) {
        final List<AdminAreaStartAppRelTbl> filteredAdminAreaStartAppRelTbls = new ArrayList<>();
        for (final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTbls) {
            processAdminAreaStartAppRel(filteredAdminAreaStartAppRelTbls,
                    adminAreaStartAppRelTbl, isViewInActive);
        }
        final List<AdminAreaStartAppRelTbl> newAdminAreaStartAppRelTbls = filteredAdminAreaStartAppRelTbls;
        return newAdminAreaStartAppRelTbls;
    }

    /**
     *
     * @param filteredAdminAreaStartAppRelTbls
     * @param adminAreaStartAppRelTbl
     * @param isViewInActive
     */
    private void processAdminAreaStartAppRel(final List<AdminAreaStartAppRelTbl> filteredAdminAreaStartAppRelTbls,
            final AdminAreaStartAppRelTbl adminAreaStartAppRelTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(adminAreaStartAppRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredAdminAreaStartAppRelTbls.add(adminAreaStartAppRelTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredAdminAreaStartAppRelTbls.add(adminAreaStartAppRelTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param usersTbl
     * @return UsersTbl
     */
    public final UsersTbl filterUserResponse(final boolean isViewInActive,
            final UsersTbl usersTbl) {
        UsersTbl activeUsersTbl = null;
        final Status dbStatusValue = Status.valueOf(usersTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeUsersTbl = usersTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeUsersTbl = usersTbl;
                }
                break;
            default:
                break;
        }
        return activeUsersTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param userApplicationsTbl
     * @return UserApplicationsTbl
     */
    public final UserApplicationsTbl filterUserApplicationResponse(final boolean isViewInActive,
            final UserApplicationsTbl userApplicationsTbl) {
        UserApplicationsTbl activeUserApplicationsTbl = null;
        final Status dbStatusValue = Status.valueOf(userApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeUserApplicationsTbl = userApplicationsTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeUserApplicationsTbl = userApplicationsTbl;
                }
                break;
            default:
                break;
        }
        return activeUserApplicationsTbl;
    }
    
    
    
    public final UserStartAppRelTbl filterUserStartApplicationResponse(final boolean isViewInActive,
            final UserStartAppRelTbl userStartAppRelTbl) {
    	UserStartAppRelTbl activeUserStartAppRelationTbl = null;
        final Status dbStatusValue = Status.valueOf(userStartAppRelTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
            	activeUserStartAppRelationTbl = userStartAppRelTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                	activeUserStartAppRelationTbl = userStartAppRelTbl;
                }
                break;
            default:
                break;
        }
        return activeUserStartAppRelationTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param projectApplicationsTbl
     * @return ProjectApplicationsTbl
     */
    public final ProjectApplicationsTbl filterProjectApplicationResponse(final boolean isViewInActive,
            final ProjectApplicationsTbl projectApplicationsTbl) {
        ProjectApplicationsTbl activeProjectApplicationsTbl = null;
        final Status dbStatusValue = Status.valueOf(projectApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeProjectApplicationsTbl = projectApplicationsTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeProjectApplicationsTbl = projectApplicationsTbl;
                }
                break;
            default:
                break;
        }
        return activeProjectApplicationsTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param aaTbls
     * @return AdminAreasTbl
     */
    public final Iterable<AdminAreasTbl> filterAdminAreaResponse(final boolean isViewInActive,
            final Iterable<AdminAreasTbl> aaTbls) {
        final List<AdminAreasTbl> filteredAdminAreaTbls = new ArrayList<>();
        for (final AdminAreasTbl aaTbl : aaTbls) {
            final Status dbStatusValue = Status.valueOf(aaTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredAdminAreaTbls.add(aaTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredAdminAreaTbls.add(aaTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Iterable<AdminAreasTbl> adminAreasTbls = filteredAdminAreaTbls;
        return adminAreasTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param projectsTbls
     * @return ProjectsTbl
     */
    public final Iterable<ProjectsTbl> filterProjectResponse(final boolean isViewInActive,
            final Iterable<ProjectsTbl> projectsTbls) {
        final List<ProjectsTbl> filteredProjectTbls = new ArrayList<>();
        for (final ProjectsTbl projectsTbl : projectsTbls) {
            final Status dbStatusValue = Status.valueOf(projectsTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredProjectTbls.add(projectsTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredProjectTbls.add(projectsTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Iterable<ProjectsTbl> projectTbls = filteredProjectTbls;
        return projectTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param baseApplicationsTbls
     * @return BaseApplicationsTbl
     */
    public final Iterable<BaseApplicationsTbl> filterBaseApplicationResponse(final boolean isViewInActive,
            final Iterable<BaseApplicationsTbl> baseApplicationsTbls) {
        final List<BaseApplicationsTbl> filteredBaseApplicationsTbls = new ArrayList<>();
        for (final BaseApplicationsTbl baseApplicationsTbl : baseApplicationsTbls) {
            final Status dbStatusValue = Status.valueOf(baseApplicationsTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredBaseApplicationsTbls.add(baseApplicationsTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredBaseApplicationsTbls.add(baseApplicationsTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Iterable<BaseApplicationsTbl> newBaseApplicationsTbls = filteredBaseApplicationsTbls;
        return newBaseApplicationsTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param userApplicationsTbls
     * @return UserApplicationsTbl
     */
    public final Iterable<UserApplicationsTbl> filterUserApplicationResponse(final boolean isViewInActive,
            final Iterable<UserApplicationsTbl> userApplicationsTbls) {
        final List<UserApplicationsTbl> filteredUserApplicationsTbls = new ArrayList<>();
        for (final UserApplicationsTbl userApplicationsTbl : userApplicationsTbls) {
            final Status dbStatusValue = Status.valueOf(userApplicationsTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredUserApplicationsTbls.add(userApplicationsTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredUserApplicationsTbls.add(userApplicationsTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Iterable<UserApplicationsTbl> newUserApplicationsTbls = filteredUserApplicationsTbls;
        return newUserApplicationsTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param projectApplicationsTbls
     * @return ProjectApplicationsTbl
     */
    public final Iterable<ProjectApplicationsTbl> filterProjectApplicationResponse(final boolean isViewInActive,
            final Iterable<ProjectApplicationsTbl> projectApplicationsTbls) {
        final List<ProjectApplicationsTbl> filteredProjectApplicationsTbls = new ArrayList<>();
        for (final ProjectApplicationsTbl projectApplicationsTbl : projectApplicationsTbls) {
            processProjectApplicationResponse(filteredProjectApplicationsTbls,
                    projectApplicationsTbl, isViewInActive);
        }
        final Iterable<ProjectApplicationsTbl> newProjectApplicationsTbls = filteredProjectApplicationsTbls;
        return newProjectApplicationsTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param projectApplicationsTbls
     * @return ProjectApplicationsTbl
     */
    public final Collection<ProjectApplicationsTbl> filterProjectApplicationResponse(final boolean isViewInActive,
            final Collection<ProjectApplicationsTbl> projectApplicationsTbls) {
        final List<ProjectApplicationsTbl> filteredUserApplicationsTbls = new ArrayList<>();
        for (final ProjectApplicationsTbl projectApplicationsTbl : projectApplicationsTbls) {
            processProjectApplicationResponse(filteredUserApplicationsTbls,
                    projectApplicationsTbl, isViewInActive);
        }
        final Collection<ProjectApplicationsTbl> newProjectApplicationsTbls = filteredUserApplicationsTbls;
        return newProjectApplicationsTbls;
    }

    /**
     *
     * @param filteredUserApplicationsTbls
     * @param projectApplicationsTbl
     * @param isViewInActive
     */
    private void processProjectApplicationResponse(final List<ProjectApplicationsTbl> filteredUserApplicationsTbls,
            final ProjectApplicationsTbl projectApplicationsTbl, final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(projectApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredUserApplicationsTbls.add(projectApplicationsTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredUserApplicationsTbls.add(projectApplicationsTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param startApplicationsTbls
     * @return StartApplicationsTbl
     */
    public final Iterable<StartApplicationsTbl> filterStartApplicationResponse(final boolean isViewInActive,
            final Iterable<StartApplicationsTbl> startApplicationsTbls) {
        final List<StartApplicationsTbl> filteredStartApplicationsTbls = new ArrayList<>();
        for (final StartApplicationsTbl startApplicationsTbl : startApplicationsTbls) {
            processStartApplication(filteredStartApplicationsTbls, startApplicationsTbl, isViewInActive);
        }
        final Iterable<StartApplicationsTbl> newStartApplicationsTbls = filteredStartApplicationsTbls;
        return newStartApplicationsTbls;
    }

    /**
     *
     * @param isViewInActive
     * @param startApplicationsTbls
     * @return StartApplicationsTbl
     */
    public final Collection<StartApplicationsTbl> filterStartApplicationResponse(final boolean isViewInActive,
            final Collection<StartApplicationsTbl> startApplicationsTbls) {
        final List<StartApplicationsTbl> filteredStartApplicationsTbls = new ArrayList<>();
        for (final StartApplicationsTbl startApplicationsTbl : startApplicationsTbls) {
            processStartApplication(filteredStartApplicationsTbls, startApplicationsTbl, isViewInActive);
        }
        final Collection<StartApplicationsTbl> newStartApplicationsTbls = filteredStartApplicationsTbls;
        return newStartApplicationsTbls;
    }

    /**
     *
     * @param filteredStartApplicationsTbls
     * @param startApplicationsTbl
     * @param isViewInActive
     */
    private void processStartApplication(final List<StartApplicationsTbl> filteredStartApplicationsTbls,
            final StartApplicationsTbl startApplicationsTbl,
            final boolean isViewInActive) {
        final Status dbStatusValue = Status.valueOf(startApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                filteredStartApplicationsTbls.add(startApplicationsTbl);
                break;
            case INACTIVE:
                if (isViewInActive) {
                    filteredStartApplicationsTbls.add(startApplicationsTbl);
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param isViewInActive
     * @param startApplicationsTbl
     * @return StartApplicationsTbl
     */
    public final StartApplicationsTbl filterStartApplicationResponse(final boolean isViewInActive,
            final StartApplicationsTbl startApplicationsTbl) {
        StartApplicationsTbl activeStartApplicationsTbl = null;
        final Status dbStatusValue = Status.valueOf(startApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeStartApplicationsTbl = startApplicationsTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeStartApplicationsTbl = startApplicationsTbl;
                }
                break;
            default:
                break;
        }
        return activeStartApplicationsTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param baseApplicationsTbl
     * @return BaseApplicationsTbl
     */
    public final BaseApplicationsTbl filterBaseApplicationResponse(final boolean isViewInActive,
            final BaseApplicationsTbl baseApplicationsTbl) {
        BaseApplicationsTbl activeBaseApplicationsTbl = null;
        final Status dbStatusValue = Status.valueOf(baseApplicationsTbl.getStatus());
        switch (dbStatusValue) {
            case ACTIVE:
                activeBaseApplicationsTbl = baseApplicationsTbl;
                break;
            case INACTIVE:
                if (isViewInActive) {
                    activeBaseApplicationsTbl = baseApplicationsTbl;
                }
                break;
            default:
                break;
        }
        return activeBaseApplicationsTbl;
    }

    /**
     *
     * @param isViewInActive
     * @param usersTbls
     * @return UsersTbl
     */
    public final Iterable<UsersTbl> filterUserResponse(final boolean isViewInActive,
            final Iterable<UsersTbl> usersTbls) {
        final List<UsersTbl> filteredUsersTbls = new ArrayList<>();
        for (final UsersTbl userTbl : usersTbls) {
            final Status dbStatusValue = Status.valueOf(userTbl.getStatus());
            switch (dbStatusValue) {
                case ACTIVE:
                    filteredUsersTbls.add(userTbl);
                    break;
                case INACTIVE:
                    if (isViewInActive) {
                        filteredUsersTbls.add(userTbl);
                    }
                    break;
                default:
                    break;
            }
        }
        final Iterable<UsersTbl> newUsersTbls = filteredUsersTbls;
        return newUsersTbls;
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isSiteAdminAreaInactiveAssignmentAllowed(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String siteId = siteAdminAreaRelRequest.getSiteId();
        final String adminAreaId = siteAdminAreaRelRequest.getAdminAreaId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != siteId && !siteId.equals("")
                && null != adminAreaId && !adminAreaId.equals("")) {
            final SitesTbl sitesTbl = this.siteJpaDao.findOne(siteId);
            final AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findOne(adminAreaId);
            assignmentAllowedMap.put("sitesTbl", sitesTbl);
            assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
            if (null != sitesTbl && null != adminAreasTbl) {
                final Status siteStatus = Status.valueOf(sitesTbl.getStatus());
                final Status adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((siteStatus == Status.ACTIVE)
                        && (adminAreaStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param directoryRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isDirectoryInactiveAssignmentAllowed(final DirectoryRelRequest directoryRelRequest,
            final boolean isInactiveAssignment) {
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        boolean isAssignmentAllowed = false;
        final String objectId = directoryRelRequest.getObjectId();
        if (null != objectId && !objectId.equals("")) {
            final DirectoryObjectType directoryObjectType = directoryRelRequest.getObjectType();
            switch (directoryObjectType) {
                case PROJECT:
                    isAssignmentAllowed
                            = isProjectAssignmentAllowed(isInactiveAssignment,
                                    directoryRelRequest, assignmentAllowedMap);
                    break;
                case USER:
                    isAssignmentAllowed
                            = isUserAssignmentAllowed(isInactiveAssignment,
                                    directoryRelRequest, assignmentAllowedMap);
                    break;
                case USERAPPLICATION:
                    isAssignmentAllowed
                            = isUserApplicationAssignmentAllowed(isInactiveAssignment,
                                    directoryRelRequest, assignmentAllowedMap);
                    break;
                case PROJECTAPPLICATION:
                    isAssignmentAllowed
                            = isProjectApplicationAssignmentAllowed(isInactiveAssignment,
                                    directoryRelRequest, assignmentAllowedMap);
                    break;
                default:
                    break;

            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param projectId
     * @return boolean
     */
    private boolean isProjectAssignmentAllowed(final boolean isInactiveAssignment,
            final DirectoryRelRequest directoryRelRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final ProjectsTbl projectsTbl = this.projectJpaDao.findOne(directoryRelRequest.getObjectId());
        final DirectoryTbl directoryTbl = this.directoryJpaDao.findOne(directoryRelRequest.getDirectoryId());
        if (null != projectsTbl && null != directoryTbl) {
            assignmentAllowedMap.put("projectsTbl", projectsTbl);
            assignmentAllowedMap.put("directoryTbl", directoryTbl);
            final Status projectStatus = Status.valueOf(projectsTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((projectStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isProjectAssignmentAllowed chk project or directory");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param projectId
     * @return boolean
     */
    private boolean isProjectApplicationAssignmentAllowed(final boolean isInactiveAssignment,
            final DirectoryRelRequest directoryRelRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(directoryRelRequest.getObjectId());
        final DirectoryTbl directoryTbl = this.directoryJpaDao.findOne(directoryRelRequest.getDirectoryId());
        if (null != projectApplicationsTbl && null != directoryTbl) {
            assignmentAllowedMap.put("projectApplicationsTbl", projectApplicationsTbl);
            assignmentAllowedMap.put("directoryTbl", directoryTbl);
            final Status projectAppStatus = Status.valueOf(projectApplicationsTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((projectAppStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isProjectApplicationAssignmentAllowed chk project app or directory");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param projectId
     * @return boolean
     */
    private boolean isUserAssignmentAllowed(final boolean isInactiveAssignment,
            final DirectoryRelRequest directoryRelRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final UsersTbl usersTbl = this.userJpaDao.findOne(directoryRelRequest.getObjectId());
        final DirectoryTbl directoryTbl = this.directoryJpaDao.findOne(directoryRelRequest.getDirectoryId());
        if (null != usersTbl && null != directoryTbl) {
            assignmentAllowedMap.put("usersTbl", usersTbl);
            assignmentAllowedMap.put("directoryTbl", directoryTbl);
            final Status userStatus = Status.valueOf(usersTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((userStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isUserAssignmentAllowed chk user or directory");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param projectId
     * @return boolean
     */
    private boolean isUserApplicationAssignmentAllowed(final boolean isInactiveAssignment,
            final DirectoryRelRequest directoryRelRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(directoryRelRequest.getObjectId());
        final DirectoryTbl directoryTbl = this.directoryJpaDao.findOne(directoryRelRequest.getDirectoryId());
        if (null != userApplicationsTbl && null != directoryTbl) {
            assignmentAllowedMap.put("userApplicationsTbl", userApplicationsTbl);
            assignmentAllowedMap.put("directoryTbl", directoryTbl);
            final Status userAppStatus = Status.valueOf(userApplicationsTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((userAppStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isUserApplicationAssignmentAllowed chk user app or directory");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param userProjectRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isUserProjectInactiveAssignmentAllowed(final UserProjectRelRequest userProjectRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String projectId = userProjectRelRequest.getProjectId();
        final String userId = userProjectRelRequest.getUserId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != projectId && !projectId.equals("")
                && null != userId && !userId.equals("")) {
            final ProjectsTbl projectsTbl = this.projectJpaDao.findOne(projectId);
            final UsersTbl usersTbl = this.userJpaDao.findOne(userId);
            if (null != projectsTbl && null != usersTbl) {
                assignmentAllowedMap.put("projectsTbl", projectsTbl);
                assignmentAllowedMap.put("usersTbl", usersTbl);
                final Status projectStatus = Status.valueOf(projectsTbl.getStatus());
                final Status userStatus = Status.valueOf(usersTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((projectStatus == Status.ACTIVE)
                        && (userStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userUserAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isUserUserAppInactiveAssignmentAllowed(final UserUserAppRelRequest userUserAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String userAppId = userUserAppRelRequest.getUserAppId();
        final String userId = userUserAppRelRequest.getUserId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != userAppId && !userAppId.equals("")
                && null != userId && !userId.equals("")) {
            final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(userAppId);
            final UsersTbl usersTbl = this.userJpaDao.findOne(userId);
            if (null != userApplicationsTbl && null != usersTbl) {
                assignmentAllowedMap.put("userApplicationsTbl", userApplicationsTbl);
                assignmentAllowedMap.put("usersTbl", usersTbl);
                final Status userAppStatus = Status.valueOf(userApplicationsTbl.getStatus());
                final Status userStatus = Status.valueOf(usersTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((userAppStatus == Status.ACTIVE)
                        && (userStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userStartAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isUserStartAppInactiveAssignmentAllowed(final UserStartAppRelRequest userStartAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String startAppId = userStartAppRelRequest.getStartAppId();
        final String userId = userStartAppRelRequest.getUserId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != startAppId && !startAppId.equals("")
                && null != userId && !userId.equals("")) {
            final StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findOne(startAppId);
            final UsersTbl usersTbl = this.userJpaDao.findOne(userId);
            if (null != startApplicationsTbl && null != usersTbl) {
                assignmentAllowedMap.put("startApplicationsTbl", startApplicationsTbl);
                assignmentAllowedMap.put("usersTbl", usersTbl);
                final Status startAppStatus = Status.valueOf(startApplicationsTbl.getStatus());
                final Status userStatus = Status.valueOf(usersTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((startAppStatus == Status.ACTIVE)
                        && (userStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userProjectAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isUserProjectAppInactiveAssignmentAllowed(final UserProjectAppRelRequest userProjectAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String projectAppId = userProjectAppRelRequest.getProjectAppId();
        final String userProjectId = userProjectAppRelRequest.getUserProjectRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != projectAppId && !projectAppId.equals("")
                && null != userProjectId && !userProjectId.equals("")) {
            final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(projectAppId);
            final UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(userProjectId);
            if (null != projectApplicationsTbl && null != userProjectRelTbl) {
                final UsersTbl usersTbl = userProjectRelTbl.getUserId();
                assignmentAllowedMap.put("projectApplicationsTbl", projectApplicationsTbl);
                assignmentAllowedMap.put("usersTbl", usersTbl);
                final Status projectAppStatus = Status.valueOf(projectApplicationsTbl.getStatus());
                final Status userStatus = Status.valueOf(usersTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((projectAppStatus == Status.ACTIVE)
                        && (userStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param configRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isAdminMenuInactiveAssignmentAllowed(final AdminMenuConfigRequest configRequest,
            final boolean isInactiveAssignment) {
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        boolean isAssignmentAllowed = false;
        final String objectId = configRequest.getObjectId();
        if (null != objectId && !objectId.equals("")) {
            final AdminMenuConfig adminMenuConfig = configRequest.getObjectType();
            switch (adminMenuConfig) {
                case USER:
                    isAssignmentAllowed
                            = isUserAssignmentAllowed(isInactiveAssignment,
                                    configRequest, assignmentAllowedMap);
                    break;
                case USERAPPLICATION:
                    isAssignmentAllowed
                            = isUserApplicationAssignmentAllowed(isInactiveAssignment,
                                    configRequest, assignmentAllowedMap);
                    break;
                case PROJECTAPPLICATION:
                    isAssignmentAllowed
                            = isProjectApplicationAssignmentAllowed(isInactiveAssignment,
                                    configRequest, assignmentAllowedMap);
                    break;
                default:
                    break;

            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param configRequest
     * @return boolean
     */
    private boolean isUserAssignmentAllowed(final boolean isInactiveAssignment,
            final AdminMenuConfigRequest configRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final UsersTbl usersTbl = this.userJpaDao.findOne(configRequest.getObjectId());
        if (null != usersTbl) {
            assignmentAllowedMap.put("usersTbl", usersTbl);
            final Status userStatus = Status.valueOf(usersTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((userStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isUserAssignmentAllowed chk user");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param configRequest
     * @return boolean
     */
    private boolean isUserApplicationAssignmentAllowed(final boolean isInactiveAssignment,
            final AdminMenuConfigRequest configRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(configRequest.getObjectId());
        if (null != userApplicationsTbl) {
            assignmentAllowedMap.put("userApplicationsTbl", userApplicationsTbl);
            final Status userAppStatus = Status.valueOf(userApplicationsTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((userAppStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isUserApplicationAssignmentAllowed chk user application");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param isInactiveAssignment
     * @param configRequest
     * @return boolean
     */
    private boolean isProjectApplicationAssignmentAllowed(final boolean isInactiveAssignment,
            final AdminMenuConfigRequest configRequest,
            final Map<String, Object> assignmentAllowedMap) {
        boolean isAssignmentAllowed = false;
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(configRequest.getObjectId());
        if (null != projectApplicationsTbl) {
            assignmentAllowedMap.put("projectApplicationsTbl", projectApplicationsTbl);
            final Status projectStatus = Status.valueOf(projectApplicationsTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((projectStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isProjectApplicationAssignmentAllowed chk project application");
        }
        return isAssignmentAllowed;
    }

    /**
     *
     * @param adminAreaProjectRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isProjectAdminAreaInactiveAssignmentAllowed(final AdminAreaProjectRelRequest adminAreaProjectRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String projectId = adminAreaProjectRelRequest.getProjectId();
        final String siteAdminAreaId = adminAreaProjectRelRequest.getSiteAdminAreaRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != projectId && !projectId.equals("")
                && null != siteAdminAreaId && !siteAdminAreaId.equals("")) {
            final ProjectsTbl projectsTbl = this.projectJpaDao.findOne(projectId);
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaId);
            final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
            assignmentAllowedMap.put("projectsTbl", projectsTbl);
            assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
            if (null != projectsTbl && null != adminAreasTbl) {
                final Status projectStatus = Status.valueOf(projectsTbl.getStatus());
                final Status adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((projectStatus == Status.ACTIVE)
                        && (adminAreaStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param adminAreaUserAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isUserAppAdminAreaInactiveAssignmentAllowed(final AdminAreaUserAppRelRequest adminAreaUserAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String userAppId = adminAreaUserAppRelRequest.getUserAppId();
        final String siteAdminAreaId = adminAreaUserAppRelRequest.getSiteAdminAreaRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != userAppId && !userAppId.equals("")
                && null != siteAdminAreaId && !siteAdminAreaId.equals("")) {
            final UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(userAppId);
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaId);
            final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
            assignmentAllowedMap.put("userApplicationsTbl", userApplicationsTbl);
            assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
            if (null != userApplicationsTbl && null != adminAreasTbl) {
                final Status userAppStatus = Status.valueOf(userApplicationsTbl.getStatus());
                final Status adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((userAppStatus == Status.ACTIVE)
                        && (adminAreaStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param adminAreaStartAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isStartAppAdminAreaInactiveAssignmentAllowed(final AdminAreaStartAppRelRequest adminAreaStartAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String startAppId = adminAreaStartAppRelRequest.getStartAppId();
        final String siteAdminAreaId = adminAreaStartAppRelRequest.getSiteAdminAreaRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != startAppId && !startAppId.equals("")
                && null != siteAdminAreaId && !siteAdminAreaId.equals("")) {
            final StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findOne(startAppId);
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaId);
            final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
            assignmentAllowedMap.put("startApplicationsTbl", startApplicationsTbl);
            assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
            if (null != startApplicationsTbl && null != adminAreasTbl) {
                final Status startAppStatus = Status.valueOf(startApplicationsTbl.getStatus());
                final Status adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((startAppStatus == Status.ACTIVE)
                        && (adminAreaStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param projectStartAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isProjectStartAppInactiveAssignmentAllowed(final ProjectStartAppRelRequest projectStartAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String startAppId = projectStartAppRelRequest.getStartAppId();
        final String adminAreaProjectRelId = projectStartAppRelRequest.getAdminAreaProjectRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != startAppId && !startAppId.equals("")
                && null != adminAreaProjectRelId && !adminAreaProjectRelId.equals("")) {
            final StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findOne(startAppId);
            final AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjectRelId);
            final ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
            assignmentAllowedMap.put("startApplicationsTbl", startApplicationsTbl);
            assignmentAllowedMap.put("projectsTbl", projectsTbl);
            if (null != startApplicationsTbl && null != projectsTbl) {
                final Status startAppStatus = Status.valueOf(startApplicationsTbl.getStatus());
                final Status projectStatus = Status.valueOf(projectsTbl.getStatus());
                if (isInactiveAssignment) {
                    isAssignmentAllowed = true;
                } else if ((startAppStatus == Status.ACTIVE)
                        && (projectStatus == Status.ACTIVE)) {
                    isAssignmentAllowed = true;
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isProjectAppAdminAreaInactiveAssignmentAllowed(final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        final String projectAppId = adminAreaProjectAppRelRequest.getProjectAppId();
        final String adminAreaProjectRelId = adminAreaProjectAppRelRequest.getAdminAreaProjectRelId();
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        if (null != projectAppId && !projectAppId.equals("")
                && null != adminAreaProjectRelId && !adminAreaProjectRelId.equals("")) {
            final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(projectAppId);
            final AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjectRelId);
            if (null != projectApplicationsTbl && null != adminAreaProjectRelTbl) {
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                if (null != siteAdminAreaRelTbl) {
                    final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                    if (null != adminAreasTbl) {
                        assignmentAllowedMap.put("projectApplicationsTbl", projectApplicationsTbl);
                        assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
                        final Status projectAppStatus = Status.valueOf(projectApplicationsTbl.getStatus());
                        final Status adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                        if (isInactiveAssignment) {
                            isAssignmentAllowed = true;
                        } else if ((projectAppStatus == Status.ACTIVE)
                                && (adminAreaStatus == Status.ACTIVE)) {
                            isAssignmentAllowed = true;
                        }
                    }
                }
            }
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param roleUserRelRequest
     * @param isInactiveAssignment
     * @return Map
     */
    public final Map<String, Object> isRoleUserInactiveAssignmentAllowed(final RoleUserRelRequest roleUserRelRequest,
            final boolean isInactiveAssignment) {
        boolean isAssignmentAllowed = false;
        Status adminAreaStatus = null;
        final Map<String, Object> assignmentAllowedMap = new HashMap<>();
        final String userId = roleUserRelRequest.getUserId();
        final String roleId = roleUserRelRequest.getRoleId();
        final String roleAdminAreaId = roleUserRelRequest.getRoleAdminAreaRelId();
        final UsersTbl usersTbl = this.userJpaDao.findOne(userId);
        final RolesTbl rolesTbl = this.roleJpaDao.findOne(roleId);
        if (null != usersTbl && null != rolesTbl) {
            if (null != roleAdminAreaId) {
                final RoleAdminAreaRelTbl roleAdminAreaRelTbl = this.roleAdminAreaRelJpaDao.findOne(roleAdminAreaId);
                final AdminAreasTbl adminAreasTbl = roleAdminAreaRelTbl.getAdminAreaId();
                if (null != adminAreasTbl) {
                    assignmentAllowedMap.put("adminAreasTbl", adminAreasTbl);
                    adminAreaStatus = Status.valueOf(adminAreasTbl.getStatus());
                }
            }
            assignmentAllowedMap.put("usersTbl", usersTbl);
            assignmentAllowedMap.put("rolesTbl", rolesTbl);
            final Status userStatus = Status.valueOf(usersTbl.getStatus());
            if (isInactiveAssignment) {
                isAssignmentAllowed = true;
            } else if ((userStatus == Status.ACTIVE) && null == adminAreaStatus) {
                isAssignmentAllowed = true;
            } else if ((userStatus == Status.ACTIVE)
                    && (adminAreaStatus == Status.ACTIVE)) {
                isAssignmentAllowed = true;
            }
        } else {
            LOG.info("isRoleUserInactiveAssignmentAllowed chk user or role");
        }
        assignmentAllowedMap.put("isAssignmentAllowed", isAssignmentAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param <T>
     * @param validateObject
     * @param objectName
     * @param objectId
     */
    public <T> void checkObject(T validateObject, final String objectName,
            final String objectId) {
        if (null == validateObject) {
            final String[] param = {objectId};
            switch (objectName) {
                case "Area":
                    throw new XMObjectNotFoundException("Admin Area not found", "P_ERR0002", param);
                case "Project":
                    throw new XMObjectNotFoundException("No Projects found", "S_ERR0007", param);
                case "ProjectApp":
                    throw new XMObjectNotFoundException("No Project App found", "P_ERR0014", param);
                case "SiteAdminArea":
                    throw new XMObjectNotFoundException("No Site Admin Area found", "P_ERR0026", param);
                case "AdminAreaProject":
                    throw new XMObjectNotFoundException("No Admin Area Project Relation found", "AA_ERR0001");
                case "StartApp":
                    throw new XMObjectNotFoundException("No Start App found", "S_ERR0013", param);
                case "UserApp":
                    throw new XMObjectNotFoundException("No User App found", "S_ERR0010", param);
                case "User":
                    throw new XMObjectNotFoundException("No User found", "P_ERR0008", param);
                case "Site":
                    throw new XMObjectNotFoundException("No Site found", "P_ERR0010", param);
                case "BaseApp":
                    throw new XMObjectNotFoundException("No Base App found", "P_ERR0016", param);
                case "UserProject":
                    throw new XMObjectNotFoundException("No User Project found", "P_ERR0024", param);
                case "UserStartApp":
                    throw new XMObjectNotFoundException("No User Start App found", "P_ERR0023", param);
            }
        }
    }
    
    
    
    

    /**
     *
     * @param adminAreaId
     * @param isActive
     * @return AdminAreasTbl
     */
    public AdminAreasTbl validateAdminArea(final String adminAreaId,
            final boolean isActive) {
        AdminAreasTbl adminAreasTbl = this.adminAreaJpaDao.findOne(adminAreaId);
        checkObject(adminAreasTbl, "Area", adminAreaId);
        adminAreasTbl = filterAdminAreaResponse(isActive, adminAreasTbl);
        checkObject(adminAreasTbl, "Area", adminAreaId);
        return adminAreasTbl;
    }

    /**
     *
     * @param siteId
     * @param isActive
     * @return SitesTbl
     */
    public SitesTbl validateSite(final String siteId,
            final boolean isActive) {
        SitesTbl sitesTbl = this.siteJpaDao.findOne(siteId);
        checkObject(sitesTbl, "Site", siteId);
        sitesTbl = filterSiteResponse(isActive, sitesTbl);
        checkObject(sitesTbl, "Site", siteId);
        return sitesTbl;
    }

    /**
     *
     * @param projectId
     * @param isActive
     * @return ProjectsTbl
     */
    public ProjectsTbl validateProject(final String projectId,
            final boolean isActive) {
        ProjectsTbl projectsTbl = this.projectJpaDao.findOne(projectId);
        checkObject(projectsTbl, "Project", projectId);
        projectsTbl = filterProjectResponse(isActive, projectsTbl);
        checkObject(projectsTbl, "Project", projectId);
        return projectsTbl;
    }

    /**
     *
     * @param projectAppId
     * @param isActive
     * @return ProjectApplicationsTbl
     */
    public ProjectApplicationsTbl validateProjectApp(final String projectAppId,
            final boolean isActive) {
        ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(projectAppId);
        checkObject(projectApplicationsTbl, "ProjectApp", projectAppId);
        projectApplicationsTbl = filterProjectApplicationResponse(isActive, projectApplicationsTbl);
        checkObject(projectApplicationsTbl, "ProjectApp", projectAppId);
        return projectApplicationsTbl;
    }

    /**
     *
     * @param startAppId
     * @param isActive
     * @return StartApplicationsTbl
     */
    public StartApplicationsTbl validateStartApp(final String startAppId,
            final boolean isActive) {
        StartApplicationsTbl startApplicationsTbl = this.startApplicationJpaDao.findOne(startAppId);
        checkObject(startApplicationsTbl, "StartApp", startAppId);
        startApplicationsTbl = filterStartApplicationResponse(isActive, startApplicationsTbl);
        checkObject(startApplicationsTbl, "StartApp", startAppId);
        return startApplicationsTbl;
    }
    
    
    public UserStartAppRelTbl validateUserStartApp(final String userStartAppId,
            final boolean isActive) {
        UserStartAppRelTbl userStartAppRel = this.userStartAppRelJpaDao.findOne(userStartAppId);
        checkObject(userStartAppRel, "UserStartApp", userStartAppId);
        userStartAppRel = filterUserStartApplicationResponse(isActive, userStartAppRel);
        checkObject(userStartAppRel, "UserStartApp", userStartAppId);
        return userStartAppRel;
    }

    /**
     *
     * @param baseAppId
     * @param isActive
     * @return BaseApplicationsTbl
     */
    public BaseApplicationsTbl validateBaseApp(final String baseAppId,
            final boolean isActive) {
        BaseApplicationsTbl baseApplicationsTbl = this.baseApplicationJpaDao.findOne(baseAppId);
        checkObject(baseApplicationsTbl, "BaseApp", baseAppId);
        baseApplicationsTbl = filterBaseApplicationResponse(isActive, baseApplicationsTbl);
        checkObject(baseApplicationsTbl, "BaseApp", baseAppId);
        return baseApplicationsTbl;
    }

    /**
     *
     * @param userAppId
     * @param isActive
     * @return UserApplicationsTbl
     */
    public UserApplicationsTbl validateUserApp(final String userAppId,
            final boolean isActive) {
        UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(userAppId);
        checkObject(userApplicationsTbl, "UserApp", userAppId);
        userApplicationsTbl = filterUserApplicationResponse(isActive, userApplicationsTbl);
        checkObject(userApplicationsTbl, "UserApp", userAppId);
        return userApplicationsTbl;
    }

    /**
     *
     * @param siteAdminAreaId
     * @param isActive
     * @return SiteAdminAreaRelTbl
     */
    public SiteAdminAreaRelTbl validateSiteAdminArea(final String siteAdminAreaId,
            final boolean isActive) {
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaId);
        checkObject(siteAdminAreaRelTbl, "SiteAdminArea", siteAdminAreaId);
        siteAdminAreaRelTbl = filterSiteAdminAreaRel(isActive, siteAdminAreaRelTbl);
        checkObject(siteAdminAreaRelTbl, "SiteAdminArea", siteAdminAreaId);
        return siteAdminAreaRelTbl;
    }

    /**
     *
     * @param adminAreaProjectRelId
     * @param isActive
     * @return AdminAreaProjectRelTbl
     */
    public AdminAreaProjectRelTbl validateAdminAreaProject(final String adminAreaProjectRelId,
            final boolean isActive) {
        AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjectRelId);
        checkObject(adminAreaProjectRelTbl, "AdminAreaProject", adminAreaProjectRelId);
        adminAreaProjectRelTbl = filterAdminAreaProjectRel(isActive, adminAreaProjectRelTbl);
        checkObject(adminAreaProjectRelTbl, "AdminAreaProject", adminAreaProjectRelId);
        return adminAreaProjectRelTbl;
    }
    
    
    public UserProjectRelTbl validateUserProjectRel(final String userProjectRelId,
            final boolean isActive) {
        UserProjectRelTbl userProjectRelTbl = this.userProjectRelJpaDao.findOne(userProjectRelId);
        checkObject(userProjectRelTbl, "UserProject", userProjectRelId);
        userProjectRelTbl = filterUserProjectResponse(isActive, userProjectRelTbl);
        checkObject(userProjectRelTbl, "UserProject", userProjectRelId);
        return userProjectRelTbl;
    }

    /**
     *
     * @param userId
     * @param isActive
     * @return UsersTbl
     */
    public UsersTbl validateUser(final String userId,
            final boolean isActive) {
        UsersTbl usersTbl = this.userJpaDao.findOne(userId);
        checkObject(usersTbl, "User", userId);
        usersTbl = filterUserResponse(isActive, usersTbl);
        checkObject(usersTbl, "User", userId);
        return usersTbl;
    }
}
