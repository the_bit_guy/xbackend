/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author dhana
 */
@Component
public class MailSenderTask {

    private static final Logger LOG
            = LoggerFactory.getLogger(MailSenderTask.class);

    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;

    @Autowired
    private SendMail sendMail;

    private final String STAGE_NEW = "new";
    private final String STAGE_INPROGRESS = "inprogress";
    private final String STAGE_SUCCESS = "success";
    private final String STAGE_FAILURE = "failure";

    @Value("${cron.mail.failure.retry.count}")
    private int rtcCount;

    @Scheduled(cron = "${cron.expression}")
    public void newMailSendTask() {
       // LOG.info("********** Inside newMailSendTask *************");
        //get the records from mailQ with stage as new
        Iterable<MailQueueTbl> mailQueueTblsWithStageNew
                = this.mailQueueJpaDao.findByStage(STAGE_NEW);
        mailQueueTblsWithStageNew.forEach(mailQueueTblStageNew -> {
            mailQueueTblStageNew.setStage(STAGE_INPROGRESS);
        });
        Iterable<MailQueueTbl> mailQueueTblStageInPorgress
                = this.mailQueueJpaDao.save(mailQueueTblsWithStageNew);
        mailQueueTblStageInPorgress = this.mailQueueJpaDao.findByStage(STAGE_INPROGRESS);
        this.inProgressMailTaskProcess(mailQueueTblStageInPorgress);
       // LOG.info("********** Exit newMailSendTask *************");
    }

    @Scheduled(cron = "${cron.expression}")
	public void failureMailSendTask() {
		// LOG.info("********** Inside failureMailSendTask *************");
		// get the records from mailQ with stage as new
		Iterable<MailQueueTbl> mailQueueTblsWithStageF = this.mailQueueJpaDao.findByStage(STAGE_FAILURE);
		mailQueueTblsWithStageF.forEach(mailQueueTblStageF -> {

			String rtcFaceLess = mailQueueTblStageF.getReTryCount();
			if (rtcFaceLess != null) {
				int rtcValue = Integer.parseInt(rtcFaceLess);
				if (rtcValue < rtcCount) {
					rtcValue++;
					mailQueueTblStageF.setStage(STAGE_INPROGRESS);
					mailQueueTblStageF.setReTryCount(Integer.toString(rtcValue));
					MailQueueTbl mailQueueTblStageIP = this.mailQueueJpaDao.save(mailQueueTblStageF);
					this.doSingleMailTaskProcess(mailQueueTblStageIP);
				}
			}
		});

		// LOG.info("********** Exit failureMailSendTask *************");
	}

    private void inProgressMailTaskProcess(
            Iterable<MailQueueTbl> mailQueueTblStageInPorgress) {
        mailQueueTblStageInPorgress.forEach((mailQueueTblStageIP) -> {
            this.doSingleMailTaskProcess(mailQueueTblStageIP);
        });

    }

	private void doSingleMailTaskProcess(MailQueueTbl mailQueueTblStageIP) {
		String subject = mailQueueTblStageIP.getSubject();
		String toMail = mailQueueTblStageIP.getToMail();
		String textMessage = mailQueueTblStageIP.getTextMessage();
		try {
			this.sendMail.sendEmail(toMail, subject, textMessage);
			mailQueueTblStageIP.setStage(STAGE_SUCCESS);
			this.mailQueueJpaDao.save(mailQueueTblStageIP);
		} catch (Exception ex) {
			LOG.debug("ex {}", ex);
			mailQueueTblStageIP.setStage(STAGE_FAILURE);
			mailQueueTblStageIP.setRemarks(ex.getMessage());
			this.mailQueueJpaDao.save(mailQueueTblStageIP);
		}
	}

}
