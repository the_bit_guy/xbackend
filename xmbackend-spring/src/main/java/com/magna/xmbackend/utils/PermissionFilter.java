package com.magna.xmbackend.utils;

import com.magna.xmbackend.entities.PermissionApiMappingTbl;
import com.magna.xmbackend.entities.PermissionTbl;
import com.magna.xmbackend.vo.enums.PermissionType;
import com.magna.xmbackend.vo.permission.PermissionApiResponse;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class PermissionFilter {

    private static final Logger LOG
            = LoggerFactory.getLogger(PermissionFilter.class);

    // Stores Key as PERMISSION_ID & Value as NAME
    private Map<String, String> aaBasedPerMap = new HashMap<>();
    // Stores Key as PERMISSION_ID & Value as NAME
    private Map<String, String> objectPerMap = new HashMap<>();
    // Stores Key as PERMISSION_ID & Value as NAME
    private Map<String, String> relationPerMap = new HashMap<>();
    // Stores Key as PERMISSION_ID & Value as NAME	
    private Map<String, String> unauthorizedPerMap = new HashMap<>();
    // Stores Key as PERMISSION_API_MAPPING_ID & Value as API
    private Map<String, String> relationApiMap = new HashMap<>();
    // Stores Key as PERMISSION_API_MAPPING_ID & Value as PERMISSION_ID
    private Map<String, String> perRelationApiMap = new HashMap<>();

    /**
     *
     * @param permissionResponse
     */
    public void filterByPermissionType(final PermissionResponse permissionResponse) {
        LOG.debug("filterByPermissionType start");
        final Iterable<PermissionTbl> permissionTbls = permissionResponse.getPermissionTbls();
        for (final PermissionTbl permissionTbl : permissionTbls) {
            final PermissionType permissionType = PermissionType.valueOf(permissionTbl.getPermissionType());
            switch (permissionType) {
                case OBJECT_PERMISSION:
                    getObjectPerMap().put(permissionTbl.getPermissionId(), permissionTbl.getName());
                    break;
                case RELATION_PERMISSION:
                    getRelationPerMap().put(permissionTbl.getPermissionId(), permissionTbl.getName());
                    break;
                case AA_BASED_RELATION_PERMISSION:
                    getAaBasedPerMap().put(permissionTbl.getPermissionId(), permissionTbl.getName());
                    break;
                case UNAUTHORIZED:
                    getUnauthorizedPerMap().put(permissionTbl.getPermissionId(), permissionTbl.getName());
                    break;
                default:
                    break;
            }
        }
        LOG.debug("filterByPermissionType end");
    }

    /**
     *
     * @param permissionApiResponse
     */
    public void addrelationApiMap(final PermissionApiResponse permissionApiResponse) {
        LOG.debug(">>addrelationApiMap start");
        final Iterable<PermissionApiMappingTbl> permissionApiMappingTbls
                = permissionApiResponse.getPermissionApiMappingTbls();
        for (final PermissionApiMappingTbl permissionApiMappingTbl : permissionApiMappingTbls) {
            getRelationApiMap().put(permissionApiMappingTbl.getPermissionApiMappingId(),
                    permissionApiMappingTbl.getApi());
            getPerRelationApiMap().put(permissionApiMappingTbl.getPermissionApiMappingId(),
                    permissionApiMappingTbl.getPermissionId().getPermissionId());
        }
        LOG.debug("<<addrelationApiMap end");
    }

    /**
     *
     * @param requestPath
     * @return List
     */
    public List<String> getRelationApiMapValue(final String requestPath) {
        final List<String> result = getRelationApiMap().entrySet().stream()
                .filter(map -> requestPath.matches(map.getValue()))
                .map(map -> map.getValue())
                .collect(Collectors.toList());
        return result;
    }

    /**
     *
     * @param requestPath
     * @return List
     */
    public List<String> getRelationApiMapKey(final String requestPath) {
        final List<String> result = getRelationApiMap().entrySet().stream()
                .filter(map -> requestPath.matches(map.getValue()))
                .map(map -> map.getKey())
                .collect(Collectors.toList());
        return result;
    }

    /**
     * @return the aaBasedPerMap
     */
    public Map<String, String> getAaBasedPerMap() {
        return aaBasedPerMap;
    }

    /**
     * @param aaBasedPerMap the aaBasedPerMap to set
     */
    public void setAaBasedPerMap(Map<String, String> aaBasedPerMap) {
        this.aaBasedPerMap = aaBasedPerMap;
    }

    /**
     * @return the objectPerMap
     */
    public Map<String, String> getObjectPerMap() {
        return objectPerMap;
    }

    /**
     * @param objectPerMap the objectPerMap to set
     */
    public void setObjectPerMap(Map<String, String> objectPerMap) {
        this.objectPerMap = objectPerMap;
    }

    /**
     * @return the relationPerMap
     */
    public Map<String, String> getRelationPerMap() {
        return relationPerMap;
    }

    /**
     * @param relationPerMap the relationPerMap to set
     */
    public void setRelationPerMap(Map<String, String> relationPerMap) {
        this.relationPerMap = relationPerMap;
    }

    /**
     * @return the relationApiMap
     */
    public Map<String, String> getRelationApiMap() {
        return relationApiMap;
    }

    /**
     * @param relationApiMap the relationApiMap to set
     */
    public void setRelationApiMap(Map<String, String> relationApiMap) {
        this.relationApiMap = relationApiMap;
    }

    /**
     * @return the perRelationApiMap
     */
    public Map<String, String> getPerRelationApiMap() {
        return perRelationApiMap;
    }

    /**
     * @param perRelationApiMap the perRelationApiMap to set
     */
    public void setPerRelationApiMap(Map<String, String> perRelationApiMap) {
        this.perRelationApiMap = perRelationApiMap;
    }

    /**
     * @return the unauthorizedPerMap
     */
    public Map<String, String> getUnauthorizedPerMap() {
        return unauthorizedPerMap;
    }

    /**
     * @param unauthorizedPerMap the unauthorizedPerMap to set
     */
    public void setUnauthorizedPerMap(Map<String, String> unauthorizedPerMap) {
        this.unauthorizedPerMap = unauthorizedPerMap;
    }

    /**
     * clears the cache
     */
    public void clearCache() {
        aaBasedPerMap.clear();
        objectPerMap.clear();
        relationPerMap.clear();
        relationApiMap.clear();
        perRelationApiMap.clear();
        unauthorizedPerMap.clear();
    }

}
