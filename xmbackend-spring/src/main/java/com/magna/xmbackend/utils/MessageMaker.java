package com.magna.xmbackend.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectAppTranslationTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTranslationTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.LanguageJpaDao;
import com.magna.xmbackend.vo.adminArea.AdminAreaTranslation;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation;
import com.magna.xmbackend.vo.jpa.site.SiteTranslation;
import com.magna.xmbackend.vo.project.ProjectTranslation;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation;
import com.magna.xmbackend.vo.startApplication.StartApplicationTranslation;
import com.magna.xmbackend.vo.userApplication.UserApplicationTranslation;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageMaker.
 *
 * @author vijay
 */
@Component
public class MessageMaker {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(MessageMaker.class);

    /**
     * The message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * The language jpa dao.
     */
    @Autowired
    private LanguageJpaDao languageJpaDao;

    /**
     * Extract message.
     *
     * @param languageCode the language code
     * @param errorCode the error code
     * @param paramArray the param array
     * @return the string
     */
    public final String extractMessage(final String languageCode, final String errorCode, final String[] paramArray) {
        final java.util.Locale local = new Locale(languageCode);
        final String message = messageSource.getMessage(errorCode, paramArray, local);
        LOG.debug("message={}", message);
        return message;
    }

    /**
     * Extract from exception.
     *
     * @param ccre the ccre
     * @return Map
     */
    public final Map<String, String> extractFromException(final CannotCreateRelationshipException ccre) {
        final Map<String, String> statusMap = new HashMap<>();
        final Map<String, String[]> paramMap = ccre.getParamMap();
        paramMap.forEach((languageCode, nameArray) -> {
            final String message = this.extractMessage(languageCode, ccre.getErrCode(), nameArray);
            statusMap.put(languageCode, message);
        });
        return statusMap;
    }

    /**
     * Extract from exception.
     *
     * @param ccoe the ccoe
     * @return the map
     */
    public final Map<String, String> extractFromException(final CannotCreateObjectException ccoe) {
        final Map<String, String> statusMap = new HashMap<>();
        final Map<String, String[]> paramMap = ccoe.getParamMap();
        paramMap.forEach((languageCode, nameArray) -> {
            final String message = this.extractMessage(languageCode, ccoe.getErrCode(), nameArray);
            statusMap.put(languageCode, message);
        });
        return statusMap;
    }

    /**
     * Extract from exception.
     *
     * @param objectNotFoundException the object not found exception
     * @return the map
     */
    public final Map<String, String> extractFromException(final XMObjectNotFoundException objectNotFoundException) {
        final Map<String, String> statusMap = new HashMap<>();
        Iterable<LanguagesTbl> languagesTbls = this.languageJpaDao.findAll();
        languagesTbls.forEach(languageTbl -> {
            final String[] paramArr = objectNotFoundException.getParam();
            final String languageCode = languageTbl.getLanguageCode();
            final String message = this.extractMessage(languageCode, objectNotFoundException.getErrCode(), paramArr);
            statusMap.put(languageCode, message);
        });
        return statusMap;
    }

    /**
     * Gets the i 18 n code map.
     *
     * @param zeroIndexMap the zero index map
     * @param firstIndexMap the first index map
     * @return Map
     */
    public final Map<String, String[]> geti18nCodeMap(final Map<String, String> zeroIndexMap,
            final Map<String, String> firstIndexMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        zeroIndexMap.forEach((languageCode, name) -> {
            final String zeroIndexName = "'" + name + "'";
            final String firstIndexName = "'" + firstIndexMap.get(languageCode) + "'";
            LOG.debug("zeroIndexName={} firstIndexName={}", zeroIndexName, firstIndexName);
            final String[] param = {zeroIndexName, firstIndexName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the i 18 n code map.
     *
     * @param zeroIndexMap the zero index map
     * @param firstIndexMap the first index map
     * @param secondIndex the second index
     * @return Map
     */
    public final Map<String, String[]> geti18nCodeMap(final Map<String, String> zeroIndexMap,
            final Map<String, String> firstIndexMap, final Map<String, String> secondIndex) {
        final Map<String, String[]> paramMap = new HashMap<>();
        zeroIndexMap.forEach((languageCode, name) -> {
            final String zeroIndexName = "'" + name + "'";
            final String firstIndexName = "'" + firstIndexMap.get(languageCode) + "'";
            final String secondIndexName = "'" + secondIndex.get(languageCode) + "'";
            LOG.debug("zeroIndexName={} firstIndexName={}", zeroIndexName, firstIndexName);
            final String[] param = {zeroIndexName, firstIndexName, secondIndexName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the i 18 n code map.
     *
     * @param zeroIndexMap the zero index map
     * @param firstIndexMap the first index map
     * @param secondIndexMap the second index map
     * @param thirdIndexMap the third index map
     * @return the i 18 n code map
     */
    public final Map<String, String[]> geti18nCodeMap(final Map<String, String> zeroIndexMap,
            final Map<String, String> firstIndexMap, final Map<String, String> secondIndexMap,
            final Map<String, String> thirdIndexMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        zeroIndexMap.forEach((languageCode, name) -> {
            final String zeroIndexName = "'" + name + "'";
            final String firstIndexName = "'" + firstIndexMap.get(languageCode) + "'";
            final String secondIndexName = "'" + secondIndexMap.get(languageCode) + "'";
            final String thirdIndexName = "'" + thirdIndexMap.get(languageCode) + "'";
            LOG.debug("zeroIndexName={} firstIndexName={}", zeroIndexName, firstIndexName);
            final String[] param = {zeroIndexName, firstIndexName, secondIndexName, thirdIndexName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the base app name withi 18 n code.
     *
     * @param translations the translations
     * @param name the name
     * @return the base app name withi 18 n code
     */
    public Map<String, String[]> getBaseAppNameWithi18nCode(List<BaseApplicationTransulation> translations,
            String name) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (BaseApplicationTransulation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the directory project rel withi 18 n code.
     *
     * @param directoryTransMap the directory trans map
     * @param projectTransMap the project trans map
     * @return the directory project rel withi 18 n code
     */
    public Map<String, String[]> getDirectoryProjectRelWithi18nCode(final Map<String, String> directoryTransMap,
            final Map<String, String> projectTransMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        directoryTransMap.forEach((languageCode, name) -> {
            final String dirName = "'" + name + "'";
            final String projName = "'" + projectTransMap.get(languageCode) + "'";
            LOG.debug("dirName={} projName={}", dirName, projName);
            final String[] param = {dirName, projName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the directory user rel withi 18 n code.
     *
     * @param directoryTransMap the directory trans map
     * @param userTransMap the user trans map
     * @return the directory user rel withi 18 n code
     */
    public Map<String, String[]> getDirectoryUserRelWithi18nCode(final Map<String, String> directoryTransMap,
            final Map<String, String> userTransMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        directoryTransMap.forEach((languageCode, name) -> {
            final String dirName = "'" + name + "'";
            final String userName = "'" + userTransMap.get(languageCode) + "'";
            LOG.debug("dirName={} userName={}", dirName, userName);
            final String[] param = {dirName, userName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the directory user app rel withi 18 n code.
     *
     * @param directoryTransMap the directory trans map
     * @param userAppTransMap the user app trans map
     * @return the directory user app rel withi 18 n code
     */
    public Map<String, String[]> getDirectoryUserAppRelWithi18nCode(final Map<String, String> directoryTransMap,
            final Map<String, String> userAppTransMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        directoryTransMap.forEach((languageCode, name) -> {
            final String dirName = "'" + name + "'";
            final String userAppName = "'" + userAppTransMap.get(languageCode) + "'";
            LOG.debug("dirName={} userName={}", dirName, userAppName);
            final String[] param = {dirName, userAppName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the project app name withi 18 n code.
     *
     * @param translations the translations
     * @param name the name
     * @return the project app name withi 18 n code
     */
    public Map<String, String[]> getProjectAppNameWithi18nCode(List<ProjectApplicationTransulation> translations,
            String name) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (ProjectApplicationTransulation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the directory proj app rel withi 18 n code.
     *
     * @param directoryTransMap the directory trans map
     * @param projAppTransMap the proj app trans map
     * @return the directory proj app rel withi 18 n code
     */
    public Map<String, String[]> getDirectoryProjAppRelWithi18nCode(final Map<String, String> directoryTransMap,
            final Map<String, String> projAppTransMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        directoryTransMap.forEach((languageCode, name) -> {
            final String dirName = "'" + name + "'";
            final String projAppName = "'" + projAppTransMap.get(languageCode) + "'";
            LOG.debug("dirName={} projAppName={}", dirName, projAppName);
            final String[] param = {dirName, projAppName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the user app name withi 18 n code.
     *
     * @param translations the translations
     * @param name the name
     * @return the user app name withi 18 n code
     */
    public Map<String, String[]> getUserAppNameWithi18nCode(List<UserApplicationTranslation> translations,
            String name) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (UserApplicationTranslation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the start app name withi 18 n code.
     *
     * @param translations the translations
     * @param name the name
     * @return the start app name withi 18 n code
     */
    public Map<String, String[]> getStartAppNameWithi18nCode(List<StartApplicationTranslation> translations,
            String name) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (StartApplicationTranslation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the site name withi 18 n code.
     *
     * @param translations the translations
     * @param siteName the site name
     * @return the site name withi 18 n code
     */
    public Map<String, String[]> getSiteNameWithi18nCode(final List<SiteTranslation> translations,
            final String siteName) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (SiteTranslation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + siteName + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the project name withi 18 n code.
     *
     * @param translations the translations
     * @param projectName the project name
     * @return the project name withi 18 n code
     */
    public Map<String, String[]> getProjectNameWithi18nCode(List<ProjectTranslation> translations,
            String projectName) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (ProjectTranslation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + projectName + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the site admin area rel withi 18 n code.
     *
     * @param siteTransMap the site trans map
     * @param adminAreaTransMap the admin area trans map
     * @return the site admin area rel withi 18 n code
     */
    public Map<String, String[]> getSiteAdminAreaRelWithi18nCode(final Map<String, String> siteTransMap,
            final Map<String, String> adminAreaTransMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        siteTransMap.forEach((languageCode, name) -> {
            final String siteName = "'" + name + "'";
            final String adminAreaName = "'" + adminAreaTransMap.get(languageCode) + "'";
            LOG.debug("siteName={} adminAreaName={}", siteName, adminAreaName);
            final String[] param = {siteName, adminAreaName};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the admin area name withi 18 n code.
     *
     * @param translations the translations
     * @param name the name
     * @return the admin area name withi 18 n code
     */
    public Map<String, String[]> getAdminAreaNameWithi18nCode(List<AdminAreaTranslation> translations, String name) {
        final Map<String, String[]> paramMap = new HashMap<>();
        for (AdminAreaTranslation translation : translations) {
            final String languageCode = translation.getLanguageCode();
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        }
        return paramMap;
    }

    /**
     * Gets the user role rel withi 18 n code.
     *
     * @param userRoleMap the user role map
     * @return the user role rel withi 18 n code
     */
    public Map<String, String[]> getUserRoleRelWithi18nCode(final Map<String, String> userRoleMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        userRoleMap.forEach((userId, roleId) -> {
            LOG.debug("userId={} roleId={}", userId, roleId);
            final String[] param = {"'" + userId + "'", "'" + roleId + "'"};
            paramMap.put("en", param);
            paramMap.put("de", param);
        });
        return paramMap;
    }

    /**
     * Gets the i 18 n code map.
     *
     * @param zeroIndexMap the zero index map
     * @return the i 18 n code map
     */
    public final Map<String, String[]> geti18nCodeMap(final Map<String, String> zeroIndexMap) {
        final Map<String, String[]> paramMap = new HashMap<>();
        zeroIndexMap.forEach((languageCode, name) -> {
            final String[] param = {"'" + name + "'"};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the group user rel withi 18 n code.
     *
     * @param userName the user name
     * @param groupName the group name
     * @return the group user rel withi 18 n code
     */
    public Map<String, String[]> getGroupUserRelWithi18nCode(final String userName, final String groupName) {
        final Map<String, String[]> paramMap = new HashMap<>();
        final Iterable<LanguagesTbl> languageTbls = this.languageJpaDao.findAll();
        languageTbls.forEach((languageCode) -> {
            LOG.debug("userName={} groupName={}", userName, groupName);
            final String[] param = {"'" + userName + "'", "'" + groupName + "'"};
            paramMap.put(languageCode.getLanguageCode(), param);
        });
        return paramMap;
    }

    /**
     * Gets the group project rel withi 18 n code.
     *
     * @param projectsTransMap the projects trans map
     * @param groupName the group name
     * @return the group project rel withi 18 n code
     */
    public Map<String, String[]> getGroupProjectRelWithi18nCode(final Map<String, String> projectsTransMap,
            final String groupName) {
        final Map<String, String[]> paramMap = new HashMap<>();
        projectsTransMap.forEach((languageCode, name) -> {
            final String projName = projectsTransMap.get(languageCode);
            LOG.debug("projName ={} groupName={}", projName, groupName);
            final String[] param = {"'" + projName + "'", "'" + groupName + "'"};
            paramMap.put(languageCode, param);
        });
        return paramMap;
    }

    /**
     * Gets the admin area names.
     *
     * @param adminAreasTbl the admin areas tbl
     * @return Map
     */
    public final Map<String, String> getAdminAreaNames(final AdminAreasTbl adminAreasTbl) {
        final Map<String, String> adminAreaNames = new HashMap<>();
        final String adminAreaName = adminAreasTbl.getName();
        final Collection<AdminAreaTranslationTbl> adminAreaTranslationTblCollection = adminAreasTbl
                .getAdminAreaTranslationTblCollection();
        for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTblCollection) {
            final LanguagesTbl languagesTbl = adminAreaTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            adminAreaNames.put(languageCode, adminAreaName);
        }
        return adminAreaNames;
    }

    /**
     * Gets the start app names.
     *
     * @param startApplicationsTbl the start applications tbl
     * @return Map
     */
    public Map<String, String> getStartAppNames(final StartApplicationsTbl startApplicationsTbl) {
        final Map<String, String> startAppNames = new HashMap<>();
        String name = startApplicationsTbl.getName();
        final Collection<StartAppTranslationTbl> startAppTranslationTblCollection = startApplicationsTbl
                .getStartAppTranslationTblCollection();
        startAppTranslationTblCollection.forEach((startAppTranslationTbl) -> {
            final LanguagesTbl languagesTbl = startAppTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            startAppNames.put(languageCode, name);
        });
        return startAppNames;
    }

    /**
     * Gets the user app names.
     *
     * @param userApplicationsTbl the user applications tbl
     * @return Map
     */
    public Map<String, String> getUserAppNames(final UserApplicationsTbl userApplicationsTbl) {
        final String name = userApplicationsTbl.getName();
        final Map<String, String> userAppNames = new HashMap<>();
        final Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userApplicationsTbl
                .getUserAppTranslationTblCollection();
        userAppTranslationTblCollection.forEach((userAppTranslationTbl) -> {
            final LanguagesTbl languagesTbl = userAppTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            userAppNames.put(languageCode, name);
        });
        return userAppNames;
    }

    /**
     * Gets the project names.
     *
     * @param projectsTbl the projects tbl
     * @return Map
     */
    public final Map<String, String> getProjectNames(final ProjectsTbl projectsTbl) {
        final String name = projectsTbl.getName();
        final Map<String, String> projectNames = new HashMap<>();
        final Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsTbl
                .getProjectTranslationTblCollection();
        for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
            final LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            projectNames.put(languageCode, name);
        }
        return projectNames;
    }

    /**
     * Gets the project app names.
     *
     * @param projectApplicationsTbl the project applications tbl
     * @return Map
     */
    public final Map<String, String> getProjectAppNames(final ProjectApplicationsTbl projectApplicationsTbl) {
        String name = projectApplicationsTbl.getName();
        final Map<String, String> projectAppNames = new HashMap<>();
        final Collection<ProjectAppTranslationTbl> projectAppTranslationTblCollection = projectApplicationsTbl
                .getProjectAppTranslationTblCollection();
        for (ProjectAppTranslationTbl projectAppTranslationTbl : projectAppTranslationTblCollection) {
            final LanguagesTbl languagesTbl = projectAppTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            projectAppNames.put(languageCode, name);
        }
        return projectAppNames;
    }

    /**
     * Gets the user names.
     *
     * @param usersTbl the users tbl
     * @return Map
     */
    public final Map<String, String> getUserNames(final UsersTbl usersTbl) {
        final Map<String, String> userNames = new HashMap<>();
        final Collection<UserTranslationTbl> userTranslationTblCollection = usersTbl.getUserTranslationTblCollection();
        for (UserTranslationTbl userTranslationTbl : userTranslationTblCollection) {
            final LanguagesTbl languagesTbl = userTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            final String fullName = usersTbl.getUsername();
            userNames.put(languageCode, fullName);
        }
        return userNames;
    }

    /**
     * Gets the site names.
     *
     * @param sitesTbl the sites tbl
     * @return Map
     */
    public final Map<String, String> getSiteNames(final SitesTbl sitesTbl) {
        final String name = sitesTbl.getName();
        final Map<String, String> siteNames = new HashMap<>();
        final Collection<SiteTranslationTbl> siteTranslationTbls = sitesTbl.getSiteTranslationTblCollection();
        for (final SiteTranslationTbl siteTranslationTbl : siteTranslationTbls) {
            final LanguagesTbl languagesTbl = siteTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            siteNames.put(languageCode, name);
        }
        return siteNames;
    }
}
