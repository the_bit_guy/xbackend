/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.opendxm.vo;

/**
 * The Class ActiveUserProjectVO.
 *
 * @author shashwat.anand
 */
public class OpenDXMUserProjectVO {

	/** The user name. */
	private String userName;
	
	/** The project name. */
	private String projectName;

	/** The status. */
	private String status;

	/**
	 * Instantiates a new active user project VO.
	 */
	public OpenDXMUserProjectVO() {
	}

	/**
	 * Instantiates a new active user project VO.
	 *
	 * @param userName the user name
	 * @param projectName the project name
	 * @param status the status
	 */
	public OpenDXMUserProjectVO(String userName, String projectName, String status) {
		super();
		this.userName = userName;
		this.projectName = projectName;
		this.status = status;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the project name.
	 *
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets the project name.
	 *
	 * @param projectName            the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
