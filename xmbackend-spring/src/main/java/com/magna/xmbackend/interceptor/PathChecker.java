package com.magna.xmbackend.interceptor;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.AdminMenuConfigJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.jpa.rel.dao.DirectoryRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.GroupRelJpaDao;
import com.magna.xmbackend.mgr.PermissionMgr;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.utils.PermissionFilter;
import com.magna.xmbackend.vo.permission.PermissionApiResponse;
import com.magna.xmbackend.vo.permission.PermissionResponse;
import com.magna.xmbackend.vo.roles.AuthorizationResponse;
import com.magna.xmbackend.vo.user.AuthResponse;

/**
 *
 * @author dhana
 */
@Component
public class PathChecker implements HandlerInterceptor {

    private static final Logger LOG
            = LoggerFactory.getLogger(PathChecker.class);

    private final List<String> byPassPaths;

    @Autowired
    private PermissionMgr permissionMgr;

    @Autowired
    private PermissionFilter permissionFilter;

    @Autowired
    UserMgr userMgr;

    @Autowired
    ProjectMgr projectMgr;

    @Autowired
    UserApplicationMgr userApplicationMgr;

    @Autowired
    ProjectApplicationMgr projectApplicationMgr;

    @Autowired
    DirectoryRelJpaDao directoryRelJpaDao;

    @Autowired
    GroupRelJpaDao groupRelJpaDao;

    @Autowired
    AdminMenuConfigJpaDao adminMenuConfigJpaDao;

    @Autowired
    private UserTktJpaDao userTktJpaDao;

    public PathChecker() {
        this.byPassPaths = new ArrayList<>();
        //this.byPassPaths.add("/opendxm/activeUsers");
        //this.byPassPaths.add("/opendxm/activeProjects");
        //this.byPassPaths.add("/opendxm/activeUserProjects");
        this.byPassPaths.add("/opendxm/getProjects");
        this.byPassPaths.add("/opendxm/getUsers");
        this.byPassPaths.add("/opendxm/getUserProjects");
        this.byPassPaths.add("/auth/login");
        this.byPassPaths.add("/appauth/xmenuUserAccessCheck");
    }

    /**
     *
     * @param servletRequest
     * @param servletResponse
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest servletRequest,
            HttpServletResponse servletResponse, Object o) throws Exception {

        LOG.debug(">>>>>>>>>>>>>debug Inside preHandle<<<<<<<<<<<<<<");

        boolean isAccessAllowed = false;

        if (this.byPassPaths.stream().anyMatch((byPassPath)
                -> (servletRequest.getServletPath().contains(byPassPath)))) {
            return true;
        }

        String tkt = servletRequest.getHeader("TKT");

        if (tkt == null || !this.checkTkt(tkt)) {
            servletResponse.setContentType("application/json");
            String authMessage = formTktNotValidResponse("hidden", "Not a Valid token");
            PrintWriter out = servletResponse.getWriter();
            servletResponse.setStatus(HttpStatus.FORBIDDEN.value());
            out.println(authMessage);
            return false;
        }

        LOG.debug("USER_NAME={}", servletRequest.getHeader("USER_NAME"));
        LOG.debug("adminAreaId={}", servletRequest.getHeader("ADMIN_AREA_ID"));
        LOG.info(">>>>>>>>>>>>>debug Inside preHandle<<<<<<<<<<<<<<");
        LOG.info("getServletPath()={}", servletRequest.getServletPath());
        LOG.info("USER_NAME={}", servletRequest.getHeader("USER_NAME"));
        LOG.info("adminAreaId={}", servletRequest.getHeader("ADMIN_AREA_ID"));
        String requestPath = servletRequest.getServletPath();
        final String userName = servletRequest.getHeader("USER_NAME");
        final String adminAreaId = servletRequest.getHeader("ADMIN_AREA_ID");
        LOG.info("requestPath={}", requestPath);
        if (null != requestPath && null != userName) {
            servletRequest.setAttribute("userName", userName);
            servletRequest.setAttribute("adminAreaId", adminAreaId);
            if (permissionFilter.getAaBasedPerMap().isEmpty()) {
                final PermissionResponse permissionResponse = permissionMgr.findAll();
                permissionFilter.filterByPermissionType(permissionResponse);
                final PermissionApiResponse permissionApiResponse = permissionMgr.retrieveAllPermissionApiMappingRelation();
                // Store the relation api in the map with key as permission id
                permissionFilter.addrelationApiMap(permissionApiResponse);
            }
            LOG.debug("obj per={}", permissionFilter.getObjectPerMap());
            LOG.debug("rel per={}", permissionFilter.getRelationPerMap());
            LOG.debug("AA Based={}", permissionFilter.getAaBasedPerMap());
            LOG.debug("rel per API={}", permissionFilter.getRelationApiMap());
            // Check the requested api is find Flavour or Validation Flavour
            // If yes, allow to access the api without any validation
            LOG.debug("Before requestPath={}", requestPath);
            requestPath = parseRequestPath(requestPath);
            LOG.debug("After requestPath={}", requestPath);
            if (null != requestPath) {
                final boolean isExclusion = isExclusion(requestPath);
                LOG.debug("isExclusion={}", isExclusion);
                // If no, check whether user is permitted to access this api path
                if (!isExclusion) {
                    isAccessAllowed = permissionMgr.isAccessAllowed(requestPath, userName, adminAreaId);
                    /*isAccessAllowed = permissionMgr.validateUserApiHavingPermission(requestPath, userName, null);
                    LOG.debug("isAccessAllowed w/0 AA={}", isAccessAllowed);
                    // If false, try with Admin Area Id
                    if (!isAccessAllowed) {
                        isAccessAllowed = permissionMgr.validateUserApiHavingPermission(requestPath, userName, adminAreaId);
                        LOG.debug("isAccessAllowed with AA={}", isAccessAllowed);
                    }*/
                } else {
                    isAccessAllowed = true;
                }
            }
            if (!isAccessAllowed) {
                servletResponse.setContentType("application/json");
                if (null == requestPath) {
                    requestPath = servletRequest.getServletPath();
                }
                final String authMessage = formAuthorizationResponse(userName, requestPath);
                final PrintWriter out = servletResponse.getWriter();
                servletResponse.setStatus(HttpStatus.FORBIDDEN.value());
                out.println(authMessage);
            }
        }
        return isAccessAllowed;
    }

    /**
     *
     * @param servletRequest
     * @param servletResponse
     * @param o
     * @param mav
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest servletRequest,
            HttpServletResponse servletResponse, Object o,
            ModelAndView mav) throws Exception {
        LOG.info("******Inside postHandle********");
        if (this.byPassPaths.stream().anyMatch((byPassPath)
                -> (servletRequest.getServletPath().contains(byPassPath)))) {
            return;
        }
        String requestPath = servletRequest.getServletPath();
        String parseRequestPath = this.parseRequestPath(requestPath);
        if (parseRequestPath.contains("delete") || parseRequestPath.contains("multiDelete")) {
            this.removeRelationObjects(servletRequest, parseRequestPath);
        }
    }

    private void removeRelationObjects(HttpServletRequest servletRequest, String parseRequestPath) {
        String subUrl = parseRequestPath.split("/")[1];
        switch (subUrl) {
            case "user":
                LOG.info("user delted!");
                Object userAttr = servletRequest.getAttribute("userId");
                if (null != userAttr) {
                    String userId = userAttr.toString();
                    UsersTbl usersTbl = this.userMgr.findById(userId);
                    if (null == usersTbl) {
                        this.removeObjectFromDirectoryRelations(userId);
                        this.removeObjectFromGroupRelations(userId);
                        this.removeObjectFromAdminMenuConfig(userId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> userIds = (Set<String>) servletRequest.getAttribute("userIds");
                if (null != userIds) {
                    userIds.forEach(uId -> {
                        UsersTbl usersTbl = this.userMgr.findById(uId);
                        if (null == usersTbl) {
                            this.removeObjectFromDirectoryRelations(uId);
                            this.removeObjectFromGroupRelations(uId);
                            this.removeObjectFromAdminMenuConfig(uId);
                        }
                    });
                }
                break;

            case "project":
                LOG.info("project deleted!");
                Object projectAttr = servletRequest.getAttribute("projectId");
                if (null != projectAttr) {
                    String projectId = projectAttr.toString();
                    ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
                    if (null == projectsTbl) {
                        this.removeObjectFromDirectoryRelations(projectId);
                        this.removeObjectFromGroupRelations(projectId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> projectIds = (Set<String>) servletRequest.getAttribute("projectIds");
                if (null != projectIds) {
                    projectIds.forEach(pId -> {
                        UsersTbl usersTbl = this.userMgr.findById(pId);
                        if (null == usersTbl) {
                            this.removeObjectFromDirectoryRelations(pId);
                            this.removeObjectFromGroupRelations(pId);
                        }
                    });
                }
                break;
            case "userApplication":
                LOG.info("userApplication deleted!");
                Object userAppAttr = servletRequest.getAttribute("userAppId");
                if (null != userAppAttr) {
                    String userAppId = userAppAttr.toString();
                    UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppId);
                    if (null == userApplicationsTbl) {
                        this.removeObjectFromDirectoryRelations(userAppId);
                        this.removeObjectFromGroupRelations(userAppId);
                        this.removeObjectFromAdminMenuConfig(userAppId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> userAppIds = (Set<String>) servletRequest.getAttribute("userAppIds");
                if (null != userAppIds) {
                    userAppIds.forEach(userApId -> {
                        UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userApId);
                        if (null == userApplicationsTbl) {
                            this.removeObjectFromDirectoryRelations(userApId);
                            this.removeObjectFromGroupRelations(userApId);
                            this.removeObjectFromAdminMenuConfig(userApId);
                        }
                    });
                }
                break;
            case "projectApplication":
                LOG.info("projectApplication deleted!");
                Object projectAppAttr = servletRequest.getAttribute("projectAppId");
                if (null != projectAppAttr) {
                    String projectAppId = projectAppAttr.toString();
                    ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projectAppId);
                    if (null == projectApplicationsTbl) {
                        this.removeObjectFromDirectoryRelations(projectAppId);
                        this.removeObjectFromGroupRelations(projectAppId);
                        this.removeObjectFromAdminMenuConfig(projectAppId);
                    }
                }

                @SuppressWarnings("unchecked") Set<String> projectAppIds = (Set<String>) servletRequest.getAttribute("projectAppIds");
                if (null != projectAppIds) {
                    projectAppIds.forEach(projApId -> {
                        ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projApId);
                        if (null == projectApplicationsTbl) {
                            this.removeObjectFromDirectoryRelations(projApId);
                            this.removeObjectFromGroupRelations(projApId);
                            this.removeObjectFromAdminMenuConfig(projApId);
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param id
     */
    private void removeObjectFromDirectoryRelations(final String objectId) {
        final List<DirectoryRefTbl> directoryRefTbls = this.directoryRelJpaDao.findByObjectId(objectId);
        if (!directoryRefTbls.isEmpty()) {
            this.directoryRelJpaDao.delete(directoryRefTbls);
        }
    }

    private void removeObjectFromGroupRelations(final String objectId) {
        List<GroupRefTbl> grpRefTbls = this.groupRelJpaDao.findByObjectId(objectId);
        if (!grpRefTbls.isEmpty()) {
            this.groupRelJpaDao.delete(grpRefTbls);
        }
    }

    private void removeObjectFromAdminMenuConfig(final String objectId) {
        List<AdminMenuConfigTbl> adminMenuConfTbls = this.adminMenuConfigJpaDao.findByObjectId(objectId);
        if (!adminMenuConfTbls.isEmpty()) {
            this.adminMenuConfigJpaDao.delete(adminMenuConfTbls);
        }
    }

    /**
     *
     * @param servletRequest
     * @param servletResponse
     * @param o
     * @param excptn
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest servletRequest,
            HttpServletResponse servletResponse, Object o,
            Exception excptn) throws Exception {
        LOG.info(">>>>>>>>>>>>>Inside afterCompletion<<<<<<<<<<<<<<<");
    }

    /**
     *
     * @param userId
     * @param requestPath
     * @return message
     * @throws Exception
     */
    private String formAuthorizationResponse(final String userId,
            final String requestPath) throws Exception {
        final AuthorizationResponse ar = new AuthorizationResponse();
        final ObjectMapper objectMapper = new ObjectMapper();
        ar.setPath(requestPath);
        ar.setUserId(userId);
        ar.setMessage(userId + " Not Authorized to access " + requestPath);
        return objectMapper.writeValueAsString(ar);
    }

    private String formTktNotValidResponse(final String username,
            final String message) throws Exception {
        AuthResponse ar = new AuthResponse();
        ObjectMapper objectMapper = new ObjectMapper();
        ar.setMessage(message);
        ar.setUsername(username);
        return objectMapper.writeValueAsString(ar);
    }

    private boolean checkTkt(String tkt) {
        boolean isTktValid = true;
        UserTkt userTkt = this.userTktJpaDao.findByTkt(tkt);
        if (null == userTkt) {
            isTktValid = false;
        }
        return isTktValid;
    }

    /**
     *
     * @param requestPath
     * @return boolean
     */
    private boolean isExclusion(final String requestPath) {
        return permissionMgr.isExclusionApi(requestPath);
    }

    /**
     *
     * @param requestPath
     * @return String
     */
    private String parseRequestPath(final String requestPath) {
        String newRequestPath = null;
        LOG.info("requestPath={}", requestPath);
        final String[] pathArray = requestPath.split("/");
        final StringJoiner joinPath = new StringJoiner("/", "/", "");
        joinPath.add(pathArray[1]).add(pathArray[2]);
        List<String> result = permissionFilter.getRelationApiMapValue(joinPath.toString());
        LOG.info("result={}", result);
        if (result.isEmpty() && pathArray.length > 3) {
            joinPath.add(pathArray[3]);
            result = permissionFilter.getRelationApiMapValue(joinPath.toString());
        }
        if (result.size() > 0) {
            newRequestPath = result.get(0);
        }
        LOG.info("newRequestPath={}", newRequestPath);
        return newRequestPath;
    }
}
