/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.interceptor;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dhana
 */
@Component
public class AdminHistoryBaseObjectAuditor implements HandlerInterceptor {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminHistoryBaseObjectAuditor.class);

    private final List<String> baseObjectAuditPath;

    public AdminHistoryBaseObjectAuditor() {
        this.baseObjectAuditPath = new ArrayList<>();
        this.baseObjectAuditPath.add("/project/save");
//        this.baseObjectAuditPath.add("/site/find");
    }

    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse hsr1, 
            Object o) throws Exception {
        LOG.info(">>> AdminHistoryBaseObjectAuditor > preHandle < <<<");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse, Object o, 
            ModelAndView mav) throws Exception {
        LOG.info(">>> AdminHistoryBaseObjectAuditor > postHandle < <<<");
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse, Object o,
            Exception excptn) throws Exception {
        LOG.info(">>> AdminHistoryBaseObjectAuditor > afterCompletion < <<<");

        String requestPath = httpServletRequest.getServletPath();
        LOG.info("requestPath is {}", requestPath);

        if (this.baseObjectAuditPath.stream().anyMatch((audiPath)
                -> (requestPath.contains(audiPath)))) {
            this.baseObjAudit(requestPath, httpServletRequest, 
                    httpServletResponse);

        }
    }

    private void baseObjAudit(String requestPath,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse) {
        LOG.debug("baseObjAudit requestPath is {}", requestPath);
    }

}
