/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient.client.language;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.vo.language.Language;
import com.magna.xmbackend.vo.language.LanguageResponse;
import com.magna.xmbackend.vo.language.LingualResponse;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dhana
 */
public class LanguageInvoker {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI
            = "http://localhost:8888";

    public LanguageInvoker() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("language");
    }

    public void getAllLanguages() throws ClientErrorException {
        Response response = webTarget.path("findAll")
                .request().header("Content-Type", MediaType.APPLICATION_JSON_TYPE)
                .get();
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            String lingualResponse = response.readEntity(String.class);
            
//            Iterable<LanguagesTbl> languagesTbls = lingualResponse.getLanguagesTbls();
            System.out.println("languagesTbls::" + lingualResponse);

        } else {
            System.out.println("status::" + status);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("> Main");
        LanguageInvoker invoker = new LanguageInvoker();
        invoker.getAllLanguages();
        System.out.println("< Main");
    }

}
