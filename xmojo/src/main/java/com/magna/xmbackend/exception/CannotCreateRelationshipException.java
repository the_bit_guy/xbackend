package com.magna.xmbackend.exception;

import java.util.Map;

/**
 *
 * @author dhana
 */
public class CannotCreateRelationshipException extends RuntimeException {

    private String errCode;
    private String[] param;
    private Map<String, String[]> paramMap;

    public CannotCreateRelationshipException() {
        super();
    }

    /**
     *
     * @param message
     * @param errCode
     */
    public CannotCreateRelationshipException(String message, String errCode) {
        super(message);
        this.errCode = errCode;
    }

    /**
     *
     * @param message
     * @param errCode
     * @param param
     */
    public CannotCreateRelationshipException(String message, String errCode, String[] param) {
        super(message);
        this.errCode = errCode;
        this.param = param;
    }

    /**
     *
     * @param message
     * @param errCode
     * @param paramMap
     */
    public CannotCreateRelationshipException(String message, String errCode,
            Map<String, String[]> paramMap) {
        super(message);
        this.errCode = errCode;
        this.paramMap = paramMap;
    }

    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /**
     *
     * @return Message
     */
    @Override
    public String getMessage() {
        return super.getMessage() + " with ErrorCode :" + this.errCode;
    }

    /**
     *
     * @return errCode
     */
    public String getErrCode() {
        return this.errCode;
    }

    /**
     * @return the param
     */
    public String[] getParam() {
        return param;
    }

    /**
     * @return the paramMap
     */
    public Map<String, String[]> getParamMap() {
        return paramMap;
    }
}
