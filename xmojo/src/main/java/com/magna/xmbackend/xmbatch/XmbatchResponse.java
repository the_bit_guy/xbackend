/**
 * 
 */
package com.magna.xmbackend.xmbatch;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class XmbatchResponse {
	
	private List<Map<String, Object>> queryResultSet;
	
	public XmbatchResponse() {
	}

	/**
	 * @param queryResultSet
	 */
	public XmbatchResponse(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}

	/**
	 * @return the queryResultSet
	 */
	public List<Map<String, Object>> getQueryResultSet() {
		return queryResultSet;
	}

	/**
	 * @param queryResultSet the queryResultSet to set
	 */
	public void setQueryResultSet(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}
	
}
