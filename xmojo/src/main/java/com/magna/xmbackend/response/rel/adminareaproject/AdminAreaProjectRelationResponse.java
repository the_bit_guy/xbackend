/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareaproject;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaProjectRelationResponse {
	
	 	private String adminAreaProjectRelId;
	    private String status;
	    private ProjectsTbl projectId;
	    private SiteAdminAreaRelTbl siteAdminAreaRelId;
	    
	    public AdminAreaProjectRelationResponse() {
		}

		/**
		 * @param adminAreaProjectRelId
		 * @param status
		 * @param projectId
		 * @param siteAdminAreaRelId
		 */
		public AdminAreaProjectRelationResponse(String adminAreaProjectRelId, String status, ProjectsTbl projectId,
				SiteAdminAreaRelTbl siteAdminAreaRelId) {
			this.adminAreaProjectRelId = adminAreaProjectRelId;
			this.status = status;
			this.projectId = projectId;
			this.siteAdminAreaRelId = siteAdminAreaRelId;
		}

		/**
		 * @return the adminAreaProjectRelId
		 */
		public String getAdminAreaProjectRelId() {
			return adminAreaProjectRelId;
		}

		/**
		 * @param adminAreaProjectRelId the adminAreaProjectRelId to set
		 */
		public void setAdminAreaProjectRelId(String adminAreaProjectRelId) {
			this.adminAreaProjectRelId = adminAreaProjectRelId;
		}

		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}

		/**
		 * @param status the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}

		/**
		 * @return the projectId
		 */
		public ProjectsTbl getProjectId() {
			return projectId;
		}

		/**
		 * @param projectId the projectId to set
		 */
		public void setProjectId(ProjectsTbl projectId) {
			this.projectId = projectId;
		}

		/**
		 * @return the siteAdminAreaRelId
		 */
		public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
			return siteAdminAreaRelId;
		}

		/**
		 * @param siteAdminAreaRelId the siteAdminAreaRelId to set
		 */
		public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
			this.siteAdminAreaRelId = siteAdminAreaRelId;
		}
	    
	    
}
