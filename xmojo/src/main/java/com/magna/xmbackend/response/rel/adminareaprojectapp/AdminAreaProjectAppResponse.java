package com.magna.xmbackend.response.rel.adminareaprojectapp;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;

/**
 * 
 * @author shashwat.anand
 *
 */
public class AdminAreaProjectAppResponse {
	
	private AdminAreaProjAppRelTbl adminAreaProjAppRelTbl;
	private AdminAreasTbl adminAreasTbl;
	
	public AdminAreaProjectAppResponse() {
	}
	
	public AdminAreaProjectAppResponse(AdminAreaProjAppRelTbl adminAreaProjAppRelTbl, AdminAreasTbl adminAreasTbl) {
		super();
		this.adminAreaProjAppRelTbl = adminAreaProjAppRelTbl;
		this.adminAreasTbl = adminAreasTbl;
	}
	/**
	 * @return the adminAreaProjAppRelTbl
	 */
	public AdminAreaProjAppRelTbl getAdminAreaProjAppRelTbl() {
		return adminAreaProjAppRelTbl;
	}
	/**
	 * @param adminAreaProjAppRelTbl the adminAreaProjAppRelTbl to set
	 */
	public void setAdminAreaProjAppRelTbl(AdminAreaProjAppRelTbl adminAreaProjAppRelTbl) {
		this.adminAreaProjAppRelTbl = adminAreaProjAppRelTbl;
	}
	/**
	 * @return the adminAreasTbl
	 */
	public AdminAreasTbl getAdminAreasTbl() {
		return adminAreasTbl;
	}
	/**
	 * @param adminAreasTbl the adminAreasTbl to set
	 */
	public void setAdminAreasTbl(AdminAreasTbl adminAreasTbl) {
		this.adminAreasTbl = adminAreasTbl;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminAreaProjectAppResponse [adminAreaProjAppRelTbl=" + adminAreaProjAppRelTbl + ", adminAreasTbl="
				+ adminAreasTbl + "]";
	}

}
