package com.magna.xmbackend.response.rel.adminareaproject;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;

/**
 * 
 * @author shashwat.anand
 *
 */
public class AdminAreaProjectResponse {
	private AdminAreaProjectRelTbl adminAreaProjectRelTbls;
	private AdminAreasTbl adminAreasTbls;

	public AdminAreaProjectResponse() {
    }

	public AdminAreaProjectResponse(AdminAreaProjectRelTbl adminAreaProjectRelTbls, AdminAreasTbl adminAreasTbls) {
		super();
		this.adminAreaProjectRelTbls = adminAreaProjectRelTbls;
		this.adminAreasTbls = adminAreasTbls;
	}

	/**
	 * @return the adminAreaProjectRelTbls
	 */
	public AdminAreaProjectRelTbl getAdminAreaProjectRelTbls() {
		return adminAreaProjectRelTbls;
	}

	/**
	 * @param adminAreaProjectRelTbls the adminAreaProjectRelTbls to set
	 */
	public void setAdminAreaProjectRelTbls(AdminAreaProjectRelTbl adminAreaProjectRelTbls) {
		this.adminAreaProjectRelTbls = adminAreaProjectRelTbls;
	}

	/**
	 * @return the adminAreasTbls
	 */
	public AdminAreasTbl getAdminAreasTbls() {
		return adminAreasTbls;
	}

	/**
	 * @param adminAreasTbls the adminAreasTbls to set
	 */
	public void setAdminAreasTbls(AdminAreasTbl adminAreasTbls) {
		this.adminAreasTbls = adminAreasTbls;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdminAreaProjectResponse [adminAreaProjectRelTbls=" + adminAreaProjectRelTbls + ", adminAreasTbls="
				+ adminAreasTbls + "]";
	}
}
