/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

/**
 *
 * @author dhana
 */
public class NotificationRequest {

    //list of userNames's
    private List<String> usersToNotify;

    private String notificationEvent;

    private String subject;

    private String message;

    private String includeRemarks;

    private String sendToAssignedUser;

    private String projectExpNoticePeriod;

    private CreationInfo creationInfo;

    //message, subject, will be taken from db tables
    /**
     * @return the usersToNotify
     */
    public List<String> getUsersToNotify() {
        return usersToNotify;
    }

    /**
     * @param usersToNotify the usersToNotify to set
     */
    public void setUsersToNotify(List<String> usersToNotify) {
        this.usersToNotify = usersToNotify;
    }

    /**
     * @return the notificationEvent
     */
    public String getNotificationEvent() {
        return notificationEvent;
    }

    /**
     * @param notificationEvent the notificationEvent to set
     */
    public void setNotificationEvent(String notificationEvent) {
        this.notificationEvent = notificationEvent;
    }

  

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the includeRemarks
     */
    public String getIncludeRemarks() {
        return includeRemarks;
    }

    /**
     * @param includeRemarks the includeRemarks to set
     */
    public void setIncludeRemarks(String includeRemarks) {
        this.includeRemarks = includeRemarks;
    }

    /**
     * @return the sendToAssignedUser
     */
    public String getSendToAssignedUser() {
        return sendToAssignedUser;
    }

    /**
     * @param sendToAssignedUser the sendToAssignedUser to set
     */
    public void setSendToAssignedUser(String sendToAssignedUser) {
        this.sendToAssignedUser = sendToAssignedUser;
    }

    /**
     * @return the projectExpNoticePeriod
     */
    public String getProjectExpNoticePeriod() {
        return projectExpNoticePeriod;
    }

    /**
     * @param projectExpNoticePeriod the projectExpNoticePeriod to set
     */
    public void setProjectExpNoticePeriod(String projectExpNoticePeriod) {
        this.projectExpNoticePeriod = projectExpNoticePeriod;
    }

}
