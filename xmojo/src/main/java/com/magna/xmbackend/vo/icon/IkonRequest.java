/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.icon;

/**
 *
 * @author dhana
 */
public class IkonRequest {

    private String id;
    private String iconName;
    private String iconType;

    public IkonRequest() {
    }

    public IkonRequest(String id, String iconName, String iconType) {
        this.id = id;
        this.iconName = iconName;
        this.iconType = iconType;
    }

    /**
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the iconName
     */
    public final String getIconName() {
        return iconName;
    }

    /**
     * @param iconName the iconName to set
     */
    public final void setIconName(final String iconName) {
        this.iconName = iconName;
    }

    /**
     * @return the iconType
     */
    public final String getIconType() {
        return iconType;
    }

    /**
     * @param iconType the iconType to set
     */
    public final void setIconType(final String iconType) {
        this.iconType = iconType;
    }

}
