package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class AdminAreaStartAppRelRequest {

    private String siteAdminAreaRelId;
    private String startAppId;
    private String status;

    public AdminAreaStartAppRelRequest() {
    }

    public AdminAreaStartAppRelRequest(
            String siteAdminAreaRelId, String startAppId, String status) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.startAppId = startAppId;
        this.status = status;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public final String getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public final void setSiteAdminAreaRelId(final String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the startAppId
     */
    public final String getStartAppId() {
        return startAppId;
    }

    /**
     * @param startAppId the startAppId to set
     */
    public final void setStartAppId(final String startAppId) {
        this.startAppId = startAppId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("AdminAreaStartAppRelRequest{")
                .append("siteAdminAreaRelId=").append(getSiteAdminAreaRelId())
                .append(", startAppId=").append(getStartAppId())
                .append(", status=").append(getStatus())
                .append("}").toString();
    }

}
