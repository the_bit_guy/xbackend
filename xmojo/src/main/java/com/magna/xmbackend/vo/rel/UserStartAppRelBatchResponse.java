package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.UserStartAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class UserStartAppRelBatchResponse {

    private List<UserStartAppRelTbl> userStartAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserStartAppRelBatchResponse() {
    }

    /**
     *
     * @param userStartAppRelTbls
     * @param statusMaps
     */
    public UserStartAppRelBatchResponse(List<UserStartAppRelTbl> userStartAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.userStartAppRelTbls = userStartAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the userStartAppRelTbls
     */
    public final List<UserStartAppRelTbl> getUserStartectAppRelTbls() {
        return userStartAppRelTbls;
    }

    /**
     * @param userStartAppRelTbls the userStartAppRelTbls to set
     */
    public final void setUserStartectAppRelTbls(final List<UserStartAppRelTbl> userStartAppRelTbls) {
        this.userStartAppRelTbls = userStartAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
