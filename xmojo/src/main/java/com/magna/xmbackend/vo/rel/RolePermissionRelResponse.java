package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.RolePermissionRelTbl;

/**
 *
 * @author vijay
 */
public class RolePermissionRelResponse {

    private Iterable<RolePermissionRelTbl> rolePermissionRelTbls;

    public RolePermissionRelResponse() {
    }

    /**
     *
     * @param rolePermissionRelTbl
     */
    public RolePermissionRelResponse(Iterable<RolePermissionRelTbl> rolePermissionRelTbl) {
        this.rolePermissionRelTbls = rolePermissionRelTbl;
    }

    /**
     * @return the rolePermissionRelTbls
     */
    public final Iterable<RolePermissionRelTbl> getRolePermissionRelTbls() {
        return rolePermissionRelTbls;
    }

    /**
     * @param rolePermissionRelTbls the rolePermissionRelTbls to set
     */
    public final void setRolePermissionRelTbls(final Iterable<RolePermissionRelTbl> rolePermissionRelTbls) {
        this.rolePermissionRelTbls = rolePermissionRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("RolePermissionRelResponse{")
                .append("rolePermissionRelTbls=").append(getRolePermissionRelTbls())
                .append("}").toString();
    }
}
