package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class UserProjectAppRelBatchRequest {

    private List<UserProjectAppRelRequest> userProjectAppRelRequests;

    public UserProjectAppRelBatchRequest() {
    }

    /**
     *
     * @param userProjectAppRelRequests
     */
    public UserProjectAppRelBatchRequest(List<UserProjectAppRelRequest> userProjectAppRelRequests) {
        this.userProjectAppRelRequests = userProjectAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<UserProjectAppRelRequest> getUserProjectAppRelRequests() {
        return userProjectAppRelRequests;
    }

    /**
     *
     * @param userProjectAppRelRequests
     */
    public final void setUserProjectAppRelRequests(final List<UserProjectAppRelRequest> userProjectAppRelRequests) {
        this.userProjectAppRelRequests = userProjectAppRelRequests;
    }

}
