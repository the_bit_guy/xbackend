package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author dhana
 */
public class SiteAdminAreaRelBatchRequest {

    private List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests;

    public SiteAdminAreaRelBatchRequest() {
    }

    /**
     *
     * @param siteAdminAreaRelRequests
     */
    public SiteAdminAreaRelBatchRequest(List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests) {
        this.siteAdminAreaRelRequests = siteAdminAreaRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<SiteAdminAreaRelRequest> getSiteAdminAreaRelRequests() {
        return siteAdminAreaRelRequests;
    }

    /**
     *
     * @param siteAdminAreaRelRequests
     */
    public final void setSiteAdminAreaRelRequests(final List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests) {
        this.siteAdminAreaRelRequests = siteAdminAreaRelRequests;
    }

}
