package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class UserStartAppRelRequest {

    private String userId;
    private String startAppId;
    private String status;

    public UserStartAppRelRequest() {
    }

    public UserStartAppRelRequest(String userId, 
            String startAppId, String status) {
        this.userId = userId;
        this.startAppId = startAppId;
        this.status = status;
    }

    /**
     * @return the userId
     */
    public final String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public final void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the startAppId
     */
    public final String getStartAppId() {
        return startAppId;
    }

    /**
     * @param startAppId the startAppId to set
     */
    public final void setStartAppId(final String startAppId) {
        this.startAppId = startAppId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("UserStartAppRelRequest{")
                .append("userId=").append(getUserId())
                .append(", startAppId=").append(getStartAppId())
                .append(", status=").append(getStatus())
                .append("}").toString();
    }

}
