package com.magna.xmbackend.vo;

import java.util.List;
import java.util.Map;

public class XMBatchVO {


	List<Map<String, Object>> resultSet;
	
	public XMBatchVO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param resultSet
	 */
	public XMBatchVO(List<Map<String, Object>> resultSet) {
		this.resultSet = resultSet;
	}

	public List<Map<String, Object>> getResultSet() {
		return resultSet;
	}

	public void setResultSet(List<Map<String, Object>> resultSet) {
		this.resultSet = resultSet;
	}



	
	
	
}
