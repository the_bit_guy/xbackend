package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class UserStartAppRelBatchRequest {

    private List<UserStartAppRelRequest> userStartAppRelRequests;

    public UserStartAppRelBatchRequest() {
    }

    /**
     *
     * @param userStartAppRelRequests
     */
    public UserStartAppRelBatchRequest(List<UserStartAppRelRequest> userStartAppRelRequests) {
        this.userStartAppRelRequests = userStartAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<UserStartAppRelRequest> getUserStartAppRelRequests() {
        return userStartAppRelRequests;
    }

    /**
     *
     * @param userStartAppRelRequests
     */
    public final void setUserStartAppRelRequests(final List<UserStartAppRelRequest> userStartAppRelRequests) {
        this.userStartAppRelRequests = userStartAppRelRequests;
    }

}
