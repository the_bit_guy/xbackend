/**
 * 
 */
package com.magna.xmbackend.vo.userSettings;

import java.util.List;

import com.magna.xmbackend.entities.UserSettingsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserSettingsResponse {

	private List<UserSettingsTbl> userSettingsTbls;
	
	public UserSettingsResponse() {
	}

	public UserSettingsResponse(List<UserSettingsTbl> userSettingsTbls) {
		this.userSettingsTbls = userSettingsTbls;
	}

	/**
	 * @return the userSettingsTbls
	 */
	public List<UserSettingsTbl> getUserSettingsTbls() {
		return userSettingsTbls;
	}

	/**
	 * @param userSettingsTbls the userSettingsTbls to set
	 */
	public void setUserSettingsTbls(List<UserSettingsTbl> userSettingsTbls) {
		this.userSettingsTbls = userSettingsTbls;
	}
	
	
}
