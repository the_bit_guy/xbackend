/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum LdapAttribute implements Serializable {
	LDAP_CN("cn"), 
	LDAP_SAMACCOUNTNAME("sAMAccountName"), 
	LDAP_SN("sn"), 
	LDAP_TELEPHONENUMBER("telephoneNumber"), 
	LDAP_USERPRINCIPLENAME("userPrincipalName"), 
	LDAP_GIVENNAME("givenName"), 
	LDAP_MAIL("mail"),
	LDAP_DEPARTMENT("department"),
	LDAP_USERPRINCIPALNAME("userPrincipalName"),
	LDAP_USERACCOUNTCONTROL("userAccountControl");
	
	private String value;

	/**
	 * @param value
	 */
	private LdapAttribute(String value) {
		this.value = value;
	}
	
	public String toString() {
		return this.value;
	}
}
