package com.magna.xmbackend.vo.jpa.site;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaProjRel {

    private String adminAreaProjRelId;
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    private ProjectsTbl projectsTbl;
    private String status;

    public SiteAdminAreaRelIdWithAdminAreaProjRel() {
    }

    /**
     *
     * @param siteAdminAreaRelId
     * @param projectsTbl
     * @param adminAreaProjRelId
     */
    public SiteAdminAreaRelIdWithAdminAreaProjRel(SiteAdminAreaRelTbl siteAdminAreaRelId,
            ProjectsTbl projectsTbl, String adminAreaProjRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.projectsTbl = projectsTbl;
        this.adminAreaProjRelId = adminAreaProjRelId;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public final SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public final void setSiteAdminAreaRelId(final SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the projectsTbl
     */
    public final ProjectsTbl getProjectsTbl() {
        return projectsTbl;
    }

    /**
     * @param projectsTbl the projectsTbl to set
     */
    public final void setProjectsTbl(final ProjectsTbl projectsTbl) {
        this.projectsTbl = projectsTbl;
    }

    /**
     * @return the adminAreaProjRelId
     */
    public final String getAdminAreaProjRelId() {
        return adminAreaProjRelId;
    }

    /**
     * @param adminAreaProjRelId the adminAreaProjRelId to set
     */
    public final void setAdminAreaProjRelId(final String adminAreaProjRelId) {
        this.adminAreaProjRelId = adminAreaProjRelId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(String status) {
        this.status = status;
    }
}
