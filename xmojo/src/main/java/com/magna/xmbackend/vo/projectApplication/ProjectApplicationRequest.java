package com.magna.xmbackend.vo.projectApplication;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

public class ProjectApplicationRequest {

    private String id;
    private String iconId;
    private String name;
    private String description;
    private String baseAppId;
    private String isSingleton;
    private String isParent;

    private String position;
    private String status;

    private CreationInfo creationInfo;

    private List<ProjectApplicationTransulation> projectApplicationTransulations;

    public ProjectApplicationRequest() {
    }

    /**
     * Instantiates a new project application request.
     *
     * @param id the id
     * @param name the name
     * @param desc the desc
     * @param iconId the icon id
     * @param baseAppId the base app id
     * @param isParent the is parent
     * @param position the position
     * @param status the status
     * @param isSingleton the is singleton
     * @param creationInfo the creation info
     * @param projectApplicationTransulations the project application transulations
     */
    public ProjectApplicationRequest(String id, String name, String desc, String iconId, String baseAppId,
            String isParent, String position, String status, String isSingleton,
            CreationInfo creationInfo, List<ProjectApplicationTransulation> projectApplicationTransulations) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.iconId = iconId;
        this.baseAppId = baseAppId;
        this.isParent = isParent;
        this.position = position;
        this.status = status;
        this.isSingleton = isSingleton;
        this.creationInfo = creationInfo;
        this.projectApplicationTransulations = projectApplicationTransulations;
    }

    /**
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
     * @return the iconId
     */
    public final String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public final void setIconId(final String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the baseAppId
     */
    public final String getBaseAppId() {
        return baseAppId;
    }

    /**
     * @param baseAppId the baseAppId to set
     */
    public final void setBaseAppId(final String baseAppId) {
        this.baseAppId = baseAppId;
    }

    /**
     * @return the isParent
     */
    public final String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent the isParent to set
     */
    public final void setIsParent(final String isParent) {
        this.isParent = isParent;
    }

    /**
     * @return the position
     */
    public final String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public final void setPosition(final String position) {
        this.position = position;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the creationInfo
     */
    public final CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public final void setCreationInfo(final CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the projectApplicationTransulations
     */
    public final List<ProjectApplicationTransulation> getProjectApplicationTransulations() {
        return projectApplicationTransulations;
    }

    /**
     * @param projectApplicationTransulations the
     * projectApplicationTransulations to set
     */
    public final void setProjectApplicationTransulations(final List<ProjectApplicationTransulation> projectApplicationTransulations) {
        this.projectApplicationTransulations = projectApplicationTransulations;
    }

    /**
     * @return the isSingleton
     */
    public final String getIsSingleton() {
        return isSingleton;
    }

    /**
     * @param isSingleton the isSingleton to set
     */
    public final void setIsSingleton(final String isSingleton) {
        this.isSingleton = isSingleton;
    }

}
