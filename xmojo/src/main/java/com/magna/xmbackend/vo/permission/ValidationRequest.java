package com.magna.xmbackend.vo.permission;

/**
 *
 * @author Vijay
 */
public class ValidationRequest {

    private String userName;
    private String permissionName;
    private String permissionType;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the permissionName
     */
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * @param permissionName the permissionName to set
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    /**
     * @return the permissionType
     */
    public String getPermissionType() {
        return permissionType;
    }

    /**
     * @param permissionType the permissionType to set
     */
    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("ValidationRequest{")
                .append(" userName=").append(getUserName())
                .append(", permissionName=").append(getPermissionName())
                .append(", permissionType=").append(getPermissionType())
                .append("}").toString();
    }
}
