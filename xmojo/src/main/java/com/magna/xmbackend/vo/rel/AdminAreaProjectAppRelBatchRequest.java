package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectAppRelBatchRequest {

    private List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests;

    public AdminAreaProjectAppRelBatchRequest() {
    }

    /**
     *
     * @param adminAreaProjectAppRelRequests
     */
    public AdminAreaProjectAppRelBatchRequest(List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests) {
        this.adminAreaProjectAppRelRequests = adminAreaProjectAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<AdminAreaProjectAppRelRequest> getAdminAreaProjectAppRelRequests() {
        return adminAreaProjectAppRelRequests;
    }

    /**
     *
     * @param adminAreaProjectAppRelRequests
     */
    public final void setAdminAreaProjectAppRelRequests(final List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests) {
        this.adminAreaProjectAppRelRequests = adminAreaProjectAppRelRequests;
    }

}
