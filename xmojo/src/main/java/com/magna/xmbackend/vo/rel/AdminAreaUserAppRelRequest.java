package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class AdminAreaUserAppRelRequest {

	private String id;
    private String siteAdminAreaRelId;
    private String userAppId;
    private String status;
    private String relationType;

    public AdminAreaUserAppRelRequest() {
    }

    public AdminAreaUserAppRelRequest(String siteAdminAreaRelId, 
            String userAppId, String status, String relationType) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.userAppId = userAppId;
        this.status = status;
        this.relationType = relationType;
    }

    
    /**
	 * @param id
	 * @param siteAdminAreaRelId
	 * @param userAppId
	 * @param status
	 * @param relationType
	 */
	public AdminAreaUserAppRelRequest(String id, String siteAdminAreaRelId, String userAppId, String status,
			String relationType) {
		this.id = id;
		this.siteAdminAreaRelId = siteAdminAreaRelId;
		this.userAppId = userAppId;
		this.status = status;
		this.relationType = relationType;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
     * @return the siteAdminAreaRelId
     */
    public final String getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public final void setSiteAdminAreaRelId(final String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the userAppId
     */
    public final String getUserAppId() {
        return userAppId;
    }

    /**
     * @param userAppId the userAppId to set
     */
    public final void setUserAppId(final String userAppId) {
        this.userAppId = userAppId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the relationType
     */
    public final String getRelationType() {
        return relationType;
    }

    /**
     * @param relationType the relationType to set
     */
    public final void setRelationType(final String relationType) {
        this.relationType = relationType;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("AdminAreaUserAppRelRequest{")
                .append("siteAdminAreaRelId=").append(getSiteAdminAreaRelId())
                .append(", userAppId=").append(getUserAppId())
                .append(", status=").append(getStatus())
                .append(", relationType=").append(getRelationType())
                .append("}").toString();
    }

}
