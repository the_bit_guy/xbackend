package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class AdminAreaProjectAppRelBatchResponse {

    private List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaProjectAppRelBatchResponse() {
    }

    /**
     *
     * @param adminAreaProjAppRelTbls
     * @param statusMaps
     */
    public AdminAreaProjectAppRelBatchResponse(List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.adminAreaProjAppRelTbls = adminAreaProjAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the adminAreaProjAppRelTbls
     */
    public final List<AdminAreaProjAppRelTbl> getAdminAreaProjectAppRelTbls() {
        return adminAreaProjAppRelTbls;
    }

    /**
     * @param adminAreaProjAppRelTbls the adminAreaProjAppRelTbls to set
     */
    public final void setAdminAreaProjectAppRelTbls(final List<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls) {
        this.adminAreaProjAppRelTbls = adminAreaProjAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
