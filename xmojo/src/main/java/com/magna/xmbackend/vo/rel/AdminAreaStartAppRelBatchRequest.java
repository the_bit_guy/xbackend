package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class AdminAreaStartAppRelBatchRequest {

    private List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests;

    public AdminAreaStartAppRelBatchRequest() {
    }

    /**
     *
     * @param adminAreaStartAppRelRequests
     */
    public AdminAreaStartAppRelBatchRequest(List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests) {
        this.adminAreaStartAppRelRequests = adminAreaStartAppRelRequests;
    }

    /**
     *
     * @return List
     */
    public final List<AdminAreaStartAppRelRequest> getAdminAreaStartAppRelRequests() {
        return adminAreaStartAppRelRequests;
    }

    /**
     *
     * @param adminAreaStartAppRelRequests
     */
    public final void setAdminAreaStartAppRelRequests(final List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequests) {
        this.adminAreaStartAppRelRequests = adminAreaStartAppRelRequests;
    }

}
