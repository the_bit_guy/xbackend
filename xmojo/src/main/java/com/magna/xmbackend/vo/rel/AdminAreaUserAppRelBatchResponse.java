package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class AdminAreaUserAppRelBatchResponse {

    private List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaUserAppRelBatchResponse() {
    }

    /**
     *
     * @param adminAreaUserAppRelTbls
     * @param statusMaps
     */
    public AdminAreaUserAppRelBatchResponse(List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the adminAreaUserAppRelTbls
     */
    public final List<AdminAreaUserAppRelTbl> getAdminAreaUserAppRelTbls() {
        return adminAreaUserAppRelTbls;
    }

    /**
     * @param adminAreaUserAppRelTbls the adminAreaStartAppRelTbls to set
     */
    public final void setAdminAreaUserAppRelTbls(final List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
