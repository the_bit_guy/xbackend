/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.language;

import java.io.Serializable;
import java.util.List;

public class LanguageResponse implements Serializable {

    private List<Language> languages;

    public LanguageResponse() {
    }

    public LanguageResponse(List<Language> languages) {
        this.languages = languages;
    }

    /**
     * @return the languages
     */
    public List<Language> getLanguages() {
        return languages;
    }

    /**
     * @param languages the languages to set
     */
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

}
