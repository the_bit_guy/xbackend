package com.magna.xmbackend.vo.icon;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.XMObject;
import java.io.Serializable;

public class Icon extends BaseObject implements Serializable {

    private String iconName;

    public Icon() {
    }

    /**
     *
     * @param iconName
     */
    public Icon(String iconName) {
        this.iconName = iconName;
    }

    /**
     *
     * @param iconName
     * @param xMObject
     */
    public Icon(final String iconName, final XMObject xMObject) {
        super(xMObject);
        this.iconName = iconName;
    }

    /**
     * @return the iconName
     */
    public final String getIconName() {
        return iconName;
    }

    /**
     * @param iconName the iconName to set
     */
    public final void setIconName(final String iconName) {
        this.iconName = iconName;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuffer().append("Icon{")
                .append("iconName=").append(iconName)
                .append(", XMObject=").append(getxMObject())
                .append("}").toString();
    }
}
