package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class RoleAdminAreaRelResponse {

    private Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls;
    private List<Map<String, String>> statusMaps;

    public RoleAdminAreaRelResponse() {
    }

    /**
	 * @param statusMaps
	 */
	public RoleAdminAreaRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param roleAdminAreaRelTbls
     * @param statusMaps
     */
    public RoleAdminAreaRelResponse(Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls,
            List<Map<String, String>> statusMaps) {
        this.roleAdminAreaRelTbls = roleAdminAreaRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the roleAdminAreaRelTbls
     */
    public final Iterable<RoleAdminAreaRelTbl> getRoleAdminAreaRelTbls() {
        return roleAdminAreaRelTbls;
    }

    /**
     * @param roleAdminAreaRelTbls the roleAdminAreaRelTbls to set
     */
    public final void setRoleAdminAreaRelTbls(final Iterable<RoleAdminAreaRelTbl> roleAdminAreaRelTbls) {
        this.roleAdminAreaRelTbls = roleAdminAreaRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("RoleAdminAreaRelResponse{")
                .append("roleAdminAreaRelTbls=").append(getRoleAdminAreaRelTbls())
                .append("statusMaps=").append(getStatusMap())
                .append("}").toString();
    }
}
