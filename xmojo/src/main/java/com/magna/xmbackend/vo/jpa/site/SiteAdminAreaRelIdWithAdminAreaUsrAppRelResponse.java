package com.magna.xmbackend.vo.jpa.site;

import java.util.List;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse {

    private List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaUsrAppRel;

    public SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse() {
    }

    /**
     *
     * @param saariwaauars
     */
    public SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse(List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> saariwaauars) {
        this.siteAdminAreaRelIdWithAdminAreaUsrAppRel = saariwaauars;
    }

    /**
     *
     * @return siteAdminAreaRelIdWithAdminAreaUsrAppRel
     */
    public final List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> getSiteAdminAreaRelIdWithAdminAreaUsrAppRel() {
        return siteAdminAreaRelIdWithAdminAreaUsrAppRel;
    }

    /**
     *
     * @param saariwaauars
     */
    public final void setSiteAdminAreaRelIdWithAdminAreaUsrAppRel(final List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> saariwaauars) {
        this.siteAdminAreaRelIdWithAdminAreaUsrAppRel = saariwaauars;
    }

}
