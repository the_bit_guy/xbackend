/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;

/**
 *
 * @author dhana
 */
public class NotificationResponse {

	private EmailNotificationConfigTbl emailNotificationConfigTbl;
    private Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls;
    private List<Map<String, String>> statusMaps;

    public NotificationResponse() {
    }

    public NotificationResponse(Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls) {
        this.emailNotifyToUserRelTbls = emailNotifyToUserRelTbls;
    }
    
    /**
	 * @param emailNotifyToUserRelTbls
	 * @param statusMaps
	 */
	public NotificationResponse(Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls,
			List<Map<String, String>> statusMaps) {
		this.emailNotifyToUserRelTbls = emailNotifyToUserRelTbls;
		this.statusMaps = statusMaps;
	}

	/**
	 * @param emailNotificationConfigTbl
	 * @param emailNotifyToUserRelTbls
	 */
	public NotificationResponse(EmailNotificationConfigTbl emailNotificationConfigTbl,
			Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls, List<Map<String, String>> statusMaps) {
		this.emailNotificationConfigTbl = emailNotificationConfigTbl;
		this.emailNotifyToUserRelTbls = emailNotifyToUserRelTbls;
		this.statusMaps = statusMaps;
	}
	
	/**
	 * @param emailNotificationConfigTbl
	 */
	public NotificationResponse(EmailNotificationConfigTbl emailNotificationConfigTbl) {
		this.emailNotificationConfigTbl = emailNotificationConfigTbl;
	}

	/**
	 * @return the emailNotificationConfigTbl
	 */
	public EmailNotificationConfigTbl getEmailNotificationConfigTbl() {
		return emailNotificationConfigTbl;
	}

	/**
	 * @param emailNotificationConfigTbl the emailNotificationConfigTbl to set
	 */
	public void setEmailNotificationConfigTbl(EmailNotificationConfigTbl emailNotificationConfigTbl) {
		this.emailNotificationConfigTbl = emailNotificationConfigTbl;
	}

	public Iterable<EmailNotifyToUserRelTbl> getEmailNotifyToUserRelTbls() {
        return emailNotifyToUserRelTbls;
    }

    public void setEmailNotifyToUserRelTbls(Iterable<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls) {
        this.emailNotifyToUserRelTbls = emailNotifyToUserRelTbls;
    }

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

    
}
