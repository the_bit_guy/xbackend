package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelIdWithProjectStartAppRelResponse {

    private List<AdminAreaProjectRelIdWithProjectStartAppRel> adminAreaProjectRelIdWithProjectStartAppRels;

    public AdminAreaProjectRelIdWithProjectStartAppRelResponse() {
    }

    /**
     *
     * @param aapriwpsars
     */
    public AdminAreaProjectRelIdWithProjectStartAppRelResponse(List<AdminAreaProjectRelIdWithProjectStartAppRel> aapriwpsars) {
        this.adminAreaProjectRelIdWithProjectStartAppRels = aapriwpsars;
    }

    /**
     *
     * @return adminAreaProjectRelIdWithProjectStartAppRels
     */
    public final List<AdminAreaProjectRelIdWithProjectStartAppRel> getAdminAreaProjectRelIdWithProjectStartAppRels() {
        return adminAreaProjectRelIdWithProjectStartAppRels;
    }

    /**
     *
     * @param aapriwpsars
     */
    public final void setAdminAreaProjectRelIdWithProjectStartAppRels(final List<AdminAreaProjectRelIdWithProjectStartAppRel> aapriwpsars) {
        this.adminAreaProjectRelIdWithProjectStartAppRels = aapriwpsars;
    }

}
