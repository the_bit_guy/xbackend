package com.magna.xmbackend.vo;

import java.util.Map;
import java.util.Objects;

public class Message {

    private Map<String, String> messageMap;

    public Message() {
    }

    /**
     *
     * @param messageMap
     */
    public Message(Map<String, String> messageMap) {
        this.messageMap = messageMap;
    }

    /**
     *
     * @param messageMap
     */
    public final void setMessageMap(final Map<String, String> messageMap) {
        this.messageMap = messageMap;
    }

    /**
     *
     * @return messageMap
     */
    public final Map<String, String> getMessageMap() {
        return messageMap;
    }

    /**
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (!Objects.equals(this.messageMap, other.messageMap)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuffer().append("Message{")
                .append("messageMap=").append(messageMap)
                .append("}").toString();
    }
}
