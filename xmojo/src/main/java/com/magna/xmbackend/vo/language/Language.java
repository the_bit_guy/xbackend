package com.magna.xmbackend.vo.language;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.XMObject;
import java.io.Serializable;

public class Language extends BaseObject implements Serializable {

    private String languageName; // English
    private String languageCode; // en

    public Language() {
    }

    /**
     *
     * @param languageName
     */
    public Language(String languageName) {
        this.languageName = languageName;
    }

    /**
     *
     * @param languageName
     * @param xMObject
     */
    public Language(String languageName, XMObject xMObject) {
        super(xMObject);
        this.languageName = languageName;
    }

    /**
     * @return the languageName
     */
    public final String getLanguageName() {
        return languageName;
    }

    /**
     * @param languageName the languageName to set
     */
    public final void setLanguageName(final String languageName) {
        this.languageName = languageName;
    }

    /**
     *
     * @return languageCode
     */
    public final String getLanguageCode() {
        return languageCode;
    }

    /**
     *
     * @param languageCode
     */
    public final void setLanguageCode(final String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuffer().append("Language{")
                .append(", languageName=")
                .append(languageName).append(", languageCode=")
                .append(languageCode).append(", xMObject=")
                .append(getxMObject()).append("}").toString();
    }

}
