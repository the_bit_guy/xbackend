/**
 * 
 */
package com.magna.xmbackend.vo.hotline;

/**
 * @author Bhabadyuti Bal
 *
 */
public class HotlineUserUserAppRelRequest {

	private String userId;
    private String adminAreaId;
    private String siteId;
    private String userAppId;
    private String userRelationType;
	
    public HotlineUserUserAppRelRequest() {
	}

	/**
	 * @param userId
	 * @param adminAreaId
	 * @param siteId
	 * @param userAppId
	 * @param userRelationType
	 */
	public HotlineUserUserAppRelRequest(String userId, String adminAreaId, String siteId, String userAppId,
			String userRelationType) {
		this.userId = userId;
		this.adminAreaId = adminAreaId;
		this.siteId = siteId;
		this.userAppId = userAppId;
		this.userRelationType = userRelationType;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the adminAreaId
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * @param adminAreaId the adminAreaId to set
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the userAppId
	 */
	public String getUserAppId() {
		return userAppId;
	}

	/**
	 * @param userAppId the userAppId to set
	 */
	public void setUserAppId(String userAppId) {
		this.userAppId = userAppId;
	}

	/**
	 * @return the userRelationType
	 */
	public String getUserRelationType() {
		return userRelationType;
	}

	/**
	 * @param userRelationType the userRelationType to set
	 */
	public void setUserRelationType(String userRelationType) {
		this.userRelationType = userRelationType;
	}

}
