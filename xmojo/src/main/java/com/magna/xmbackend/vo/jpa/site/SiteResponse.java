/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.jpa.site;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.SitesTbl;

/**
 *
 * @author dhana
 */
public class SiteResponse {

    private Iterable<SitesTbl> sitesTbls;
    private List<Map<String, String>> statusMaps;
    
        
    public SiteResponse() {
    }

        
    /**
	 * @param statusMaps
	 */
	public SiteResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}


	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	public SiteResponse(Iterable<SitesTbl> sitesTbls) {
        this.sitesTbls = sitesTbls;
    }

    public void setSitesTbls(Iterable<SitesTbl> sitesTbls) {
        this.sitesTbls = sitesTbls;
    }

    public Iterable<SitesTbl> getSitesTbls() {
        return sitesTbls;
    }

}
