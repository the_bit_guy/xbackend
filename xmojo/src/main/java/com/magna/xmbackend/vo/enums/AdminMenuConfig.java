/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum AdminMenuConfig implements Serializable{
	USER, USERAPPLICATION, PROJECTAPPLICATION
}
