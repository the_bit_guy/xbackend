package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectAppRelResponse {

    private Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaProjectAppRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public AdminAreaProjectAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param adminAreaProjectAppRelTbl
     */
    public AdminAreaProjectAppRelResponse(Iterable<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbl) {
        this.adminAreaProjAppRelTbls = adminAreaProjectAppRelTbl;
    }

    /**
     * @return the AdminAreaProjAppRelTbls
     */
    public Iterable<AdminAreaProjAppRelTbl> getAdminAreaProjAppRelTbls() {
        return adminAreaProjAppRelTbls;
    }

    /**
     * @param adminAreaProjAppRelTbls the adminAreaProjAppRelTbls to set
     */
    public void setAdminAreaProjAppRelTbls(Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls) {
        this.adminAreaProjAppRelTbls = adminAreaProjAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("AdminAreaProjectAppRelResponse{")
                .append("adminAreaProjectRelTbls=").append(adminAreaProjAppRelTbls)
                .append("}").toString();
    }
}
