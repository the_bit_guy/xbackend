package com.magna.xmbackend.vo;

public class Attribute {

    private String name;
    private String value;

    public Attribute() {
    }

    /**
     *
     * @param name
     * @param value
     */
    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public final String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public final void setValue(final String value) {
        this.value = value;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuffer().append("Attribute{")
                .append("name=").append(name).append(", value=")
                .append(value).append("}").toString();
    }
}
