package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class AdminAreaStartAppRelBatchResponse {

    private List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaStartAppRelBatchResponse() {
    }

    /**
     *
     * @param adminAreaStartAppRelTbls
     * @param statusMaps
     */
    public AdminAreaStartAppRelBatchResponse(List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.adminAreaStartAppRelTbls = adminAreaStartAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the adminAreaStartAppRelTbls
     */
    public final List<AdminAreaStartAppRelTbl> getAdminAreaStartAppRelTbls() {
        return adminAreaStartAppRelTbls;
    }

    /**
     * @param adminAreaStartAppRelTbls the adminAreaStartAppRelTbls to set
     */
    public final void setAdminAreaProjectAppRelTbls(final List<AdminAreaStartAppRelTbl> adminAreaStartAppRelTbls) {
        this.adminAreaStartAppRelTbls = adminAreaStartAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
