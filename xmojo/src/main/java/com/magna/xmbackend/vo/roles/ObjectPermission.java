package com.magna.xmbackend.vo.roles;

import java.io.Serializable;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.enums.PermissionType;

/**
 *
 * @author Admin
 */
public class ObjectPermission implements Serializable {

    private String rolePermissionRelationId;
    private String permissionId;
    private PermissionType permissionType;
    private CreationType creationType;

    /**
     * @return the permissionId
     */
    public String getPermissionId() {
        return permissionId;
    }

    /**
     * @param permissionId the permissionId to set
     */
    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    /**
     * @return the permissionType
     */
    public PermissionType getPermissionType() {
        return permissionType;
    }

    /**
     * @param permissionType the permissionType to set
     */
    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }

    /**
     * @return the creationType
     */
    public CreationType getCreationType() {
        return creationType;
    }

    /**
     * @param creationType the creationType to set
     */
    public void setCreationType(CreationType creationType) {
        this.creationType = creationType;
    }

    /**
     * @return the rolePermissionRelationId
     */
    public String getRolePermissionRelationId() {
        return rolePermissionRelationId;
    }

    /**
     * @param rolePermissionRelationId the rolePermissionRelationId to set
     */
    public void setRolePermissionRelationId(String rolePermissionRelationId) {
        this.rolePermissionRelationId = rolePermissionRelationId;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("ObjectPermission{")
                .append(", permissionType=").append(getPermissionType())
                .append(", rolePermissionRelationId=").append(getRolePermissionRelationId())
                .append(", permissionType=").append(getPermissionType())
                .append(", creationType=").append(getCreationType())
                .append("}").toString();
    }

}
