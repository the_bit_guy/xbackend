/**
 * 
 */
package com.magna.xmbackend.vo.group;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class GroupRelBatchRequest {

	List<GroupRelRequest> groupRelBatchRequest;
	
	public GroupRelBatchRequest() {
	}

	/**
	 * @param groupRelBatchRequest
	 */
	public GroupRelBatchRequest(List<GroupRelRequest> groupRelBatchRequest) {
		this.groupRelBatchRequest = groupRelBatchRequest;
	}

	/**
	 * @return the groupRelBatchRequest
	 */
	public List<GroupRelRequest> getGroupRelBatchRequest() {
		return groupRelBatchRequest;
	}

	/**
	 * @param groupRelBatchRequest the groupRelBatchRequest to set
	 */
	public void setGroupRelBatchRequest(List<GroupRelRequest> groupRelBatchRequest) {
		this.groupRelBatchRequest = groupRelBatchRequest;
	}
	
}
