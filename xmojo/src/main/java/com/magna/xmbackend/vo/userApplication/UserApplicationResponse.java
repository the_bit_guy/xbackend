/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.userApplication;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 *
 * @author dhana
 */
public class UserApplicationResponse {

    private Iterable<UserApplicationsTbl> uatsUserApplicationsTbls;
    private List<Map<String, String>> statusMaps;

    public UserApplicationResponse() {
    }

    
    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}


	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	/**
	 * @param statusMaps
	 */
	public UserApplicationResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	public UserApplicationResponse(Iterable<UserApplicationsTbl> uatsUserApplicationsTbls) {
        this.uatsUserApplicationsTbls = uatsUserApplicationsTbls;
    }

    public void setUatsUserApplicationsTbls(Iterable<UserApplicationsTbl> uatsUserApplicationsTbls) {
        this.uatsUserApplicationsTbls = uatsUserApplicationsTbls;
    }

    public Iterable<UserApplicationsTbl> getUatsUserApplicationsTbls() {
        return uatsUserApplicationsTbls;
    }

}
