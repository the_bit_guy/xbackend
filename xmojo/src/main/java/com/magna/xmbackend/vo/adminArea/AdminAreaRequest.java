/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.adminArea;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaRequest {

    private String id;
    private String iconId;
    private String name;
    private String status;
    private long singleTonAppTimeOut;
    private String hotlineContactNo;
    private String hotlineContactEmail;
    private CreationInfo creationInfo;

    private List<AdminAreaTranslation> adminAreaTranslations;

    public AdminAreaRequest() {
    }

    
    /**
     * Instantiates a new admin area request.
     *
     * @param id the id
     * @param iconId the icon id
     * @param status the status
     * @param name the name
     * @param singleTonAppTimeOut the single ton app time out
     * @param hotlineContactNo the hotline contact no
     * @param hotlineContactEmail the hotline contact email
     * @param creationInfo the creation info
     * @param adminAreaTranslations the admin area translations
     */
    public AdminAreaRequest(String id, String iconId, String status, String name, long singleTonAppTimeOut,
            String hotlineContactNo, String hotlineContactEmail,
            CreationInfo creationInfo, List<AdminAreaTranslation> adminAreaTranslations) {
        this.id = id;
        this.iconId = iconId;
        this.name = name;
        this.status = status;
        this.singleTonAppTimeOut = singleTonAppTimeOut;
        this.hotlineContactNo = hotlineContactNo;
        this.hotlineContactEmail = hotlineContactEmail;
        this.creationInfo = creationInfo;
        this.adminAreaTranslations = adminAreaTranslations;
    }

    /**
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
     * @return the iconId
     */
    public final String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public final void setIconId(final String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the creationInfo
     */
    public final CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public final void setCreationInfo(final CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the adminAreaTranslations
     */
    public final List<AdminAreaTranslation> getAdminAreaTranslations() {
        return adminAreaTranslations;
    }

    /**
     * @param adminAreaTranslations the adminAreaTranslations to set
     */
    public final void setAdminAreaTranslations(final List<AdminAreaTranslation> adminAreaTranslations) {
        this.adminAreaTranslations = adminAreaTranslations;
    }

    /**
     * @return the singleTonAppTimeOut
     */
    public final long getSingleTonAppTimeOut() {
        return singleTonAppTimeOut;
    }

    /**
     * @param singleTonAppTimeOut the singleTonAppTimeOut to set
     */
    public final void setSingleTonAppTimeOut(final long singleTonAppTimeOut) {
        this.singleTonAppTimeOut = singleTonAppTimeOut;
    }

    /**
     * @return the hotlineContactNo
     */
    public final String getHotlineContactNo() {
        return hotlineContactNo;
    }

    /**
     * @param hotlineContactNo the hotlineContactNo to set
     */
    public final void setHotlineContactNo(final String hotlineContactNo) {
        this.hotlineContactNo = hotlineContactNo;
    }

    /**
     * @return the hotlineContactEmail
     */
    public final String getHotlineContactEmail() {
        return hotlineContactEmail;
    }

    /**
     * @param hotlineContactEmail the hotlineContactEmail to set
     */
    public final void setHotlineContactEmail(final String hotlineContactEmail) {
        this.hotlineContactEmail = hotlineContactEmail;
    }

}
