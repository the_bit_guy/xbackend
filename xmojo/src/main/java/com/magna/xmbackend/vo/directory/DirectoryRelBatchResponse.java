/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.DirectoryRefTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryRelBatchResponse {
	
	private List<DirectoryRefTbl> directoryRefTbls;
	private List<Map<String, String>> statusMaps;

	public DirectoryRelBatchResponse() {
	}
	
	/**
	 * @param directoryRefTbls
	 * @param statusMaps
	 */
	public DirectoryRelBatchResponse(List<DirectoryRefTbl> directoryRefTbls, List<Map<String, String>> statusMaps) {
		this.directoryRefTbls = directoryRefTbls;
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the directoryRefTbls
	 */
	public List<DirectoryRefTbl> getDirectoryRefTbls() {
		return directoryRefTbls;
	}

	/**
	 * @param directoryRefTbls the directoryRefTbls to set
	 */
	public void setDirectoryRefTbls(List<DirectoryRefTbl> directoryRefTbls) {
		this.directoryRefTbls = directoryRefTbls;
	}
	
	

}
