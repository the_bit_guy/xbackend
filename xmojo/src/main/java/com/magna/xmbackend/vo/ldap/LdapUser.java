/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.ldap;

import java.io.Serializable;

/**
 *
 * @author dhana
 */
public class LdapUser implements Serializable {

    /**
     * @return the userAccountControl
     */
    public String getUserAccountControl() {
        return userAccountControl;
    }

    /**
     * @param userAccountControl the userAccountControl to set
     */
    public void setUserAccountControl(String userAccountControl) {
        this.userAccountControl = userAccountControl;
    }

    /**
     *
     */
    private static final long serialVersionUID = 9081527761576640803L;

    private String sAMAccountName;
    private String cn;
    private String sn;
    private String telephoneNumber;
    private String email;
    private String givenName;
    private String department;
    private String userPrincipalName;
    private String userAccountControl;

    /**
     * @return the cn
     */
    public synchronized final String getCn() {
        return cn;
    }

    /**
     * @param cn the cn to set
     */
    public synchronized final void setCn(String cn) {
        this.cn = cn;
    }

    /**
	 * @return the sAMAccountName
	 */
	public synchronized final String getsAMAccountName() {
		return sAMAccountName;
	}

	/**
	 * @param sAMAccountName the sAMAccountName to set
	 */
	public synchronized final void setsAMAccountName(String sAMAccountName) {
		this.sAMAccountName = sAMAccountName;
	}

	/**
     * @return the sn
     */
    public synchronized final String getSn() {
        return sn;
    }

    /**
     * @param sn the sn to set
     */
    public synchronized final void setSn(String sn) {
        this.sn = sn;
    }





	/**
	 * @return the telephoneNumber
	 */
	public synchronized final String getTelephoneNumber() {
		return telephoneNumber;
	}

	/**
	 * @param telephoneNumber the telephoneNumber to set
	 */
	public synchronized final void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	
	/**
	 * @return the email
	 */
	public synchronized final String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public synchronized final void setEmail(String email) {
		this.email = email;
	}

	
	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("User [");
        if (sAMAccountName != null) {
            builder.append("sAMAccountName=");
            builder.append(sAMAccountName);
            builder.append(", ");
        }
        if (cn != null) {
            builder.append("cn=");
            builder.append(cn);
            builder.append(", ");
        }
        if (sn != null) {
            builder.append("sn=");
            builder.append(sn);
            builder.append(", ");
        }
        if (telephoneNumber != null) {
            builder.append("telephoneNumber=");
            builder.append(telephoneNumber);
        }
        if (email != null) {
            builder.append("email=");
            builder.append(email);
        }
        if (givenName != null) {
            builder.append("givenName=");
            builder.append(givenName);
        }
        if (department != null) {
            builder.append("department=");
            builder.append(department);
        }
        if (userPrincipalName != null) {
            builder.append("userPrincipalName=");
            builder.append(userPrincipalName);
        }
        builder.append("]");
        return builder.toString();
    }

	/**
	 * @return the department
	 */
	public synchronized String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public synchronized void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the userPrincipalName
	 */
	public synchronized String getUserPrincipalName() {
		return userPrincipalName;
	}

	/**
	 * @param userPrincipalName the userPrincipalName to set
	 */
	public synchronized void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

}
