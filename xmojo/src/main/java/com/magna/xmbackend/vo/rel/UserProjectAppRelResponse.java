package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserProjAppRelTbl;

/**
 *
 * @author vijay
 */
public class UserProjectAppRelResponse {

    private Iterable<UserProjAppRelTbl> userProjAppRelTbls;
    private List<Map<String, String>> statusMaps;

    /**
	 * @param statusMaps
	 */
	public UserProjectAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public UserProjectAppRelResponse() {
    }

    /**
     *
     * @param userProjectAppRelTbl
     */
    public UserProjectAppRelResponse(Iterable<UserProjAppRelTbl> userProjectAppRelTbl) {
        this.userProjAppRelTbls = userProjectAppRelTbl;
    }

    /**
     * @return the AdminAreaProjAppRelTbls
     */
    public final Iterable<UserProjAppRelTbl> getUserProjAppRelTbls() {
        return userProjAppRelTbls;
    }

    /**
     * @param userProjAppRelTbls the adminAreaProjAppRelTbls to set
     */
    public final void setUserProjAppRelTbls(final Iterable<UserProjAppRelTbl> userProjAppRelTbls) {
        this.userProjAppRelTbls = userProjAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("UserProjectAppRelResponse{")
                .append("userProjAppRelTbls=").append(userProjAppRelTbls)
                .append("}").toString();
    }
}
