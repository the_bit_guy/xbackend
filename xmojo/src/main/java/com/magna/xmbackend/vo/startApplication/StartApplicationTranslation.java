/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.startApplication;

import com.magna.xmbackend.vo.CreationInfo;

/**
 *
 * @author dhana
 */
public class StartApplicationTranslation {

    private String id;
    private String description;
    private String remarks;
    private String languageCode;
    private CreationInfo creationInfo;

    public StartApplicationTranslation() {
    }

    /**
     *
     * @param id
     * @param name
     * @param description
     * @param remarks
     * @param languageCode
     * @param creationInfo
     */
    public StartApplicationTranslation(String id, String name,
            String description, String remarks, String languageCode,
            CreationInfo creationInfo) {
        this.id = id;
        this.description = description;
        this.remarks = remarks;
        this.languageCode = languageCode;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * @param languageCode the languageCode to set
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

}
