/**
 * 
 */
package com.magna.xmbackend.vo.ldap;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LdapUsersResponse {

	private List<LdapUser> ldapUsers;
	private List<Map<String, String>> statusMaps;
	
	public LdapUsersResponse() {
	}

	/**
	 * @param ldapUsers
	 * @param statusMaps
	 */
	public LdapUsersResponse(List<LdapUser> ldapUsers, List<Map<String, String>> statusMaps) {
		this.ldapUsers = ldapUsers;
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the ldapUsers
	 */
	public List<LdapUser> getLdapUsers() {
		return ldapUsers;
	}

	/**
	 * @param ldapUsers the ldapUsers to set
	 */
	public void setLdapUsers(List<LdapUser> ldapUsers) {
		this.ldapUsers = ldapUsers;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}
	
}
