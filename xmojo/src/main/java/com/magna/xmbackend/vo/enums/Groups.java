/**
 * 
 */
package com.magna.xmbackend.vo.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum Groups {
	USER, PROJECT, USERAPPLICATION, PROJECTAPPLICATION
}
