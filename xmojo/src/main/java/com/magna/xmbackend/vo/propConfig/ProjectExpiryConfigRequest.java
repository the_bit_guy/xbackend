/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.EnumMap;

import com.magna.xmbackend.vo.enums.ProjectExpiryKey;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ProjectExpiryConfigRequest {
	
	private EnumMap<ProjectExpiryKey, String> projectExpiryKeyValue;
	
	public ProjectExpiryConfigRequest() {
	}

	/**
	 * @param projectExpiryKeyValue
	 */
	public ProjectExpiryConfigRequest(EnumMap<ProjectExpiryKey, String> projectExpiryKeyValue) {
		this.projectExpiryKeyValue = projectExpiryKeyValue;
	}

	/**
	 * @return the projectExpiryKeyValue
	 */
	public EnumMap<ProjectExpiryKey, String> getProjectExpiryKeyValue() {
		return projectExpiryKeyValue;
	}

	/**
	 * @param projectExpiryKeyValue the projectExpiryKeyValue to set
	 */
	public void setProjectExpiryKeyValue(EnumMap<ProjectExpiryKey, String> projectExpiryKeyValue) {
		this.projectExpiryKeyValue = projectExpiryKeyValue;
	}
	
	
	
}
