/**
 * 
 */
package com.magna.xmbackend.vo.userHistory;

import com.magna.xmbackend.entities.UserHistoryTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserHistoryResponse {

	private Iterable<UserHistoryTbl> userHistoryTbl;
	
	public UserHistoryResponse() {
	}

	/**
	 * @param userHistoryTbl
	 */
	public UserHistoryResponse(Iterable<UserHistoryTbl> userHistoryTbl) {
		this.userHistoryTbl = userHistoryTbl;
	}

	/**
	 * @return the userHistoryTbl
	 */
	public Iterable<UserHistoryTbl> getUserHistoryTbl() {
		return userHistoryTbl;
	}

	/**
	 * @param userHistoryTbl the userHistoryTbl to set
	 */
	public void setUserHistoryTbl(Iterable<UserHistoryTbl> userHistoryTbl) {
		this.userHistoryTbl = userHistoryTbl;
	}
	
	
	
	
}
