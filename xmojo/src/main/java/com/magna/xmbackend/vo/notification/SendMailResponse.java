/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class SendMailResponse {

    private String ack;
    private List<Map<String, String>> statusMaps;

    public SendMailResponse() {
    }
    
	public SendMailResponse(String ack, List<Map<String, String>> statusMaps) {
		this.ack = ack;
		this.statusMaps = statusMaps;
	}

	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public SendMailResponse(String ack) {
        this.ack = ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getAck() {
        return ack;
    }

}
