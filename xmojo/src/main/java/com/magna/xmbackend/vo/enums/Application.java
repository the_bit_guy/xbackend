package com.magna.xmbackend.vo.enums;

public enum Application {
	CAX_START_ADMIN("caxstartadmin"),
	CAX_START_MENU("caxstartmenu"),
	CAX_START_HOTLINE("caxstarthotline"),
	CAX_START_BATCH("caxstartbatch"),
	CAX_ADMIN_MENU("caxadminmenu");
	
	/**
	 * Instantiates a new Application.
	 *
	 * @param applicationName the String
	 */
	Application(String applicationName) {
		this.applicationName = applicationName;
	}

	/** The applicationName. */
	private String applicationName;

	/**
	 * @return the applicationName
	 */
	public String getApplicationName() {
		return applicationName;
	}

	public String toString() {
		return this.applicationName;
	}
	
	public static Application getApplication(String applicationName) {
        for (Application value : values()) {
            if (applicationName.equalsIgnoreCase(value.getApplicationName())) {
                return value;
            }
        }
        return null;
    }
}
