package com.magna.xmbackend.vo.roles;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RoleRequest implements Serializable {

    private String roleId;
    private String name;
    private String description;
    private List<ObjectPermission> objectPermissionList;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the objectPermissionList
     */
    public List<ObjectPermission> getObjectPermissionList() {
        return objectPermissionList;
    }

    /**
     * @param objectPermissionList the objectPermissionList to set
     */
    public void setObjectPermissionList(List<ObjectPermission> objectPermissionList) {
        this.objectPermissionList = objectPermissionList;
    }

    /**
     * @return the roleId
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("RoleRequest{")
                .append("name=").append(getName())
                .append(", description=").append(getDescription())
                .append(", objectPermissionList=")
                .append(getObjectPermissionList()).append("}").toString();
    }

}
