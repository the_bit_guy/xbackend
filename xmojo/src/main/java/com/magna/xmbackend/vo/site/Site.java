package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.icon.Icon;
import java.io.Serializable;

public class Site extends BaseObject implements Serializable {

    private String status;
    private Icon icon;

    /**
     *
     * @return
     */
    public final String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public final Icon getIcon() {
        return icon;
    }

    /**
     *
     * @param icon
     */
    public final void setIcon(final Icon icon) {
        this.icon = icon;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("Site{")
                .append("status=").append(status)
                .append(", icon=").append(icon).append(", xMObject=")
                .append(getxMObject()).append("}").toString();
    }
}
