package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class UserProjectRelBatchResponse {

    private List<UserProjectRelTbl> userProjectRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserProjectRelBatchResponse() {
    }

    /**
     *
     * @param userProjectRelTbls
     * @param statusMaps
     */
    public UserProjectRelBatchResponse(List<UserProjectRelTbl> userProjectRelTbls,
            List<Map<String, String>> statusMaps) {
        this.userProjectRelTbls = userProjectRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the userProjectRelTbls
     */
    public final List<UserProjectRelTbl> getUserProjectRelTbls() {
        return userProjectRelTbls;
    }

    /**
     * @param userProjectRelTbls the userProjectRelTbls to set
     */
    public final void setUserProjectRelTbls(final List<UserProjectRelTbl> userProjectRelTbls) {
        this.userProjectRelTbls = userProjectRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
