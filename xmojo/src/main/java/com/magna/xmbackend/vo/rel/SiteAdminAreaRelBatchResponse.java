package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class SiteAdminAreaRelBatchResponse {

    private List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls;
    private List<Map<String, String>> statusMaps;

    public SiteAdminAreaRelBatchResponse() {
    }

    /**
     * 
     * @param siteAdminAreaRelTbls
     * @param statusMaps 
     */
    public SiteAdminAreaRelBatchResponse(List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls, 
            List<Map<String, String>> statusMaps) {
        this.siteAdminAreaRelTbls = siteAdminAreaRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the siteAdminAreaRelTbls
     */
    public final List<SiteAdminAreaRelTbl> getSiteAdminAreaRelTbls() {
        return siteAdminAreaRelTbls;
    }

    /**
     * @param siteAdminAreaRelTbls the siteAdminAreaRelTbls to set
     */
    public final void setSiteAdminAreaRelTbls(final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        this.siteAdminAreaRelTbls = siteAdminAreaRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
