/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

import java.util.Set;

import com.magna.xmbackend.entities.LiveMessageTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageStatusResponseWrapper {

	//List<LiveMessageStatusResponse> liveMsgStatusResponses;
	private Set<LiveMessageTbl> liveMessageTbls;
	
	public LiveMessageStatusResponseWrapper() {
	}

	/**
	 * @param liveMessageTbls
	 */
	public LiveMessageStatusResponseWrapper(Set<LiveMessageTbl> liveMessageTbls) {
		this.liveMessageTbls = liveMessageTbls;
	}

	/**
	 * @return the liveMessageTbls
	 */
	public Set<LiveMessageTbl> getLiveMessageTbls() {
		return liveMessageTbls;
	}

	/**
	 * @param liveMessageTbls the liveMessageTbls to set
	 */
	public void setLiveMessageTbls(Set<LiveMessageTbl> liveMessageTbls) {
		this.liveMessageTbls = liveMessageTbls;
	}

}
