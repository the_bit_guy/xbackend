/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.userAccountStatus;

import com.magna.xmbackend.entities.UserAccountStatusTbl;

/**
 *
 * @author dhana
 */
public class UserAccountStatusResponse {

    public UserAccountStatusResponse() {
    }

    public UserAccountStatusResponse(Iterable<UserAccountStatusTbl> userAccountStatusTbls) {
        this.userAccountStatusTbls = userAccountStatusTbls;
    }

    private Iterable<UserAccountStatusTbl> userAccountStatusTbls;

    /**
     * @return the userAccountStatusTbls
     */
    public Iterable<UserAccountStatusTbl> getUserAccountStatusTbls() {
        return userAccountStatusTbls;
    }

    /**
     * @param userAccountStatusTbls the userAccountStatusTbls to set
     */
    public void setUserAccountStatusTbls(Iterable<UserAccountStatusTbl> userAccountStatusTbls) {
        this.userAccountStatusTbls = userAccountStatusTbls;
    }

}
