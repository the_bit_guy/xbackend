/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryRelBatchRequest {

	private List<DirectoryRelRequest> directoryRelRequests;
	
	public DirectoryRelBatchRequest() {
	}

	
	/**
	 * @param directoryRelRequests
	 */
	public DirectoryRelBatchRequest(List<DirectoryRelRequest> directoryRelRequests) {
		this.directoryRelRequests = directoryRelRequests;
	}

	/**
	 * @return the directoryRelRequests
	 */
	public List<DirectoryRelRequest> getDirectoryRelRequests() {
		return directoryRelRequests;
	}

	/**
	 * @param directoryRelRequests the directoryRelRequests to set
	 */
	public void setDirectoryRelRequests(final List<DirectoryRelRequest> directoryRelRequests) {
		this.directoryRelRequests = directoryRelRequests;
	}
	
	
}
