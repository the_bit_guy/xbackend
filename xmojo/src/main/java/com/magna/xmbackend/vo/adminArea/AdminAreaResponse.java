package com.magna.xmbackend.vo.adminArea;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminAreasTbl;

/**
 * The Class AdminAreaResponse.
 */
public class AdminAreaResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Iterable<AdminAreasTbl> adminAreasTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaResponse() {
    }
    
    

    /**
	 * @param statusMaps
	 */
	public AdminAreaResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public AdminAreaResponse(Iterable<AdminAreasTbl> adminAreasTbls) {
        this.adminAreasTbls = adminAreasTbls;
    }

    public void setAdminAreasTbls(Iterable<AdminAreasTbl> adminAreasTbls) {
        this.adminAreasTbls = adminAreasTbls;
    }

    public Iterable<AdminAreasTbl> getAdminAreasTbls() {
        return adminAreasTbls;
    }

}
