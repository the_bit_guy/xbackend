package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelResponse {

    private Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaProjectRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public AdminAreaProjectRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param adminAreaProjectRelTbl
     */
    public AdminAreaProjectRelResponse(Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbl) {
        this.adminAreaProjectRelTbls = adminAreaProjectRelTbl;
    }

    /**
     * @return the adminAreaProjectRelTbls
     */
    public Iterable<AdminAreaProjectRelTbl> getAdminAreaProjectRelTbls() {
        return adminAreaProjectRelTbls;
    }

    /**
     * @param adminAreaProjectRelTbls the adminAreaProjectRelTbls to set
     */
    public void setAdminAreaProjectRelTbls(Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls) {
        this.adminAreaProjectRelTbls = adminAreaProjectRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("AdminAreaProjectRelResponse{")
                .append("adminAreaProjectRelTbls=").append(adminAreaProjectRelTbls)
                .append("}").toString();
    }
}
