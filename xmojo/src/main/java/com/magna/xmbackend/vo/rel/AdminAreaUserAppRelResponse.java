package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;

/**
 *
 * @author vijay
 */
public class AdminAreaUserAppRelResponse {

    private Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaUserAppRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public AdminAreaUserAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param adminAreaUserAppRelTbl
     */
    public AdminAreaUserAppRelResponse(Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbl) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbl;
    }

    /**
     * @return the AdminAreaProjAppRelTbls
     */
    public Iterable<AdminAreaUserAppRelTbl> getAdminAreaUserAppRelTbls() {
        return adminAreaUserAppRelTbls;
    }

    /**
     * @param adminAreaUserAppRelTbls the adminAreaUserAppRelTbls to set
     */
    public void setAdminAreaUserAppRelTbls(Iterable<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls) {
        this.adminAreaUserAppRelTbls = adminAreaUserAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("AdminAreaUserAppRelResponse{")
                .append("adminAreaProjectRelTbls=").append(adminAreaUserAppRelTbls)
                .append("}").toString();
    }
}
