package com.magna.xmbackend.vo.user;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UsersTbl;

/**
 *
 * @author vijay
 */
public class UserResponse {

    private Iterable<UsersTbl> userTbls;
    private List<Map<String, String>> statusMaps;

    public UserResponse() {
    }

    
    
	/**
	 * @param statusMaps
	 */
	public UserResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	/**
	 * @param userTbls
	 */
	public UserResponse(Iterable<UsersTbl> userTbls) {
		this.userTbls = userTbls;
	}



	/**
	 * @param userTbls
	 * @param statusMaps
	 */
	public UserResponse(Iterable<UsersTbl> userTbls, List<Map<String, String>> statusMaps) {
		this.userTbls = userTbls;
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the userTbls
	 */
	public Iterable<UsersTbl> getUserTbls() {
		return userTbls;
	}

	/**
	 * @param userTbls the userTbls to set
	 */
	public void setUserTbls(Iterable<UsersTbl> userTbls) {
		this.userTbls = userTbls;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

}
