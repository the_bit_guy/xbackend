/**
 * 
 */
package com.magna.xmbackend.vo.userApplication;

import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserApplicationMenuResponse {

	private UserApplicationsTbl userApplicationsTbl;
	private Iterable<UserApplicationsTbl> userAppChildren;
	
	public UserApplicationMenuResponse() {
	}

	/**
	 * @param userApplicationsTbl
	 * @param userAppChildren
	 */
	public UserApplicationMenuResponse(UserApplicationsTbl userApplicationsTbl,
			Iterable<UserApplicationsTbl> userAppChildren) {
		this.userApplicationsTbl = userApplicationsTbl;
		this.userAppChildren = userAppChildren;
	}

	/**
	 * @return the userApplicationsTbl
	 */
	public UserApplicationsTbl getUserApplicationsTbl() {
		return userApplicationsTbl;
	}

	/**
	 * @param userApplicationsTbl the userApplicationsTbl to set
	 */
	public void setUserApplicationsTbl(UserApplicationsTbl userApplicationsTbl) {
		this.userApplicationsTbl = userApplicationsTbl;
	}

	/**
	 * @return the userAppChildren
	 */
	public Iterable<UserApplicationsTbl> getUserAppChildren() {
		return userAppChildren;
	}

	/**
	 * @param userAppChildren the userAppChildren to set
	 */
	public void setUserAppChildren(Iterable<UserApplicationsTbl> userAppChildren) {
		this.userAppChildren = userAppChildren;
	}
	
	
}
