/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.userApplication;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

/**
 *
 * @author dhana
 */
public class UserApplicationRequest {

    private String id;
    private String name;
    private String description;
    private String iconId;
    private String baseAppId;
    private String status;
    private String isParent;
    private String isSingleTon;
    private String position;
    private CreationInfo creationInfo;

    private List<UserApplicationTranslation> userApplicationTranslations;

    public UserApplicationRequest() {
    }

    /**
     * Instantiates a new user application request.
     *
     * @param id the id
     * @param name the name
     * @param description the description
     * @param iconId the icon id
     * @param baseAppId the base app id
     * @param status the status
     * @param isParent the is parent
     * @param position the position
     * @param isSingleTon the is single ton
     * @param creationInfo the creation info
     * @param userApplicationTranslations the user application translations
     */
    public UserApplicationRequest(String id, String name, String description, String iconId, String baseAppId,
            String status, String isParent, String position,
            String isSingleTon, CreationInfo creationInfo,
            List<UserApplicationTranslation> userApplicationTranslations) {
        this.id = id;
        this.iconId = iconId;
        this.baseAppId = baseAppId;
        this.status = status;
        this.isParent = isParent;
        this.position = position;
        this.isSingleTon = isSingleTon;
        this.creationInfo = creationInfo;
        this.userApplicationTranslations = userApplicationTranslations;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
     * @return the iconId
     */
    public String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the baseAppId
     */
    public String getBaseAppId() {
        return baseAppId;
    }

    /**
     * @param baseAppId the baseAppId to set
     */
    public void setBaseAppId(String baseAppId) {
        this.baseAppId = baseAppId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the isParent
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * @param isParent the isParent to set
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the userApplicationTranslations
     */
    public List<UserApplicationTranslation> getUserApplicationTranslations() {
        return userApplicationTranslations;
    }

    /**
     * @return the isSingleTon
     */
    public String getIsSingleTon() {
        return isSingleTon;
    }

    /**
     * @param isSingleTon the isSingleTon to set
     */
    public void setIsSingleTon(String isSingleTon) {
        this.isSingleTon = isSingleTon;
    }

    /**
     * @param userApplicationTranslations the userApplicationTranslations to set
     */
    public void setUserApplicationTranslations(
            List<UserApplicationTranslation> userApplicationTranslations) {
        this.userApplicationTranslations = userApplicationTranslations;
    }
}
