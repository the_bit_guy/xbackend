package com.magna.xmbackend.vo;

import java.io.Serializable;

public class CreationInfo implements Serializable {

    private String creationDate;
    private String updationDate;

    /**
     *
     * @return creationDate
     */
    public final String getCreationDate() {
        return creationDate;
    }

    /**
     *
     * @param creationDate
     */
    public final void setCreationDate(final String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     *
     * @return updationDate
     */
    public final String getUpdationDate() {
        return updationDate;
    }

    /**
     *
     * @param updationDate
     */
    public final void setUpdationDate(final String updationDate) {
        this.updationDate = updationDate;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("CreationInfo{")
                .append("creationDate=").append(creationDate)
                .append(", updationDate=").append(updationDate)
                .append("}").toString();
    }
}
