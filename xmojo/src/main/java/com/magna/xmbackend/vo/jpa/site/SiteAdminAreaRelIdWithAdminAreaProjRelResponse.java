package com.magna.xmbackend.vo.jpa.site;

import java.util.List;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaProjRelResponse {

    private List<SiteAdminAreaRelIdWithAdminAreaProjRel> siteAdminAreaRelIdWithAdminAreaProjRel;

    public SiteAdminAreaRelIdWithAdminAreaProjRelResponse() {
    }

    /**
     *
     * @param areaRelIdWithAdminAreas
     */
    public SiteAdminAreaRelIdWithAdminAreaProjRelResponse(List<SiteAdminAreaRelIdWithAdminAreaProjRel> areaRelIdWithAdminAreas) {
        this.siteAdminAreaRelIdWithAdminAreaProjRel = areaRelIdWithAdminAreas;
    }

    /**
     *
     * @return siteAdminAreaRelIdWithAdminAreaProjRel
     */
    public final List<SiteAdminAreaRelIdWithAdminAreaProjRel> getSiteAdminAreaRelIdWithAdminAreaProjRel() {
        return siteAdminAreaRelIdWithAdminAreaProjRel;
    }

    /**
     *
     * @param siteAdminAreaRelIdWithAdminAreaProjRel
     */
    public final void setSiteAdminAreaRelIdWithAdminAreaProjRel(final List<SiteAdminAreaRelIdWithAdminAreaProjRel> siteAdminAreaRelIdWithAdminAreaProjRel) {
        this.siteAdminAreaRelIdWithAdminAreaProjRel = siteAdminAreaRelIdWithAdminAreaProjRel;
    }

}
