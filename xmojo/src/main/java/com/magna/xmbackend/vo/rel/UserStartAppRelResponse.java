package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.UserStartAppRelTbl;

/**
 *
 * @author vijay
 */
public class UserStartAppRelResponse {

    private Iterable<UserStartAppRelTbl> userStartAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserStartAppRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public UserStartAppRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
     *
     * @param userStartAppRelTbl
     */
    public UserStartAppRelResponse(Iterable<UserStartAppRelTbl> userStartAppRelTbl) {
        this.userStartAppRelTbls = userStartAppRelTbl;
    }

    /**
     * @return the userStartAppRelTbls
     */
    public Iterable<UserStartAppRelTbl> getUserStartAppRelTbls() {
        return userStartAppRelTbls;
    }

    /**
     * @param userStartAppRelTbls the userStartAppRelTbls to set
     */
    public void setUserStartAppRelTbls(Iterable<UserStartAppRelTbl> userStartAppRelTbls) {
        this.userStartAppRelTbls = userStartAppRelTbls;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("UserStartAppRelResponse{")
                .append("userStartAppRelTbls=").append(userStartAppRelTbls)
                .append("}").toString();
    }
}
