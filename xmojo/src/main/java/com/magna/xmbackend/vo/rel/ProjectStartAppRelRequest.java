package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class ProjectStartAppRelRequest {

    private String adminAreaProjectRelId;
    private String startAppId;
    private String status;

    public ProjectStartAppRelRequest() {
    }

    public ProjectStartAppRelRequest(String adminAreaProjectRelId, String startAppId, String status) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
        this.startAppId = startAppId;
        this.status = status;
    }

    /**
     * @return the adminAreaProjectRelId
     */
    public final String getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    /**
     * @param adminAreaProjectRelId the adminAreaProjectRelId to set
     */
    public final void setAdminAreaProjectRelId(final String adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    /**
     * @return the startAppId
     */
    public final String getStartAppId() {
        return startAppId;
    }

    /**
     * @param startAppId the startAppId to set
     */
    public final void setStartAppId(final String startAppId) {
        this.startAppId = startAppId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("ProjectStartAppRelRequest{")
                .append("adminAreaProjectRelId=").append(getAdminAreaProjectRelId())
                .append(", startAppId=").append(getStartAppId())
                .append(", status=").append(getStatus())
                .append("}").toString();
    }

}
