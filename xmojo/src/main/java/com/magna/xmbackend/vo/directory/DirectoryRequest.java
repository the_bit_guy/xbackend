/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import java.util.List;

import com.magna.xmbackend.vo.CreationInfo;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryRequest {
	private String id;
	private String name;
    private String iconId;
    private CreationInfo creationInfo;
    
    List<DirectoryTransalation> directoryTranslations;
    
    
    /**
     * Instantiates a new directory request.
     *
     * @param id the id
     * @param name the name
     * @param iconId the icon id
     * @param creationInfo the creation info
     * @param directoryTranslations the directory translations
     */
    public DirectoryRequest(String id, String name, String iconId, CreationInfo creationInfo,
			List<DirectoryTransalation> directoryTranslations) {
		super();
		this.id = id;
		this.name = name;
		this.iconId = iconId;
		this.creationInfo = creationInfo;
		this.directoryTranslations = directoryTranslations;
	}


	public DirectoryRequest() {
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the iconId
	 */
	public String getIconId() {
		return iconId;
	}


	/**
	 * @param iconId the iconId to set
	 */
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}


	/**
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}


	/**
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}


	/**
	 * @return the directoryTranslations
	 */
	public List<DirectoryTransalation> getDirectoryTranslations() {
		return directoryTranslations;
	}


	/**
	 * @param directoryTranslations the directoryTranslations to set
	 */
	public void setDirectoryTranslations(List<DirectoryTransalation> directoryTranslations) {
		this.directoryTranslations = directoryTranslations;
	}


	
}
