/**
 * 
 */
package com.magna.xmbackend.vo.directory;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DirectoryRelResponseWrapper {
	
	private List<DirectoryRelResponse> directoryRelResponses;
	
	public DirectoryRelResponseWrapper() {
	}

	/**
	 * @param directoryRelResponses
	 */
	public DirectoryRelResponseWrapper(List<DirectoryRelResponse> directoryRelResponses) {
		this.directoryRelResponses = directoryRelResponses;
	}

	/**
	 * @return the directoryRelResponses
	 */
	public List<DirectoryRelResponse> getDirectoryRelResponses() {
		return directoryRelResponses;
	}

	/**
	 * @param directoryRelResponses the directoryRelResponses to set
	 */
	public void setDirectoryRelResponses(List<DirectoryRelResponse> directoryRelResponses) {
		this.directoryRelResponses = directoryRelResponses;
	}
	
}
