package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.icon.Icon;
import java.io.Serializable;

public class AllSites implements Serializable {

    private String siteId;
    private String isSiteActive;
    private String siteCreateDate;
    private String siteUpdateDate;
    private String siteTransId;
    private String siteTransName;
    private String siteTransDesc;
    private String siteTransRemarks;
    private String siteLanguageCode;
    private String siteTransCreateDate;
    private String siteTransUpdateDate;
    private Icon icon;

    /**
     *
     * @return siteId
     */
    public final String getSiteId() {
        return siteId;
    }

    /**
     *
     * @param siteId
     */
    public final void setSiteId(final String siteId) {
        this.siteId = siteId;
    }

    /**
     *
     * @return isSiteActive
     */
    public final String getIsSiteActive() {
        return isSiteActive;
    }

    /**
     *
     * @param isSiteActive
     */
    public final void setIsSiteActive(final String isSiteActive) {
        this.isSiteActive = isSiteActive;
    }

    /**
     *
     * @return siteCreateDate
     */
    public final String getSiteCreateDate() {
        return siteCreateDate;
    }

    /**
     *
     * @param siteCreateDate
     */
    public final void setSiteCreateDate(final String siteCreateDate) {
        this.siteCreateDate = siteCreateDate;
    }

    /**
     *
     * @return
     */
    public final String getSiteUpdateDate() {
        return siteUpdateDate;
    }

    /**
     *
     * @param siteUpdateDate
     */
    public final void setSiteUpdateDate(final String siteUpdateDate) {
        this.siteUpdateDate = siteUpdateDate;
    }

    /**
     *
     * @return siteTransId
     */
    public final String getSiteTransId() {
        return siteTransId;
    }

    /**
     *
     * @param siteTransId
     */
    public final void setSiteTransId(final String siteTransId) {
        this.siteTransId = siteTransId;
    }

    /**
     *
     * @return siteTransName
     */
    public final String getSiteTransName() {
        return siteTransName;
    }

    /**
     *
     * @param siteTransName
     */
    public final void setSiteTransName(final String siteTransName) {
        this.siteTransName = siteTransName;
    }

    /**
     *
     * @return siteTransDesc
     */
    public final String getSiteTransDesc() {
        return siteTransDesc;
    }

    /**
     *
     * @param siteTransDesc
     */
    public final void setSiteTransDesc(final String siteTransDesc) {
        this.siteTransDesc = siteTransDesc;
    }

    /**
     *
     * @return siteTransRemarks
     */
    public final String getSiteTransRemarks() {
        return siteTransRemarks;
    }

    /**
     *
     * @param siteTransRemarks
     */
    public final void setSiteTransRemarks(final String siteTransRemarks) {
        this.siteTransRemarks = siteTransRemarks;
    }

    /**
     *
     * @return siteTransCreateDate
     */
    public final String getSiteTransCreateDate() {
        return siteTransCreateDate;
    }

    /**
     *
     * @param siteTransCreateDate
     */
    public final void setSiteTransCreateDate(final String siteTransCreateDate) {
        this.siteTransCreateDate = siteTransCreateDate;
    }

    /**
     *
     * @return siteTransUpdateDate
     */
    public final String getSiteTransUpdateDate() {
        return siteTransUpdateDate;
    }

    /**
     *
     * @param siteTransUpdateDate
     */
    public final void setSiteTransUpdateDate(final String siteTransUpdateDate) {
        this.siteTransUpdateDate = siteTransUpdateDate;
    }

    /**
     *
     * @return siteLanguageCode
     */
    public final String getSiteLanguageCode() {
        return siteLanguageCode;
    }

    /**
     *
     * @param siteLanguageCode
     */
    public final void setSiteLanguageCode(final String siteLanguageCode) {
        this.siteLanguageCode = siteLanguageCode;
    }

    /**
     *
     * @return icon
     */
    public final Icon getIcon() {
        return icon;
    }

    /**
     *
     * @param icon
     */
    public final void setIcon(final Icon icon) {
        this.icon = icon;
    }
}
