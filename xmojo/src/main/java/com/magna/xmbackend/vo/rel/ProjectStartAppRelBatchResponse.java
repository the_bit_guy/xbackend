package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class ProjectStartAppRelBatchResponse {

    private List<ProjectStartAppRelTbl> projectStartAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public ProjectStartAppRelBatchResponse() {
    }

    /**
     *
     * @param projectStartAppRelTbls
     * @param statusMaps
     */
    public ProjectStartAppRelBatchResponse(List<ProjectStartAppRelTbl> projectStartAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.projectStartAppRelTbls = projectStartAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the projectStartAppRelTbls
     */
    public final List<ProjectStartAppRelTbl> getProjectStartAppRelTbls() {
        return projectStartAppRelTbls;
    }

    /**
     * @param projectStartAppRelTbls the projectStartAppRelTbls to set
     */
    public final void setAdminAreaProjectAppRelTbls(final List<ProjectStartAppRelTbl> projectStartAppRelTbls) {
        this.projectStartAppRelTbls = projectStartAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
