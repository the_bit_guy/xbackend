/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.project;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.ProjectsTbl;

/**
 *
 * @author dhana
 */
public class ProjectResponse {

    private Iterable<ProjectsTbl> projectsTbls;
    private List<Map<String, String>> statusMaps; 

    public ProjectResponse() {
    }
    
    

    public ProjectResponse(Iterable<ProjectsTbl> projectsTbls) {
        this.projectsTbls = projectsTbls;
    }

    /**
	 * @param statusMaps
	 */
	public ProjectResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}



	public void setProjectsTbls(Iterable<ProjectsTbl> projectsTbls) {
        this.projectsTbls = projectsTbls;
    }

    public Iterable<ProjectsTbl> getProjectsTbls() {
        return projectsTbls;
    }



	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}



	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

}
