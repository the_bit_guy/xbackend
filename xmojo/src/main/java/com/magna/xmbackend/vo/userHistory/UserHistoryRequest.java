/**
 * 
 */
package com.magna.xmbackend.vo.userHistory;

import java.util.Date;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserHistoryRequest {

	private Date logTime;
	private String host;
    private String site;
    private String adminArea;
    private String project;
    private String application;
    private Integer pid;
    private String result;
    private String args;
    private String userStatus="true";
    private String userName;
    private String queryCondition;
    private int queryLimit;
    /**
	 * @return the logTime
	 */
	public Date getLogTime() {
		return logTime;
	}

	/**
	 * @param logTime the logTime to set
	 */
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	
    
    /**
	 * @return the queryCondition
	 */
	public String getQueryCondition() {
		return queryCondition;
	}

	/**
	 * @param queryCondition the queryCondition to set
	 */
	public void setQueryCondition(String queryCondition) {
		this.queryCondition = queryCondition;
	}

	/**
	 * @return the queryLimit
	 */
	public int getQueryLimit() {
		return queryLimit;
	}

	/**
	 * @param queryLimit the queryLimit to set
	 */
	public void setQueryLimit(int queryLimit) {
		this.queryLimit = queryLimit;
	}

	public UserHistoryRequest() {
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * @return the adminArea
	 */
	public String getAdminArea() {
		return adminArea;
	}

	/**
	 * @param adminArea the adminArea to set
	 */
	public void setAdminArea(String adminArea) {
		this.adminArea = adminArea;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * @return the pid
	 */
	public Integer getPid() {
		return pid;
	}

	/**
	 * @param pid the pid to set
	 */
	public void setPid(Integer pid) {
		this.pid = pid;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the args
	 */
	public String getArgs() {
		return args;
	}

	/**
	 * @param args the args to set
	 */
	public void setArgs(String args) {
		this.args = args;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}

	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

}
