/**
 * 
 */
package com.magna.xmbackend.vo.liveMessage;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LiveMessageResponseWrapper {

	private List<LiveMessageResponse> liveMessageResponseWrapper;
	
	public LiveMessageResponseWrapper() {
	}
	
	/**
	 * @param liveMessageResponseWrapper
	 */
	public LiveMessageResponseWrapper(
			List<LiveMessageResponse> liveMessageResponseWrapper) {
		this.liveMessageResponseWrapper = liveMessageResponseWrapper;
	}

	/**
	 * @return the liveMessageResponseWrapper
	 */
	public List<LiveMessageResponse> getLiveMessageResponseWrapper() {
		return liveMessageResponseWrapper;
	}

	/**
	 * @param liveMessageResponseWrapper the liveMessageResponseWrapper to set
	 */
	public void setLiveMessageResponseWrapper(
			List<LiveMessageResponse> liveMessageResponseWrapper) {
		this.liveMessageResponseWrapper = liveMessageResponseWrapper;
	}
	
}
