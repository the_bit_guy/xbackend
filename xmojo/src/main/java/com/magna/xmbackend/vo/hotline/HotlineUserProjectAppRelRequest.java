/**
 * 
 */
package com.magna.xmbackend.vo.hotline;

/**
 * @author Bhabadyuti Bal
 *
 */
public class HotlineUserProjectAppRelRequest {

	private String userId;
	private String projectId;
	private String adminAreaId;
	private String projectAppId;
	private String userRelationType;
	
	public HotlineUserProjectAppRelRequest() {
	}

	/**
	 * @param userId
	 * @param projectId
	 * @param adminArealId
	 * @param projectAppId
	 * @param userRelationType
	 */
	public HotlineUserProjectAppRelRequest(String userId, String projectId, String adminAreaId, String projectAppId,
			String userRelationType) {
		this.userId = userId;
		this.projectId = projectId;
		this.adminAreaId = adminAreaId;
		this.projectAppId = projectAppId;
		this.userRelationType = userRelationType;
	}



	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	/**
	 * @return the adminAreaId
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * @param adminAreaId the adminAreaId to set
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * @return the projectAppId
	 */
	public String getProjectAppId() {
		return projectAppId;
	}

	/**
	 * @param projectAppId the projectAppId to set
	 */
	public void setProjectAppId(String projectAppId) {
		this.projectAppId = projectAppId;
	}

	/**
	 * @return the userRelationType
	 */
	public String getUserRelationType() {
		return userRelationType;
	}

	/**
	 * @param userRelationType the userRelationType to set
	 */
	public void setUserRelationType(String userRelationType) {
		this.userRelationType = userRelationType;
	}
	
}
