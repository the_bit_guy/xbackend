package com.magna.xmbackend.vo.projectApplication;

import com.magna.xmbackend.vo.CreationInfo;

public class ProjectApplicationTransulation {
    private String id;
    private String languageCode;
    
    private String name;
    private String description;
    private String remarks;
    
    private CreationInfo creationInfo;

    public ProjectApplicationTransulation() {
    }

    public ProjectApplicationTransulation(String id, String languageCode, 
            String name, String description, String remarks, 
            CreationInfo creationInfo) {
        this.id = id;
        this.languageCode = languageCode;
        this.name = name;
        this.description = description;
        this.remarks = remarks;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * @param languageCode the languageCode to set
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }
    
    
    
    
}
