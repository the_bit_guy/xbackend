/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.liveMessage;

/**
 *
 * @author dhana
 */
public class LiveMessageConfigCreateRequest { 

	private String liveMessageConfigId;
    private String siteId;
    private String adminAreaId;
    private String projectId;
    private String userId;

    public LiveMessageConfigCreateRequest() {
    }
    
    /**
	 * @return the liveMessageConfigId
	 */
	public String getLiveMessageConfigId() {
		return liveMessageConfigId;
	}

	/**
	 * @param liveMessageConfigId the liveMessageConfigId to set
	 */
	public void setLiveMessageConfigId(String liveMessageConfigId) {
		this.liveMessageConfigId = liveMessageConfigId;
	}



	public LiveMessageConfigCreateRequest(String siteId, String adminAreaId,
            String projectId, String userId) {
        this.siteId = siteId;
        this.adminAreaId = adminAreaId;
        this.projectId = projectId;
        this.userId = userId;
    }

    /**
     * @return the siteId
     */
    public String getSiteId() {
        return siteId;
    }

    /**
     * @param siteId the siteId to set
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * @return the adminAreaId
     */
    public String getAdminAreaId() {
        return adminAreaId;
    }

    /**
     * @param adminAreaId the adminAreaId to set
     */
    public void setAdminAreaId(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adminAreaId == null) ? 0 : adminAreaId.hashCode());
		result = prime * result + ((liveMessageConfigId == null) ? 0 : liveMessageConfigId.hashCode());
		result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
		result = prime * result + ((siteId == null) ? 0 : siteId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiveMessageConfigCreateRequest other = (LiveMessageConfigCreateRequest) obj;
		if (adminAreaId == null) {
			if (other.adminAreaId != null)
				return false;
		} else if (!adminAreaId.equals(other.adminAreaId))
			return false;
		if (liveMessageConfigId == null) {
			if (other.liveMessageConfigId != null)
				return false;
		} else if (!liveMessageConfigId.equals(other.liveMessageConfigId))
			return false;
		if (projectId == null) {
			if (other.projectId != null)
				return false;
		} else if (!projectId.equals(other.projectId))
			return false;
		if (siteId == null) {
			if (other.siteId != null)
				return false;
		} else if (!siteId.equals(other.siteId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
    
    

}
