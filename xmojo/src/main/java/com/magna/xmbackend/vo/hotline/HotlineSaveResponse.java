/**
 * 
 */
package com.magna.xmbackend.vo.hotline;

import com.magna.xmbackend.vo.rel.UserProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public class HotlineSaveResponse {

	private UserProjectRelBatchResponse userProjectRelBatchResponse;
	private UserUserAppRelBatchResponse userUserAppRelBatchResponse;
	private UserProjectAppRelBatchResponse userProjectAppRelBatchResponse;
	/**
	 * @return the userProjectRelBatchResponse
	 */
	public UserProjectRelBatchResponse getUserProjectRelBatchResponse() {
		return userProjectRelBatchResponse;
	}
	/**
	 * @param userProjectRelBatchResponse the userProjectRelBatchResponse to set
	 */
	public void setUserProjectRelBatchResponse(UserProjectRelBatchResponse userProjectRelBatchResponse) {
		this.userProjectRelBatchResponse = userProjectRelBatchResponse;
	}
	/**
	 * @return the userUserAppRelBatchResponse
	 */
	public UserUserAppRelBatchResponse getUserUserAppRelBatchResponse() {
		return userUserAppRelBatchResponse;
	}
	/**
	 * @param userUserAppRelBatchResponse the userUserAppRelBatchResponse to set
	 */
	public void setUserUserAppRelBatchResponse(UserUserAppRelBatchResponse userUserAppRelBatchResponse) {
		this.userUserAppRelBatchResponse = userUserAppRelBatchResponse;
	}
	/**
	 * @return the userProjectAppRelBatchResponse
	 */
	public UserProjectAppRelBatchResponse getUserProjectAppRelBatchResponse() {
		return userProjectAppRelBatchResponse;
	}
	/**
	 * @param userProjectAppRelBatchResponse the userProjectAppRelBatchResponse to set
	 */
	public void setUserProjectAppRelBatchResponse(UserProjectAppRelBatchResponse userProjectAppRelBatchResponse) {
		this.userProjectAppRelBatchResponse = userProjectAppRelBatchResponse;
	}
	
}
