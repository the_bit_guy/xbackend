package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class UserProjectRelRequest {

    private String userId;
    private String projectId;
    private String userName;
    private String projectName;
    private String status;
    private String projectExpiryDays;

    public UserProjectRelRequest() {
    }

    /**
     *
     * @param userId
     * @param projectId
     * @param status
     */
    public UserProjectRelRequest(String userId, String projectId,
            String status, String projectExpiryDays) {
        this.userId = userId;
        this.projectId = projectId;
        this.status = status;
        this.projectExpiryDays = projectExpiryDays;
    }

    /**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectExpiryDays
	 */
	public String getProjectExpiryDays() {
		return projectExpiryDays;
	}

	/**
	 * @param projectExpiryDays the projectExpiryDays to set
	 */
	public void setProjectExpiryDays(String projectExpiryDays) {
		this.projectExpiryDays = projectExpiryDays;
	}

	/**
     * @return the userId
     */
    public final String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public final void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the projectId
     */
    public final String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public final void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("UserProjectRelRequest{")
                .append("userId=").append(userId)
                .append(", projectId=").append(projectId)
                .append(", status=").append(status)
                .append("}").toString();
    }
}
