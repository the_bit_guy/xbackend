package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class UserUserAppRelRequest {

	private String id;
    private String userId;
    private String siteAdminAreaRelId;
    private String userAppId;
    private String userRelationType;
    private boolean isHotlineRequest = false;
    


	public UserUserAppRelRequest() {
    }

    /**
     *
     * @param userId
     * @param projectId
     * @param relationType
     */
    public UserUserAppRelRequest(String userId, String siteAdminAreaRelId, String projectId,
            String relationType) {
        this.userId = userId;
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.userAppId = projectId;
        this.userRelationType = relationType;
    }

    /**
	 * @return the isHotlineRequest
	 */
	public boolean isHotlineRequest() {
		return isHotlineRequest;
	}

	/**
	 * @param isHotlineRequest the isHotlineRequest to set
	 */
	public void setHotlineRequest(boolean isHotlineRequest) {
		this.isHotlineRequest = isHotlineRequest;
	}

	/**
	 * @param id
	 * @param userId
	 * @param siteAdminAreaRelId
	 * @param userAppId
	 * @param userRelationType
	 */
	public UserUserAppRelRequest(String id, String userId, String siteAdminAreaRelId, String userAppId,
			String userRelationType) {
		this.id = id;
		this.userId = userId;
		this.siteAdminAreaRelId = siteAdminAreaRelId;
		this.userAppId = userAppId;
		this.userRelationType = userRelationType;
	}

	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the siteAdminAreaRelId
	 */
	public String getSiteAdminAreaRelId() {
		return siteAdminAreaRelId;
	}

	/**
	 * @param siteAdminAreaRelId the siteAdminAreaRelId to set
	 */
	public void setSiteAdminAreaRelId(String siteAdminAreaRelId) {
		this.siteAdminAreaRelId = siteAdminAreaRelId;
	}

	/**
     * @return the userId
     */
    public final String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public final void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the userAppId
     */
    public final String getUserAppId() {
        return userAppId;
    }

    /**
     * @param userAppId the userAppId to set
     */
    public final void setUserAppId(final String userAppId) {
        this.userAppId = userAppId;
    }

    /**
     * @return the userRelationType
     */
    public final String getUserRelationType() {
        return userRelationType;
    }

    /**
     * @param userRelationType the userRelationType to set
     */
    public final void setUserRelationType(final String userRelationType) {
        this.userRelationType = userRelationType;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("UserUserAppRelRequest{")
                .append("userId=").append(getUserId())
                .append(", userAppId=").append(getUserAppId())
                .append(", userRelationType=").append(getUserRelationType())
                .append("}").toString();
    }

}
