package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public enum CreationType implements Serializable {
    ADD, DELETE
}
