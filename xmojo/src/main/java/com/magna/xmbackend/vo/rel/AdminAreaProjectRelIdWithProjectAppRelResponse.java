package com.magna.xmbackend.vo.rel;

import java.util.List;

/**
 *
 * @author vijay
 */
public class AdminAreaProjectRelIdWithProjectAppRelResponse {

    private List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels;

    public AdminAreaProjectRelIdWithProjectAppRelResponse() {
    }

    /**
     *
     * @param saariwaauars
     */
    public AdminAreaProjectRelIdWithProjectAppRelResponse(List<AdminAreaProjectRelIdWithProjectAppRel> saariwaauars) {
        this.adminAreaProjectRelIdWithProjectAppRels = saariwaauars;
    }

    /**
     *
     * @return adminAreaProjectRelIdWithProjectAppRels
     */
    public final List<AdminAreaProjectRelIdWithProjectAppRel> getAdminAreaProjectRelIdWithProjectAppRels() {
        return adminAreaProjectRelIdWithProjectAppRels;
    }

    /**
     *
     * @param aapriwpars
     */
    public final void setAdminAreaProjectRelIdWithProjectAppRels(final List<AdminAreaProjectRelIdWithProjectAppRel> aapriwpars) {
        this.adminAreaProjectRelIdWithProjectAppRels = aapriwpars;
    }

}
