/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.baseApplication;

import com.magna.xmbackend.vo.CreationInfo;
import java.util.List;

/**
 *
 * @author dhana
 */
public class BaseApplicationRequest {
    private String id;
    private String name;
    private String iconId;
    private String status;
    private String platforms;
    private String program;
    private CreationInfo creationInfo;

    private List<BaseApplicationTransulation> baseApplicationTransulations;

    public BaseApplicationRequest() {
    }

    public BaseApplicationRequest(String id, String name, String iconId, String status, String platforms, String program, CreationInfo creationInfo, List<BaseApplicationTransulation> baseApplicationTransulations) {
        this.id = id;
        this.name = name;
        this.iconId = iconId;
        this.status = status;
        this.platforms = platforms;
        this.program = program;
        this.creationInfo = creationInfo;
        this.baseApplicationTransulations = baseApplicationTransulations;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
     * @return the iconId
     */
    public String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the platforms
     */
    public String getPlatforms() {
        return platforms;
    }

    /**
     * @param platforms the platforms to set
     */
    public void setPlatforms(String platforms) {
        this.platforms = platforms;
    }

    /**
     * @return the program
     */
    public String getProgram() {
        return program;
    }

    /**
     * @param program the program to set
     */
    public void setProgram(String program) {
        this.program = program;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the baseApplicationTransulations
     */
    public List<BaseApplicationTransulation> getBaseApplicationTransulations() {
        return baseApplicationTransulations;
    }

    /**
     * @param baseApplicationTransulations the baseApplicationTransulations to
     * set
     */
    public void setBaseApplicationTransulations(List<BaseApplicationTransulation> baseApplicationTransulations) {
        this.baseApplicationTransulations = baseApplicationTransulations;
    }

}
