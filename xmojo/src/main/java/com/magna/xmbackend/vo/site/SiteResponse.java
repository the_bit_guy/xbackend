package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.CreationInfo;

public class SiteResponse {

    private SiteObject siteObject;
    private CreationInfo creationInfo;

    /**
     *
     * @return siteObject
     */
    public final SiteObject getSiteObject() {
        return siteObject;
    }

    /**
     *
     * @param siteObject
     */
    public final void setSiteObject(final SiteObject siteObject) {
        this.siteObject = siteObject;
    }

    /**
     *
     * @return creationInfo
     */
    public final CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     *
     * @param creationInfo
     */
    public final void setCreationInfo(final CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("SiteResponse{")
                .append("siteObject=").append(siteObject)
                .append("creationInfo=").append(creationInfo)
                .append("}").toString();
    }

}
