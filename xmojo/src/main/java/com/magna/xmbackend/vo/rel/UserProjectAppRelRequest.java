package com.magna.xmbackend.vo.rel;

/**
 *
 * @author vijay
 */
public class UserProjectAppRelRequest {

	private String id;
	private String userProjectRelId;
	private String adminAreaProjRelId;
	private String projectAppId;
	private String userRelationType;
	private boolean isHotlineRequest = false;

	public UserProjectAppRelRequest() {
	}

	/**
	 * @param userProjectRelId
	 * @param siteAdminAreaRelId
	 * @param projectAppId
	 * @param userRelationType
	 */
	public UserProjectAppRelRequest(String userProjectRelId, String adminAreaProjRelId, String projectAppId,
			String userRelationType) {
		this.userProjectRelId = userProjectRelId;
		this.adminAreaProjRelId = adminAreaProjRelId;
		this.projectAppId = projectAppId;
		this.userRelationType = userRelationType;
	}


	/**
	 * @param id
	 * @param userProjectRelId
	 * @param adminAreaProjRelId
	 * @param projectAppId
	 * @param userRelationType
	 */
	public UserProjectAppRelRequest(String id, String userProjectRelId, String adminAreaProjRelId, String projectAppId,
			String userRelationType) {
		this.id = id;
		this.userProjectRelId = userProjectRelId;
		this.adminAreaProjRelId = adminAreaProjRelId;
		this.projectAppId = projectAppId;
		this.userRelationType = userRelationType;
	}

	
	
	/**
	 * @return the isHotlineRequest
	 */
	public boolean isHotlineRequest() {
		return isHotlineRequest;
	}

	/**
	 * @param isHotlineRequest the isHotlineRequest to set
	 */
	public void setHotlineRequest(boolean isHotlineRequest) {
		this.isHotlineRequest = isHotlineRequest;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the adminAreaProjRelId
	 */
	public String getAdminAreaProjRelId() {
		return adminAreaProjRelId;
	}

	/**
	 * @param adminAreaProjRelId the adminAreaProjRelId to set
	 */
	public void setAdminAreaProjRelId(String adminAreaProjRelId) {
		this.adminAreaProjRelId = adminAreaProjRelId;
	}

	/**
	 * @return the userProjectRelId
	 */
	public final String getUserProjectRelId() {
		return userProjectRelId;
	}

	/**
	 * @param userProjectRelId
	 *            the userProjectRelId to set
	 */
	public final void setUserProjectRelId(final String userProjectRelId) {
		this.userProjectRelId = userProjectRelId;
	}

	/**
	 * @return the projectAppId
	 */
	public final String getProjectAppId() {
		return projectAppId;
	}

	/**
	 * @param projectAppId
	 *            the projectAppId to set
	 */
	public final void setProjectAppId(final String projectAppId) {
		this.projectAppId = projectAppId;
	}

	/**
	 * @return the userRelationType
	 */
	public final String getUserRelationType() {
		return userRelationType;
	}

	/**
	 * @param userRelationType
	 *            the userRelationType to set
	 */
	public final void setUserRelationType(final String userRelationType) {
		this.userRelationType = userRelationType;
	}

	/**
	 *
	 * @return object as string
	 */
	@Override
	public final String toString() {
		return new StringBuffer().append("UserProjectAppRelRequest{").append("userProjectRelId=")
				.append(getUserProjectRelId()).append(", projectAppId=").append(getProjectAppId())
				.append(", relationType=").append(getUserRelationType()).append("}").toString();
	}

}
