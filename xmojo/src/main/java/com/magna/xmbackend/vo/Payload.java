package com.magna.xmbackend.vo;

import java.util.List;

public class Payload {

    private List<Attribute> attributes;

    public Payload() {
    }

    /**
     *
     * @param attributes
     */
    public Payload(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     *
     * @return attributes
     */
    public final List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuffer().append("Payload{")
                .append("attributes=").append(attributes)
                .append("}").toString();
    }
}
