/**
 * 
 */
package com.magna.xmbackend.vo.enums;

import java.io.Serializable;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum LdapKey implements Serializable {
	LDAP_URL, LDAP_PORT_NUMBER, LDAP_BASE, LDAP_USERNAME, LDAP_PASSWORD
}
