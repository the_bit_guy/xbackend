package com.magna.xmbackend.vo;

import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.icon.Icon;
import java.io.Serializable;

public class XMPojo extends BaseObject implements Serializable {

    private Status status;//can be ACTIVE, INACTIVE, DELETED
    private Icon icon;

    public XMPojo() {
    }

    /**
     *
     * @param status
     * @param icon
     */
    public XMPojo(Status status, Icon icon) {
        this.status = status;
        this.icon = icon;
    }

    /**
     *
     * @param status
     * @param icon
     * @param xMObject
     */
    public XMPojo(Status status, Icon icon, XMObject xMObject) {
        super(xMObject);
        this.status = status;
        this.icon = icon;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the icon
     */
    public Icon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuffer().append("XMPojo{")
                .append("status=").append(status)
                .append(", icon=").append(icon)
                .append("}").toString();
    }
}
