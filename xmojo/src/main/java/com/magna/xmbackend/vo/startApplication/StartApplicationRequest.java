package com.magna.xmbackend.vo.startApplication;

import java.util.Date;
import java.util.List;

import com.magna.xmbackend.vo.CreationInfo;

/**
 *
 * @author dhana
 */
public class StartApplicationRequest {

    private String id;
    private String name;
    private String iconId;
    private String baseAppId;
    private String status;
    private Boolean isMessage = false;
    private Date startMsgExpiryDate;
    private CreationInfo creationInfo;

    private List<StartApplicationTranslation> startApplicationTranslations;

    public StartApplicationRequest() {
    }

	/**
	 * Instantiates a new start application request.
	 *
	 * @param id the id
	 * @param name the name
	 * @param iconId the icon id
	 * @param baseAppId the base app id
	 * @param status the status
	 * @param isMessage the is message
	 * @param creationInfo the creation info
	 * @param startApplicationTranslations the start application translations
	 * @param startMsgExpiryDate the start msg expiry date
	 */
	public StartApplicationRequest(String id, String name, String iconId, String baseAppId, String status, Boolean isMessage,
			CreationInfo creationInfo, List<StartApplicationTranslation> startApplicationTranslations,
			Date startMsgExpiryDate) {
		this.id = id;
		this.name = name;
		this.iconId = iconId;
		this.baseAppId = baseAppId;
		this.status = status;
		this.isMessage = isMessage;
		this.creationInfo = creationInfo;
		this.startApplicationTranslations = startApplicationTranslations;
		this.startMsgExpiryDate = startMsgExpiryDate;
	}

	/**
	 * @return the startMsgExpiryDate
	 */
	public Date getStartMsgExpiryDate() {
		return startMsgExpiryDate;
	}


	/**
	 * @param startMsgExpiryDate the startMsgExpiryDate to set
	 */
	public void setStartMsgExpiryDate(Date startMsgExpiryDate) {
		this.startMsgExpiryDate = startMsgExpiryDate;
	}


	/**
	 * @return the isMessage
	 */
	public Boolean getIsMessage() {
		return isMessage;
	}


	/**
	 * @param isMessage the isMessage to set
	 */
	public void setIsMessage(Boolean isMessage) {
		this.isMessage = isMessage;
	}


	/**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
     * @return the iconId
     */
    public String getIconId() {
        return iconId;
    }

    /**
     * @param iconId the iconId to set
     */
    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    /**
     * @return the baseAppId
     */
    public String getBaseAppId() {
        return baseAppId;
    }

    /**
     * @param baseAppId the baseAppId to set
     */
    public void setBaseAppId(String baseAppId) {
        this.baseAppId = baseAppId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

    /**
     * @return the startApplicationTranslations
     */
    public List<StartApplicationTranslation> getStartApplicationTranslations() {
        return startApplicationTranslations;
    }

    /**
     * @param startApplicationTranslations the startApplicationTranslations to
     * set
     */
    public void setStartApplicationTranslations(List<StartApplicationTranslation> startApplicationTranslations) {
        this.startApplicationTranslations = startApplicationTranslations;
    }

}
