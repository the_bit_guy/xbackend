/**
 * 
 */
package com.magna.xmbackend.vo.hotline;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class HotlineSaveRequest {

	List<HotlineUserProjectRelRequest> userProjectAdditionList;
	List<HotlineUserProjectRelRequest> userProjectDeletionList;
	List<HotlineUserUserAppRelRequest> userUserAppRelAdditionList;
	List<HotlineUserUserAppRelRequest> userUserAppRelDeletionList;
	List<HotlineUserProjectAppRelRequest> userProjectAppRelAdditionList;
	List<HotlineUserProjectAppRelRequest> userProjectAppRelDeletionList;
	/**
	 * @return the userProjectAdditionList
	 */
	public List<HotlineUserProjectRelRequest> getUserProjectAdditionList() {
		return userProjectAdditionList;
	}
	/**
	 * @param userProjectAdditionList the userProjectAdditionList to set
	 */
	public void setUserProjectAdditionList(List<HotlineUserProjectRelRequest> userProjectAdditionList) {
		this.userProjectAdditionList = userProjectAdditionList;
	}
	/**
	 * @return the userProjectDeletionList
	 */
	public List<HotlineUserProjectRelRequest> getUserProjectDeletionList() {
		return userProjectDeletionList;
	}
	/**
	 * @param userProjectDeletionList the userProjectDeletionList to set
	 */
	public void setUserProjectDeletionList(List<HotlineUserProjectRelRequest> userProjectDeletionList) {
		this.userProjectDeletionList = userProjectDeletionList;
	}
	/**
	 * @return the userUserAppRelAdditionList
	 */
	public List<HotlineUserUserAppRelRequest> getUserUserAppRelAdditionList() {
		return userUserAppRelAdditionList;
	}
	/**
	 * @param userUserAppRelAdditionList the userUserAppRelAdditionList to set
	 */
	public void setUserUserAppRelAdditionList(List<HotlineUserUserAppRelRequest> userUserAppRelAdditionList) {
		this.userUserAppRelAdditionList = userUserAppRelAdditionList;
	}
	/**
	 * @return the userUserAppRelDeletionList
	 */
	public List<HotlineUserUserAppRelRequest> getUserUserAppRelDeletionList() {
		return userUserAppRelDeletionList;
	}
	/**
	 * @param userUserAppRelDeletionList the userUserAppRelDeletionList to set
	 */
	public void setUserUserAppRelDeletionList(List<HotlineUserUserAppRelRequest> userUserAppRelDeletionList) {
		this.userUserAppRelDeletionList = userUserAppRelDeletionList;
	}
	/**
	 * @return the userProjectAppRelAdditionList
	 */
	public List<HotlineUserProjectAppRelRequest> getUserProjectAppRelAdditionList() {
		return userProjectAppRelAdditionList;
	}
	/**
	 * @param userProjectAppRelAdditionList the userProjectAppRelAdditionList to set
	 */
	public void setUserProjectAppRelAdditionList(List<HotlineUserProjectAppRelRequest> userProjectAppRelAdditionList) {
		this.userProjectAppRelAdditionList = userProjectAppRelAdditionList;
	}
	/**
	 * @return the userProjectAppRelDeletionList
	 */
	public List<HotlineUserProjectAppRelRequest> getUserProjectAppRelDeletionList() {
		return userProjectAppRelDeletionList;
	}
	/**
	 * @param userProjectAppRelDeletionList the userProjectAppRelDeletionList to set
	 */
	public void setUserProjectAppRelDeletionList(List<HotlineUserProjectAppRelRequest> userProjectAppRelDeletionList) {
		this.userProjectAppRelDeletionList = userProjectAppRelDeletionList;
	}
	
}
