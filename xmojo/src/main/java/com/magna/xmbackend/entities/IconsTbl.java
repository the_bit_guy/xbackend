/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ICONS_TBL")
@NamedQueries({
    @NamedQuery(name = "IconsTbl.findAll", query = "SELECT i FROM IconsTbl i")})
public class IconsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ICON_ID")
    private String iconId;
    @Column(name = "ICON_NAME")
    private String iconName;
    @Column(name = "ICON_TYPE")
    private String iconType;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<UserApplicationsTbl> userApplicationsTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<BaseApplicationsTbl> baseApplicationsTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<SitesTbl> sitesTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<UsersTbl> usersTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<GroupsTbl> groupsTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<AdminAreasTbl> adminAreasTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<ProjectsTbl> projectsTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<DirectoryTbl> directoryTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<ProjectApplicationsTbl> projectApplicationsTblCollection;
    @JsonBackReference
    @OneToMany(mappedBy = "iconId")
    private Collection<StartApplicationsTbl> startApplicationsTblCollection;

    public IconsTbl() {
    }

    public IconsTbl(String iconId) {
        this.iconId = iconId;
    }

    public String getIconId() {
        return iconId;
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconType() {
        return iconType;
    }

    public void setIconType(String iconType) {
        this.iconType = iconType;
    }

    public Collection<UserApplicationsTbl> getUserApplicationsTblCollection() {
        return userApplicationsTblCollection;
    }

    public void setUserApplicationsTblCollection(Collection<UserApplicationsTbl> userApplicationsTblCollection) {
        this.userApplicationsTblCollection = userApplicationsTblCollection;
    }

    public Collection<BaseApplicationsTbl> getBaseApplicationsTblCollection() {
        return baseApplicationsTblCollection;
    }

    public void setBaseApplicationsTblCollection(Collection<BaseApplicationsTbl> baseApplicationsTblCollection) {
        this.baseApplicationsTblCollection = baseApplicationsTblCollection;
    }

    public Collection<SitesTbl> getSitesTblCollection() {
        return sitesTblCollection;
    }

    public void setSitesTblCollection(Collection<SitesTbl> sitesTblCollection) {
        this.sitesTblCollection = sitesTblCollection;
    }

    public Collection<UsersTbl> getUsersTblCollection() {
        return usersTblCollection;
    }

    public void setUsersTblCollection(Collection<UsersTbl> usersTblCollection) {
        this.usersTblCollection = usersTblCollection;
    }

    public Collection<GroupsTbl> getGroupsTblCollection() {
        return groupsTblCollection;
    }

    public void setGroupsTblCollection(Collection<GroupsTbl> groupsTblCollection) {
        this.groupsTblCollection = groupsTblCollection;
    }

    public Collection<AdminAreasTbl> getAdminAreasTblCollection() {
        return adminAreasTblCollection;
    }

    public void setAdminAreasTblCollection(Collection<AdminAreasTbl> adminAreasTblCollection) {
        this.adminAreasTblCollection = adminAreasTblCollection;
    }

    public Collection<ProjectsTbl> getProjectsTblCollection() {
        return projectsTblCollection;
    }

    public void setProjectsTblCollection(Collection<ProjectsTbl> projectsTblCollection) {
        this.projectsTblCollection = projectsTblCollection;
    }

    public Collection<DirectoryTbl> getDirectoryTblCollection() {
        return directoryTblCollection;
    }

    public void setDirectoryTblCollection(Collection<DirectoryTbl> directoryTblCollection) {
        this.directoryTblCollection = directoryTblCollection;
    }

    public Collection<ProjectApplicationsTbl> getProjectApplicationsTblCollection() {
        return projectApplicationsTblCollection;
    }

    public void setProjectApplicationsTblCollection(Collection<ProjectApplicationsTbl> projectApplicationsTblCollection) {
        this.projectApplicationsTblCollection = projectApplicationsTblCollection;
    }

    public Collection<StartApplicationsTbl> getStartApplicationsTblCollection() {
        return startApplicationsTblCollection;
    }

    public void setStartApplicationsTblCollection(Collection<StartApplicationsTbl> startApplicationsTblCollection) {
        this.startApplicationsTblCollection = startApplicationsTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iconId != null ? iconId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IconsTbl)) {
            return false;
        }
        IconsTbl other = (IconsTbl) object;
        if ((this.iconId == null && other.iconId != null) || (this.iconId != null && !this.iconId.equals(other.iconId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.IconsTbl[ iconId=" + iconId + " ]";
    }
    
}
