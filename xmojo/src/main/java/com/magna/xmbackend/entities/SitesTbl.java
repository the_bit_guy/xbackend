/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "SITES_TBL")
@NamedQueries({
    @NamedQuery(name = "SitesTbl.findAll", query = "SELECT s FROM SitesTbl s")})
public class SitesTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SITE_ID")
    private String siteId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId")
    private Collection<SiteTranslationTbl> siteTranslationTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId")
    private Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId")
    private Collection<UserSettingsTbl> userSettingsTblCollection;

    public SitesTbl() {
    }

    public SitesTbl(String siteId) {
        this.siteId = siteId;
    }

    public SitesTbl(String siteId, String name, String status, Date createDate, Date updateDate) {
        this.siteId = siteId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<SiteTranslationTbl> getSiteTranslationTblCollection() {
        return siteTranslationTblCollection;
    }

    public void setSiteTranslationTblCollection(Collection<SiteTranslationTbl> siteTranslationTblCollection) {
        this.siteTranslationTblCollection = siteTranslationTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<SiteAdminAreaRelTbl> getSiteAdminAreaRelTblCollection() {
        return siteAdminAreaRelTblCollection;
    }

    public void setSiteAdminAreaRelTblCollection(Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection) {
        this.siteAdminAreaRelTblCollection = siteAdminAreaRelTblCollection;
    }

    public Collection<UserSettingsTbl> getUserSettingsTblCollection() {
        return userSettingsTblCollection;
    }

    public void setUserSettingsTblCollection(Collection<UserSettingsTbl> userSettingsTblCollection) {
        this.userSettingsTblCollection = userSettingsTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siteId != null ? siteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SitesTbl)) {
            return false;
        }
        SitesTbl other = (SitesTbl) object;
        if ((this.siteId == null && other.siteId != null) || (this.siteId != null && !this.siteId.equals(other.siteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.SitesTbl[ siteId=" + siteId + " ]";
    }
    
}
