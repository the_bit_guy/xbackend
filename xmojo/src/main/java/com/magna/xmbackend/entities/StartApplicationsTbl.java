/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "START_APPLICATIONS_TBL")
@NamedQueries({
    @NamedQuery(name = "StartApplicationsTbl.findAll", query = "SELECT s FROM StartApplicationsTbl s")})
public class StartApplicationsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "START_APPLICATION_ID")
    private String startApplicationId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "IS_MESSAGE")
    private String isMessage;
    @Column(name = "START_MESSAGE_EXPIRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startMessageExpiryDate;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "startApplicationId")
    private Collection<UserStartAppRelTbl> userStartAppRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "startApplicationId")
    private Collection<ProjectStartAppRelTbl> projectStartAppRelTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "startApplicationId")
    private Collection<StartAppTranslationTbl> startAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "startApplicationId")
    private Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection;
    @JoinColumn(name = "BASE_APPLICATION_ID", referencedColumnName = "BASE_APPLICATION_ID")
    @ManyToOne
    private BaseApplicationsTbl baseApplicationId;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;

    public StartApplicationsTbl() {
    }

    public StartApplicationsTbl(String startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    public StartApplicationsTbl(String startApplicationId, String name, String isMessage, String status, Date createDate, Date updateDate) {
        this.startApplicationId = startApplicationId;
        this.name = name;
        this.isMessage = isMessage;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getStartApplicationId() {
        return startApplicationId;
    }

    public void setStartApplicationId(String startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsMessage() {
        return isMessage;
    }

    public void setIsMessage(String isMessage) {
        this.isMessage = isMessage;
    }

    public Date getStartMessageExpiryDate() {
        return startMessageExpiryDate;
    }

    public void setStartMessageExpiryDate(Date startMessageExpiryDate) {
        this.startMessageExpiryDate = startMessageExpiryDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<UserStartAppRelTbl> getUserStartAppRelTblCollection() {
        return userStartAppRelTblCollection;
    }

    public void setUserStartAppRelTblCollection(Collection<UserStartAppRelTbl> userStartAppRelTblCollection) {
        this.userStartAppRelTblCollection = userStartAppRelTblCollection;
    }

    public Collection<ProjectStartAppRelTbl> getProjectStartAppRelTblCollection() {
        return projectStartAppRelTblCollection;
    }

    public void setProjectStartAppRelTblCollection(Collection<ProjectStartAppRelTbl> projectStartAppRelTblCollection) {
        this.projectStartAppRelTblCollection = projectStartAppRelTblCollection;
    }

    public Collection<StartAppTranslationTbl> getStartAppTranslationTblCollection() {
        return startAppTranslationTblCollection;
    }

    public void setStartAppTranslationTblCollection(Collection<StartAppTranslationTbl> startAppTranslationTblCollection) {
        this.startAppTranslationTblCollection = startAppTranslationTblCollection;
    }

    public Collection<AdminAreaStartAppRelTbl> getAdminAreaStartAppRelTblCollection() {
        return adminAreaStartAppRelTblCollection;
    }

    public void setAdminAreaStartAppRelTblCollection(Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection) {
        this.adminAreaStartAppRelTblCollection = adminAreaStartAppRelTblCollection;
    }

    public BaseApplicationsTbl getBaseApplicationId() {
        return baseApplicationId;
    }

    public void setBaseApplicationId(BaseApplicationsTbl baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (startApplicationId != null ? startApplicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StartApplicationsTbl)) {
            return false;
        }
        StartApplicationsTbl other = (StartApplicationsTbl) object;
        if ((this.startApplicationId == null && other.startApplicationId != null) || (this.startApplicationId != null && !this.startApplicationId.equals(other.startApplicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.StartApplicationsTbl[ startApplicationId=" + startApplicationId + " ]";
    }
    
}
