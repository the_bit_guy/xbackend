/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROJECTS_TBL")
@NamedQueries({
    @NamedQuery(name = "ProjectsTbl.findAll", query = "SELECT p FROM ProjectsTbl p")})
public class ProjectsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROJECT_ID")
    private String projectId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<UserProjectRelTbl> userProjectRelTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<ProjectTranslationTbl> projectTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectId")
    private Collection<UserSettingsTbl> userSettingsTblCollection;

    public ProjectsTbl() {
    }

    public ProjectsTbl(String projectId) {
        this.projectId = projectId;
    }

    public ProjectsTbl(String projectId, String name, String status, Date createDate, Date updateDate) {
        this.projectId = projectId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<AdminAreaProjectRelTbl> getAdminAreaProjectRelTblCollection() {
        return adminAreaProjectRelTblCollection;
    }

    public void setAdminAreaProjectRelTblCollection(Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection) {
        this.adminAreaProjectRelTblCollection = adminAreaProjectRelTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<UserProjectRelTbl> getUserProjectRelTblCollection() {
        return userProjectRelTblCollection;
    }

    public void setUserProjectRelTblCollection(Collection<UserProjectRelTbl> userProjectRelTblCollection) {
        this.userProjectRelTblCollection = userProjectRelTblCollection;
    }

    public Collection<ProjectTranslationTbl> getProjectTranslationTblCollection() {
        return projectTranslationTblCollection;
    }

    public void setProjectTranslationTblCollection(Collection<ProjectTranslationTbl> projectTranslationTblCollection) {
        this.projectTranslationTblCollection = projectTranslationTblCollection;
    }

    public Collection<UserSettingsTbl> getUserSettingsTblCollection() {
        return userSettingsTblCollection;
    }

    public void setUserSettingsTblCollection(Collection<UserSettingsTbl> userSettingsTblCollection) {
        this.userSettingsTblCollection = userSettingsTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectId != null ? projectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectsTbl)) {
            return false;
        }
        ProjectsTbl other = (ProjectsTbl) object;
        if ((this.projectId == null && other.projectId != null) || (this.projectId != null && !this.projectId.equals(other.projectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.ProjectsTbl[ projectId=" + projectId + " ]";
    }
    
}
