/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_HISTORY_RELATIONS_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminHistoryRelationsTbl.findAll", query = "SELECT a FROM AdminHistoryRelationsTbl a")})
public class AdminHistoryRelationsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
   @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADMIN_HISTORY_REL_OBJECTS_SEQ")
    @SequenceGenerator(name = "ADMIN_HISTORY_REL_OBJECTS_SEQ", sequenceName = "ADMIN_HISTORY_REL_OBJECTS_SEQ", allocationSize = 1)
    private Long id;
    @Basic(optional = false)
    @Column(name = "LOG_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;
    @Basic(optional = false)
    @Column(name = "ADMIN_NAME")
    private String adminName;
    @Basic(optional = false)
    @Column(name = "API_REQUEST_PATH")
    private String apiRequestPath;
    @Basic(optional = false)
    @Column(name = "OPERATION")
    private String operation;
    @Column(name = "SITE")
    private String site;
    @Column(name = "ADMIN_AREA")
    private String adminArea;
    @Column(name = "USER_APPLICATION")
    private String userApplication;
    @Column(name = "PROJECT")
    private String project;
    @Column(name = "PROJECT_APPLICATION")
    private String projectApplication;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "START_APPLICATION")
    private String startApplication;
    @Column(name = "RELATION_TYPE")
    private String relationType;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "GROUP_NAME")
    private String groupName;
    @Column(name = "DIRECTORY")
    private String directory;
    @Column(name = "ROLE")
    private String role;
    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;
    @Column(name = "RESULT")
    private String result;

    public AdminHistoryRelationsTbl() {
    }

    public AdminHistoryRelationsTbl(Long id) {
        this.id = id;
    }

    public AdminHistoryRelationsTbl(Long id, Date logTime, String adminName, String apiRequestPath, String operation) {
        this.id = id;
        this.logTime = logTime;
        this.adminName = adminName;
        this.apiRequestPath = apiRequestPath;
        this.operation = operation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getApiRequestPath() {
        return apiRequestPath;
    }

    public void setApiRequestPath(String apiRequestPath) {
        this.apiRequestPath = apiRequestPath;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getAdminArea() {
        return adminArea;
    }

    public void setAdminArea(String adminArea) {
        this.adminArea = adminArea;
    }

    public String getUserApplication() {
        return userApplication;
    }

    public void setUserApplication(String userApplication) {
        this.userApplication = userApplication;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProjectApplication() {
        return projectApplication;
    }

    public void setProjectApplication(String projectApplication) {
        this.projectApplication = projectApplication;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartApplication() {
        return startApplication;
    }

    public void setStartApplication(String startApplication) {
        this.startApplication = startApplication;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminHistoryRelationsTbl)) {
            return false;
        }
        AdminHistoryRelationsTbl other = (AdminHistoryRelationsTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminHistoryRelationsTbl[ id=" + id + " ]";
    }
    
}
