/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_MENU_CONFIG_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminMenuConfigTbl.findAll", query = "SELECT a FROM AdminMenuConfigTbl a")})
public class AdminMenuConfigTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_MENU_CONFIG_ID")
    private String adminMenuConfigId;
    @Basic(optional = false)
    @Column(name = "OBJECT_TYPE")
    private String objectType;
    @Basic(optional = false)
    @Column(name = "OBJECT_ID")
    private String objectId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public AdminMenuConfigTbl() {
    }

    public AdminMenuConfigTbl(String adminMenuConfigId) {
        this.adminMenuConfigId = adminMenuConfigId;
    }

    public AdminMenuConfigTbl(String adminMenuConfigId, String objectType, String objectId, Date createDate, Date updateDate) {
        this.adminMenuConfigId = adminMenuConfigId;
        this.objectType = objectType;
        this.objectId = objectId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getAdminMenuConfigId() {
        return adminMenuConfigId;
    }

    public void setAdminMenuConfigId(String adminMenuConfigId) {
        this.adminMenuConfigId = adminMenuConfigId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminMenuConfigId != null ? adminMenuConfigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminMenuConfigTbl)) {
            return false;
        }
        AdminMenuConfigTbl other = (AdminMenuConfigTbl) object;
        if ((this.adminMenuConfigId == null && other.adminMenuConfigId != null) || (this.adminMenuConfigId != null && !this.adminMenuConfigId.equals(other.adminMenuConfigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminMenuConfigTbl[ adminMenuConfigId=" + adminMenuConfigId + " ]";
    }
    
}
