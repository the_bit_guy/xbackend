/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "LIVE_MESSAGE_CONFIG_TBL")
@NamedQueries({
    @NamedQuery(name = "LiveMessageConfigTbl.findAll", query = "SELECT l FROM LiveMessageConfigTbl l")})
public class LiveMessageConfigTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LIVE_MESSAGE_CONFIG_ID")
    private String liveMessageConfigId;
    @Column(name = "SITE_ID")
    private String siteId;
    @Column(name = "ADMIN_AREA_ID")
    private String adminAreaId;
    @Column(name = "PROJECT_ID")
    private String projectId;
    @Column(name = "USER_ID")
    private String userId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LIVE_MESSAGE_ID", referencedColumnName = "LIVE_MESSAGE_ID")
    @ManyToOne(optional = false)
    private LiveMessageTbl liveMessageId;

    public LiveMessageConfigTbl() {
    }

    public LiveMessageConfigTbl(String liveMessageConfigId) {
        this.liveMessageConfigId = liveMessageConfigId;
    }

    public LiveMessageConfigTbl(String liveMessageConfigId, Date createDate, Date updateDate) {
        this.liveMessageConfigId = liveMessageConfigId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getLiveMessageConfigId() {
        return liveMessageConfigId;
    }

    public void setLiveMessageConfigId(String liveMessageConfigId) {
        this.liveMessageConfigId = liveMessageConfigId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getAdminAreaId() {
        return adminAreaId;
    }

    public void setAdminAreaId(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LiveMessageTbl getLiveMessageId() {
        return liveMessageId;
    }

    public void setLiveMessageId(LiveMessageTbl liveMessageId) {
        this.liveMessageId = liveMessageId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liveMessageConfigId != null ? liveMessageConfigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiveMessageConfigTbl)) {
            return false;
        }
        LiveMessageConfigTbl other = (LiveMessageConfigTbl) object;
        if ((this.liveMessageConfigId == null && other.liveMessageConfigId != null) || (this.liveMessageConfigId != null && !this.liveMessageConfigId.equals(other.liveMessageConfigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.LiveMessageConfigTbl[ liveMessageConfigId=" + liveMessageConfigId + " ]";
    }
    
}
