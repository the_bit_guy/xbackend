/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USERS_TBL")
@NamedQueries({
    @NamedQuery(name = "UsersTbl.findAll", query = "SELECT u FROM UsersTbl u")})
public class UsersTbl implements Comparable<UsersTbl>, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_ID")
    private String userId;
    @Basic(optional = false)
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "FULL_NAME")
    private String fullName;
    @Column(name = "EMAIL_ID")
    private String emailId;
    @Column(name = "TELEPHONE_NUMBER")
    private String telephoneNumber;
    @Column(name = "DEPARTMENT")
    private String department;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<LiveMessageStatusTbl> liveMessageStatusTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UserTranslationTbl> userTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UserStartAppRelTbl> userStartAppRelTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<RoleUserRelTbl> roleUserRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UserUserAppRelTbl> userUserAppRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UserProjectRelTbl> userProjectRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UserSettingsTbl> userSettingsTblCollection;

    public UsersTbl() {
    }

    public UsersTbl(String userId) {
        this.userId = userId;
    }

    public UsersTbl(String userId, String username, String status, Date createDate, Date updateDate) {
        this.userId = userId;
        this.username = username;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<LiveMessageStatusTbl> getLiveMessageStatusTblCollection() {
        return liveMessageStatusTblCollection;
    }

    public void setLiveMessageStatusTblCollection(Collection<LiveMessageStatusTbl> liveMessageStatusTblCollection) {
        this.liveMessageStatusTblCollection = liveMessageStatusTblCollection;
    }

    public Collection<UserTranslationTbl> getUserTranslationTblCollection() {
        return userTranslationTblCollection;
    }

    public void setUserTranslationTblCollection(Collection<UserTranslationTbl> userTranslationTblCollection) {
        this.userTranslationTblCollection = userTranslationTblCollection;
    }

    public Collection<UserStartAppRelTbl> getUserStartAppRelTblCollection() {
        return userStartAppRelTblCollection;
    }

    public void setUserStartAppRelTblCollection(Collection<UserStartAppRelTbl> userStartAppRelTblCollection) {
        this.userStartAppRelTblCollection = userStartAppRelTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<RoleUserRelTbl> getRoleUserRelTblCollection() {
        return roleUserRelTblCollection;
    }

    public void setRoleUserRelTblCollection(Collection<RoleUserRelTbl> roleUserRelTblCollection) {
        this.roleUserRelTblCollection = roleUserRelTblCollection;
    }

    public Collection<EmailNotifyToUserRelTbl> getEmailNotifyToUserRelTblCollection() {
        return emailNotifyToUserRelTblCollection;
    }

    public void setEmailNotifyToUserRelTblCollection(Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTblCollection) {
        this.emailNotifyToUserRelTblCollection = emailNotifyToUserRelTblCollection;
    }

    public Collection<UserUserAppRelTbl> getUserUserAppRelTblCollection() {
        return userUserAppRelTblCollection;
    }

    public void setUserUserAppRelTblCollection(Collection<UserUserAppRelTbl> userUserAppRelTblCollection) {
        this.userUserAppRelTblCollection = userUserAppRelTblCollection;
    }

    public Collection<UserProjectRelTbl> getUserProjectRelTblCollection() {
        return userProjectRelTblCollection;
    }

    public void setUserProjectRelTblCollection(Collection<UserProjectRelTbl> userProjectRelTblCollection) {
        this.userProjectRelTblCollection = userProjectRelTblCollection;
    }

    public Collection<UserSettingsTbl> getUserSettingsTblCollection() {
        return userSettingsTblCollection;
    }

    public void setUserSettingsTblCollection(Collection<UserSettingsTbl> userSettingsTblCollection) {
        this.userSettingsTblCollection = userSettingsTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersTbl)) {
            return false;
        }
        UsersTbl other = (UsersTbl) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UsersTbl[ userId=" + userId + " ]";
    }

	@Override
	public int compareTo(UsersTbl usersTbl) {
		String userName = usersTbl.getUsername();
		String currentObjUserName = this.getUsername();
		if (userName != null && currentObjUserName != null && !userName.isEmpty() && !currentObjUserName.isEmpty())
			return currentObjUserName.compareTo(userName);
		return 0;
	}
    
}
