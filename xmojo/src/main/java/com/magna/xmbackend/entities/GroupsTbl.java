/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "GROUPS_TBL")
@NamedQueries({
    @NamedQuery(name = "GroupsTbl.findAll", query = "SELECT g FROM GroupsTbl g")})
public class GroupsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GROUP_ID")
    private String groupId;
    @Column(name = "GROUP_TYPE")
    private String groupType;
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "groupId")
    private Collection<GroupTranslationTbl> groupTranslationTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupId")
    private Collection<GroupRefTbl> groupRefTblCollection;

    public GroupsTbl() {
    }

    public GroupsTbl(String groupId) {
        this.groupId = groupId;
    }

    public GroupsTbl(String groupId, Date createDate, Date updateDate) {
        this.groupId = groupId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getName() {
    	String decName = null;
		try {
			if (name != null) {
				decName = java.net.URLDecoder.decode(name, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<GroupTranslationTbl> getGroupTranslationTblCollection() {
        return groupTranslationTblCollection;
    }

    public void setGroupTranslationTblCollection(Collection<GroupTranslationTbl> groupTranslationTblCollection) {
        this.groupTranslationTblCollection = groupTranslationTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<GroupRefTbl> getGroupRefTblCollection() {
        return groupRefTblCollection;
    }

    public void setGroupRefTblCollection(Collection<GroupRefTbl> groupRefTblCollection) {
        this.groupRefTblCollection = groupRefTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupsTbl)) {
            return false;
        }
        GroupsTbl other = (GroupsTbl) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.GroupsTbl[ groupId=" + groupId + " ]";
    }
    
}
