/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ROLE_USER_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "RoleUserRelTbl.findAll", query = "SELECT r FROM RoleUserRelTbl r")})
public class RoleUserRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ROLE_USER_REL_ID")
    private String roleUserRelId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")
    @ManyToOne(optional = false)
    private RolesTbl roleId;
    @JoinColumn(name = "ROLE_ADMIN_AREA_REL_ID", referencedColumnName = "ROLE_ADMIN_AREA_REL_ID")
    @ManyToOne
    private RoleAdminAreaRelTbl roleAdminAreaRelId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;

    public RoleUserRelTbl() {
    }

    public RoleUserRelTbl(String roleUserRelId) {
        this.roleUserRelId = roleUserRelId;
    }

    public RoleUserRelTbl(String roleUserRelId, Date createDate, Date updateDate) {
        this.roleUserRelId = roleUserRelId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getRoleUserRelId() {
        return roleUserRelId;
    }

    public void setRoleUserRelId(String roleUserRelId) {
        this.roleUserRelId = roleUserRelId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public RolesTbl getRoleId() {
        return roleId;
    }

    public void setRoleId(RolesTbl roleId) {
        this.roleId = roleId;
    }

    public RoleAdminAreaRelTbl getRoleAdminAreaRelId() {
        return roleAdminAreaRelId;
    }

    public void setRoleAdminAreaRelId(RoleAdminAreaRelTbl roleAdminAreaRelId) {
        this.roleAdminAreaRelId = roleAdminAreaRelId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleUserRelId != null ? roleUserRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleUserRelTbl)) {
            return false;
        }
        RoleUserRelTbl other = (RoleUserRelTbl) object;
        if ((this.roleUserRelId == null && other.roleUserRelId != null) || (this.roleUserRelId != null && !this.roleUserRelId.equals(other.roleUserRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.RoleUserRelTbl[ roleUserRelId=" + roleUserRelId + " ]";
    }
    
}
