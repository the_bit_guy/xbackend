/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROPERTY_CONFIG_TBL")
@NamedQueries({
    @NamedQuery(name = "PropertyConfigTbl.findAll", query = "SELECT p FROM PropertyConfigTbl p")})
public class PropertyConfigTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROPERTY_CONFIG_ID")
    private String propertyConfigId;
    @Basic(optional = false)
    @Column(name = "CATEGORY")
    private String category;
    @Basic(optional = false)
    @Column(name = "PROPERTY")
    private String property;
    @Basic(optional = false)
    @Column(name = "VALUE")
    private String value;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public PropertyConfigTbl() {
    }

    public PropertyConfigTbl(String propertyConfigId) {
        this.propertyConfigId = propertyConfigId;
    }

    public PropertyConfigTbl(String propertyConfigId, String category, String property, String value, Date createDate, Date updateDate) {
        this.propertyConfigId = propertyConfigId;
        this.category = category;
        this.property = property;
        this.value = value;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getPropertyConfigId() {
        return propertyConfigId;
    }

    public void setPropertyConfigId(String propertyConfigId) {
        this.propertyConfigId = propertyConfigId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (propertyConfigId != null ? propertyConfigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropertyConfigTbl)) {
            return false;
        }
        PropertyConfigTbl other = (PropertyConfigTbl) object;
        if ((this.propertyConfigId == null && other.propertyConfigId != null) || (this.propertyConfigId != null && !this.propertyConfigId.equals(other.propertyConfigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.PropertyConfigTbl[ propertyConfigId=" + propertyConfigId + " ]";
    }
    
}
