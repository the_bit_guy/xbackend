/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "MAIL_QUEUE_TBL")
@NamedQueries({
    @NamedQuery(name = "MailQueueTbl.findAll", query = "SELECT m FROM MailQueueTbl m")})
public class MailQueueTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "MAIL_QUEUE_ID")
    private String mailQueueId;
    @Basic(optional = false)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @Column(name = "TO_MAIL")
    private String toMail;
    @Column(name = "FROM_MAIL")
    private String fromMail;
    @Basic(optional = false)
    @Column(name = "SUBJECT")
    private String subject;
    @Basic(optional = false)
    @Lob
    @Column(name = "TEXT_MESSAGE")
    private String textMessage;
    @Basic(optional = false)
    @Column(name = "EVENT")
    private String event;
    @Basic(optional = false)
    @Column(name = "STAGE")
    private String stage;
    @Column(name = "RE_TRY_COUNT")
    private String reTryCount;
    @Column(name = "DELAY_SEND")
    private String delaySend;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "REMARKS")
    private String remarks;

    public MailQueueTbl() {
    }

    public MailQueueTbl(String mailQueueId) {
        this.mailQueueId = mailQueueId;
    }

    public MailQueueTbl(String mailQueueId, String username, String toMail, String subject, String textMessage, String event, String stage, Date createDate, Date updateDate) {
        this.mailQueueId = mailQueueId;
        this.username = username;
        this.toMail = toMail;
        this.subject = subject;
        this.textMessage = textMessage;
        this.event = event;
        this.stage = stage;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    /**
	 * @param mailQueueId
	 * @param username
	 * @param toMail
	 * @param fromMail
	 * @param subject
	 * @param textMessage
	 * @param event
	 * @param stage
	 * @param createDate
	 * @param updateDate
	 */
	public MailQueueTbl(String mailQueueId, String username, String toMail, String fromMail, String subject,
			String textMessage, String event, String stage, Date createDate, Date updateDate) {
		this.mailQueueId = mailQueueId;
		this.username = username;
		this.toMail = toMail;
		this.fromMail = fromMail;
		this.subject = subject;
		this.textMessage = textMessage;
		this.event = event;
		this.stage = stage;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public String getMailQueueId() {
        return mailQueueId;
    }

    public void setMailQueueId(String mailQueueId) {
        this.mailQueueId = mailQueueId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToMail() {
        return toMail;
    }

    public void setToMail(String toMail) {
        this.toMail = toMail;
    }

    public String getFromMail() {
        return fromMail;
    }

    public void setFromMail(String fromMail) {
        this.fromMail = fromMail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getReTryCount() {
        return reTryCount;
    }

    public void setReTryCount(String reTryCount) {
        this.reTryCount = reTryCount;
    }

    public String getDelaySend() {
        return delaySend;
    }

    public void setDelaySend(String delaySend) {
        this.delaySend = delaySend;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mailQueueId != null ? mailQueueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MailQueueTbl)) {
            return false;
        }
        MailQueueTbl other = (MailQueueTbl) object;
        if ((this.mailQueueId == null && other.mailQueueId != null) || (this.mailQueueId != null && !this.mailQueueId.equals(other.mailQueueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.MailQueueTbl[ mailQueueId=" + mailQueueId + " ]";
    }
    
}
