/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_PROJECT_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "UserProjectRelTbl.findAll", query = "SELECT u FROM UserProjectRelTbl u")})
public class UserProjectRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_PROJECT_REL_ID")
    private String userProjectRelId;
    @Basic(optional = false)
    @Column(name = "PROJECT_EXPIRY_DAYS")
    private String projectExpiryDays;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    @ManyToOne(optional = false)
    private ProjectsTbl projectId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userProjectRelId")
    private Collection<UserProjAppRelTbl> userProjAppRelTblCollection;

    public UserProjectRelTbl() {
    }

    public UserProjectRelTbl(String userProjectRelId) {
        this.userProjectRelId = userProjectRelId;
    }

    public UserProjectRelTbl(String userProjectRelId, String projectExpiryDays, String status) {
        this.userProjectRelId = userProjectRelId;
        this.projectExpiryDays = projectExpiryDays;
        this.status = status;
    }

    public String getUserProjectRelId() {
        return userProjectRelId;
    }

    public void setUserProjectRelId(String userProjectRelId) {
        this.userProjectRelId = userProjectRelId;
    }

    public String getProjectExpiryDays() {
        return projectExpiryDays;
    }

    public void setProjectExpiryDays(String projectExpiryDays) {
        this.projectExpiryDays = projectExpiryDays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProjectsTbl getProjectId() {
        return projectId;
    }

    public void setProjectId(ProjectsTbl projectId) {
        this.projectId = projectId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    public Collection<UserProjAppRelTbl> getUserProjAppRelTblCollection() {
        return userProjAppRelTblCollection;
    }

    public void setUserProjAppRelTblCollection(Collection<UserProjAppRelTbl> userProjAppRelTblCollection) {
        this.userProjAppRelTblCollection = userProjAppRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userProjectRelId != null ? userProjectRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProjectRelTbl)) {
            return false;
        }
        UserProjectRelTbl other = (UserProjectRelTbl) object;
        if ((this.userProjectRelId == null && other.userProjectRelId != null) || (this.userProjectRelId != null && !this.userProjectRelId.equals(other.userProjectRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserProjectRelTbl[ userProjectRelId=" + userProjectRelId + " ]";
    }
    
}
