/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "START_APP_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "StartAppTranslationTbl.findAll", query = "SELECT s FROM StartAppTranslationTbl s")})
public class StartAppTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "START_APP_TRANSLATION_ID")
    private String startAppTranslationId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;
    @JsonBackReference
    @JoinColumn(name = "START_APPLICATION_ID", referencedColumnName = "START_APPLICATION_ID")
    @ManyToOne
    private StartApplicationsTbl startApplicationId;

    public StartAppTranslationTbl() {
    }

    public StartAppTranslationTbl(String startAppTranslationId) {
        this.startAppTranslationId = startAppTranslationId;
    }

    public StartAppTranslationTbl(String startAppTranslationId, Date createDate, Date updateDate) {
        this.startAppTranslationId = startAppTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getStartAppTranslationId() {
        return startAppTranslationId;
    }

    public void setStartAppTranslationId(String startAppTranslationId) {
        this.startAppTranslationId = startAppTranslationId;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    public StartApplicationsTbl getStartApplicationId() {
        return startApplicationId;
    }

    public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (startAppTranslationId != null ? startAppTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StartAppTranslationTbl)) {
            return false;
        }
        StartAppTranslationTbl other = (StartAppTranslationTbl) object;
        if ((this.startAppTranslationId == null && other.startAppTranslationId != null) || (this.startAppTranslationId != null && !this.startAppTranslationId.equals(other.startAppTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.StartAppTranslationTbl[ startAppTranslationId=" + startAppTranslationId + " ]";
    }
    
}
