/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "BASE_APP_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "BaseAppTranslationTbl.findAll", query = "SELECT b FROM BaseAppTranslationTbl b")})
public class BaseAppTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BASE_APP_TRANSLATION_ID")
    private String baseAppTranslationId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @JoinColumn(name = "BASE_APPLICATION_ID", referencedColumnName = "BASE_APPLICATION_ID")
    @ManyToOne
    private BaseApplicationsTbl baseApplicationId;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;

    public BaseAppTranslationTbl() {
    }

    public BaseAppTranslationTbl(String baseAppTranslationId) {
        this.baseAppTranslationId = baseAppTranslationId;
    }

    public BaseAppTranslationTbl(String baseAppTranslationId, Date createDate, Date updateDate) {
        this.baseAppTranslationId = baseAppTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getBaseAppTranslationId() {
        return baseAppTranslationId;
    }

    public void setBaseAppTranslationId(String baseAppTranslationId) {
        this.baseAppTranslationId = baseAppTranslationId;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public BaseApplicationsTbl getBaseApplicationId() {
        return baseApplicationId;
    }

    public void setBaseApplicationId(BaseApplicationsTbl baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (baseAppTranslationId != null ? baseAppTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BaseAppTranslationTbl)) {
            return false;
        }
        BaseAppTranslationTbl other = (BaseAppTranslationTbl) object;
        if ((this.baseAppTranslationId == null && other.baseAppTranslationId != null) || (this.baseAppTranslationId != null && !this.baseAppTranslationId.equals(other.baseAppTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.BaseAppTranslationTbl[ baseAppTranslationId=" + baseAppTranslationId + " ]";
    }
    
}
