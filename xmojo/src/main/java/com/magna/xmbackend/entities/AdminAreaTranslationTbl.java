/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREA_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreaTranslationTbl.findAll", query = "SELECT a FROM AdminAreaTranslationTbl a")})
public class AdminAreaTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_TRANSLATION_ID")
    private String adminAreaTranslationId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @JoinColumn(name = "ADMIN_AREA_ID", referencedColumnName = "ADMIN_AREA_ID")
    @ManyToOne(optional = false)
    private AdminAreasTbl adminAreaId;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;

    public AdminAreaTranslationTbl() {
    }

    public AdminAreaTranslationTbl(String adminAreaTranslationId) {
        this.adminAreaTranslationId = adminAreaTranslationId;
    }

    public AdminAreaTranslationTbl(String adminAreaTranslationId, Date createDate, Date updateDate) {
        this.adminAreaTranslationId = adminAreaTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getAdminAreaTranslationId() {
        return adminAreaTranslationId;
    }

    public void setAdminAreaTranslationId(String adminAreaTranslationId) {
        this.adminAreaTranslationId = adminAreaTranslationId;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public AdminAreasTbl getAdminAreaId() {
        return adminAreaId;
    }

    public void setAdminAreaId(AdminAreasTbl adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaTranslationId != null ? adminAreaTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreaTranslationTbl)) {
            return false;
        }
        AdminAreaTranslationTbl other = (AdminAreaTranslationTbl) object;
        if ((this.adminAreaTranslationId == null && other.adminAreaTranslationId != null) || (this.adminAreaTranslationId != null && !this.adminAreaTranslationId.equals(other.adminAreaTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreaTranslationTbl[ adminAreaTranslationId=" + adminAreaTranslationId + " ]";
    }
    
}
