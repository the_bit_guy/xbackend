/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREA_START_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreaStartAppRelTbl.findAll", query = "SELECT a FROM AdminAreaStartAppRelTbl a")})
public class AdminAreaStartAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_START_APP_REL_ID")
    private String adminAreaStartAppRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "SITE_ADMIN_AREA_REL_ID", referencedColumnName = "SITE_ADMIN_AREA_REL_ID")
    @ManyToOne(optional = false)
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    @JoinColumn(name = "START_APPLICATION_ID", referencedColumnName = "START_APPLICATION_ID")
    @ManyToOne(optional = false)
    private StartApplicationsTbl startApplicationId;

    public AdminAreaStartAppRelTbl() {
    }

    public AdminAreaStartAppRelTbl(String adminAreaStartAppRelId) {
        this.adminAreaStartAppRelId = adminAreaStartAppRelId;
    }

    public AdminAreaStartAppRelTbl(String adminAreaStartAppRelId, String status) {
        this.adminAreaStartAppRelId = adminAreaStartAppRelId;
        this.status = status;
    }

    public String getAdminAreaStartAppRelId() {
        return adminAreaStartAppRelId;
    }

    public void setAdminAreaStartAppRelId(String adminAreaStartAppRelId) {
        this.adminAreaStartAppRelId = adminAreaStartAppRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public StartApplicationsTbl getStartApplicationId() {
        return startApplicationId;
    }

    public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaStartAppRelId != null ? adminAreaStartAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreaStartAppRelTbl)) {
            return false;
        }
        AdminAreaStartAppRelTbl other = (AdminAreaStartAppRelTbl) object;
        if ((this.adminAreaStartAppRelId == null && other.adminAreaStartAppRelId != null) || (this.adminAreaStartAppRelId != null && !this.adminAreaStartAppRelId.equals(other.adminAreaStartAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreaStartAppRelTbl[ adminAreaStartAppRelId=" + adminAreaStartAppRelId + " ]";
    }
    
}
