/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PERMISSION_API_MAPPING_TBL")
@NamedQueries({
    @NamedQuery(name = "PermissionApiMappingTbl.findAll", query = "SELECT p FROM PermissionApiMappingTbl p")})
public class PermissionApiMappingTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERMISSION_API_MAPPING_ID")
    private String permissionApiMappingId;
    @Basic(optional = false)
    @Column(name = "API")
    private String api;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID")
    @ManyToOne(optional = false)
    private PermissionTbl permissionId;

    public PermissionApiMappingTbl() {
    }

    public PermissionApiMappingTbl(String permissionApiMappingId) {
        this.permissionApiMappingId = permissionApiMappingId;
    }

    public PermissionApiMappingTbl(String permissionApiMappingId, String api, Date createDate, Date updateDate) {
        this.permissionApiMappingId = permissionApiMappingId;
        this.api = api;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getPermissionApiMappingId() {
        return permissionApiMappingId;
    }

    public void setPermissionApiMappingId(String permissionApiMappingId) {
        this.permissionApiMappingId = permissionApiMappingId;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public PermissionTbl getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(PermissionTbl permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionApiMappingId != null ? permissionApiMappingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionApiMappingTbl)) {
            return false;
        }
        PermissionApiMappingTbl other = (PermissionApiMappingTbl) object;
        if ((this.permissionApiMappingId == null && other.permissionApiMappingId != null) || (this.permissionApiMappingId != null && !this.permissionApiMappingId.equals(other.permissionApiMappingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.PermissionApiMappingTbl[ permissionApiMappingId=" + permissionApiMappingId + " ]";
    }
    
}
