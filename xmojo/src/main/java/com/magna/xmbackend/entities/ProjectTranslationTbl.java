/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROJECT_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "ProjectTranslationTbl.findAll", query = "SELECT p FROM ProjectTranslationTbl p")})
public class ProjectTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROJECT_TRANSLATION_ID")
    private String projectTranslationId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;
    @JsonBackReference
    @JoinColumn(name = "PROJECT_ID", referencedColumnName = "PROJECT_ID")
    @ManyToOne(optional = false)
    private ProjectsTbl projectId;

    public ProjectTranslationTbl() {
    }

    public ProjectTranslationTbl(String projectTranslationId) {
        this.projectTranslationId = projectTranslationId;
    }

    public ProjectTranslationTbl(String projectTranslationId, Date createDate, Date updateDate) {
        this.projectTranslationId = projectTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getProjectTranslationId() {
        return projectTranslationId;
    }

    public void setProjectTranslationId(String projectTranslationId) {
        this.projectTranslationId = projectTranslationId;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

        return decDescription;
    }
    
    public String getDescriptionWithoutDecoding() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decRemarks;
    }
    
    public String getRemarksWithoutDecoding() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    public ProjectsTbl getProjectId() {
        return projectId;
    }

    public void setProjectId(ProjectsTbl projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectTranslationId != null ? projectTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectTranslationTbl)) {
            return false;
        }
        ProjectTranslationTbl other = (ProjectTranslationTbl) object;
        if ((this.projectTranslationId == null && other.projectTranslationId != null) || (this.projectTranslationId != null && !this.projectTranslationId.equals(other.projectTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.ProjectTranslationTbl[ projectTranslationId=" + projectTranslationId + " ]";
    }
    
}
