/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "DIRECTORY_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "DirectoryTranslationTbl.findAll", query = "SELECT d FROM DirectoryTranslationTbl d")})
public class DirectoryTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DIRECTORY_TRANSLATION_ID")
    private String directoryTranslationId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @JoinColumn(name = "DIRECTORY_ID", referencedColumnName = "DIRECTORY_ID")
    @ManyToOne
    private DirectoryTbl directoryId;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;

    public DirectoryTranslationTbl() {
    }

    public DirectoryTranslationTbl(String directoryTranslationId) {
        this.directoryTranslationId = directoryTranslationId;
    }

    public DirectoryTranslationTbl(String directoryTranslationId, Date createDate, Date updateDate) {
        this.directoryTranslationId = directoryTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getDirectoryTranslationId() {
        return directoryTranslationId;
    }

    public void setDirectoryTranslationId(String directoryTranslationId) {
        this.directoryTranslationId = directoryTranslationId;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public DirectoryTbl getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(DirectoryTbl directoryId) {
        this.directoryId = directoryId;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (directoryTranslationId != null ? directoryTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DirectoryTranslationTbl)) {
            return false;
        }
        DirectoryTranslationTbl other = (DirectoryTranslationTbl) object;
        if ((this.directoryTranslationId == null && other.directoryTranslationId != null) || (this.directoryTranslationId != null && !this.directoryTranslationId.equals(other.directoryTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.DirectoryTranslationTbl[ directoryTranslationId=" + directoryTranslationId + " ]";
    }
    
}
