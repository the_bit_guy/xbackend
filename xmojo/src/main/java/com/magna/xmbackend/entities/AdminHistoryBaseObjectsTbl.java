/**
 *
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Bhabadyuti Bal
 *
 */
@Entity
@Table(name = "ADMIN_HISTORY_BASE_OBJECTS_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminHistoryBaseObjectsTbl.findAll", query = "SELECT a FROM AdminHistoryBaseObjectsTbl a")})
public class AdminHistoryBaseObjectsTbl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADMIN_HISTORY_BASE_OBJECTS_SEQ")
    @SequenceGenerator(name = "ADMIN_HISTORY_BASE_OBJECTS_SEQ", sequenceName = "ADMIN_HISTORY_BASE_OBJECTS_SEQ", allocationSize = 1)
    private Long id;
    @Basic(optional = false)
    @Column(name = "LOG_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;
    @Basic(optional = false)
    @Column(name = "ADMIN_NAME")
    private String adminName;
    @Basic(optional = false)
    @Column(name = "API_REQUEST_PATH")
    private String apiRequestPath;
    @Basic(optional = false)
    @Column(name = "OPERATION")
    private String operation;
    @Basic(optional = false)
    @Column(name = "OBJECT_NAME")
    private String objectName;
    @Basic(optional = false)
    @Column(name = "CHANGES")
    private String changes;
    @Basic(optional = false)
    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;
    @Basic(optional = false)
    @Column(name = "RESULT")
    private String result;

    public AdminHistoryBaseObjectsTbl() {
    }

    public AdminHistoryBaseObjectsTbl(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getApiRequestPath() {
        return apiRequestPath;
    }

    public void setApiRequestPath(String apiRequestPath) {
        this.apiRequestPath = apiRequestPath;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getChanges() {
        return changes;
    }

    public void setChanges(String changes) {
        this.changes = changes;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminHistoryBaseObjectsTbl)) {
            return false;
        }
        AdminHistoryBaseObjectsTbl other = (AdminHistoryBaseObjectsTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl[ id=" + id + " ]";
    }

}
