/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROJECT_START_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "ProjectStartAppRelTbl.findAll", query = "SELECT p FROM ProjectStartAppRelTbl p")})
public class ProjectStartAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROJECT_START_APP_REL_ID")
    private String projectStartAppRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "ADMIN_AREA_PROJECT_REL_ID", referencedColumnName = "ADMIN_AREA_PROJECT_REL_ID")
    @ManyToOne(optional = false)
    private AdminAreaProjectRelTbl adminAreaProjectRelId;
    @JoinColumn(name = "START_APPLICATION_ID", referencedColumnName = "START_APPLICATION_ID")
    @ManyToOne(optional = false)
    private StartApplicationsTbl startApplicationId;

    public ProjectStartAppRelTbl() {
    }

    public ProjectStartAppRelTbl(String projectStartAppRelId) {
        this.projectStartAppRelId = projectStartAppRelId;
    }

    public ProjectStartAppRelTbl(String projectStartAppRelId, String status) {
        this.projectStartAppRelId = projectStartAppRelId;
        this.status = status;
    }

    public String getProjectStartAppRelId() {
        return projectStartAppRelId;
    }

    public void setProjectStartAppRelId(String projectStartAppRelId) {
        this.projectStartAppRelId = projectStartAppRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AdminAreaProjectRelTbl getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    public void setAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    public StartApplicationsTbl getStartApplicationId() {
        return startApplicationId;
    }

    public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
        this.startApplicationId = startApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectStartAppRelId != null ? projectStartAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectStartAppRelTbl)) {
            return false;
        }
        ProjectStartAppRelTbl other = (ProjectStartAppRelTbl) object;
        if ((this.projectStartAppRelId == null && other.projectStartAppRelId != null) || (this.projectStartAppRelId != null && !this.projectStartAppRelId.equals(other.projectStartAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.ProjectStartAppRelTbl[ projectStartAppRelId=" + projectStartAppRelId + " ]";
    }
    
}
