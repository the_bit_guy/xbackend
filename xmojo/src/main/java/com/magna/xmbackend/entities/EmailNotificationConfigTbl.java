/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "EMAIL_NOTIFICATION_CONFIG_TBL")
@NamedQueries({
    @NamedQuery(name = "EmailNotificationConfigTbl.findAll", query = "SELECT e FROM EmailNotificationConfigTbl e")})
public class EmailNotificationConfigTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EMAIL_NOTIFICATION_CONFIG_ID")
    private String emailNotificationConfigId;
    @Basic(optional = false)
    @Column(name = "EVENT")
    private String event;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "SUBJECT")
    private String subject;
    @Basic(optional = false)
    @Column(name = "MESSAGE")
    private String message;
    @Column(name = "INCLUDE_REMARKS")
    private String includeRemarks;
    @Column(name = "SEND_TO_ASSIGNED_USER")
    private String sendToAssignedUser;
    @Column(name = "PROJECT_EXPIRY_NOTICE_PERIOD")
    private String projectExpiryNoticePeriod;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emailNotificationConfigId")
    private Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTblCollection;

    public EmailNotificationConfigTbl() {
    }

    public EmailNotificationConfigTbl(String emailNotificationConfigId) {
        this.emailNotificationConfigId = emailNotificationConfigId;
    }

    public EmailNotificationConfigTbl(String emailNotificationConfigId, String event, String description, String subject, String message, Date createDate, Date updateDate) {
        this.emailNotificationConfigId = emailNotificationConfigId;
        this.event = event;
        this.description = description;
        this.subject = subject;
        this.message = message;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getEmailNotificationConfigId() {
        return emailNotificationConfigId;
    }

    public void setEmailNotificationConfigId(String emailNotificationConfigId) {
        this.emailNotificationConfigId = emailNotificationConfigId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIncludeRemarks() {
        return includeRemarks;
    }

    public void setIncludeRemarks(String includeRemarks) {
        this.includeRemarks = includeRemarks;
    }

    public String getSendToAssignedUser() {
        return sendToAssignedUser;
    }

    public void setSendToAssignedUser(String sendToAssignedUser) {
        this.sendToAssignedUser = sendToAssignedUser;
    }

    public String getProjectExpiryNoticePeriod() {
        return projectExpiryNoticePeriod;
    }

    public void setProjectExpiryNoticePeriod(String projectExpiryNoticePeriod) {
        this.projectExpiryNoticePeriod = projectExpiryNoticePeriod;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<EmailNotifyToUserRelTbl> getEmailNotifyToUserRelTblCollection() {
        return emailNotifyToUserRelTblCollection;
    }

    public void setEmailNotifyToUserRelTblCollection(Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTblCollection) {
        this.emailNotifyToUserRelTblCollection = emailNotifyToUserRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emailNotificationConfigId != null ? emailNotificationConfigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailNotificationConfigTbl)) {
            return false;
        }
        EmailNotificationConfigTbl other = (EmailNotificationConfigTbl) object;
        if ((this.emailNotificationConfigId == null && other.emailNotificationConfigId != null) || (this.emailNotificationConfigId != null && !this.emailNotificationConfigId.equals(other.emailNotificationConfigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.EmailNotificationConfigTbl[ emailNotificationConfigId=" + emailNotificationConfigId + " ]";
    }
    
}
